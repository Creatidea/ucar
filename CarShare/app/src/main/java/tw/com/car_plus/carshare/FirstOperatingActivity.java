package tw.com.car_plus.carshare;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.neilchen.complextoolkit.util.checkversion.CheckUpdateActionListener;
import com.neilchen.complextoolkit.util.checkversion.CheckVersionManager;
import com.neilchen.complextoolkit.util.customdialog.CustomDialog;

import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.notification.model.PushData;
import tw.com.car_plus.carshare.operating.OperatingFragment;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;

/**
 * Created by neil on 2017/11/28.
 */

public class FirstOperatingActivity extends FragmentActivity {

    // ----------------------------------------------------
    private SharedPreferenceUtil sp;
    private CheckVersionManager checkVersionManager;

    // ----------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_operating);

        init();
    }

    // ----------------------------------------------------
    public void init() {

        checkVersionManager = new CheckVersionManager(this);
        sp = new SharedPreferenceUtil(this);

        checkVersionManager.setForceUpdate(true);
        setUpdateDialog(checkVersionManager.dialog);

        if (BuildConfig.IS_INTERNAL) {
            checkGoToPage();
        } else {
            checkVersionManager.checkCustomUrl(ConnectInfo.VERSION, new CheckUpdateActionListener() {
                @Override
                public void onLatestListener() {
                    checkGoToPage();
                }
            });
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查前往的頁面
     */
    private void checkGoToPage() {

        if (sp.getFirst()) {
            // 顯示操作模式
            OperatingFragment fragment = new OperatingFragment();
            fragment.setEnterLogin(new OperatingFragment.OnEnterLoginListener() {
                @Override
                public void onEnter() {
                    sp.setFirst(false);
                    finish();
                    goToIndex();
                }
            });

            goFragment(R.id.container, fragment);

        } else {
            finish();
            goToIndex();
        }
    }

    // ----------------------------------------------------
    private void goToIndex() {

        Bundle bundle = getIntent().getExtras();
        Intent intent = new Intent();

        if (bundle != null) {
            PushData pushData = bundle.getParcelable("pushData");

            if (pushData != null) {
                Location location = new Location("Location");
                location.setLatitude(pushData.getLatitude());
                location.setLongitude(pushData.getLongitude());
                MapModeFragment.parkingLocation = location;
            }
        }

        intent.setClass(this, IndexActivity.class);
        startActivity(intent);
    }

    // ----------------------------------------------------
    private void goFragment(int containerViewId, Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerViewId, fragment);
        transaction.commit();
    }

    //-----------------------------------------------------

    /**
     * 設定更新dialog樣式
     *
     * @param dialog
     */
    private void setUpdateDialog(CustomDialog dialog) {

        dialog.setTitleTextColor(Color.BLACK);
        dialog.setDividerWidth(2);
        dialog.setDivider(Color.BLACK);
        dialog.setMessageTextSize(16);
        dialog.setMessageTextColor(Color.BLACK);
        dialog.setPositiveBackgroundColor(ContextCompat.getColor(this, R.color.main_bg));
        dialog.setPositiveTextColor(Color.WHITE);
        dialog.setPositiveTextSize(16);
    }


    // ----------------------------------------------------
}
