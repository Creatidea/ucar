package tw.com.car_plus.carshare;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import net.hockeyapp.android.CrashManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.operating.OperatingFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.container_framelayout)
    FrameLayout containerFramelayout;

    private SharedPreferenceUtil sharedPreferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        init();
    }

    // ----------------------------------------------------
    private void init() {
        sharedPreferenceUtil = new SharedPreferenceUtil(this);

        if (sharedPreferenceUtil.getFirst()) {
            goToOperatingFragment();
        } else {
            containerFramelayout.setVisibility(View.GONE);
            if (sharedPreferenceUtil.getPassword().length() > 0) {
                goToIndex(false);
            }
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    // ----------------------------------------------------
    @OnClick({R.id.login, R.id.register})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login:
                goToIndex(false);
                break;
            case R.id.register:
                goToIndex(true);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 前往第一次的操作畫面
     */
    private void goToOperatingFragment() {

        OperatingFragment fragment = new OperatingFragment();
        fragment.setEnterLogin(new OperatingFragment.OnEnterLoginListener() {
            @Override
            public void onEnter() {
                sharedPreferenceUtil.setFirst(false);
                containerFramelayout.setVisibility(View.GONE);
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_framelayout, fragment);
        transaction.commit();

    }

    // ----------------------------------------------------
    private void goToIndex(boolean isRegister) {

        Intent intent = new Intent();
        intent.setClass(this, IndexActivity.class);
        intent.putExtra("isRegister", isRegister);
        startActivity(intent);
        finish();
    }

    // ----------------------------------------------------

    /**
     * hockeyapp 註冊Crashes
     */
    private void checkForCrashes() {
        CrashManager.register(this);
    }

    // ----------------------------------------------------
}
