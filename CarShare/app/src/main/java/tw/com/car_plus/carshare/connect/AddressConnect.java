package tw.com.car_plus.carshare.connect;

import android.content.Context;
import android.widget.Toast;

import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;

/**
 * Created by neil on 2017/4/13.
 */

public class AddressConnect extends BaseConnect {

    // ----------------------------------------------------
    public AddressConnect(Context context) {
        super(context);
    }

    // ----------------------------------------------------

    /**
     * 取得縣市的相關資料
     */
    public void connectGetCity() {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CITY, null, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                List<City> cityList = new ArrayList<>();

                try {
                    JSONArray cityArray = response.getJSONArray("ReturnData");

                    for (int i = 0; i < cityArray.length(); i++) {
                        City city = parser.getJSONData(cityArray.getJSONObject(i).toString(), City.class);
                        cityList.add(city);
                    }

                    EventCenter.getInstance().sendCityEvent(cityList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    //--------------------------------------------------

    /**
     * 取得某縣市的所有區域
     *
     * @param cityType 縣市的type
     */
    public void connectGetArea(final int cityType) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(String.format(ConnectInfo.AREA, cityType), null, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                List<Area> areaList = new ArrayList<>();

                try {

                    JSONArray areaArray = response.getJSONArray("ReturnData");
                    for (int i = 0; i < areaArray.length(); i++) {
                        Area area = parser.getJSONData(areaArray.getJSONObject(i).toString(), Area.class);
                        areaList.add(area);
                    }

                    EventCenter.getInstance().sendAreaEvent(areaList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    // ----------------------------------------------------
}
