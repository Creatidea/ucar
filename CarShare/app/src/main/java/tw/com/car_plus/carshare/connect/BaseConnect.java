package tw.com.car_plus.carshare.connect;

import android.content.Context;

import com.neilchen.complextoolkit.http.HttpControl;
import com.neilchen.complextoolkit.util.json.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;

/**
 * Created by winni on 2017/3/2.
 */

public abstract class BaseConnect {

    // ----------------------------------------------------
    protected HttpControl httpControl;
    protected JSONParser parser;
    protected SharedPreferenceUtil sp;
    protected Context context;

    // ----------------------------------------------------
    public BaseConnect(Context context) {
        httpControl = new HttpControl(context);
        parser = new JSONParser();
        sp = new SharedPreferenceUtil(context);
        this.context = context;
    }

    // ----------------------------------------------------

    /**
     * 讀取資料
     */
    protected void loadData(JSONObject response, IStatus status) {

        try {

            if (response.getBoolean("Status")) {
                if (status != null) {
                    status.statusEvent();
                }
            } else {

                JSONArray errorArray = response.getJSONArray("Errors");
                if (errorArray.length() != 0) {
                    EventCenter.getInstance().sendErrorEvent(errorArray.getJSONObject(0).getString("ErrorMsg"));
                } else {
                    EventCenter.getInstance().sendErrorEvent(context.getString(R.string.server_error));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------
    public interface IStatus {
        void statusEvent();
    }

    // ----------------------------------------------------
}
