package tw.com.car_plus.carshare.connect;

import tw.com.car_plus.carshare.BuildConfig;

/**
 * Created by winni on 2017/3/2.
 */

public class ConnectInfo {

    //測試站
    private static final String WEB_DEV = "http://59.125.115.212/";
    //正式站
    private static final String WEB = "http://220.128.149.246/";

    // ----------------------------------------------------

    /**
     * 取的對應的ConnectServerHost
     *
     * @return
     */
    private static String getServerHost() {

        if (BuildConfig.IS_INTERNAL) {
            return WEB_DEV;
        } else {
            return WEB;
        }
    }

    // ----------------------------------------------------
    /**
     * 版本號
     */
    public static final String VERSION = getServerHost() + "app/Version_Android.json";
    /**
     * 建立會員
     */
    public static final String ADD_MEMBER = getServerHost() + "api/Member/Add";
    //取得縣市的資料
    public static final String CITY = getServerHost() + "api/Location/GetCity";
    //根據縣市取得該縣市的區域資料
    public static final String AREA = getServerHost() + "api/Location/GetArea/%d";
    //取得驗證碼
    public static final String IDENTIFYING_CODE = getServerHost() + "api/Member/GetVerifyCode";
    //確認驗證碼是否有效
    public static final String CHECK_EFFECTIVE = getServerHost() + "api/Member/Verify";
    //上傳圖片
    public static final String UPLOAD_FILE = getServerHost() + "api/Upload/AddTempFile";
    //註冊駕照與身分證的資料
    public static final String REGISTER_CREDENTIALS = getServerHost() + "api/Member/RegisterMemberCredentials";
    //註冊信用卡
    public static final String REGISTER_CREDIT_CARD = getServerHost() + "api/Member/RegisterVerifyCreditCard";
    //送出審核
    public static final String APPLY_VERIFY = getServerHost() + "api/Member/ApplyVerify";
    //註冊推播裝置
    public static final String REGISTER_DEVICE = getServerHost() + "api/Member/RegisterDevice";
    //註消推播裝置
    public static final String DISABLE_DEVICE = getServerHost() + "api/Member/DisableDevice";

    //登入
    public static final String LOGIN = getServerHost() + "api/Member/Login";
    //附近租車資料
    public static final String PARKING = getServerHost() + "api/Parkinglot/GetParkinglotGroup";
    //取得車輛資訊
    public static final String GET_CAR = getServerHost() + "api/Car/GetCar/%d";
    //估算租車前的預估金額
    public static final String ESTIMATE_AMOUNT_SIMPLE = getServerHost() + "api/Order/CalSimpleAmount";
    //預約租車
    public static final String RESERVATION_CAR = getServerHost() + "api/Order/PreOrder";
    //延長取車時間
    public static final String PROLONG_PICK_UP_CAR = getServerHost() + "api/Order/ExtenPreorder";
    //預約成功後開始取車
    public static final String START_PICK_UP_CAR = getServerHost() + "api/Order/StartGetCar";
    //喚醒vig
    public static final String WAKE_VIG = getServerHost() + "api/HaitecVig/WakeToBLE/%d";
    //取得Ble相關資料(華創)
    public static final String GET_BLE = getServerHost() + "api/HaitecVig/GetBLE";
    //傳送APPData(華創)
    public static final String SEND_APP_DATA = getServerHost() + "api/HaitecVig/CommunicateToBLE";
    //確認VIG是否有拿到Key(華創)
    public static final String CHECK_VIG_KEY = getServerHost() + "api/HaitecVig/CheckVigKey/";
    //下達指令給IKeyServer(車王)
    public static final String COMMAND_TO_IKEY = getServerHost() + "api/Mobiletron/CommandToIKey";
    //確認VIG是否有拿到Key(車王)
    public static final String CHECK_MOBILETRON_VIG_KEY = getServerHost() + "api/Mobiletron/CheckVigKey";
    //如key未下成功可以跑這隻api
    public static final String SEND_CANCEL_ERROR_ORDER = getServerHost() + "api/Order/CancelErrorOrder";

    public static final String GET_KEY = getServerHost() + "api/Mobiletron/GetKey";
    //取消取車
    public static final String CANCEL_PICK_UP_CAR = getServerHost() + "api/Order/CanclePreorder";
    //完成取車
    public static final String FINISH_PICK_UP_CAR = getServerHost() + "api/Order/FinishGetCar";
    //估算租金
    public static final String ESTIMATE_AMOUNT = getServerHost() + "api/Order/EstimatedAmount";
    //更新里程
    public static final String UPDATE_MILEAGE = getServerHost() + "api/Order/GetOrderOdoMeter";
    //取得充電站的資料
    public static final String GET_CHARGING_STATION = getServerHost() + "api/Charge/GetChargeStationBasic";
    //取得充電座的資料
    public static final String GET_CHARGING_STATION_DETAIL = getServerHost() + "api/Charge/GetChargeStationInfo/%s";
    //取得檢查是否有特殊還車
    public static final String GET_CHECK_SPECIAL_RETURN_CAR = getServerHost() + "api/Order/IsSpecialReturn";

    //忘記密碼
    public static final String FORGOT_PASSWORD = getServerHost() + "api/Member/ForgetMemberPassword";
    //重設密碼
    public static final String CHANGE_PASSWORD = getServerHost() + "api/Member/ChangeMemberPassword";
    //取得會員資料
    public static final String USER_INFO = getServerHost() + "api/Member/LoadMemberData";
    //檢查引擎是否熄火勒
    public static final String CHECK_ENGINE_STATUS = getServerHost() + "api/Order/GetEngineStatus";
    //開始還車(地圖用)
    public static final String RETURN_CAR = getServerHost() + "api/Order/StartReturnCar";
    //確認車子是否有上鎖
    public static final String CHECK_LOCK = getServerHost() + "api/Order/IsCarLock";
    //取得帳單資訊
    public static final String GET_BILL_INFO = getServerHost() + "api/Order/GetBillInfo";
    //取得帳單明細(企業帳號)
    public static final String GET_BILL_INFO_COMPANY = getServerHost() + "api/Order/GetCompanyBillInfo";
    //取消還車
    public static final String CANCEL_CAR = getServerHost() + "api/Order/CancelReturnCar";
    //取得紅利資訊
    public static final String BONUS = getServerHost() + "api/Order/GetBonusInfo";
    //還車付款
    public static final String PAY = getServerHost() + "api/Order/ReturnCar";
    //還車付款-企業用戶
    public static final String PAY_COMPANY = getServerHost() + "api/Order/CompanyReturnCar";
    //信用卡付款網址
    public static final String BANK_PAYMENT = getServerHost() + "%s";
    //使用者的發票資料
    public static final String USER_INVOICE = getServerHost() + "api/Member/LoadMemberInvoice";
    //傳送使用者的發票資訊
    public static final String SEND_USER_INVOICE = getServerHost() + "api/Member/UpdateMemberInvoice";

    //更新使用者資料
    public static final String UPDATE_USER_DATA = getServerHost() + "api/Member/Update";
    //取得會員的信用卡資料
    public static final String USER_CREDIT_CARD = getServerHost() + "api/Member/LoadMemberCreditCard";
    //設定使用者設定的付款信用卡
    public static final String SET_PAY_CREDIT_CARD = getServerHost() + "api/Member/SetPayCreditCard";
    //刪除使用者的信用卡
    public static final String DELETE_CREDIT_CARD = getServerHost() + "api/Member/StopMemberCreditCard";
    //租車記錄
    public static final String RENT_RECORD = getServerHost() + "api/Order/GetOrderRecords";
    //租車記錄詳細頁
    public static final String RENT_RECORD_DETAIL = getServerHost() + "api/Order/GetOrderDetails";
    //取得審查結果
    public static final String VERIFY_RESULT = getServerHost() + "api/Member/VerifyResult";
    //取得優惠方案
    public static final String PREFERENTIAL = getServerHost() + "api/Promotion/GetPromotionList";
    //最新消息
    public static final String NEWS = getServerHost() + "api/News/GetNewsList";
    //訂閱與取消訂閱停車場通知
    public static final String SUBSCRIBE_PARKING = getServerHost() + "api/Parkinglot/SubscribeParkinglot";
    // 取得使用者有訂閱哪些停車場
    public static final String REMIND_PARKING = getServerHost() + "api/Parkinglot/GetParkinglotSubscribe/";

    // ----------------------------------------------------
    /**
     * 圖片的路徑
     */
    public static final String IMAGE_PATH = getServerHost() + "%s";

    // ----------------------------------------------------
}
