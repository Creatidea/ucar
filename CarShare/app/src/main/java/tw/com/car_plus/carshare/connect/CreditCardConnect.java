package tw.com.car_plus.carshare.connect;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.model.CreditCards;
import tw.com.car_plus.carshare.register.model.CreditCardsData;

/**
 * Created by winni on 2017/3/8.
 * 註冊信用卡
 */

public class CreditCardConnect extends BaseConnect {

    private static AsyncHttpClient client;
    private Context context;
    private User user;

    public CreditCardConnect(Context context) {
        super(context);
        this.context = context;
        client = new AsyncHttpClient();
        user = sp.getUserData();
    }

    //--------------------------------------------------

    /**
     * 註冊信用卡
     *
     * @param creditCards
     */
    public void connectRegisterCreditCard(CreditCards creditCards) {

        final LoadingDialogManager loadingDialogManager = new LoadingDialogManager(context);
        loadingDialogManager.show();

        StringEntity entity = new StringEntity(new Gson().toJson(creditCards), "utf-8");

        client.post(context, ConnectInfo.REGISTER_CREDIT_CARD, entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        CreditCardsData creditCardsBean = parser.getJSONData(response.toString(), CreditCardsData.class);

                        if (creditCardsBean != null) {
                            EventCenter.getInstance().sendCreditCardsEvent(creditCardsBean);
                        }
                    }
                });

                loadingDialogManager.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                loadingDialogManager.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                loadingDialogManager.dismiss();
            }
        });

    }

    //--------------------------------------------------

    /**
     * 取得會員的信用卡資訊
     */
    public void getUserCreditCard() {

        RequestParams params = new RequestParams();
        params.put("AcctId", sp.getAcctId());

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.USER_CREDIT_CARD, params, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendCreditCardsEvent(parser.getJSONData(response.toString(), CreditCardsData.class));
                    }
                });

            }
        });
    }

    //--------------------------------------------------

    /**
     * 設定使用者付款的信用卡
     *
     * @param serialNo
     */
    public void setPayCreditCard(String serialNo) {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("SerialNo", serialNo);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.SET_PAY_CREDIT_CARD, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendSetPayCard(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    //--------------------------------------------------

    /**
     * 刪除使用者的信用卡
     *
     * @param serialNo
     */
    public void sendDeleteCreditCard(String serialNo) {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("SerialNo", serialNo);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.DELETE_CREDIT_CARD, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendDeleteCreditCard(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

}
