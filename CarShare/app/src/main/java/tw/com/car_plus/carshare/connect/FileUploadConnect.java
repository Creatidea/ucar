package tw.com.car_plus.carshare.connect;

import android.content.Context;
import android.graphics.Bitmap;

import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;
import com.neilchen.complextoolkit.util.json.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.UploadFile;

/**
 * Created by winni on 2017/3/7.
 */

public class FileUploadConnect extends BaseConnect {

    private JSONParser jsonParser;
    private String errorMessage;

    public FileUploadConnect(Context context) {
        super(context);
        jsonParser = new JSONParser();
        errorMessage = context.getString(R.string.server_error);
    }

    //--------------------------------------------------

    /**
     * 上傳照片
     *
     * @param path 路徑
     */
    public void ConnectUploadFile(String path) {

        RequestParams params = new RequestParams();

        try {
            params.put("imgData", new File(path));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        uploadFile(params);
    }
    //--------------------------------------------------

    /**
     * 上傳照片
     *
     * @param bitmap
     */
    public void ConnectUploadFile(Bitmap bitmap) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();

        RequestParams params = new RequestParams();
        params.put("imgData", new ByteArrayInputStream(imageBytes), "signature.jpg");

        uploadFile(params);

    }

    //--------------------------------------------------
    private void uploadFile(RequestParams params) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.UPLOAD_FILE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                try {

                    if (response.getBoolean("Status")) {

                        UploadFile uploadFile = jsonParser.getJSONData(response.toString(), UploadFile.class);
                        EventCenter.getInstance().sendUploadFileEvent(uploadFile);

                    } else {

                        JSONArray jsonArray = response.getJSONArray("Errors");

                        if (jsonArray != null && jsonArray.length() > 0) {
                            errorMessage = jsonArray.getJSONObject(0).getString("ErrorMsg");
                        }

                        EventCenter.getInstance().sendErrorEvent(errorMessage);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendErrorEvent(errorMessage);
            }

            @Override
            public void onFailForString(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailForString(statusCode, headers, responseString, throwable);
                EventCenter.getInstance().sendErrorEvent(errorMessage);
            }
        });

    }

    //--------------------------------------------------
}
