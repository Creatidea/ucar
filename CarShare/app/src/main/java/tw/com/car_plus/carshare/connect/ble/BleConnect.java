package tw.com.car_plus.carshare.connect.ble;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;
import com.neilchen.complextoolkit.util.HttpLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.model.ErrorMessage;
import tw.com.car_plus.carshare.rent.success.model.CheckVig;
import tw.com.car_plus.carshare.service.model.CommandData;

/**
 * Created by jhen on 2017/7/12.
 */

public class BleConnect extends BaseConnect {

    private String uuid = "";
    private String androidVersion = "";
    private HttpLoader loader;

    public BleConnect(Context context) {
        super(context);
        uuid = sp.getUUID();
        androidVersion = String.format(context.getString(R.string.android_version), Build.VERSION.RELEASE, Build.VERSION.SDK_INT);
        loader = new HttpLoader(context);
    }

    // ----------------------------------------------------

    /**
     * 喚醒Vig
     *
     * @param carId
     */
    public void wakeToVIG(int carId, final IStatus iStatus) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(String.format(ConnectInfo.WAKE_VIG, carId), null, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                loadData(response, iStatus);
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 通知RentServer讀取BLE的AppData
     *
     * @param acctId
     * @param commandData
     */
    public void getBleData(final String acctId, final CommandData commandData) {

        RequestParams params = new RequestParams();
        params.put("BleType", commandData.getType());
        params.put("AccId", acctId);
        params.put("UniqeId", uuid);
        params.put("CarId", commandData.getCarId());
        params.put("OsVersion", androidVersion);
        params.put("OrderId", commandData.getOrderId());
        params.put("ReservationStart", commandData.getStartTime());
        params.put("ReservationEnd", commandData.getEndTime());

        loader.setTimeout(100 * 1000);
        loader.setResponseTimeout(100 * 1000);
        loader.post(ConnectInfo.GET_BLE, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Log.e("BleConnect", "getBleData:" + response.toString());

                try {

                    if (response.getBoolean("Status")) {

                        try {

                            int status = response.getInt("ResponseStatus");
                            if (status > 0) {
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorCode(status);
                                EventCenter.getInstance().sendErrorCommandToServerHaitec(errorMessage);
                            }

                        } catch (Exception e) {
                            EventCenter.getInstance().sendAppData(getAppData(response));
                        }

                    } else {

                        JSONArray jsonArray = response.getJSONArray("Errors");

                        if (jsonArray.length() > 0) {

                            int errorCode = jsonArray.getJSONObject(0).getInt("ErrorCode");

                            if (errorCode == 200) {
                                getBleData(acctId, commandData);
                            } else {

                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorCode(errorCode);
                                errorMessage.setErrorMessage(jsonArray.getJSONObject(0).getString("ErrorMessage"));
                                EventCenter.getInstance().sendErrorCommandToServerHaitec(errorMessage);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    getBleData(acctId, commandData);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    getBleData(acctId, commandData);
                } else {
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorCode(-1);
                    EventCenter.getInstance().sendErrorCommandToServerHaitec(errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, final String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    getBleData(acctId, commandData);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    getBleData(acctId, commandData);
                } else {
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorCode(-1);
                    EventCenter.getInstance().sendErrorCommandToServerHaitec(errorMessage);
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * get server key.(車王)
     */
    public void loadServerKey(final int orderId) {

        RequestParams params = new RequestParams();
        params.put("OrderId", orderId);
        params.put("AcctId", sp.getAcctId());

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.setTimeout(100 * 1000);
        httpControl.setResponseTimeout(100 * 1000);
        httpControl.connect(ConnectInfo.GET_KEY, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response.getBoolean("Status")) {
                        EventCenter.getInstance().sendKey(response.getString("Data"));
                    } else {
                        JSONArray jsonArray = response.getJSONArray("Errors");
                        if (jsonArray.length() > 0) {
                            EventCenter.getInstance().getServerKeyError(jsonArray.getJSONObject(0).getInt("ErrorCode"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, final JSONObject errorResponse) {

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    loadServerKey(orderId);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    loadServerKey(orderId);
                } else {
                    EventCenter.getInstance().getServerKeyError(0);
                }
            }

            @Override
            public void onFailForString(int statusCode, Header[] headers, final String responseString, Throwable throwable) {

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    loadServerKey(orderId);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    loadServerKey(orderId);
                } else {
                    EventCenter.getInstance().getServerKeyError(0);
                }
            }
        });
    }
    // ----------------------------------------------------

    /**
     * 傳送RentServer傳送給泛用型相關指令
     *
     * @param acctId
     * @param commandData
     */
    public void sendCommand(final String acctId, final CommandData commandData) {


        RequestParams params = new RequestParams();
        params.put("BleType", commandData.getType());
        params.put("AccId", acctId);
        params.put("UniqeId", uuid);
        params.put("CarId", commandData.getCarId());
        params.put("OsVersion", androidVersion);
        params.put("OrderId", commandData.getOrderId());
        params.put("ReservationStart", commandData.getStartTime());
        params.put("ReservationEnd", commandData.getEndTime());

        loader.setTimeout(100 * 1000);
        loader.setResponseTimeout(100 * 1000);
        loader.post(ConnectInfo.COMMAND_TO_IKEY, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Log.e("BleConnect", "sendCreateKey:" + response.toString());

                try {
                    EventCenter.getInstance().sendCommandToServerMobiletron(response.getBoolean("Status"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    sendCommand(acctId, commandData);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    sendCommand(acctId, commandData);
                } else {
                    EventCenter.getInstance().sendCommandToServerMobiletron(false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, final String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    sendCommand(acctId, commandData);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    sendCommand(acctId, commandData);
                } else {
                    EventCenter.getInstance().sendCommandToServerMobiletron(false);
                }
            }
        });

    }

    // ----------------------------------------------------

    /**
     * @param appData
     */
    public void sendAppData(final String appData, final String acctId) {

        SyncHttpClient syncHttpClient = new SyncHttpClient();
        syncHttpClient.setTimeout(50 * 1000);
        syncHttpClient.setResponseTimeout(50 * 1000);

        RequestParams params = new RequestParams();
        params.put("AccId", acctId);
        params.put("UniqeId", uuid);
        params.put("OsVersion", androidVersion);
        params.put("AppData", appData);

        Log.e("sendAppData", "AccId:" + acctId);
        Log.e("sendAppData", "UniqeId:" + uuid);
        Log.e("sendAppData", "OsVersion:" + androidVersion);
        Log.e("sendAppData", "AppData:" + appData);

        syncHttpClient.post(ConnectInfo.SEND_APP_DATA, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Log.e("BleConnect", "sendAppData:" + response.toString());

                try {

                    if (response.getBoolean("Status")) {

                        EventCenter.getInstance().sendAppData(getAppData(response));

                    } else {
                        EventCenter.getInstance().sendAppDataError(appData);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                if (throwable.getCause() instanceof ConnectTimeoutException) {
                    EventCenter.getInstance().sendAppDataError(appData);
                } else if (throwable.getCause() instanceof SocketTimeoutException) {
                    EventCenter.getInstance().sendAppDataError(appData);
                } else {
                    EventCenter.getInstance().sendAppData("ERROR");
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 確認華創版的VIG是否有拿到Key
     *
     * @param orderId 訂單編號
     */
    public void checkHaitecVIGKey(int orderId) {

        loader.setTimeout(40 * 1000);
        loader.setResponseTimeout(40 * 1000);
        loader.post(ConnectInfo.CHECK_VIG_KEY + String.valueOf(orderId), null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                EventCenter.getInstance().sendIsVIGKey(parser.getJSONData(response.toString(), CheckVig.class));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                EventCenter.getInstance().sendIsVIGKey(new CheckVig());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendIsVIGKey(new CheckVig());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendIsVIGKey(new CheckVig());
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 確認車王版的Vig是否拿到key
     *
     * @param orderId
     * @param acctId
     */
    public void checkMobiletronVIGKey(int orderId, String acctId) {

        RequestParams params = new RequestParams();
        params.put("OrderId", orderId);
        params.put("AccId", acctId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CHECK_MOBILETRON_VIG_KEY, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                try {

                    if (response.getBoolean("Status") && response.getBoolean("KeyInVig")) {
                        EventCenter.getInstance().sendMobiletronIsVIGKey(true);
                    } else {
                        EventCenter.getInstance().sendMobiletronIsVIGKey(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendMobiletronIsVIGKey(false);
            }

            @Override
            public void onFailForString(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailForString(statusCode, headers, responseString, throwable);
                EventCenter.getInstance().sendMobiletronIsVIGKey(false);
            }

            @Override
            public void onFailForArray(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailForArray(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendMobiletronIsVIGKey(false);
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得AppData(華創)
     *
     * @param response
     * @return
     */
    private String getAppData(JSONObject response) {

        String appData = "";

        try {
            if (response.getString("AppData").length() > 0 && response.getString("ResponseStatus").equals("SUCCESS")) {

                appData = response.getString("AppData") + ",SUCCESS";

            } else {

                switch (response.getString("ResponseStatus")) {
                    case "CONTINUE":
                        appData = response.getString("AppData");
                        break;
                    case "SUCCESS":
                        appData = "SUCCESS";
                        break;
                    default:
                        appData = "ERROR";
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return appData;
    }

    // ----------------------------------------------------
}
