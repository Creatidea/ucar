package tw.com.car_plus.carshare.connect.login;

import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;

/**
 * Created by winni on 2017/3/9.
 */

public class LoginConnect extends BaseConnect {

    public LoginConnect(Context context) {
        super(context);
        httpControl.setTimeout(30 * 1000);
    }

    //--------------------------------------------------

    /**
     * 個人登入
     *
     * @param account  帳號
     * @param password 密碼
     */
    public void connectLogin(int type, String account, String password) {

        RequestParams params = new RequestParams();
        params.put("LoginId", account);
        params.put("Pwd", password);

        login(type, params);
    }

    //--------------------------------------------------

    /**
     * 企業登入
     *
     * @param account  帳號
     * @param password 密碼
     */
    public void connectLoginCompany(int type, String account, String password, String companyTaxId) {

        RequestParams params = new RequestParams();
        params.put("LoginId", account);
        params.put("Pwd", password);
        params.put("LoginType", 2);
        params.put("CompanyId", companyTaxId);

        login(type, params);
    }

    //--------------------------------------------------

    /**
     * 登入
     *
     * @param type
     * @param params
     */
    private void login(final int type, RequestParams params) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.LOGIN, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                try {

                    JSONArray errorArray = response.getJSONArray("Errors");

                    if (errorArray.length() == 0) {
                        User user = parser.getJSONData(response.toString(), User.class);
                        EventCenter.getInstance().sendUserEvent(type, user);
                    } else {
                        EventCenter.getInstance().sendErrorEvent(errorArray.getJSONObject(0).getString("ErrorMsg"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    //--------------------------------------------------

    /**
     * 忘記密碼
     *
     * @param loginId  使用者帳號
     * @param birthday 生日
     */
    public void connectForgotPassword(String loginId, String birthday) {

        RequestParams params = new RequestParams();
        params.put("LoginId", loginId);
        params.put("Birthday", birthday);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.FORGOT_PASSWORD, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            EventCenter.getInstance().sendForgotPasswordEvent(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
    }

    //--------------------------------------------------

}
