package tw.com.car_plus.carshare.connect.news;

import android.content.Context;

import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.news.model.News;

/**
 * Created by winni on 2017/6/7.
 */

public class NewsConnect extends BaseConnect {

    public NewsConnect(Context context) {
        super(context);
    }

    // ----------------------------------------------------

    /**
     * 取得最新消息
     */
    public void getNewsData() {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.NEWS, null, new JsonHandler() {

            @Override
            public void onSuccessForArray(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccessForArray(statusCode, headers, response);

                List<News> newses = new ArrayList<>();

                try {

                    for (int i = 0; i < response.length(); i++) {
                        News news = parser.getJSONData(response.getJSONObject(i).toString(), News.class);
                        newses.add(news);
                    }

                    EventCenter.getInstance().sendNews(newses);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
