package tw.com.car_plus.carshare.connect.preferential;

import android.content.Context;

import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.preferential.model.Preferential;

/**
 * Created by winni on 2017/6/1.
 */

public class PreferentialConnect extends BaseConnect {

    public PreferentialConnect(Context context) {
        super(context);
    }

    /**
     * 取得優惠方案
     */
    public void sendPreferentialsInfo() {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.PREFERENTIAL, null, new JsonHandler() {
            @Override
            public void onSuccessForArray(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccessForArray(statusCode, headers, response);

                List<Preferential> preferentials = new ArrayList<>();

                try {

                    for (int i = 0; i < response.length(); i++) {

                        Preferential preferential = parser.getJSONData(response.getJSONObject(i).toString(), Preferential.class);
                        preferentials.add(preferential);
                    }

                    EventCenter.getInstance().sendPreferential(preferentials);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
