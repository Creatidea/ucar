package tw.com.car_plus.carshare.connect.record;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.record.detail.model.RentRecordDetail;
import tw.com.car_plus.carshare.record.model.RentRecord;

/**
 * Created by winni on 2017/5/8.
 */

public class RecordConnect extends BaseConnect {

    private User user;

    public RecordConnect(Context context) {
        super(context);
        user = sp.getUserData();
    }

    // ----------------------------------------------------

    /**
     * 取得租車記錄
     */
    public void getRecordData(boolean isCompany) {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("IsCompanyOrder", isCompany);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.RENT_RECORD, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                List<RentRecord> rentRecords = new ArrayList<>();

                try {

                    for (int i = 0; i < response.getJSONArray("OrderRecords").length(); i++) {

                        JSONObject jsonObject = response.getJSONArray("OrderRecords").getJSONObject(i);
                        RentRecord rentRecord = parser.getJSONData(jsonObject.toString(), RentRecord.class);
                        rentRecords.add(rentRecord);
                    }

                    EventCenter.getInstance().sendRentRecord(rentRecords);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得租車記錄的詳細資訊
     *
     * @param orderId
     */
    public void getRecordDetailData(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.RENT_RECORD_DETAIL, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendRentRecordDetail(parser.getJSONData(response.toString(), RentRecordDetail.class));
                    }
                });
            }
        });
    }

}
