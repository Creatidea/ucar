package tw.com.car_plus.carshare.connect.register;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.register.model.RegisterInfo;
import tw.com.car_plus.carshare.register.model.UserAccountData;

/**
 * Created by winni on 2017/3/2.
 */

public class RegisterConnect extends BaseConnect {

    private static AsyncHttpClient client;
    private Context context;

    //--------------------------------------------------
    public RegisterConnect(Context context) {
        super(context);
        this.context = context;

        httpControl.setTimeout(15 * 1000);
        client = new AsyncHttpClient();
    }

    //--------------------------------------------------

    /**
     * 取得縣市的相關資料
     */
    public void connectGetCity() {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CITY, null, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                List<City> cityList = new ArrayList<>();

                try {
                    JSONArray cityArray = response.getJSONArray("ReturnData");

                    for (int i = 0; i < cityArray.length(); i++) {
                        City city = parser.getJSONData(cityArray.getJSONObject(i).toString(), City.class);
                        cityList.add(city);
                    }

                    EventCenter.getInstance().sendCityEvent(cityList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    //--------------------------------------------------

    /**
     * 取得某縣市的所有區域
     *
     * @param cityType 縣市的type
     */
    public void connectGetArea(final int cityType) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(String.format(ConnectInfo.AREA, cityType), null, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                List<Area> areaList = new ArrayList<>();

                try {

                    JSONArray areaArray = response.getJSONArray("ReturnData");
                    for (int i = 0; i < areaArray.length(); i++) {
                        Area area = parser.getJSONData(areaArray.getJSONObject(i).toString(), Area.class);
                        areaList.add(area);
                    }

                    EventCenter.getInstance().sendAreaEvent(areaList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });

    }

    //--------------------------------------------------

    /**
     * 傳送註冊的資料
     *
     * @param registerInfo
     */
    public void connectSendUserInfo(final RegisterInfo registerInfo) {

        StringEntity entity = new StringEntity(new Gson().toJson(registerInfo), "utf-8");

        client.post(context, ConnectInfo.ADD_MEMBER, entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            UserAccountData userAccountData = new UserAccountData();
                            userAccountData.setAcctId(response.getString("AcctId"));
                            userAccountData.setLoginId(registerInfo.getLoginId());
                            EventCenter.getInstance().sendSuccessEvent(userAccountData);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });

    }

    //--------------------------------------------------

    /**
     * 傳送驗證碼
     *
     * @param acctId
     * @param loginId
     */
    public void connectVerifyCode(String acctId, String loginId) {

        RequestParams params = new RequestParams();
        params.put("AcctId", acctId);
        params.put("LoginID", loginId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.IDENTIFYING_CODE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            EventCenter.getInstance().sendIdentifyingCodeEvent(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 傳送驗證碼是否正確
     *
     * @param userAccountData
     * @param verifyCode
     */
    public void connectCheckEffectiveCode(final UserAccountData userAccountData, String verifyCode) {

        RequestParams params = new RequestParams();
        params.put("LoginID", userAccountData.getLoginId());
        params.put("VerifyCode", verifyCode);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CHECK_EFFECTIVE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        EventCenter.getInstance().sendSuccessEvent(userAccountData);
                    }
                });

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    //--------------------------------------------------

    /**
     * 註冊證件照片
     *
     * @param userAccountData               使用者資料
     * @param driverLicensePositiveFileName 駕照正面
     * @param driverLicenseNegativeFileName 駕照反面
     * @param idCardPositiveFileName        身份證正面
     * @param idCardNegativeFileName        身分證反面
     */
    public void connectCredentials(final UserAccountData userAccountData, String driverLicensePositiveFileName, String
            driverLicenseNegativeFileName, String idCardPositiveFileName, String idCardNegativeFileName) {

        RequestParams params = new RequestParams();
        params.put("AcctId", userAccountData.getAcctId());
        params.put("Jurisdiction", "");
        params.put("IdCardHead", idCardPositiveFileName);
        params.put("IdCardTail", idCardNegativeFileName);
        params.put("DriverLicenceHead", driverLicensePositiveFileName);
        params.put("DriverLicenceTail", driverLicenseNegativeFileName);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.REGISTER_CREDENTIALS, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendSuccessEvent(userAccountData);
                    }
                });

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });

    }

    //--------------------------------------------------

    /**
     * 送出審核
     *
     * @param userAccountData
     */
    public void connectApplyVerify(final UserAccountData userAccountData) {

        StringEntity entity = new StringEntity(new Gson().toJson(userAccountData), "utf-8");

        client.post(context, ConnectInfo.APPLY_VERIFY, entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendSuccessEvent(userAccountData);
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        });
    }

}
