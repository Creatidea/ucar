package tw.com.car_plus.carshare.connect.rent;

import android.content.Context;
import android.location.Location;
import android.support.annotation.IntDef;

import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.rent.model.CarDetailInfo;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStation;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStationDetail;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.EstimateAmount;
import tw.com.car_plus.carshare.rent.success.model.Mileage;
import tw.com.car_plus.carshare.rent.success.model.Order;
import tw.com.car_plus.carshare.rent.success.model.Postpone;

import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType.HAITEC;
import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType.MOBILETRON;

/**
 * Created by neil on 2017/3/14.
 */

public class RentConnect extends BaseConnect {

    // ----------------------------------------------------
    public static final int NONE = 0;
    public static final int ONE_KM = 1;
    public static final int FIVE_KM = 5;
    public static final int TEN_KM = 10;
    public static final int FIFTEEN_KM = 15;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NONE, ONE_KM, FIVE_KM, TEN_KM, FIFTEEN_KM})
    public @interface DistanceMode {
    }

    private User user;

    // ----------------------------------------------------
    public RentConnect(Context context) {
        super(context);
        user = sp.getUserData();
    }

    // ----------------------------------------------------

    /**
     * 租車資訊
     */
    public void loadRentInfo(Location userLocation, @DistanceMode int distance, int areaId) {

        final RequestParams params = new RequestParams();
        params.put("Longitude", userLocation.getLongitude());
        params.put("Latitude", userLocation.getLatitude());
        if (distance != NONE) {
            params.put("DistanceByKm", distance);
        } else {
            params.put("AreaId", areaId);
        }

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.PARKING, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                List<Parking> parkings = new ArrayList<>();

                try {
                    for (int apiCount = 0; apiCount < response.getJSONArray("ParkinglotGroup").length(); apiCount++) {

                        parkings.add(parser.getJSONData(response.getJSONArray("ParkinglotGroup").getJSONObject(apiCount).toString(), Parking.class));
                    }
                    EventCenter.getInstance().sendParkingEvent(parkings);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得車輛資訊
     *
     * @param carId
     */
    public void getCarInfoConnect(int carId) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(String.format(ConnectInfo.GET_CAR, carId), null, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            EventCenter.getInstance().sendCarInfo(parser.getJSONData(response.getJSONObject("CarDetailInfo").toString(),
                                    CarDetailInfo.class));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得租車前的預估金額
     *
     * @param pickUpCarDateTime
     * @param returnCarDateTime
     * @param carDetailInfo
     */
    public void estimateSimpleAmountConnect(String pickUpCarDateTime, String returnCarDateTime, CarDetailInfo carDetailInfo) {

        RequestParams params = new RequestParams();
        params.put("CarId", carDetailInfo.getCarId());
        params.put("RentLimitByDay", carDetailInfo.getRentLimitByDay());
        params.put("RentByQuarter", carDetailInfo.getRentByQuarter());
        params.put("RentByNight", carDetailInfo.getRentByNight());
        params.put("NightTimeStart", carDetailInfo.getNightTimeStart());
        params.put("NightTimeEnd", carDetailInfo.getNightTimeEnd());
        params.put("ReservationStart", pickUpCarDateTime);
        params.put("ReservationEnd", returnCarDateTime);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.ESTIMATE_AMOUNT_SIMPLE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                try {

                    JSONArray errorArray = response.getJSONArray("Errors");

                    if (errorArray.length() == 0) {
                        EventCenter.getInstance().sendSimpleEstimateAmount(response.getInt("TotalAmount"));
                    } else {
                        EventCenter.getInstance().sendErrorEvent(errorArray.getJSONObject(0).getString("ErrorMsg"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 預約租車
     *
     * @param carId         車子的id
     * @param pickUpCarTime 取車時間
     * @param returnCarTime 還車時間
     */
    public void reservationCarConnect(@CarVigType.CarVig final int vigType, final int carId, final String pickUpCarTime, final String returnCarTime) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("CarId", carId);
        params.put("ReservationStart", pickUpCarTime);
        params.put("ReservationEnd", returnCarTime);
        params.put("IsCompanyOrder", user.getCompanyAccount() != null);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.RESERVATION_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        ReservationInfo reservationInfo = parser.getJSONData(response.toString(), ReservationInfo.class);

                        switch (vigType) {
                            case HAITEC:

                                if (reservationInfo != null) {
                                    EventCenter.getInstance().sendReservationSuccess(true);
                                    EventCenter.getInstance().sendReservationInfoToIndex(reservationInfo);
                                } else {
                                    EventCenter.getInstance().sendReservationSuccess(false);
                                }

                                break;
                            case MOBILETRON:
                                EventCenter.getInstance().sendReservationInfo(reservationInfo);
                                break;
                        }
                    }
                });
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendErrorEvent(context.getString(R.string.server_error));
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 設定延長取車的時間按一次10分鐘
     *
     * @param orderId
     */
    public void prolongPickUpCarConnect(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.PROLONG_PICK_UP_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendPostponeInfo(parser.getJSONData(response.toString(), Postpone.class));
                    }
                });
            }

        });

    }

    // ----------------------------------------------------

    /**
     * 開始取車
     */
    public void loadPickUpTheCar(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.START_PICK_UP_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendPickUpTheCar(parser.getJSONData(response.toString(), Order.class));
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取消取車
     */
    public void cancelPickUpCar(int orderId, boolean isSysCancel) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);
        params.put("IsSysCancel", isSysCancel);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CANCEL_PICK_UP_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendCancelCar(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 完成取車
     *
     * @param orderId
     * @param signatureUrl
     * @param carPhotos
     */
    public void getCarFinish(final int orderId, final String signatureUrl, final List<String> carPhotos) {

        RequestParams params = new RequestParams();
        params.put("Signature", signatureUrl);

        if (carPhotos.size() != 0) {
            params.put("CarPics", carPhotos);
        }

        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.FINISH_PICK_UP_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                try {

                    if (response.getBoolean("Status")) {
                        EventCenter.getInstance().sendStartRentCar(parser.getJSONData(response.toString(), CarFinish.class));
                    } else {

                        JSONArray jsonArray = response.getJSONArray("Errors");
                        if (jsonArray.length() > 0) {
                            EventCenter.getInstance().sendPickUpTheCarError(jsonArray.getJSONObject(0).getString("ErrorMsg"));
                        }
//                        getCarFinish(orderId, signatureUrl, carPhotos);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendStartRentCar(new CarFinish());
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 估算租金
     *
     * @param orderId
     */
    public void estimateAmountConnect(int orderId) {

        final RequestParams params = new RequestParams();

        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.ESTIMATE_AMOUNT, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendEstimatedRent(parser.getJSONData(response.toString(), EstimateAmount.class));
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 更新里程
     *
     * @param orderId
     */
    public void updateMileage(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.UPDATE_MILEAGE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendMileage(parser.getJSONData(response.toString(), Mileage.class));
                    }
                });

            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得充電站的資料
     */
    public void getChargingStation() {

        final List<ChargingStation> chargingStations = new ArrayList<>();

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.GET_CHARGING_STATION, null, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {


                            JSONArray jsonArray = response.getJSONArray("ChargeStationBasicDatas");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                ChargingStation chargingStation = parser.getJSONData(jsonArray.getJSONObject(i).toString(), ChargingStation.class);
                                chargingStation.setLongitude(Double.valueOf(chargingStation.getLng()));
                                chargingStation.setLatitude(Double.valueOf(chargingStation.getLat()));
                                chargingStation.setTitle(chargingStation.getStationName());

                                chargingStations.add(chargingStation);

                            }

                            EventCenter.getInstance().sendGetChargingStation(chargingStations);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得充電站的詳細資料
     *
     * @param stationId
     */
    public void getChargingStationDetail(String stationId) {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(String.format(ConnectInfo.GET_CHARGING_STATION_DETAIL, stationId), null, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendChargingStationDetail(parser.getJSONData(response.toString(), ChargingStationDetail.class));
                    }
                });
            }
        });
    }

    // ----------------------------------------------------
}
