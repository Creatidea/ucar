package tw.com.car_plus.carshare.connect.return_car;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;
import com.neilchen.complextoolkit.util.HttpLoader;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.rent.model.CancelErrorOrder;
import tw.com.car_plus.carshare.rent.return_car.model.BillInfo;
import tw.com.car_plus.carshare.rent.return_car.model.Bonus;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.SpecialReturn;

import static tw.com.car_plus.carshare.rent.return_car.PayActivity.INVOICE_TYPE_2;
import static tw.com.car_plus.carshare.rent.return_car.PayActivity.INVOICE_TYPE_3;


/**
 * Created by neil on 2017/4/13.
 */

public class ReturnCarConnect extends BaseConnect {

    private WebView webView;
    private User user;

    // ----------------------------------------------------
    public ReturnCarConnect(Context context) {
        super(context);
        user = sp.getUserData();
    }


    // ----------------------------------------------------

    /**
     * 取得是否特殊還車
     */
    public void getCheckSpecialReturnCar(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.GET_CHECK_SPECIAL_RETURN_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendSpecialReturn(parser.getJSONData(response.toString(), SpecialReturn.class));
                    }
                });
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得車子引擎狀態
     *
     * @param orderId
     */
    public void checkEngineStatus(int orderId) {

        RequestParams params = new RequestParams();

        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CHECK_ENGINE_STATUS, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendEngineStatus(response.getBoolean("EngineStatus"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
    }

    // ----------------------------------------------------

    /**
     * 確認車子是否已上鎖
     *
     * @param orderId
     */
    public void checkLock(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CHECK_LOCK, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendCarLock(response.getBoolean("IsCarLock"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得帳單資訊
     *
     * @param orderId
     */
    public void loadBillInfo(String url, int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(url, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                EventCenter.getInstance().sendBillInfo(parser.getJSONData(response.toString(), BillInfo.class));
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 傳送砍單的指令
     *
     * @param orderId
     */
    public void sendCancelErrorOrder(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.SEND_CANCEL_ERROR_ORDER, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                EventCenter.getInstance().sendCancelErrorOrder(parser.getJSONData(response.toString(), CancelErrorOrder.class));
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailForObject(statusCode, headers, throwable, errorResponse);
                EventCenter.getInstance().sendCancelErrorOrder(null);
            }

            @Override
            public void onFailForString(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailForString(statusCode, headers, responseString, throwable);
                EventCenter.getInstance().sendCancelErrorOrder(null);
            }

        });

    }

    // ----------------------------------------------------

    /**
     * 開始還車
     */
    public void loadReturnCarInfo(int orderId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.RETURN_CAR, params, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        EventCenter.getInstance().sendReturnCarInfo(parser.getJSONData(response.toString(), ReturnCarInfo.class));

                    }
                });
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 取消還車
     */
    public void loadCancelCar(int orderId) {

        final RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CANCEL_CAR, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        EventCenter.getInstance().sendStartRentCar(parser.getJSONData(response.toString(), CarFinish.class));

                    }
                });

            }
        });
    }

    // ----------------------------------------------------

    /**
     * 讀取紅利點數資訊
     */
    public void loadBonusInfo(int totalPrice, boolean isAgree) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("SourceAmount", totalPrice);
        params.put("IsUseBonus", isAgree);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.BONUS, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        EventCenter.getInstance().sendBonusInfo(parser.getJSONData(response.toString(), Bonus.class));

                    }
                });

            }
        });
    }

    // ----------------------------------------------------

    /**
     * 還車付款-一般用戶
     */
    public void confirmPayment(String signature, ArrayList<String> carPics, String serialNo, boolean isUseBonus, int invoiceType, int parkinglotId,
                               int carId, int orderId, String companyName, String companySn, boolean isUseCarrier, String carrierId) {

        RequestParams params = new RequestParams();
        params.put("MemberId", user.getAcctId());
        params.put("OrderId", orderId);
        for (int count = 0; count < carPics.size(); count++) {
            params.put("CarPics[" + count + "]", carPics.get(count));
        }
        params.put("Signature", signature);
        params.put("SerialNo", serialNo);
        params.put("InvoiceType", invoiceType);
        switch (invoiceType) {
            case INVOICE_TYPE_2:
                params.put("IsUseCarrier", isUseCarrier);
                if (isUseCarrier) {
                    params.put("CarrierId", carrierId);
                }
                break;
            case INVOICE_TYPE_3:
                params.put("CompanyName", companyName);
                params.put("CompanySn", companySn);
                break;
        }
        params.put("LoginId", user.getLoginId());
        params.put("IsUseBonus", isUseBonus);
        params.put("ParkinglotId", parkinglotId);
        params.put("CarId", carId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.PAY, params, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            bankPayment(String.format(ConnectInfo.BANK_PAYMENT, response.getString("PaymentUrl")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
    }

    // ----------------------------------------------------

    /**
     * 還車付款-企業用戶
     *
     * @param signature
     * @param carPics
     * @param parkinglotId
     * @param carId
     * @param orderId
     */
    public void confirmPaymentCompany(String signature, ArrayList<String> carPics, int parkinglotId, int carId, int orderId) {

        RequestParams params = new RequestParams();
        params.put("Signature", signature);
        for (int count = 0; count < carPics.size(); count++) {
            params.put("CarPics[" + count + "]", carPics.get(count));
        }

        params.put("BeaconId", 1);
        params.put("ParkinglotId", parkinglotId);
        params.put("CarId", carId);
        params.put("MemberId", user.getAcctId());
        params.put("LoginId", user.getLoginId());
        params.put("OrderId", orderId);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.PAY_COMPANY, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT_COMPANY, context.getResources().getString(R.string
                        .transaction_success));
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 銀行付款
     */
    private void bankPayment(String url) {

        final LoadingDialogManager loading = new LoadingDialogManager(context);
        loading.show();

        webView = new WebView(context);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(this, "HTMLOUT");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                if (webView != null) {
                    webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                }
                loading.dismiss();
            }
        });

        webView.loadUrl(url);
    }

    // ----------------------------------------------------

    /**
     * 讀取銀行付款正確的網址
     *
     * @param url
     */
    public void loadBankPaymentHtml(final String url) {

        final LoadingDialogManager loading = new LoadingDialogManager(context);
        loading.show();

        String url2 = url.replace(" ", "");
        HttpLoader.post(url2, null, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int i, Header[] headers, String s, Throwable throwable) {

                Document document = Jsoup.parse(s);
                if (document.select("h2").isEmpty()) {
                    webView = null;
                    EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT, context.getResources().getString(R.string
                            .transaction_success));
                } else {
                    loadBankPaymentHtml(String.format(ConnectInfo.BANK_PAYMENT, document.select("a").first().attr("href")));
                }

                loading.dismiss();
            }

            @Override
            public void onSuccess(int i, Header[] headers, String s) {
                loading.dismiss();

                Document document = Jsoup.parse(s);
                String title = document.select("title").html();

                if (title.equals(context.getString(R.string.transaction_success))) {
                    EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT, title);
                } else {
                    String errorMessage = document.select("div.error-desc").first().html();
                    EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT, title + "," + errorMessage);
                }

                webView = null;
            }

        });

    }

    // ----------------------------------------------------
    @JavascriptInterface
    public void processHTML(String html) {

        if (webView != null) {

            Document document = Jsoup.parse(html);
            boolean isForm = document.select("form").isEmpty();

            if (!isForm) {
                boolean isAction = document.select("form").first().attr("action").isEmpty();

                if (!isAction) {
                    final String url = document.select("form").first().attr("action");
                    EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT_URL, url);
                }
            } else {
                String title = document.select("title").html();
                String errorMessage = document.select("div.error-desc").first().html();
                EventCenter.getInstance().sendBankPayment(EventCenter.BANK_PAYMENT, title + errorMessage);
            }
        }
    }

    // ----------------------------------------------------
}
