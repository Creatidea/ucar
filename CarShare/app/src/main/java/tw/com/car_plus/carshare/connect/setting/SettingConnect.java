package tw.com.car_plus.carshare.connect.setting;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;

/**
 * Created by neil on 2017/4/12.
 */

public class SettingConnect extends BaseConnect {

    // ----------------------------------------------------
    public SettingConnect(Context context) {
        super(context);
    }

    // ----------------------------------------------------
    public void changePassword(String oldPassword, final String newPassword, String checkPassword) {

        RequestParams params = new RequestParams();
        params.put("AcctId", sp.getAcctId());
        params.put("OldPassword", oldPassword);
        params.put("NewPassword", newPassword);
        params.put("NewPasswordConfirm", checkPassword);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.CHANGE_PASSWORD, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            //確認是否有自動登入，有的話需要將值¬改變
                            if (sp.getPassword().length() > 0) {
                                sp.setPassword(newPassword);
                            }

                            sp.getUserData().setPassword(newPassword);
                            EventCenter.getInstance().sendSuccess(response.getBoolean("Status"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
    }

    // ----------------------------------------------------

    /**
     * 設定是否要訂閱停車場的推播
     *
     * @param parkingId   停車場的Id
     * @param isSubscribe 是否要訂閱
     */
    public void setSubscribeParking(int parkingId, final String parkingName, boolean isSubscribe) {


        RequestParams params = new RequestParams();
        params.put("AcctId", sp.getAcctId());
        params.put("ParkinglotId", parkingId);
        params.put("IsSubscribe", isSubscribe);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.SUBSCRIBE_PARKING, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        EventCenter.getInstance().sendSettingRemindParking(parkingName);
                    }
                });

            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得有設定提醒的停車場
     */
    public void loadRemindPraking() {

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.REMIND_PARKING + sp.getAcctId(), null, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                List<String> remindParking = new ArrayList<>();

                try {

                    JSONArray ja = response.getJSONArray("ParkinglotNames");

                    for (int count = 0; count < ja.length(); count++) {
                        remindParking.add(ja.getString(count));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                EventCenter.getInstance().sendRemindParking(remindParking);
            }
        });
    }

    // ----------------------------------------------------
}
