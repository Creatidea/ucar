package tw.com.car_plus.carshare.connect.user;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.model.RegisterInfo;
import tw.com.car_plus.carshare.user.model.UserInfo;
import tw.com.car_plus.carshare.user.model.VerifyResult;

/**
 * Created by neil on 2017/4/13.
 */

public class UserConnect extends BaseConnect {

    private static AsyncHttpClient client;
    private User user;

    // ----------------------------------------------------
    public UserConnect(Context context) {
        super(context);
        client = new AsyncHttpClient();
        user = sp.getUserData();
    }

    // ----------------------------------------------------

    /**
     * 使用者資訊
     */
    public void loadUserInfo() {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("LoginID", user.getLoginId());

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.USER_INFO, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {

                EventCenter.getInstance().sendUserInfo(parser.getJSONData(response.toString(), UserInfo.class));
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 傳送更新使用者資料
     *
     * @param registerInfo
     */
    public void sendUpdateUserData(RegisterInfo registerInfo) {

        final LoadingDialogManager loadingDialogManager = new LoadingDialogManager(context);
        loadingDialogManager.show();

        StringEntity entity = new StringEntity(new Gson().toJson(registerInfo), "utf-8");

        client.setConnectTimeout(15 * 1000);
        client.post(context, ConnectInfo.UPDATE_USER_DATA, entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                loadingDialogManager.dismiss();

                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {
                        try {
                            EventCenter.getInstance().sendUpdateUser(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                loadingDialogManager.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                loadingDialogManager.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                loadingDialogManager.dismiss();
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 取得使用者審查結果
     */
    public void getVerifyResult() {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.VERIFY_RESULT, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                EventCenter.getInstance().sendVerifyResult(parser.getJSONData(response.toString(), VerifyResult.class));
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 註冊推播裝置
     *
     * @param token
     */
    public void registerDevice(final String token) {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("DeviceToken", token);
        params.put("DeviceType", 2);

        httpControl.setHttpMode(HttpMode.POST);
        httpControl.connect(ConnectInfo.REGISTER_DEVICE, params, new JsonHandler() {
            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);

                try {

                    EventCenter.getInstance().sendRegisterDevice(response.getBoolean("Status"));

                } catch (JSONException e) {
                    e.printStackTrace();

                    EventCenter.getInstance().sendRegisterDevice(false);
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 註銷推播裝置
     */
    public void disableDevice() {

        RequestParams params = new RequestParams();
        params.put("AcctId", user.getAcctId());
        params.put("DeviceToken", sp.getToken());

        httpControl.setHttpMode(HttpMode.HTTPS_POST);
        httpControl.connect(ConnectInfo.DISABLE_DEVICE, params, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccessForObject(statusCode, headers, response);
                loadData(response, new IStatus() {
                    @Override
                    public void statusEvent() {

                        try {
                            EventCenter.getInstance().sendDisableDevice(response.getBoolean("Status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

    }

    // ----------------------------------------------------
}
