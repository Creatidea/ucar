package tw.com.car_plus.carshare.event;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.news.model.News;
import tw.com.car_plus.carshare.preferential.model.Preferential;
import tw.com.car_plus.carshare.record.detail.model.RentRecordDetail;
import tw.com.car_plus.carshare.record.model.RentRecord;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.register.model.CreditCardsData;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.rent.model.CancelErrorOrder;
import tw.com.car_plus.carshare.rent.model.CarDetailInfo;
import tw.com.car_plus.carshare.rent.model.ErrorMessage;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.return_car.model.BillInfo;
import tw.com.car_plus.carshare.rent.return_car.model.Bonus;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStation;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStationDetail;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.CheckVig;
import tw.com.car_plus.carshare.rent.success.model.EstimateAmount;
import tw.com.car_plus.carshare.rent.success.model.Mileage;
import tw.com.car_plus.carshare.rent.success.model.Order;
import tw.com.car_plus.carshare.rent.success.model.Postpone;
import tw.com.car_plus.carshare.rent.success.model.SpecialReturn;
import tw.com.car_plus.carshare.service.model.Time;
import tw.com.car_plus.carshare.user.info.modify.model.Address;
import tw.com.car_plus.carshare.user.model.UserInfo;
import tw.com.car_plus.carshare.user.model.VerifyResult;

/**
 * Created by winni on 2017/3/2.
 */

public class EventCenter {

    //縣市
    public static final int CITY = 11;
    //區域
    public static final int AREA = 22;
    //附近可租車輛
    public static final int PARKING = 33;

    //登入
    public static final int LOGIN = 1;
    //首頁
    public static final int INDEX = 2;
    //會員資料
    public static final int USER = 3;
    //註冊
    public static final int REGISTER = 4;
    //註冊推播
    public static final int REGISTER_DEVICE = 5;
    //註消推播
    public static final int DISABLE_DEVICE = 6;
    //設定首頁的toolbar的image
    public static final int TOOLBAR_IMAGE_INDEX = 7;
    //設定其他Activity的toolbar的image
    public static final int TOOLBAR_IMAGE = 8;
    //設定toolbar企業與個人的圖示切換
    public static final int TOOLBAR_CHANGE = 9;
    //設定toolbar企業個人切換事件為null
    public static final int TOOLBAR = 10;

    //預約租車-回到預約詳細頁
    public static final int RESERVATION = 40;

    //開始取車 -> 實體拍照
    public static final int PHOTO = 50;
    //開始取車 -> 規範
    public static final int CONTRACT = 51;
    //開始取車 -> 電子簽名
    public static final int SIGNATURE = 52;
    //取消取車
    public static final int CANCEL_CAR = 53;
    //完成取車錯誤
    public static final int GET_GAR_FINISH_ERROR = 54;

    //開始租車 -> 取得充電站資料
    public static final int CHARGING_STATION = 60;

    //開始還車 -> 檢查清單
    public static final int RETURN_CAR_CHECK = 70;
    //開始還車 -> 拍照
    public static final int RETURN_CAR_PHOTO = 71;
    //開始還車 -> 上鎖
    public static final int RETURN_CAR_LOCk = 72;
    //開始還車 -> 付款
    public static final int RETURN_CAR_SIGNATURE = 73;
    //開始還車地圖模式
    public static final int RETURN_CAR_MAP = 80;
    //銀行付款
    public static final int BANK_PAYMENT = 81;
    //銀行付款-交易成功網址
    public static final int BANK_PAYMENT_URL = 82;
    //銀行付款-企業
    public static final int BANK_PAYMENT_COMPANY = 83;

    //會員資料 -> 姓名
    public static final int USER_NAME = 90;
    //會員資料 -> 生日
    public static final int USER_BIRTHDAY = 91;
    //會員資料 -> 電話
    public static final int USER_PHONE = 92;
    //會員資料 -> 信箱
    public static final int USER_EMAIL = 93;
    //會員資料 -> 戶籍地址
    public static final int USER_ADDRESS = 94;
    //會員資料 -> 通訊地址
    public static final int USER_CONTACT_ADDRESS = 95;
    //會員資料 -> 更新會員資料
    public static final int USER_UPDATE = 96;
    //會員資料 -> 設定付費的信用卡
    public static final int USER_PAY_CREDIT_CARD = 97;
    //會員資料 -> 設定付費的信用卡(登入)
    public static final int USER_PAY_CREDIT_CARD_LOGIN = 971;
    //會員資料 -> 刪除信用卡
    public static final int USER_DELETE_CREDIT_CARD = 98;

    //租車記錄
    public static final int RENT_RECORD = 100;
    //優惠方案
    public static final int PREFERENTIAL = 200;
    //最新消息
    public static final int NEWS = 300;

    //SecurityService - onCreate
    public static final int SERVICE_ON_CREATE = 400;
    //init CA
    public static final int INIT_CA = 401;
    //init CA 完成
    public static final int INIT_CA_FINISH = 4011;
    //註冊Security
    public static final int REGISTER_SECURITY = 402;
    //完成註冊Security
    public static final int REGISTER_SECURITY_FINISH = 4021;
    //傳送指令給server(華創)
    public static final int COMMAND_TO_SERVER_HAITEC = 403;
    //傳送指令給serverError(華創)
    public static final int COMMAND_TO_SERVER_ERROR_HAITEC = 4031;
    //傳送指令給server(泛用型)
    public static final int COMMAND_TO_SERVER_MOBILETRON = 4032;
    //app data
    public static final int APP_DATA = 404;
    //app data error
    public static final int APP_DATA_ERROR = 4041;
    //重新設定CA設定
    public static final int SECURITY_RESET = 405;
    //通知資安連線
    public static final int SECURITY_CONNECT = 406;
    //資安連線狀態
    public static final int SECURITY_STATUS = 4061;
    //中斷資安連線
    public static final int SECURITY_DISCONNECT = 4062;
    //資安中斷連線狀態
    public static final int SECURITY_DISCONNECT_STATE = 4063;
    //將指令寫入SecurityMain
    public static final int MAIN_SECURITY_WRITE = 4070;
    //將讀出寫入SecurityMain後的資料
    public static final int MAIN_SECURITY_READ = 4071;
    //將指令寫入SecurityBle
    public static final int BLE_SECURITY_WRITE = 4072;
    //將資料寫入VIG
    public static final int VIG_WRITE = 4073;
    //將讀出VIG的回應
    public static final int VIG_READ = 4074;
    //寫入VIG後的狀態
    public static final int VIG_WRITE_STATUS = 4075;
    //讀到Vig封包接收狀態的資料(車王)
    public static final int READ_VIG_DATA = 4076;
    //讀到Vig是否執行Command的資料(車王)
    public static final int READ_VIG_DATA_COMMAND = 4077;
    //藍芽已死
    public static final int BLUETOOTH_ERROR = 4078;
    //通知取車作業完成
    public static final int GET_CAR_FINISH = 4080;
    //通知還車作業完成
    public static final int RETURN_CAR_FINISH = 4081;
    //通知取消作業完成
    public static final int CANCEL_CAR_FINISH = 4082;
    // 取得Server Key
    public static final int SERVER_KEY = 500;
    // 取ServerKey錯誤
    public static final int SERVER_KEY_ERROR = 501;
    // 確認VIG是否有取得KEY(華創)
    public static final int CHECK_VIG_KEY = 502;
    // 確認VIG是否有取得KEY(車王)
    public static final int CHECK_MOBILETRON_VIG_KEY = 503;
    // 提醒停車場
    public static final int REMIND_PARKING = 9000;
    //設定停車場提醒
    public static final int SETTING_REMIND_PARKING = 9001;


    //--------------------------------------------------
    private static EventCenter ourInstance = new EventCenter();

    //--------------------------------------------------
    public static EventCenter getInstance() {
        return ourInstance;
    }

    //--------------------------------------------------
    private EventCenter() {

    }

    //--------------------------------------------------
    private void sendListEvent(int type, List<?> dataList) {

        Map<String, Object> data = new HashMap<>();
        data.put("type", type);
        data.put("data", dataList);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------
    private void sendObjectEvent(int type, Object object) {
        Map<String, Object> data = new HashMap<>();
        data.put("type", type);
        data.put("data", object);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------

    /**
     * send server key.
     */
    public void sendKey(String key) {
        sendObjectEvent(SERVER_KEY, key);
    }

    //--------------------------------------------------

    /**
     * 取得ServerKey錯誤
     *
     * @param status
     */
    public void getServerKeyError(int status) {
        sendObjectEvent(SERVER_KEY_ERROR, status);
    }

    //--------------------------------------------------

    /**
     * 傳送設定首頁Toolbar的Image
     *
     * @param imageResource
     */
    public void sendSetToolbarImageIndex(int imageResource) {
        sendObjectEvent(TOOLBAR_IMAGE_INDEX, imageResource);
    }

    //--------------------------------------------------

    /**
     * 傳送設定其他Activity的Toolbar的Image
     *
     * @param imageResource
     */
    public void sendSetToolbarImage(int imageResource) {
        sendObjectEvent(TOOLBAR_IMAGE, imageResource);
    }

    //--------------------------------------------------

    /**
     * 設定Toolbar的企業與個人圖示切換
     */
    public void sendSetToolbarChange() {
        Map<String, Object> data = new HashMap<>();
        data.put("type", TOOLBAR_CHANGE);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------

    /**
     * 設定Toolbar的企業與個人圖示切換按下事件為null
     */
    public void sendSetToolbarChangeNull() {
        Map<String, Object> data = new HashMap<>();
        data.put("type", TOOLBAR);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------

    /**
     * 銀行付款資訊
     */
    public void sendBankPayment(int type, String message) {
        sendObjectEvent(type, message);
    }

    //--------------------------------------------------

    /**
     * 代表傳送成功
     */
    public void sendSuccess(Boolean isSuccess) {
        EventBus.getDefault().post(isSuccess);
    }

    //--------------------------------------------------

    /**
     * 附近可租車輛
     */
    public void sendParkingEvent(List<Parking> parkings) {
        sendListEvent(PARKING, parkings);
    }

    //--------------------------------------------------

    /**
     * 傳送縣市的資料
     *
     * @param cityList
     */
    public void sendCityEvent(List<City> cityList) {
        sendListEvent(CITY, cityList);
    }

    //--------------------------------------------------

    /**
     * 傳送區域的資料
     *
     * @param areaList
     */
    public void sendAreaEvent(List<Area> areaList) {
        sendListEvent(AREA, areaList);
    }

    //--------------------------------------------------

    /**
     * 傳送錯誤訊息用
     */
    public void sendErrorEvent(String message) {
        EventBus.getDefault().post(message);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者訊息(註冊頁)
     *
     * @param userAccountData 使用者資料
     */
    public void sendSuccessEvent(UserAccountData userAccountData) {
        EventBus.getDefault().post(userAccountData);
    }

    //--------------------------------------------------

    /**
     * 傳送正確訊息用(驗證頁)
     *
     * @param isSuccess
     */
    public void sendIdentifyingCodeEvent(Boolean isSuccess) {
        EventBus.getDefault().post(isSuccess);
    }

    //--------------------------------------------------

    /**
     * 上傳檔案
     *
     * @param uploadFile
     */
    public void sendUploadFileEvent(UploadFile uploadFile) {

        EventBus.getDefault().post(uploadFile);
    }

    //--------------------------------------------------

    /**
     * 傳送註冊信用卡的資料
     *
     * @param creditCards
     */
    public void sendCreditCardsEvent(CreditCardsData creditCards) {
        EventBus.getDefault().post(creditCards);
    }

    //--------------------------------------------------

    /**
     * 傳送User的資料
     *
     * @param user
     */
    public void sendUserEvent(int type, User user) {
        sendObjectEvent(type, user);
    }

    //--------------------------------------------------

    /**
     * 傳送車輛資訊
     *
     * @param carDetailInfo
     */
    public void sendCarInfo(CarDetailInfo carDetailInfo) {
        EventBus.getDefault().post(carDetailInfo);
    }

    //--------------------------------------------------

    /**
     * 傳送租車前的預估金額
     *
     * @param amount
     */
    public void sendSimpleEstimateAmount(Integer amount) {
        EventBus.getDefault().post(amount);
    }

    //--------------------------------------------------

    /**
     * 傳送預約成功的資料
     *
     * @param isSuccess
     */
    public void sendReservationSuccess(Boolean isSuccess) {
        EventBus.getDefault().post(isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送預約成功的資料
     *
     * @param reservationInfo
     */
    public void sendReservationInfoToIndex(ReservationInfo reservationInfo) {
        EventBus.getDefault().postSticky(reservationInfo);
    }

    //--------------------------------------------------

    /**
     * 傳送預約成功的資料到預約詳細頁
     *
     * @param reservationInfo
     */
    public void sendReservationInfo(ReservationInfo reservationInfo) {
        sendObjectEvent(RESERVATION, reservationInfo);
    }

    //--------------------------------------------------

    /**
     * 傳送延遲取車的資料
     *
     * @param postpone
     */
    public void sendPostponeInfo(Postpone postpone) {
        EventBus.getDefault().post(postpone);
    }

    //--------------------------------------------------

    /**
     * 開始取車
     */
    public void sendPickUpTheCar(Order order) {
        EventBus.getDefault().post(order);
    }

    //--------------------------------------------------

    /**
     * 取消取車
     *
     * @param isSuccess
     */
    public void sendCancelCar(Boolean isSuccess) {
        sendObjectEvent(CANCEL_CAR, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送完成取車錯誤
     *
     * @param errorMessage
     */
    public void sendPickUpTheCarError(String errorMessage) {
        sendObjectEvent(GET_GAR_FINISH_ERROR, errorMessage);
    }

    //--------------------------------------------------

    /**
     * 傳送到數時間
     */
    public void sendCountDownTime(Time time) {
        EventBus.getDefault().post(time);
    }

    //--------------------------------------------------

    /**
     * 傳送計時時間
     */
    public void sendTimeMeasuring(Time time) {
        EventBus.getDefault().post(time);
    }


    // ----------------------------------------------------

    /**
     * 傳送估算租金的資料
     *
     * @param estimateAmount
     */
    public void sendEstimatedRent(EstimateAmount estimateAmount) {
        EventBus.getDefault().post(estimateAmount);
    }

    //--------------------------------------------------

    /**
     * 開始租車
     */
    public void sendStartRentCar(CarFinish finish) {
        EventBus.getDefault().post(finish);
    }

    //--------------------------------------------------

    /**
     * 傳送更新後的里程
     *
     * @param mileage
     */
    public void sendMileage(Mileage mileage) {
        EventBus.getDefault().post(mileage);
    }

    //--------------------------------------------------

    /**
     * 傳送充電站列表資料
     *
     * @param chargingStations
     */
    public void sendGetChargingStation(List<ChargingStation> chargingStations) {
        sendListEvent(CHARGING_STATION, chargingStations);
    }

    //--------------------------------------------------

    /**
     * 傳送充電座的資料
     *
     * @param chargingStationDetail
     */
    public void sendChargingStationDetail(ChargingStationDetail chargingStationDetail) {
        EventBus.getDefault().post(chargingStationDetail);
    }

    //--------------------------------------------------

    /**
     * 傳送特殊還車的資料
     *
     * @param specialReturn
     */
    public void sendSpecialReturn(SpecialReturn specialReturn) {
        EventBus.getDefault().post(specialReturn);
    }

    //--------------------------------------------------

    /**
     * 傳送忘記密碼
     *
     * @param isSuccess
     */
    public void sendForgotPasswordEvent(Boolean isSuccess) {
        EventBus.getDefault().post(isSuccess);
    }

    //--------------------------------------------------

    /**
     * 使用者資訊
     */
    public void sendUserInfo(UserInfo userInfo) {
        EventBus.getDefault().post(userInfo);
    }

    //--------------------------------------------------

    /**
     * 傳送引擎的狀態
     *
     * @param isEngineStatus
     */
    public void sendEngineStatus(Boolean isEngineStatus) {
        EventBus.getDefault().post(isEngineStatus);
    }

    //--------------------------------------------------

    /**
     * 傳送上鎖的狀態
     *
     * @param isLock
     */
    public void sendCarLock(Boolean isLock) {
        EventBus.getDefault().post(isLock);
    }

    //--------------------------------------------------

    /**
     * 地圖還車資訊
     */
    public void sendReturnCarInfo(ReturnCarInfo info) {
        EventBus.getDefault().post(info);
    }

    //--------------------------------------------------

    /**
     * 帳單資訊
     */
    public void sendBillInfo(BillInfo info) {
        EventBus.getDefault().post(info);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者姓名
     *
     * @param name
     */
    public void sendUserName(String name) {
        sendObjectEvent(USER_NAME, name);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者生日
     *
     * @param birthday
     */
    public void sendUserBirthday(String birthday) {
        sendObjectEvent(USER_BIRTHDAY, birthday);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者電話
     *
     * @param phone
     */
    public void sendUserPhone(String phone) {
        sendObjectEvent(USER_PHONE, phone);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者信箱
     *
     * @param mail
     */
    public void sendUserMail(String mail) {
        sendObjectEvent(USER_EMAIL, mail);
    }


    //--------------------------------------------------

    /**
     * 傳送使用者戶籍地址
     *
     * @param address
     */
    public void sendUserAddress(Address address) {
        sendObjectEvent(USER_ADDRESS, address);
    }

    //--------------------------------------------------

    /**
     * 傳送使用者通訊地址
     *
     * @param contactAddress
     */
    public void sendUserContactAddress(Address contactAddress) {
        sendObjectEvent(USER_CONTACT_ADDRESS, contactAddress);
    }

    //--------------------------------------------------

    /**
     * 傳送更新使用者資料是否正確
     *
     * @param isSuccess
     */
    public void sendUpdateUser(Boolean isSuccess) {
        sendObjectEvent(USER_UPDATE, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送註冊推播
     *
     * @param isSuccess
     */
    public void sendRegisterDevice(Boolean isSuccess) {
        sendObjectEvent(REGISTER_DEVICE, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送註銷推播
     *
     * @param isSuccess
     */
    public void sendDisableDevice(Boolean isSuccess) {
        sendObjectEvent(DISABLE_DEVICE, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送設定付費信用卡是否正確
     *
     * @param isSuccess
     */
    public void sendSetPayCard(Boolean isSuccess) {
        sendObjectEvent(USER_PAY_CREDIT_CARD, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送設定付費信用卡是否正確
     *
     * @param isSuccess
     */
    public void sendDeleteCreditCard(Boolean isSuccess) {
        sendObjectEvent(USER_DELETE_CREDIT_CARD, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 紅利點數資訊
     *
     * @param bonus
     */
    public void sendBonusInfo(Bonus bonus) {
        EventBus.getDefault().post(bonus);
    }


    //--------------------------------------------------

    /**
     * 傳送租車記錄
     *
     * @param rentRecords
     */
    public void sendRentRecord(List<RentRecord> rentRecords) {
        sendListEvent(RENT_RECORD, rentRecords);
    }

    //--------------------------------------------------

    /**
     * 傳送租車記錄的詳細頁
     *
     * @param rentRecordDetail
     */
    public void sendRentRecordDetail(RentRecordDetail rentRecordDetail) {
        EventBus.getDefault().post(rentRecordDetail);
    }

    //--------------------------------------------------

    /**
     * 傳送審查結果
     *
     * @param verifyResult
     */
    public void sendVerifyResult(VerifyResult verifyResult) {
        EventBus.getDefault().post(verifyResult);
    }

    //--------------------------------------------------

    /**
     * 傳送優惠方案
     *
     * @param preferentials
     */
    public void sendPreferential(List<Preferential> preferentials) {
        sendListEvent(PREFERENTIAL, preferentials);
    }

    //--------------------------------------------------

    /**
     * 最新消息
     *
     * @param newses
     */
    public void sendNews(List<News> newses) {
        sendListEvent(NEWS, newses);
    }


    //--------------------------------------------------

    /**
     * 傳送取車
     */
    public void sendGetCar(boolean isSuccess) {
        sendObjectEvent(GET_CAR_FINISH, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送到Service的App data
     *
     * @param appData
     */
    public void sendAppData(String appData) {
        sendObjectEvent(APP_DATA, appData);
    }

    //--------------------------------------------------

    /**
     * 傳送AppDataError
     */
    public void sendAppDataError(String data) {
        sendObjectEvent(APP_DATA_ERROR, data);
    }

    //--------------------------------------------------

    /**
     * 傳送SecurityService啟動
     */
    public void sendServiceCreated() {
        sendObjectEvent(SERVICE_ON_CREATE, true);
    }

    //--------------------------------------------------

    /**
     * 傳送CA可以初始化
     *
     * @param acctId
     */
    public void sendInitializeCA(String acctId) {
        sendObjectEvent(INIT_CA, acctId);
    }

    //--------------------------------------------------

    /**
     * 完成初始化CA
     */
    public void sendInitializeCAFinish() {
        sendObjectEvent(INIT_CA_FINISH, true);
    }

    //--------------------------------------------------

    /**
     * 傳送Service設定securityType
     *
     * @param securityType
     */
    public void sendToSecurityServer(int securityType) {
        sendObjectEvent(COMMAND_TO_SERVER_HAITEC, securityType);
    }

    //--------------------------------------------------

    /**
     * 傳送取得配ke時有錯誤
     */
    public void sendErrorCommandToServerHaitec(ErrorMessage errorMessage)  {
        sendObjectEvent(COMMAND_TO_SERVER_ERROR_HAITEC, errorMessage);
    }

    //--------------------------------------------------

    /**
     * 傳送建立泛用型的Key時有錯誤
     */
    public void sendCommandToServerMobiletron(boolean isSuccess) {
        sendObjectEvent(COMMAND_TO_SERVER_MOBILETRON, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送SecurityService註冊Security
     */
    public void sendRegisterSecurity() {
        sendObjectEvent(REGISTER_SECURITY, true);
    }

    //--------------------------------------------------

    /**
     * 傳送完成註冊
     */
    public void sendRegisterSecurityFinish(boolean isSuccess) {
        sendObjectEvent(REGISTER_SECURITY_FINISH, isSuccess);
    }


    //--------------------------------------------------

    /**
     * 傳送執行SecurityConnect
     */
    public void sendSecurityConnect(String vigId) {
        sendObjectEvent(SECURITY_CONNECT, vigId);
    }

    //--------------------------------------------------

    /**
     * 傳送SecurityConnect連線狀態
     *
     * @param type
     */
    public void sendSecurityConnectStatus(int type) {
        sendObjectEvent(SECURITY_STATUS, type);
    }

    //--------------------------------------------------

    /**
     * 傳送資安連線中斷
     */
    public void sendSecurityDisConnect() {
        sendObjectEvent(SECURITY_DISCONNECT, true);
    }

    //--------------------------------------------------

    /**
     * 傳送SecurityDisConnect中斷連線狀態
     *
     * @param isSuccess
     */
    public void sendSecurityDisconnectState(Boolean isSuccess) {
        sendObjectEvent(SECURITY_DISCONNECT_STATE, isSuccess);
    }

    //--------------------------------------------------

    /**
     * 傳送重新設定CA
     */
    public void sendReSetCA() {
        sendObjectEvent(SECURITY_RESET, true);
    }

    //--------------------------------------------------

    /**
     * 傳送指令給MainSecurity
     *
     * @param value
     */
    public void sendMainSecurityWrite(byte[] value) {
        sendObjectEvent(MAIN_SECURITY_WRITE, value);
    }


    //--------------------------------------------------

    /**
     * 傳送寫入MainSecurity後的回應值
     *
     * @param data
     */
    public void sendMainSecurityRead(String data) {
        sendObjectEvent(MAIN_SECURITY_READ, data);
    }

    //--------------------------------------------------

    /**
     * 傳送指令給BLESecurity
     *
     * @param value
     */
    public void sendBLESecurityWrite(byte[] value) {
        sendObjectEvent(BLE_SECURITY_WRITE, value);
    }


    // ----------------------------------------------------

    /**
     * 傳送還車完成
     */
    public void sendReturnCarFinish(boolean isSuccess) {
        sendObjectEvent(RETURN_CAR_FINISH, isSuccess);
    }

    // ----------------------------------------------------

    /**
     * 傳送取消華創的租單資料
     *
     * @param isSuccess
     */
    public void sendCancelCarFinish(boolean isSuccess) {
        sendObjectEvent(CANCEL_CAR_FINISH, isSuccess);

    }

    //--------------------------------------------------

    /**
     * 傳送指令給VIG
     *
     * @param value
     */
    public void sendWriteVIG(byte[] value) {
        sendObjectEvent(VIG_WRITE, value);
    }

    // ----------------------------------------------------

    /**
     * 傳送寫入VIG的狀態
     *
     * @param status
     */
    public void sendWriteVIGStatus(int status) {
        sendObjectEvent(VIG_WRITE_STATUS, status);
    }

    // ----------------------------------------------------

    /**
     * 傳送藍芽已死的狀態
     *
     * @param status
     */
    public void sendBluetoothError(int status) {

        sendObjectEvent(BLUETOOTH_ERROR, status);
    }

    //--------------------------------------------------

    /**
     * 傳送讀取到的封包狀態的資料
     *
     * @param data
     */
    public void sendReadVigData(String data) {
        sendObjectEvent(READ_VIG_DATA, data);
    }

    //--------------------------------------------------

    /**
     * 讀取到Vig執行Command的資料
     *
     * @param data
     */
    public void sendReadVigDataCommand(String data) {
        sendObjectEvent(READ_VIG_DATA_COMMAND, data);
    }

    //--------------------------------------------------

    /**
     * 傳送讀出VIG回傳的資訊
     */
    public void sendReadVIG() {
        sendObjectEvent(VIG_READ, true);
    }

    //--------------------------------------------------

    /**
     * 傳送提醒停車場資訊
     */
    public void sendRemindParking(List<String> remindParking) {
        sendListEvent(REMIND_PARKING, remindParking);
    }

    //--------------------------------------------------

    /**
     * 設定停車場推播提醒
     */
    public void sendSettingRemindParking(String parkingName) {
        sendObjectEvent(SETTING_REMIND_PARKING, parkingName);
    }

    //--------------------------------------------------

    /**
     * 確認VIG是否有取得key(華創)
     *
     * @param vig
     */
    public void sendIsVIGKey(CheckVig vig) {
        sendObjectEvent(CHECK_VIG_KEY, vig);
    }

    //--------------------------------------------------

    /**
     * 確認VIG是否有取得key(車王)
     *
     * @param isHaveKey
     */
    public void sendMobiletronIsVIGKey(boolean isHaveKey) {

        sendObjectEvent(CHECK_MOBILETRON_VIG_KEY, isHaveKey);
    }

    //--------------------------------------------------

    /**
     * 傳送砍單的資料
     */
    public void sendCancelErrorOrder(CancelErrorOrder cancelErrorOrder) {
        EventBus.getDefault().post(cancelErrorOrder);
    }

    //--------------------------------------------------
}
