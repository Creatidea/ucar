package tw.com.car_plus.carshare.index;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.connect.user.UserConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.navigation.NavigationFragment;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.success.ReservationSuccessFragment;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.UserUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;


/**
 * Created by winni on 2017/3/2.
 */

public class IndexActivity extends CustomBaseActivity {

    // ----------------------------------------------------
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;
    public static ArrayList<AppCompatActivity> activities = new ArrayList<>();

    private UserUtil userUtil;
    private SecurityUtil securityUtil;
    private LoadingDialogManager loadingDialogManager;
    private boolean isIndex = false;
    private String refreshedToken = "";

    // ----------------------------------------------------
    @Override
    public void init() {

        setContentView(R.layout.activity_index);
        ButterKnife.bind(this);

        refreshedToken = FirebaseInstanceId.getInstance().getToken();

        setToolbar(R.drawable.ic_menu, R.drawable.img_toolbar_map, new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        securityUtil = new SecurityUtil(this);
        userUtil = new UserUtil(this, sp);

        if (sp.getPassword().length() > 0) {
            sendHockeyAppEvent("自動登入");
            login();
        } else {
            // 地圖模式
            sp.setUserData(null);
            sp.cleanUserData();
            goToFragment(R.id.container_framelayout, new MapModeFragment());
        }


        /* 原需要跳至登入頁面，改換跳至地圖模式
        if (getIntent().getBooleanExtra("isRegister", false)) {

            sendHockeyAppEvent("進入註冊頁");

            sp.setUserData(null);
            sp.cleanUserData();
            goToFragment(R.id.container_framelayout, new RegisterFragment());

        } else {

            if (sp.getPassword().length() > 0) {
                sendHockeyAppEvent("自動登入");
                login();
            } else {
                sendHockeyAppEvent("進入登入頁");
                sp.setUserData(null);
                sp.cleanUserData();
                goToFragment(R.id.container_framelayout, new LoginFragment());
            }
        }
        */

        goToFragment(R.id.navigation, new NavigationFragment());
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (drawerLayout.isDrawerOpen(GravityCompat.END)) {  /*Closes the Appropriate Drawer*/
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    // ----------------------------------------------------
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onSuccessConnect(ReservationInfo info) {

        EventBus.getDefault().removeStickyEvent(ReservationInfo.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("reservationInfo", info);
        goToFragment(R.id.container_framelayout, new ReservationSuccessFragment(), bundle);
    }

    //--------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.INDEX) {

            User user = (User) data.get("data");
            user.setLoginId(sp.getAccount());
            user.setPassword(sp.getPassword());
            userUtil.setUser(user);
            sp.setUserData(user);

            setToolbarChange();
            isIndex = true;
            goToRegisterDevice();

        } else if ((int) data.get("type") == EventCenter.REGISTER_DEVICE) {
            if (isIndex) {
                goToSecurityService();
            }
        } else if ((int) data.get("type") == EventCenter.SERVICE_ON_CREATE) {
            if (isIndex) {
                securityUtil.initializeCA(String.valueOf(sp.getUserData().getAcctId()));
            }

        } else if ((int) data.get("type") == EventCenter.INIT_CA_FINISH) {
            if (isIndex) {
                if (!sp.getRegisterSecurityServer()) {
                    loadingDialogManager = new LoadingDialogManager(this);
                    loadingDialogManager.show();
                    securityUtil.sendRegisterSecurity();
                } else {
                    isIndex = false;
                    goToFragment(R.id.container_framelayout, userUtil.getPageCenter().getGoToFragment());
                }
            }

        } else if ((int) data.get("type") == EventCenter.REGISTER_SECURITY_FINISH) {
            boolean isSuccess = (boolean) data.get("data");

            if (isIndex) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (loadingDialogManager != null) {
                            loadingDialogManager.dismiss();
                            isIndex = false;
                            goToFragment(R.id.container_framelayout, userUtil.getPageCenter().getGoToFragment());
                        }
                    }
                });
            }
        } else if ((int) data.get("type") == EventCenter.TOOLBAR_IMAGE_INDEX) {
            int resource = (int) data.get("data");
            toolbarImage.setImageResource(resource);
        } else if ((int) data.get("type") == EventCenter.TOOLBAR_CHANGE) {
            setToolbarChange();
        } else if ((int) data.get("type") == EventCenter.TOOLBAR) {
            toolbarChange.setOnClickListener(null);
        }
    }

    // ----------------------------------------------------

    /**
     * 自動登入
     */
    private void login() {

        LoginConnect loginConnect = new LoginConnect(this);

        if (sp.getCompanyTaxID().length() == 0) {
            loginConnect.connectLogin(EventCenter.INDEX, sp.getAccount(), sp.getPassword());
        } else {
            loginConnect.connectLoginCompany(EventCenter.INDEX, sp.getAccount(), sp.getPassword(), sp.getCompanyTaxID());
        }
    }

    // ----------------------------------------------------

    /**
     * 前往註冊推播裝置
     */
    private void goToRegisterDevice() {

        if (refreshedToken != null && refreshedToken.length() > 0) {
            new UserConnect(this).registerDevice(refreshedToken);
        } else {
            goToSecurityService();
        }
    }

    //--------------------------------------------------

    /**
     * 前往開啟資安的service
     */
    private void goToSecurityService() {

        if (sp.getUserData().getLoadMemberDataResult() != null) {
            isIndex = false;
            goToFragment(R.id.container_framelayout, userUtil.getPageCenter().getGoToFragment());
        } else {
            securityUtil.starService();
        }
    }

    // ----------------------------------------------------
}
