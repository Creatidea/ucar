package tw.com.car_plus.carshare.index;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by winni on 2017/5/8.
 * 使用者的所有類型(會員狀態、租車狀態、註冊狀態)
 */

public class UserStatus {

    /**
     * OperationStatus-未預約
     */
    public static final int RENT_NONE = 0;
    /**
     * OperationStatus-預約成功
     */
    public static final int RESERVATION = 1;
    /**
     * OperationStatus-取車中
     */
    public static final int PICK_UP_CAR = 2;
    /**
     * OperationStatus-開車中
     */
    public static final int DRIVE_CAR = 3;
    /**
     * OperationStatus-還車中
     */
    public static final int RETURN_CAR = 4;


    public static final int USER_NONE = 0;
    /**
     * VerifyStatus-新申請
     */
    public static final int NEW_MEMBER = 1;
    /**
     * VerifyStatus-審核進行中
     */
    public static final int UNDER_REVIEW = 2;
    /**
     * VerifyStatus-審核失敗
     */
    public static final int FAIL_REVIEW = 3;
    /**
     * VerifyStatus-審核成功
     */
    public static final int PASS_REVIEW = 4;
    /**
     * VerifyStatus-黑名單
     */
    public static final int BLACKLIST = 5;


    public static final int REGISTER_NONE = 0;
    /**
     * RegisterStatus-資訊填寫完成
     */
    public static final int USER_INFO = 1;
    /**
     * RegisterStatus-驗證碼完成
     */
    public static final int USER_IDENTIFYING_CODE = 2;
    /**
     * RegisterStatus-證件上傳完成
     */
    public static final int USER_CREDENTIALS = 3;
    /**
     * RegisterStatus-信用卡完成
     */
    public static final int USER_CREDIT_CARD = 4;

    /**
     * RegisterStatus-所有註冊完成
     */
    public static final int USER_REGISTER_SUCCESS = 5;

    //會員狀態
    @UserStatus.VerifyStatus
    public static int verifyStatus = UserStatus.USER_NONE;
    //註冊狀態
    @UserStatus.RegisterStatus
    public static int registerStatus = UserStatus.REGISTER_NONE;
    //租車狀態
    @UserStatus.OperationStatus
    public static int operationStatus = UserStatus.RENT_NONE;

    public UserStatus() {

    }

    // ----------------------------------------------------

    /**
     * 使用者租車狀態
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({RENT_NONE, RESERVATION, PICK_UP_CAR, DRIVE_CAR, RETURN_CAR})
    public @interface OperationStatus {
    }


    // ----------------------------------------------------

    /**
     * 會員狀態
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({USER_NONE, NEW_MEMBER, UNDER_REVIEW, FAIL_REVIEW, PASS_REVIEW, BLACKLIST})
    public @interface VerifyStatus {
    }

    // ----------------------------------------------------

    /**
     * 註冊狀態
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({REGISTER_NONE, USER_INFO, USER_IDENTIFYING_CODE, USER_CREDENTIALS, USER_CREDIT_CARD, USER_REGISTER_SUCCESS})
    public @interface RegisterStatus {
    }

    // ----------------------------------------------------

    /**
     * 設定會員狀態
     *
     * @param status
     * @return
     */
    public void setVerifyStatus(int status) {

        switch (status) {
            case NEW_MEMBER:
                verifyStatus = NEW_MEMBER;
                break;
            case UNDER_REVIEW:
                verifyStatus = UNDER_REVIEW;
                break;
            case FAIL_REVIEW:
                verifyStatus = FAIL_REVIEW;
                break;
            case PASS_REVIEW:
                verifyStatus = PASS_REVIEW;
                break;
            case BLACKLIST:
                verifyStatus = BLACKLIST;
                break;
        }

    }

    // ----------------------------------------------------

    /**
     * 設定會員註冊狀態
     *
     * @param status
     * @return
     */
    public void setRegisterStatus(int status) {

        switch (status) {
            case USER_INFO:
                registerStatus = USER_INFO;
                break;
            case USER_IDENTIFYING_CODE:
                registerStatus = USER_IDENTIFYING_CODE;
                break;
            case USER_CREDENTIALS:
                registerStatus = USER_CREDENTIALS;
                break;
            case USER_CREDIT_CARD:
                registerStatus = USER_CREDIT_CARD;
                break;
            case USER_REGISTER_SUCCESS:
                registerStatus = USER_REGISTER_SUCCESS;
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 設定會員租車狀態
     *
     * @param status
     * @return
     */
    public void setOperationStatus(int status) {

        switch (status) {
            case RENT_NONE:
                operationStatus = RENT_NONE;
                break;
            case RESERVATION:
                operationStatus = RESERVATION;
                break;
            case PICK_UP_CAR:
                operationStatus = PICK_UP_CAR;
                break;
            case DRIVE_CAR:
                operationStatus = DRIVE_CAR;
                break;
            case RETURN_CAR:
                operationStatus = RETURN_CAR;
                break;
        }

    }

    // ----------------------------------------------------

    /**
     * 重新設定使用者所有狀態
     */
    public static void resetUserStatus() {
        verifyStatus = UserStatus.USER_NONE;
        registerStatus = UserStatus.REGISTER_NONE;
        operationStatus = UserStatus.RENT_NONE;
    }

}
