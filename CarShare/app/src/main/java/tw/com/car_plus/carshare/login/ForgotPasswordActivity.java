package tw.com.car_plus.carshare.login;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.util.CheckDataUtil;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/4/10.
 */

public class ForgotPasswordActivity extends CustomBaseActivity {

    @BindView(R.id.account)
    EditText account;
    @BindView(R.id.birthday)
    EditText birthday;

    private LoginConnect loginConnect;
    private CheckDataUtil checkDataUtil;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        setToolbar(R.drawable.ic_back, R.drawable.img_toolbar_forgotpassword, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loginConnect = new LoginConnect(this);
        checkDataUtil = new CheckDataUtil();
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Boolean isSuccess) {

        if (isSuccess) {
            Toast.makeText(this, getString(R.string.send_password), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.calendar, R.id.send, R.id.clean})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.calendar:
                showDateDialog();
                break;
            case R.id.send:
                if (checkData()) {
                    loginConnect.connectForgotPassword(account.getText().toString(), birthday.getText().toString());
                }
                break;
            case R.id.clean:
                account.setText("");
                birthday.setText("");
                break;
        }
    }

    //--------------------------------------------------

    /**
     * 顯示日期的Dialog
     */
    private void showDateDialog() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                String format = String.format(getString(R.string.date_format), year, month + 1, day);
                birthday.setText(format);
            }

        }, mYear, mMonth, mDay).show();
    }

    // ----------------------------------------------------

    /**
     * 檢查資料
     *
     * @return
     */
    private boolean checkData() {

        boolean isAccount = false, isBirthday = false;

        if (checkDataUtil.checkAccount(account.getText().toString())) {
            isAccount = true;
        } else {
            account.setError(getString(R.string.format_error));
        }

        if (birthday.getText().toString().length() > 0) {
            isBirthday = true;
        } else {
            birthday.setError(getString(R.string.format_error));
        }

        return isAccount && isBirthday;
    }
}
