package tw.com.car_plus.carshare.login;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;
import com.neilchen.complextoolkit.util.net.NetworkChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.connect.user.UserConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.page.PageCenter;
import tw.com.car_plus.carshare.register.RegisterFragment;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.util.CheckDataUtil;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;

/**
 * Created by winni on 2017/3/9.
 * 登入
 */

public class LoginFragment extends CustomBaseFragment implements TextWatcher {

    @BindView(R.id.account)
    EditText account;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.automatic_login)
    CheckBox automaticLogin;
    @BindView(R.id.remember_account)
    CheckBox rememberAccount;
    @BindView(R.id.company)
    Button company;
    @BindView(R.id.personal)
    Button personal;
    @BindView(R.id.layout_company_tax_id)
    RelativeLayout layoutCompanyTaxId;
    @BindView(R.id.company_tax_id)
    EditText companyTextId;

    //--------------------------------------------------
    private SecurityUtil securityUtil;
    private LoadingDialogManager loadingDialogManager;
    private NetworkChecker networkChecker;
    private SharedPreferenceUtil sp;
    private CheckDataUtil checkDataUtil;
    private LoginConnect connect;
    private PageCenter pageCenter;
    private ImageView toolbarImage;
    private LinearLayout toolbarChange;
    private boolean lock = true, isLogin = false;
    private String refreshedToken = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.toolbarImage = ((IndexActivity) context).toolbarImage;
            this.toolbarChange = ((IndexActivity) context).toolbarChange;
        }
    }

    //--------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_login);

        securityUtil = new SecurityUtil(activity);
        connect = new LoginConnect(activity);
        networkChecker = NetworkChecker.getInstance(activity);
        sp = new SharedPreferenceUtil(activity);
        pageCenter = new PageCenter(activity);

        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        toolbarChange.setVisibility(View.GONE);

        if (sp.getAccount() != null && sp.getAccount().length() > 0) {
            rememberAccount.setChecked(true);
            companyTextId.setText(sp.getCompanyTaxID());
            account.setText(sp.getAccount());
            lock = false;
            account.setTransformationMethod(new MyPasswordTransformationMethod());
            password.requestFocus();
        }

        if (sp.getPassword() != null && sp.getPassword().length() > 0) {
            automaticLogin.setChecked(true);
            password.setText(sp.getPassword());
        }

        checkDataUtil = new CheckDataUtil();
        toolbarImage.setImageResource(R.drawable.img_toolbar_login);

        setAccount(sp.getCompanyTaxID() != null && sp.getCompanyTaxID().length() > 0 && sp.getSaveAccount());
        account.addTextChangedListener(this);

    }

    //--------------------------------------------------
    @OnClick({R.id.forgot_password, R.id.login, R.id.register, R.id.company, R.id.personal, R.id.experience})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_password:
                if (networkChecker.isNetworkAvailable()) {
                    Intent intent = new Intent();
                    intent.setClass(activity, ForgotPasswordActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.login:
                //登入
                if (checkData() && networkChecker.isNetworkAvailable()) {

                    if (company.isSelected()) {
                        connect.connectLoginCompany(EventCenter.LOGIN, account.getText().toString(), password.getText().toString(), companyTextId
                                .getText().toString());
                    } else {
                        connect.connectLogin(EventCenter.LOGIN, account.getText().toString(), password.getText().toString());
                    }
                }
                break;
            case R.id.register:
                //註冊
                if (networkChecker.isNetworkAvailable()) {
                    replaceFragment(new RegisterFragment(), false);
                }
                break;
            case R.id.personal:
                setAccount(false);
                break;
            case R.id.company:
                setAccount(true);
                break;
            case R.id.experience:
                // 體驗
                replaceFragment(new MapModeFragment(), false);
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.LOGIN) {

            User user = (User) data.get("data");

            saveAccount();
            savePassword();
            setCompanyTaxId(user);

            user.setLoginId(account.getText().toString().toUpperCase());
            user.setPassword(password.getText().toString());
            sp.setUserData(user);

            EventCenter.getInstance().sendSetToolbarChange();
            pageCenter.setUser(user);

            isLogin = true;

            sendHockeyAppEvent("登入成功", sp.getUUID(), sp.getAcctId());
            goToRegisterDevice();

        } else if ((int) data.get("type") == EventCenter.REGISTER_DEVICE) {
            sendHockeyAppEvent("完成註冊裝置", sp.getUUID(), sp.getAcctId());
            if (isLogin) {
                goToSecurityService();
            }
        } else if ((int) data.get("type") == EventCenter.SERVICE_ON_CREATE) {
            sendHockeyAppEvent("已開啟資安Service(1)", sp.getUUID(), sp.getAcctId());
            if (isLogin) {
                securityUtil.initializeCA(String.valueOf(sp.getUserData().getAcctId()));
            }

        } else if ((int) data.get("type") == EventCenter.INIT_CA_FINISH) {
            sendHockeyAppEvent("已初始化CA(2)", sp.getUUID(), sp.getAcctId());

            if (isLogin) {

                if (!sp.getRegisterSecurityServer()) {
                    sendHockeyAppEvent("資安註冊使用者(3)", sp.getUUID(), sp.getAcctId());
                    loadingDialogManager = new LoadingDialogManager(activity);
                    loadingDialogManager.show();
                    securityUtil.sendRegisterSecurity();
                } else {
                    sendHockeyAppEvent("已註冊資安註冊使用者(4)", sp.getUUID(), sp.getAcctId());
                    isLogin = false;
                    replaceFragment(pageCenter.getGoToFragment(), false);
                }
            }

        } else if ((int) data.get("type") == EventCenter.REGISTER_SECURITY_FINISH) {

            boolean isSuccess = (boolean) data.get("data");
            sendHockeyAppEvent("完成資安註冊使用者(4)", sp.getUUID(), sp.getAcctId());
            if (isLogin) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        if (loadingDialogManager != null) {
                            loadingDialogManager.dismiss();
                            isLogin = false;
                            replaceFragment(pageCenter.getGoToFragment(), false);
                        }
                    }
                });
            }
        }

    }
    //--------------------------------------------------

    /**
     * 存入帳號
     */
    private void saveAccount() {
        if (rememberAccount.isChecked() || automaticLogin.isChecked()) {
            sp.setSaveAccount(true);
            sp.setAccount(account.getText().toString().toUpperCase());
        } else {
            sp.setSaveAccount(false);
            sp.setAccount("");
        }
    }

    //--------------------------------------------------

    /**
     * 存入密碼
     */
    private void savePassword() {
        if (automaticLogin.isChecked()) {
            sp.setPassword(password.getText().toString());
        }
    }

    //--------------------------------------------------

    /**
     * 存入企業的統編
     *
     * @param user
     */
    private void setCompanyTaxId(User user) {
        if (user.getCompanyAccount() != null) {
            sp.setCompanyTaxID(user.getCompanyAccount().getCompanyId());
        }
    }

    //--------------------------------------------------

    /**
     * 檢查資料
     *
     * @return
     */
    private boolean checkData() {

        boolean isAccount = false, isPassword = false, isCompany = true;

        if (checkDataUtil.checkAccount(account.getText().toString())) {
            isAccount = true;
        } else {
            account.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkPassword(6, 16, password.getText().toString())) {
            isPassword = true;
        } else {
            password.setError(getString(R.string.format_error));
        }

        if (personal.isSelected()) {
            return isAccount && isPassword;
        } else {

            if (companyTextId.getText().length() == 0) {
                isCompany = false;
            }

            return isAccount && isPassword && isCompany;
        }

    }

    //--------------------------------------------------

    /**
     * 設定企業帳戶或個人帳戶
     *
     * @param isCompany
     */
    private void setAccount(boolean isCompany) {

        personal.setSelected(false);
        company.setSelected(false);

        if (isCompany) {
            company.setSelected(true);
            layoutCompanyTaxId.setVisibility(View.VISIBLE);
        } else {
            personal.setSelected(true);
            layoutCompanyTaxId.setVisibility(View.GONE);
        }

    }

    //--------------------------------------------------
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    //--------------------------------------------------
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 10) {
            account.clearFocus();
            password.requestFocus();
        }
    }

    //--------------------------------------------------
    @Override
    public void afterTextChanged(Editable s) {

        if (!lock) {
            account.setTransformationMethod(HideReturnsTransformationMethod.getInstance());//顯示密碼
            account.setSelection(account.getText().toString().length());
            lock = true;
        }
    }

    // ----------------------------------------------------

    /**
     * 前往註冊推播裝置
     */
    private void goToRegisterDevice() {

        if (refreshedToken != null && refreshedToken.length() > 0) {
            sendHockeyAppEvent("註冊推播裝置", sp.getUUID(), sp.getAcctId());
            new UserConnect(activity).registerDevice(refreshedToken);
        } else {
            goToSecurityService();
        }
    }

    //--------------------------------------------------

    /**
     * 前往開啟資安的service
     */
    private void goToSecurityService() {

        if (sp.getUserData().getLoadMemberDataResult() != null) {
            isLogin = false;
            replaceFragment(pageCenter.getGoToFragment(), false);
        } else {
            sendHockeyAppEvent("開啟資安service", sp.getUUID(), sp.getAcctId());
            securityUtil.starService();
        }
    }

    //--------------------------------------------------
    public class MyPasswordTransformationMethod extends PasswordTransformationMethod {

        @Override
        public CharSequence getTransformation(CharSequence source, View view) {

            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {

            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source;
            }

            public char charAt(int index) {

                if (index >= 1 && index < 7) {
                    //your own condition, when you want to hide characters.
                    return '\u2022'; // change this to bullets you want like '*' or '.'
                }

                return mSource.charAt(index);
            }

            public int length() {
                return mSource.length();
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    //--------------------------------------------------
}
