package tw.com.car_plus.carshare.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.operating.OperatingActivity;

/**
 * Created by neil on 2017/11/30.
 */

public class SignInCheckDialog extends Dialog implements View.OnClickListener {

    Button signIn;
    Button operating;
    // ----------------------------------------------------
    private Context context;
    private OnSignInClickListener listener;

    // ----------------------------------------------------
    public SignInCheckDialog(@NonNull Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_sign_in_check);
        this.context = context;

        signIn = (Button) findViewById(R.id.sign_in);
        operating = (Button) findViewById(R.id.operating);
        signIn.setOnClickListener(this);
        operating.setOnClickListener(this);
    }

    // ----------------------------------------------------
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.sign_in:
                if (listener != null) {
                    listener.onSignIn();
                }
                break;
            case R.id.operating:
                context.startActivity(new Intent(context, OperatingActivity.class));
                break;
        }

        dismiss();
    }

    // ----------------------------------------------------
    public interface OnSignInClickListener {
        void onSignIn();
    }

    // ----------------------------------------------------
    public void setOnSignInClickListener(OnSignInClickListener listener) {
        this.listener = listener;
    }

    // ----------------------------------------------------
}
