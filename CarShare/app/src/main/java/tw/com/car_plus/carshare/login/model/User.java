package tw.com.car_plus.carshare.login.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neil on 2017/3/9.
 */

public class User implements Parcelable {

    /**
     * Now : 2017-05-08T07:38:55.6705086Z
     * AcctId : 194455
     * Name : 陳煒真
     * VerifyStatus : 4
     * OperationStatus : 3
     * RegisterStatus : 4
     * PreOrder : {"BookingOnUtc":"2017-05-08T02:47:34.223","PreorderExtensionNum":0,"PreOrderExpireOnUtc":"2017-05-08T03:27:34.257",
     * "ParkinglotName":"小巨蛋","ParkinglotLongitude":"121.54953069999999","ParkinglotLatitude":"25.0519742","CarBrand":"納智捷(Luxgen)(國產) ",
     * "OrderId":5044,"CarSeries":"S3","CarNo":"RBL-1273","CarPic":"/FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3"}
     * GetCar : {"GetCarOnUtc":"2017-05-08T07:42:13.62","GetCarExpireOnUtc":"2017-05-08T07:52:13.62","CarBrand":"納智捷(Luxgen)(國產) ","CarId":23,
     * "IsExtenPreorder":false,"OrderId":5044,"CarSeries":"S3","CarNo":"RBL-1273",
     * "CarPic":"/FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3"}
     * DriveCar : {"CarId":23,"RentStartOnUtc":"2017-05-08T10:07:46.737","OrderId":5044,"CarSeries":"S3","CarNo":"RBL-1273",
     * "CarPic":"/FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3"}
     * ReturnCar : {"CarId":27,"StartReturnCarOnUtc":"2017-05-08T08:10:44.303","ReturnCarExpireOnUtc":"2017-05-08T08:20:44.303","OrderId":5044,
     * "CarSeries":"S3","CarNo":"RBL-1290","CarPic":"/FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3"}
     * LoadMemberDataResult : {"AcctId":194455,"LoginID":"F254863846","AcctName":"陳煒真","Birth":"1992/07/25","MainTelAreaCode":"","MainTel":"",
     * "MainTelExt":"","Maincell":"0988725047","HHRCityID":3,"HHRAreaID":43,"HHRAddr":"永康街","CityID":3,"AreaID":45,"Addr":"長安街",
     * "Jurisdiction":"1234567890123","Email":"s22838524@gmail.com","EpaperSts":"N","SmsSts":"N","Bonus":53}
     * Status : false
     * Errors : [{"ErrorCode":-100,"ErrorMsg":""}]
     */

    @SerializedName("Now")
    private String Now;
    @SerializedName("CompanyAccount")
    private CompanyAccountBean CompanyAccount;
    @SerializedName("AcctId")
    private String AcctId;
    private String LoginId;
    private String password;
    @SerializedName("Name")
    private String Name;
    @SerializedName("VerifyStatus")
    private int VerifyStatus;
    @SerializedName("OperationStatus")
    private int OperationStatus;
    @SerializedName("RegisterStatus")
    private int RegisterStatus;
    @SerializedName("PreOrder")
    private PreOrderBean PreOrder;
    @SerializedName("GetCar")
    private GetCarBean GetCar;
    @SerializedName("DriveCar")
    private DriveCarBean DriveCar;
    @SerializedName("ReturnCar")
    private ReturnCarBean ReturnCar;
    private LoadMemberDataResultBean LoadMemberDataResult;
    @SerializedName("Status")
    private boolean Status;
    @SerializedName("IsOtherRent")
    private boolean IsOtherRent;
    @SerializedName("IsNoCreditCard")
    private boolean IsNoCreditCard;

    @SerializedName("Errors")
    private List<ErrorsBean> Errors;

    public String getNow() {
        return Now;
    }

    public void setNow(String Now) {
        this.Now = Now;
    }

    public CompanyAccountBean getCompanyAccount() {
        return CompanyAccount;
    }

    public void setCompanyAccount(CompanyAccountBean companyAccount) {
        CompanyAccount = companyAccount;
    }

    public String getAcctId() {
        return AcctId;
    }

    public void setAcctId(String AcctId) {
        this.AcctId = AcctId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getVerifyStatus() {
        return VerifyStatus;
    }

    public void setVerifyStatus(int VerifyStatus) {
        this.VerifyStatus = VerifyStatus;
    }

    public int getOperationStatus() {
        return OperationStatus;
    }

    public void setOperationStatus(int OperationStatus) {
        this.OperationStatus = OperationStatus;
    }

    public int getRegisterStatus() {
        return RegisterStatus;
    }

    public void setRegisterStatus(int RegisterStatus) {
        this.RegisterStatus = RegisterStatus;
    }

    public PreOrderBean getPreOrder() {
        return PreOrder;
    }

    public void setPreOrder(PreOrderBean PreOrder) {
        this.PreOrder = PreOrder;
    }

    public GetCarBean getGetCar() {
        return GetCar;
    }

    public void setGetCar(GetCarBean GetCar) {
        this.GetCar = GetCar;
    }

    public DriveCarBean getDriveCar() {
        return DriveCar;
    }

    public void setDriveCar(DriveCarBean DriveCar) {
        this.DriveCar = DriveCar;
    }

    public ReturnCarBean getReturnCar() {
        return ReturnCar;
    }

    public void setReturnCar(ReturnCarBean ReturnCar) {
        this.ReturnCar = ReturnCar;
    }

    public LoadMemberDataResultBean getLoadMemberDataResult() {
        return LoadMemberDataResult;
    }

    public void setLoadMemberDataResult(LoadMemberDataResultBean LoadMemberDataResult) {
        this.LoadMemberDataResult = LoadMemberDataResult;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public boolean isOtherRent() {
        return IsOtherRent;
    }

    public void setOtherRent(boolean otherRent) {
        IsOtherRent = otherRent;
    }

    public boolean isNoCreditCard() {
        return IsNoCreditCard;
    }

    public void setNoCreditCard(boolean noCreditCard) {
        IsNoCreditCard = noCreditCard;
    }

    public List<ErrorsBean> getErrors() {
        return Errors;
    }

    public void setErrors(List<ErrorsBean> Errors) {
        this.Errors = Errors;
    }

    public static class PreOrderBean implements Parcelable {
        /**
         * BookingOnUtc : 2017-05-08T02:47:34.223
         * PreorderExtensionNum : 0
         * ReservationStartOnUtc : 2017-03-16T18:37:00Z
         * PreOrderExpireOnUtc : 2017-05-08T03:27:34.257
         * ParkinglotName : 小巨蛋
         * ParkinglotLongitude : 121.54953069999999
         * ParkinglotLatitude : 25.0519742
         * CarBrand : 納智捷(Luxgen)(國產)
         * CarEnergyType ：  1
         * OrderId : 5044
         * CarSeries : S3
         * CarNo : RBL-1273
         * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
         */

        private String BookingOnUtc;
        private int PreorderExtensionNum;
        private String ReservationStartOnUtc;
        private String PreOrderExpireOnUtc;
        private String ParkinglotName;
        private String ParkinglotLongitude;
        private String ParkinglotLatitude;
        private String CarBrand;
        private int OrderId;
        private String CarSeries;
        private String CarNo;
        private String CarPic;
        private int CarEnergyType;
        private int CarVigType;

        public String getBookingOnUtc() {
            return BookingOnUtc;
        }

        public void setBookingOnUtc(String BookingOnUtc) {
            this.BookingOnUtc = BookingOnUtc;
        }

        public int getPreorderExtensionNum() {
            return PreorderExtensionNum;
        }

        public void setPreorderExtensionNum(int PreorderExtensionNum) {
            this.PreorderExtensionNum = PreorderExtensionNum;
        }

        public String getReservationStartOnUtc() {
            return ReservationStartOnUtc;
        }

        public void setReservationStartOnUtc(String reservationStartOnUtc) {
            ReservationStartOnUtc = reservationStartOnUtc;
        }

        public String getPreOrderExpireOnUtc() {
            return PreOrderExpireOnUtc;
        }

        public void setPreOrderExpireOnUtc(String PreOrderExpireOnUtc) {
            this.PreOrderExpireOnUtc = PreOrderExpireOnUtc;
        }

        public String getParkinglotName() {
            return ParkinglotName;
        }

        public void setParkinglotName(String ParkinglotName) {
            this.ParkinglotName = ParkinglotName;
        }

        public String getParkinglotLongitude() {
            return ParkinglotLongitude;
        }

        public void setParkinglotLongitude(String ParkinglotLongitude) {
            this.ParkinglotLongitude = ParkinglotLongitude;
        }

        public String getParkinglotLatitude() {
            return ParkinglotLatitude;
        }

        public void setParkinglotLatitude(String ParkinglotLatitude) {
            this.ParkinglotLatitude = ParkinglotLatitude;
        }

        public String getCarBrand() {
            return CarBrand;
        }

        public void setCarBrand(String CarBrand) {
            this.CarBrand = CarBrand;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getCarPic() {
            return CarPic;
        }

        public void setCarPic(String CarPic) {
            this.CarPic = CarPic;
        }

        public int getCarEnergyType() {
            return CarEnergyType;
        }

        public void setCarEnergyType(int carEnergyType) {
            CarEnergyType = carEnergyType;
        }

        public int getCarVigType() {
            return CarVigType;
        }

        public void setCarVigType(int carVigType) {
            CarVigType = carVigType;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.BookingOnUtc);
            dest.writeInt(this.PreorderExtensionNum);
            dest.writeString(this.ReservationStartOnUtc);
            dest.writeString(this.PreOrderExpireOnUtc);
            dest.writeString(this.ParkinglotName);
            dest.writeString(this.ParkinglotLongitude);
            dest.writeString(this.ParkinglotLatitude);
            dest.writeString(this.CarBrand);
            dest.writeInt(this.OrderId);
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarNo);
            dest.writeString(this.CarPic);
            dest.writeInt(this.CarEnergyType);
            dest.writeInt(this.CarVigType);
        }

        public PreOrderBean() {
        }

        protected PreOrderBean(Parcel in) {
            this.BookingOnUtc = in.readString();
            this.PreorderExtensionNum = in.readInt();
            this.ReservationStartOnUtc = in.readString();
            this.PreOrderExpireOnUtc = in.readString();
            this.ParkinglotName = in.readString();
            this.ParkinglotLongitude = in.readString();
            this.ParkinglotLatitude = in.readString();
            this.CarBrand = in.readString();
            this.OrderId = in.readInt();
            this.CarSeries = in.readString();
            this.CarNo = in.readString();
            this.CarPic = in.readString();
            this.CarEnergyType = in.readInt();
            this.CarVigType = in.readInt();
        }

        public static final Parcelable.Creator<PreOrderBean> CREATOR = new Parcelable.Creator<PreOrderBean>() {
            @Override
            public PreOrderBean createFromParcel(Parcel source) {
                return new PreOrderBean(source);
            }

            @Override
            public PreOrderBean[] newArray(int size) {
                return new PreOrderBean[size];
            }
        };
    }


    public static class CompanyAccountBean implements Parcelable {

        /**
         * CompanyId : "CarPlus"
         * CompanyName : "格上租車"
         */

        private String CompanyId;
        private String CompanyName;

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.CompanyId);
            dest.writeString(this.CompanyName);
        }

        public CompanyAccountBean() {
        }

        protected CompanyAccountBean(Parcel in) {
            this.CompanyId = in.readString();
            this.CompanyName = in.readString();
        }

        public static final Parcelable.Creator<CompanyAccountBean> CREATOR = new Parcelable.Creator<CompanyAccountBean>() {
            @Override
            public CompanyAccountBean createFromParcel(Parcel source) {
                return new CompanyAccountBean(source);
            }

            @Override
            public CompanyAccountBean[] newArray(int size) {
                return new CompanyAccountBean[size];
            }
        };
    }

    public static class GetCarBean implements Parcelable {
        /**
         * GetCarOnUtc : 2017-05-08T07:42:13.62
         * GetCarExpireOnUtc : 2017-05-08T07:52:13.62
         * CarBrand : 納智捷(Luxgen)(國產)
         * CarId : 23
         * IsExtenPreorder : false
         * OrderId : 5044
         * CarSeries : S3
         * CarNo : RBL-1273
         * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
         * CarEnergyType ：  1
         */

        private String GetCarOnUtc;
        private String GetCarExpireOnUtc;
        private String CarBrand;
        private int CarId;
        private boolean IsExtenPreorder;
        private int OrderId;
        private String CarSeries;
        private String CarNo;
        private String CarPic;
        private int CarEnergyType;
        private int CarVigType;

        public String getGetCarOnUtc() {
            return GetCarOnUtc;
        }

        public void setGetCarOnUtc(String GetCarOnUtc) {
            this.GetCarOnUtc = GetCarOnUtc;
        }

        public String getGetCarExpireOnUtc() {
            return GetCarExpireOnUtc;
        }

        public void setGetCarExpireOnUtc(String GetCarExpireOnUtc) {
            this.GetCarExpireOnUtc = GetCarExpireOnUtc;
        }

        public String getCarBrand() {
            return CarBrand;
        }

        public void setCarBrand(String CarBrand) {
            this.CarBrand = CarBrand;
        }

        public int getCarId() {
            return CarId;
        }

        public void setCarId(int CarId) {
            this.CarId = CarId;
        }

        public boolean isIsExtenPreorder() {
            return IsExtenPreorder;
        }

        public void setIsExtenPreorder(boolean IsExtenPreorder) {
            this.IsExtenPreorder = IsExtenPreorder;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getCarPic() {
            return CarPic;
        }

        public void setCarPic(String CarPic) {
            this.CarPic = CarPic;
        }

        public int getCarEnergyType() {
            return CarEnergyType;
        }

        public void setCarEnergyType(int carEnergyType) {
            CarEnergyType = carEnergyType;
        }

        public int getCarVigType() {
            return CarVigType;
        }

        public void setCarVigType(int carVigType) {
            CarVigType = carVigType;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.GetCarOnUtc);
            dest.writeString(this.GetCarExpireOnUtc);
            dest.writeString(this.CarBrand);
            dest.writeInt(this.CarId);
            dest.writeByte(this.IsExtenPreorder ? (byte) 1 : (byte) 0);
            dest.writeInt(this.OrderId);
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarNo);
            dest.writeString(this.CarPic);
            dest.writeInt(this.CarEnergyType);
            dest.writeInt(this.CarVigType);
        }

        public GetCarBean() {
        }

        protected GetCarBean(Parcel in) {
            this.GetCarOnUtc = in.readString();
            this.GetCarExpireOnUtc = in.readString();
            this.CarBrand = in.readString();
            this.CarId = in.readInt();
            this.IsExtenPreorder = in.readByte() != 0;
            this.OrderId = in.readInt();
            this.CarSeries = in.readString();
            this.CarNo = in.readString();
            this.CarPic = in.readString();
            this.CarEnergyType = in.readInt();
            this.CarVigType = in.readInt();
        }

        public static final Parcelable.Creator<GetCarBean> CREATOR = new Parcelable.Creator<GetCarBean>() {
            @Override
            public GetCarBean createFromParcel(Parcel source) {
                return new GetCarBean(source);
            }

            @Override
            public GetCarBean[] newArray(int size) {
                return new GetCarBean[size];
            }
        };
    }

    public static class DriveCarBean implements Parcelable {
        /**
         * CarId : 23
         * RentStartOnUtc : 2017-05-08T10:07:46.737
         * OrderId : 5044
         * CarSeries : S3
         * CarNo : RBL-1273
         * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
         * CarEnergyType : 電動車
         */

        private int CarId;
        private String RentStartOnUtc;
        private int OrderId;
        private String CarSeries;
        private String CarNo;
        private String CarPic;
        private int CarEnergyType;
        private int CarVigType;
        private String VigId;
        private String MacAddr;

        public int getCarId() {
            return CarId;
        }

        public void setCarId(int CarId) {
            this.CarId = CarId;
        }

        public String getRentStartOnUtc() {
            return RentStartOnUtc;
        }

        public void setRentStartOnUtc(String RentStartOnUtc) {
            this.RentStartOnUtc = RentStartOnUtc;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getCarPic() {
            return CarPic;
        }

        public void setCarPic(String CarPic) {
            this.CarPic = CarPic;
        }

        public int getCarEnergyType() {
            return CarEnergyType;
        }

        public void setCarEnergyType(int carEnergyType) {
            CarEnergyType = carEnergyType;
        }

        public int getCarVigType() {
            return CarVigType;
        }

        public void setCarVigType(int carVigType) {
            CarVigType = carVigType;
        }

        public String getVigId() {
            return VigId;
        }

        public void setVigId(String vigId) {
            VigId = vigId;
        }

        public String getMacAddr() {
            return MacAddr;
        }

        public void setMacAddr(String macAddr) {
            MacAddr = macAddr;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.CarId);
            dest.writeString(this.RentStartOnUtc);
            dest.writeInt(this.OrderId);
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarNo);
            dest.writeString(this.CarPic);
            dest.writeInt(this.CarEnergyType);
            dest.writeInt(this.CarVigType);
            dest.writeString(this.VigId);
            dest.writeString(this.MacAddr);
        }

        public DriveCarBean() {
        }

        protected DriveCarBean(Parcel in) {
            this.CarId = in.readInt();
            this.RentStartOnUtc = in.readString();
            this.OrderId = in.readInt();
            this.CarSeries = in.readString();
            this.CarNo = in.readString();
            this.CarPic = in.readString();
            this.CarEnergyType = in.readInt();
            this.CarVigType = in.readInt();
            this.VigId = in.readString();
            this.MacAddr = in.readString();
        }

        public static final Parcelable.Creator<DriveCarBean> CREATOR = new Parcelable.Creator<DriveCarBean>() {
            @Override
            public DriveCarBean createFromParcel(Parcel source) {
                return new DriveCarBean(source);
            }

            @Override
            public DriveCarBean[] newArray(int size) {
                return new DriveCarBean[size];
            }
        };
    }

    public static class ReturnCarBean implements Parcelable {
        /**
         * CarId : 27
         * StartReturnCarOnUtc : 2017-05-08T08:10:44.303
         * ReturnCarExpireOnUtc : 2017-05-08T08:20:44.303
         * OrderId : 5044
         * CarSeries : S3
         * CarNo : RBL-1290
         * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
         * CarEnergyType : 電動車
         */

        private int CarId;
        private String StartReturnCarOnUtc;
        private String ReturnCarExpireOnUtc;
        private int OrderId;
        private String CarSeries;
        private String CarNo;
        private String CarPic;
        private int CarEnergyType;
        private int CarVigType;
        private String VigId;
        private String MacAddr;

        public int getCarId() {
            return CarId;
        }

        public void setCarId(int CarId) {
            this.CarId = CarId;
        }

        public String getStartReturnCarOnUtc() {
            return StartReturnCarOnUtc;
        }

        public void setStartReturnCarOnUtc(String StartReturnCarOnUtc) {
            this.StartReturnCarOnUtc = StartReturnCarOnUtc;
        }

        public String getReturnCarExpireOnUtc() {
            return ReturnCarExpireOnUtc;
        }

        public void setReturnCarExpireOnUtc(String ReturnCarExpireOnUtc) {
            this.ReturnCarExpireOnUtc = ReturnCarExpireOnUtc;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getCarPic() {
            return CarPic;
        }

        public void setCarPic(String CarPic) {
            this.CarPic = CarPic;
        }

        public int getCarEnergyType() {
            return CarEnergyType;
        }

        public void setCarEnergyType(int carEnergyType) {
            CarEnergyType = carEnergyType;
        }

        public int getCarVigType() {
            return CarVigType;
        }

        public void setCarVigType(int carVigType) {
            CarVigType = carVigType;
        }

        public String getVigId() {
            return VigId;
        }

        public void setVigId(String vigId) {
            VigId = vigId;
        }

        public String getMacAddr() {
            return MacAddr;
        }

        public void setMacAddr(String macAddr) {
            MacAddr = macAddr;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.CarId);
            dest.writeString(this.StartReturnCarOnUtc);
            dest.writeString(this.ReturnCarExpireOnUtc);
            dest.writeInt(this.OrderId);
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarNo);
            dest.writeString(this.CarPic);
            dest.writeInt(this.CarEnergyType);
            dest.writeInt(this.CarVigType);
            dest.writeString(this.VigId);
            dest.writeString(this.MacAddr);
        }

        public ReturnCarBean() {
        }

        protected ReturnCarBean(Parcel in) {
            this.CarId = in.readInt();
            this.StartReturnCarOnUtc = in.readString();
            this.ReturnCarExpireOnUtc = in.readString();
            this.OrderId = in.readInt();
            this.CarSeries = in.readString();
            this.CarNo = in.readString();
            this.CarPic = in.readString();
            this.CarEnergyType = in.readInt();
            this.CarVigType = in.readInt();
            this.VigId = in.readString();
            this.MacAddr = in.readString();
        }

        public static final Parcelable.Creator<ReturnCarBean> CREATOR = new Parcelable.Creator<ReturnCarBean>() {
            @Override
            public ReturnCarBean createFromParcel(Parcel source) {
                return new ReturnCarBean(source);
            }

            @Override
            public ReturnCarBean[] newArray(int size) {
                return new ReturnCarBean[size];
            }
        };
    }

    public static class LoadMemberDataResultBean implements Parcelable {
        /**
         * AcctId : 194455
         * LoginID : F254863846
         * AcctName : 陳煒真
         * Birth : 1992/07/25
         * MainTelAreaCode :
         * MainTel :
         * MainTelExt :
         * Maincell : 0988725047
         * HHRCityID : 3
         * HHRAreaID : 43
         * HHRAddr : 永康街
         * CityID : 3
         * AreaID : 45
         * Addr : 長安街
         * Jurisdiction : 1234567890123
         * Email : s22838524@gmail.com
         * EpaperSts : N
         * SmsSts : N
         * Bonus : 53
         */

        private int AcctId;
        private String LoginID;
        private String AcctName;
        private String Birth;
        private String MainTelAreaCode;
        private String MainTel;
        private String MainTelExt;
        private String Maincell;
        private int HHRCityID;
        private int HHRAreaID;
        private String HHRAddr;
        private int CityID;
        private int AreaID;
        private String Addr;
        private String Jurisdiction;
        private String Email;
        private String EpaperSts;
        private String SmsSts;
        private int Bonus;

        public int getAcctId() {
            return AcctId;
        }

        public void setAcctId(int AcctId) {
            this.AcctId = AcctId;
        }

        public String getLoginID() {
            return LoginID;
        }

        public void setLoginID(String LoginID) {
            this.LoginID = LoginID;
        }

        public String getAcctName() {
            return AcctName;
        }

        public void setAcctName(String AcctName) {
            this.AcctName = AcctName;
        }

        public String getBirth() {
            return Birth;
        }

        public void setBirth(String Birth) {
            this.Birth = Birth;
        }

        public String getMainTelAreaCode() {
            return MainTelAreaCode;
        }

        public void setMainTelAreaCode(String MainTelAreaCode) {
            this.MainTelAreaCode = MainTelAreaCode;
        }

        public String getMainTel() {
            return MainTel;
        }

        public void setMainTel(String MainTel) {
            this.MainTel = MainTel;
        }

        public String getMainTelExt() {
            return MainTelExt;
        }

        public void setMainTelExt(String MainTelExt) {
            this.MainTelExt = MainTelExt;
        }

        public String getMaincell() {
            return Maincell;
        }

        public void setMaincell(String Maincell) {
            this.Maincell = Maincell;
        }

        public int getHHRCityID() {
            return HHRCityID;
        }

        public void setHHRCityID(int HHRCityID) {
            this.HHRCityID = HHRCityID;
        }

        public int getHHRAreaID() {
            return HHRAreaID;
        }

        public void setHHRAreaID(int HHRAreaID) {
            this.HHRAreaID = HHRAreaID;
        }

        public String getHHRAddr() {
            return HHRAddr;
        }

        public void setHHRAddr(String HHRAddr) {
            this.HHRAddr = HHRAddr;
        }

        public int getCityID() {
            return CityID;
        }

        public void setCityID(int CityID) {
            this.CityID = CityID;
        }

        public int getAreaID() {
            return AreaID;
        }

        public void setAreaID(int AreaID) {
            this.AreaID = AreaID;
        }

        public String getAddr() {
            return Addr;
        }

        public void setAddr(String Addr) {
            this.Addr = Addr;
        }

        public String getJurisdiction() {
            return Jurisdiction;
        }

        public void setJurisdiction(String Jurisdiction) {
            this.Jurisdiction = Jurisdiction;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getEpaperSts() {
            return EpaperSts;
        }

        public void setEpaperSts(String EpaperSts) {
            this.EpaperSts = EpaperSts;
        }

        public String getSmsSts() {
            return SmsSts;
        }

        public void setSmsSts(String SmsSts) {
            this.SmsSts = SmsSts;
        }

        public int getBonus() {
            return Bonus;
        }

        public void setBonus(int Bonus) {
            this.Bonus = Bonus;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.AcctId);
            dest.writeString(this.LoginID);
            dest.writeString(this.AcctName);
            dest.writeString(this.Birth);
            dest.writeString(this.MainTelAreaCode);
            dest.writeString(this.MainTel);
            dest.writeString(this.MainTelExt);
            dest.writeString(this.Maincell);
            dest.writeInt(this.HHRCityID);
            dest.writeInt(this.HHRAreaID);
            dest.writeString(this.HHRAddr);
            dest.writeInt(this.CityID);
            dest.writeInt(this.AreaID);
            dest.writeString(this.Addr);
            dest.writeString(this.Jurisdiction);
            dest.writeString(this.Email);
            dest.writeString(this.EpaperSts);
            dest.writeString(this.SmsSts);
            dest.writeInt(this.Bonus);
        }

        public LoadMemberDataResultBean() {
        }

        protected LoadMemberDataResultBean(Parcel in) {
            this.AcctId = in.readInt();
            this.LoginID = in.readString();
            this.AcctName = in.readString();
            this.Birth = in.readString();
            this.MainTelAreaCode = in.readString();
            this.MainTel = in.readString();
            this.MainTelExt = in.readString();
            this.Maincell = in.readString();
            this.HHRCityID = in.readInt();
            this.HHRAreaID = in.readInt();
            this.HHRAddr = in.readString();
            this.CityID = in.readInt();
            this.AreaID = in.readInt();
            this.Addr = in.readString();
            this.Jurisdiction = in.readString();
            this.Email = in.readString();
            this.EpaperSts = in.readString();
            this.SmsSts = in.readString();
            this.Bonus = in.readInt();
        }

        public static final Parcelable.Creator<LoadMemberDataResultBean> CREATOR = new Parcelable.Creator<LoadMemberDataResultBean>() {
            @Override
            public LoadMemberDataResultBean createFromParcel(Parcel source) {
                return new LoadMemberDataResultBean(source);
            }

            @Override
            public LoadMemberDataResultBean[] newArray(int size) {
                return new LoadMemberDataResultBean[size];
            }
        };
    }

    public static class ErrorsBean implements Parcelable {
        /**
         * ErrorCode : -100
         * ErrorMsg :
         */

        private int ErrorCode;
        private String ErrorMsg;

        public int getErrorCode() {
            return ErrorCode;
        }

        public void setErrorCode(int ErrorCode) {
            this.ErrorCode = ErrorCode;
        }

        public String getErrorMsg() {
            return ErrorMsg;
        }

        public void setErrorMsg(String ErrorMsg) {
            this.ErrorMsg = ErrorMsg;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.ErrorCode);
            dest.writeString(this.ErrorMsg);
        }

        public ErrorsBean() {
        }

        protected ErrorsBean(Parcel in) {
            this.ErrorCode = in.readInt();
            this.ErrorMsg = in.readString();
        }

        public static final Parcelable.Creator<ErrorsBean> CREATOR = new Parcelable.Creator<ErrorsBean>() {
            @Override
            public ErrorsBean createFromParcel(Parcel source) {
                return new ErrorsBean(source);
            }

            @Override
            public ErrorsBean[] newArray(int size) {
                return new ErrorsBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Now);
        dest.writeParcelable(this.CompanyAccount, flags);
        dest.writeString(this.AcctId);
        dest.writeString(this.LoginId);
        dest.writeString(this.password);
        dest.writeString(this.Name);
        dest.writeInt(this.VerifyStatus);
        dest.writeInt(this.OperationStatus);
        dest.writeInt(this.RegisterStatus);
        dest.writeParcelable(this.PreOrder, flags);
        dest.writeParcelable(this.GetCar, flags);
        dest.writeParcelable(this.DriveCar, flags);
        dest.writeParcelable(this.ReturnCar, flags);
        dest.writeParcelable(this.LoadMemberDataResult, flags);
        dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsOtherRent ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsNoCreditCard ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.Errors);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.Now = in.readString();
        this.CompanyAccount = in.readParcelable(CompanyAccountBean.class.getClassLoader());
        this.AcctId = in.readString();
        this.LoginId = in.readString();
        this.password = in.readString();
        this.Name = in.readString();
        this.VerifyStatus = in.readInt();
        this.OperationStatus = in.readInt();
        this.RegisterStatus = in.readInt();
        this.PreOrder = in.readParcelable(PreOrderBean.class.getClassLoader());
        this.GetCar = in.readParcelable(GetCarBean.class.getClassLoader());
        this.DriveCar = in.readParcelable(DriveCarBean.class.getClassLoader());
        this.ReturnCar = in.readParcelable(ReturnCarBean.class.getClassLoader());
        this.LoadMemberDataResult = in.readParcelable(LoadMemberDataResultBean.class.getClassLoader());
        this.Status = in.readByte() != 0;
        this.IsOtherRent = in.readByte() != 0;
        this.IsNoCreditCard = in.readByte() != 0;
        this.Errors = in.createTypedArrayList(ErrorsBean.CREATOR);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
