package tw.com.car_plus.carshare.navigation;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.checkversion.CheckVersionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.login.LoginFragment;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.navigation.adapter.NavigationAdapter;
import tw.com.car_plus.carshare.navigation.model.NavigationBar;
import tw.com.car_plus.carshare.news.NewsActivity;
import tw.com.car_plus.carshare.operating.OperatingActivity;
import tw.com.car_plus.carshare.preferential.PreferentialActivity;
import tw.com.car_plus.carshare.price.RentPriceActivity;
import tw.com.car_plus.carshare.problems.CommonProblemsActivity;
import tw.com.car_plus.carshare.record.RentRecordActivity;
import tw.com.car_plus.carshare.setting.SettingPasswordActivity;
import tw.com.car_plus.carshare.user.UserInfoActivity;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;

/**
 * Created by neil on 2017/3/8.
 */
@RuntimePermissions
public class NavigationFragment extends CustomBaseFragment implements ExpandableListView.OnGroupClickListener,
        ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupExpandListener {

    @BindView(R.id.listView)
    ExpandableListView listView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.company_name)
    TextView companyName;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.sign_in_layout)
    LinearLayout signInLayout;
    @BindView(R.id.version)
    TextView version;

    // ----------------------------------------------------
    private NavigationAdapter adapter;
    private SharedPreferenceUtil sp;
    private User user;
    private DrawerLayout drawerLayout;
    private List<NavigationBar> navigationBars;
    private int[] icons;

    // ----------------------------------------------------

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.drawerLayout = ((IndexActivity) context).drawerLayout;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_navigation);

        sp = new SharedPreferenceUtil(activity);

        // 設定登入獨立按鈕
        title.setText(getString(R.string.navigation_login));

        version.setText(String.format(getString(R.string.version), new CheckVersionUtil(activity).getVersionName()));

        navigationBars = new ArrayList<>();
        icons = new int[]{
                R.drawable.ic_navigation_03, R.drawable.ic_navigation_04, R.drawable.ic_navigation_07,
                R.drawable.ic_navigation_08, R.drawable.ic_navigation_06};

        initNavigationData();

        adapter = new NavigationAdapter(activity, navigationBars);
        listView.setAdapter(adapter);
        listView.setOnGroupClickListener(this);
        listView.setOnChildClickListener(this);
        listView.setOnGroupExpandListener(this);
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.INDEX || (int) data.get("type") == EventCenter.LOGIN || (int) data.get("type") == EventCenter
                .REGISTER) {

            this.user = (User) data.get("data");

            if (user != null) {
                name.setText(user.getName());
                showCompanyName();
                // 隱藏登入獨立按鈕
                signInLayout.setVisibility(View.GONE);
            } else {
                icon.setImageResource(R.drawable.img_user);
                name.setText(getString(R.string.user));
                // 顯示登入獨立按鈕
                signInLayout.setVisibility(View.VISIBLE);
            }

            setNavigationData();

        } else if ((int) data.get("type") == EventCenter.USER) {
            this.user = (User) data.get("data");
            name.setText(user.getName());

            setNavigationData();
        }

    }

    // ----------------------------------------------------
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        //常見問題
        if (groupPosition == 3) {

            switch (childPosition) {
                case 0:
                    //租車費率
                    openActivity(RentPriceActivity.class);
                    break;
                case 1:
                    //常見問題
                    openActivity(CommonProblemsActivity.class);
                    break;
                case 2:
                    //使用說明
                    openActivity(OperatingActivity.class);
                    break;
                case 3:
                    //定型化契約
                    break;
                case 4:
                    NavigationFragmentPermissionsDispatcher.callPhoneWithCheck(this);
                    //聯絡客服
                    break;
            }

        }
        //會員設定
        else if (groupPosition == 4) {

            switch (childPosition) {
                case 0:
                    //會員資料
                    checkSignIn(UserInfoActivity.class);
                    break;
                case 1:
                    //設定密碼
                    checkSignIn(SettingPasswordActivity.class);
                    break;
                //登出
                case 2:

                    if (user != null) {
                        user = null;

                        cleanData();

                        icon.setImageResource(R.drawable.img_user);
                        name.setText(getString(R.string.user));
                        signInLayout.setVisibility(View.VISIBLE);

                        if (companyName.isShown()) {
                            companyName.setVisibility(View.GONE);
                        }

                        initNavigationData();
                        adapter.setData(navigationBars);

                        replaceFragment(new LoginFragment(), false);
                    }

                    break;
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return false;
    }

    // ----------------------------------------------------
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

        if (groupPosition != 3 && groupPosition != 4) {

            switch (groupPosition) {

                //最新消息
                case 0:
                    openActivity(NewsActivity.class);
                    break;
                //優惠方案
                case 1:
                    openActivity(PreferentialActivity.class);
                    break;
                //租車記錄
                case 2:
                    checkSignIn(RentRecordActivity.class);
                    break;
            }

            drawerLayout.closeDrawer(GravityCompat.START);
        }

        return false;
    }

    // ----------------------------------------------------
    // 關閉群組
    @Override
    public void onGroupExpand(int groupPosition) {

        for (int i = 0, count = listView.getExpandableListAdapter().getGroupCount(); i < count; i++) {
            if (groupPosition != i) {
                listView.collapseGroup(i);
            }
        }
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        NavigationFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.CALL_PHONE})
    void callPhone() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.map_mode_connection_phone)));
        startActivity(intent);
    }

    // ----------------------------------------------------
    //請求授權
    @OnShowRationale({Manifest.permission.CALL_PHONE})
    void showRationaleForLocationPermission(final PermissionRequest request) {
        request.proceed();
    }

    // ----------------------------------------------------
    // location 授權被拒
    @OnPermissionDenied({Manifest.permission.CALL_PHONE})
    void showDeniedForLocationPermission() {
        Toast.makeText(getActivity(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }


    // ----------------------------------------------------

    /**
     * 將Navigation資料重新設定
     */
    private void setNavigationData() {
        initNavigationData();
        adapter.setData(navigationBars);
    }

    // ----------------------------------------------------

    /**
     * 顯示企業帳號的企業名稱
     */
    private void showCompanyName() {

        if (user.getCompanyAccount() != null) {
            icon.setImageResource(R.drawable.img_company);
            companyName.setVisibility(View.VISIBLE);
            companyName.setText(user.getCompanyAccount().getCompanyName());
        } else {
            icon.setImageResource(R.drawable.img_user);
            companyName.setVisibility(View.GONE);
            companyName.setText("");
        }
    }

    // ----------------------------------------------------

    /**
     * 設定Navigation資料
     */
    private void initNavigationData() {

        navigationBars.clear();

        List<String> items = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.navigation)));

        for (int itemCount = 0; itemCount < items.size(); itemCount++) {

            NavigationBar item = new NavigationBar();
            item.setTitle(items.get(itemCount));
            item.setIcon(icons[itemCount]);

            if (itemCount == 3) {
                item.setSubNavigationList(getSubNavigationList(getResources().getStringArray(R.array.faq)));
            } else if (itemCount == 4) {

                List<NavigationBar.SubNavigation> subNavigations = getSubNavigationList(getResources().getStringArray(R.array.user_setting));

                if (user != null) {
                    NavigationBar.SubNavigation subNavigation = new NavigationBar.SubNavigation();
                    subNavigation.setTitle(getString(R.string.logout));
                    subNavigations.add(subNavigation);
                }

                item.setSubNavigationList(subNavigations);
            }

            // TODO: 2017/3/9  尚未設定badge
            navigationBars.add(item);
        }
    }

    // ----------------------------------------------------

    /**
     * 取得子項目資料
     *
     * @param strings
     * @return
     */
    private List<NavigationBar.SubNavigation> getSubNavigationList(String[] strings) {

        List<NavigationBar.SubNavigation> navigations = new ArrayList<>();

        for (int i = 0; i < strings.length; i++) {
            NavigationBar.SubNavigation subNavigation = new NavigationBar.SubNavigation();
            subNavigation.setTitle(strings[i]);
            navigations.add(subNavigation);
        }

        return navigations;

    }

    // ----------------------------------------------------

    /**
     * 確認使用者是否有登入
     */
    private void checkSignIn(Class className) {
        //設定密碼
        if (sp.getAcctId().length() > 0) {
            openActivity(className);
        } else {
            showToast(getString(R.string.setting_must_sign_in));
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示Toast
     */
    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------

    /**
     * 開啟Activity
     */
    private void openActivity(Class className) {

        Intent intent = new Intent();
        intent.setClass(getActivity(), className);
        getActivity().startActivity(intent);
    }

    // ----------------------------------------------------

    /**
     * 清除所有資料
     */
    private void cleanData() {
        sp.setUserData(user);
        sp.cleanUserData();
        UserStatus.resetUserStatus();
    }

    // ----------------------------------------------------
    @OnClick(R.id.sign_in_layout)
    public void onViewClicked() {
        drawerLayout.closeDrawers();
        replaceFragment(new LoginFragment(), false);
    }

    // ----------------------------------------------------
}
