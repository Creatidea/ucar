package tw.com.car_plus.carshare.navigation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.navigation.model.NavigationBar;

/**
 * Created by neil on 2017/3/8.
 */

public class NavigationAdapter extends BaseExpandableListAdapter {

    private final int MAX_RANGE = 99;
    private LayoutInflater inflater;
    private List<NavigationBar> navigationBars;

    public NavigationAdapter(Context context, List<NavigationBar> navigationBars) {
        this.inflater = LayoutInflater.from(context);
        this.navigationBars = navigationBars;
    }

    // ----------------------------------------------------
    @Override
    public int getGroupCount() {
        return navigationBars.size();
    }

    // ----------------------------------------------------
    @Override
    public int getChildrenCount(int groupPosition) {

        if (navigationBars.get(groupPosition).getSubNavigationList() != null) {
            return navigationBars.get(groupPosition).getSubNavigationList().size();
        }

        return 0;
    }

    // ----------------------------------------------------
    @Override
    public Object getGroup(int groupPosition) {
        return navigationBars.get(groupPosition);
    }

    // ----------------------------------------------------
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return navigationBars.get(groupPosition).getSubNavigationList().get(childPosition);
    }

    // ----------------------------------------------------
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public boolean hasStableIds() {
        return false;
    }

    // ----------------------------------------------------
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ItemViewHolder itemView;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_main_item_navigation, parent, false);
            itemView = new ItemViewHolder(convertView);
            convertView.setTag(itemView);

        } else {
            itemView = (ItemViewHolder) convertView.getTag();
        }

        itemView.title.setText(navigationBars.get(groupPosition).getTitle());
        Glide.with(parent.getContext()).load(navigationBars.get(groupPosition).getIcon()).into(itemView.icon);

        if (navigationBars.get(groupPosition).getSubNavigationList() != null) {
            itemView.arrow.setVisibility(View.VISIBLE);

            if (isExpanded) {//判斷選項是否打開
                itemView.arrow.setImageResource(R.drawable.ic_arrow_down_dark_blue);
            } else {
                itemView.arrow.setImageResource(R.drawable.ic_arrow_right_dark_blue);
            }

        } else {
            itemView.arrow.setVisibility(View.GONE);
        }

        setBadgeView(itemView.badge, navigationBars.get(groupPosition).getBadge());

        return convertView;
    }

    // ----------------------------------------------------
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ItemSubViewHolder itemSubViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_sub_item_navigation, parent, false);
            itemSubViewHolder = new ItemSubViewHolder(convertView);
            convertView.setTag(itemSubViewHolder);
        } else {
            itemSubViewHolder = (ItemSubViewHolder) convertView.getTag();
        }

        itemSubViewHolder.title.setText(navigationBars.get(groupPosition).getSubNavigationList().get(childPosition).getTitle());
        setBadgeView(itemSubViewHolder.badge, navigationBars.get(groupPosition).getSubNavigationList().get(childPosition).getBadge());

        if (groupPosition == 4 && childPosition == 0) {

            if (UserStatus.verifyStatus == UserStatus.USER_NONE || UserStatus.verifyStatus == UserStatus.NEW_MEMBER || UserStatus.verifyStatus ==
                    UserStatus.PASS_REVIEW) {

                itemSubViewHolder.userStatus.setVisibility(View.GONE);

            } else {

                itemSubViewHolder.userStatus.setVisibility(View.VISIBLE);

                switch (UserStatus.verifyStatus) {
                    case UserStatus.UNDER_REVIEW:
                        itemSubViewHolder.userStatus.setText(parent.getContext().getResources().getString(R.string.under_review));
                        break;
                    case UserStatus.FAIL_REVIEW:
                        itemSubViewHolder.userStatus.setText(parent.getContext().getResources().getString(R.string.fail_review));
                        break;
                    case UserStatus.BLACKLIST:
                        itemSubViewHolder.userStatus.setText(parent.getContext().getResources().getString(R.string.blacklist));
                        break;
                }
            }
        } else {
            itemSubViewHolder.userStatus.setVisibility(View.GONE);
        }

        return convertView;
    }


    // ----------------------------------------------------

    /**
     * 設定badge
     *
     * @param badgeView
     * @param badge
     */
    private void setBadgeView(TextView badgeView, int badge) {

        if (badge > 0) {
            //show badge.
            badgeView.setVisibility(View.VISIBLE);
            badgeView.setText(badge > MAX_RANGE ? (MAX_RANGE + "+") : String.valueOf(badge));
        } else {
            badgeView.setVisibility(View.INVISIBLE);
        }
    }

    // ----------------------------------------------------
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    // ----------------------------------------------------

    /**
     * 設定資料
     *
     * @param navigationBars
     */
    public void setData(List<NavigationBar> navigationBars) {
        this.navigationBars = navigationBars;
        notifyDataSetChanged();
    }

    // ----------------------------------------------------
    class ItemViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.badge)
        TextView badge;
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.arrow)
        ImageView arrow;

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
    class ItemSubViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.user_status)
        TextView userStatus;
        @BindView(R.id.badge)
        TextView badge;

        public ItemSubViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
}
