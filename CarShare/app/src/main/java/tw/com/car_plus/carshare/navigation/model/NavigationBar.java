package tw.com.car_plus.carshare.navigation.model;

import java.util.List;

/**
 * Created by neil on 2017/3/9.
 */

public class NavigationBar {

    private int icon;
    private String title;
    private int badge;
    private List<SubNavigation> subNavigationList;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }

    public List<SubNavigation> getSubNavigationList() {
        return subNavigationList;
    }

    public void setSubNavigationList(List<SubNavigation> subNavigationList) {
        this.subNavigationList = subNavigationList;
    }

    public static class SubNavigation {

        private String title;
        private int badge;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getBadge() {
            return badge;
        }

        public void setBadge(int badge) {
            this.badge = badge;
        }
    }
}
