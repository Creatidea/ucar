package tw.com.car_plus.carshare.news;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.news.NewsConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.news.adapter.NewsAdapter;
import tw.com.car_plus.carshare.news.model.News;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.PhotoViewActivity;

/**
 * Created by winni on 2017/6/7.
 */

public class NewsActivity extends CustomBaseActivity implements NewsAdapter.OnImageClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private NewsConnect newsConnect;
    private NewsAdapter newsAdapter;
    private List<News> newses;

    @Override
    public void init() {
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        IndexActivity.activities.add(this);
        setData();
        newsConnect = new NewsConnect(this);
        newsConnect.getNewsData();

    }

    // ----------------------------------------------------
    private void setData() {

        setBackToolBar(R.drawable.img_toolbar_news);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        newsAdapter = new NewsAdapter(this);
        newsAdapter.setOnImageClickListener(this);
        recyclerView.setAdapter(newsAdapter);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.NEWS) {

            newses = (List<News>) data.get("data");
            newsAdapter.setData(newses);
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
    @Override
    public void onOnClick(int position) {
        Intent intent = new Intent();
        intent.setClass(this, PhotoViewActivity.class);
        intent.putExtra("imagePath", newses.get(position).getNewsPic());
        startActivity(intent);
    }
}
