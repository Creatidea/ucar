package tw.com.car_plus.carshare.news.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.news.model.News;
import tw.com.car_plus.carshare.util.BaseRecyclerViewAdapter;

/**
 * Created by winni on 2017/6/7.
 */

public class NewsAdapter extends BaseRecyclerViewAdapter {

    private Context context;
    private List<News> newses;
    private OnImageClickListener onImageClickListener;

    public NewsAdapter(Context context) {
        this.context = context;
        newses = new ArrayList<>();
    }

    //--------------------------------------------------
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_news, parent, false));
    }

    //--------------------------------------------------
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {

            ((ViewHolder) holder).title.setText(String.format(context.getResources().getString(R.string.news_title), newses.get(position).getTitle
                    (), newses.get(position).getPublishTime()));

            if (newses.get(position).getNewsPic().length() > 0) {
                Glide.with(context).load(newses.get(position).getNewsPic()).into(((ViewHolder) holder).banner);
                ((ViewHolder) holder).banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onImageClickListener != null) {
                            onImageClickListener.onOnClick(holder.getAdapterPosition());
                        }
                    }
                });

            } else {
                ((ViewHolder) holder).banner.setVisibility(View.GONE);
            }

            ((ViewHolder) holder).content.setText(newses.get(position).getContent());

        }

    }

    //--------------------------------------------------
    @Override
    public int getItemCount() {
        return newses.size();
    }

    //--------------------------------------------------

    /**
     * 設定最新消息的資料
     *
     * @param newses
     */
    public void setData(List<News> newses) {
        this.newses = newses;
        notifyDataSetChanged();
    }

    //--------------------------------------------------

    /**
     * 設定按下圖片的事件
     *
     * @param onImageClickListener
     */
    public void setOnImageClickListener(OnImageClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    //--------------------------------------------------
    public interface OnImageClickListener {
        void onOnClick(int position);
    }

    //--------------------------------------------------
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.banner)
        ImageView banner;
        @BindView(R.id.content)
        TextView content;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            //列表item點下事件
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }
}
