package tw.com.car_plus.carshare.news.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by winni on 2017/6/7.
 */

public class News {


    /**
     * Title : 最新消息
     * Content : 最新內容最新內容最新內容最新內容最新內容
     * NewsPic : http://220.128.149.246/FileDownload/GetPic?fileName=44904e7bb966499493762401ee481571.jpg&uploadType=10
     * PublishTime : 2017/06/06
     */

    @SerializedName("Title")
    private String Title;
    @SerializedName("Content")
    private String Content;
    @SerializedName("NewsPic")
    private String NewsPic;
    @SerializedName("PublishTime")
    private String PublishTime;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getNewsPic() {
        return NewsPic;
    }

    public void setNewsPic(String NewsPic) {
        this.NewsPic = NewsPic;
    }

    public String getPublishTime() {
        return PublishTime;
    }

    public void setPublishTime(String PublishTime) {
        this.PublishTime = PublishTime;
    }
}
