package tw.com.car_plus.carshare.notification;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.notification.model.PushData;
import tw.com.car_plus.carshare.rent.MapModeFragment;


/**
 * Created by winni on 2017/12/19.
 */

public class NotificationActivity extends AppCompatActivity {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.message)
    TextView message;

    private PushData pushData;
    private String strTitle;
    private String strMessage;
    private int pushType;


    // ----------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_notification);
        ButterKnife.bind(this);
        init();
    }

    // ----------------------------------------------------
    private void init() {

        Bundle bundle = getIntent().getExtras();
        pushData = bundle.getParcelable("pushData");

        strTitle = pushData.getTitle();
        strMessage = pushData.getBody();
        pushType = pushData.getPushType();

        title.setText(strTitle);
        message.setText(strMessage);

    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                finish();
                break;
            case R.id.confirm:
                finish();
                closeActivity();
                if (pushType != 0) {
                    setParkingLocation();
                }
                break;
        }
    }

    // ----------------------------------------------------
    private void setParkingLocation() {
        Location location = new Location("Location");
        location.setLatitude(pushData.getLatitude());
        location.setLongitude(pushData.getLongitude());
        MapModeFragment.parkingLocation = location;
    }

    // ----------------------------------------------------
    private void closeActivity() {

        for (AppCompatActivity activity : IndexActivity.activities) {
            activity.finish();
        }
    }
}
