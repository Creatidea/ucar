package tw.com.car_plus.carshare.notification.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/12/19.
 */

public class PushData implements Parcelable {

    private String title;
    private String body;
    private double Longitude;
    private double Latitude;
    //0為存文字，1為停車場提醒
    private int pushType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public int getPushType() {
        return pushType;
    }

    public void setPushType(int pushType) {
        this.pushType = pushType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeDouble(this.Longitude);
        dest.writeDouble(this.Latitude);
        dest.writeInt(this.pushType);
    }

    public PushData() {
    }

    protected PushData(Parcel in) {
        this.title = in.readString();
        this.body = in.readString();
        this.Longitude = in.readDouble();
        this.Latitude = in.readDouble();
        this.pushType = in.readInt();
    }

    public static final Parcelable.Creator<PushData> CREATOR = new Parcelable.Creator<PushData>() {
        @Override
        public PushData createFromParcel(Parcel source) {
            return new PushData(source);
        }

        @Override
        public PushData[] newArray(int size) {
            return new PushData[size];
        }
    };
}
