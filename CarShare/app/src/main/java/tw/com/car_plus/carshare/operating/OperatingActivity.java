package tw.com.car_plus.carshare.operating;

import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.operating.adapter.OperatingViewHolder;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.PointViewUtil;

/**
 * Created by winni on 2017/10/16.
 */

public class OperatingActivity extends CustomBaseActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.operating_list)
    ConvenientBanner operatingList;
    @BindView(R.id.point_layout)
    LinearLayout pointLayout;

    private String operatingTitles[];
    private PointViewUtil pointViewUtil;


    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_operating);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_operating);

        IndexActivity.activities.add(this);
        operatingTitles = getResources().getStringArray(R.array.operating);
        pointViewUtil = new PointViewUtil(this, pointLayout);
        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        Integer operatingImage[] = new Integer[]{R.drawable.img_operating_step_01, R.drawable.img_operating_step_02, R.drawable.img_operating_step_03,
                R.drawable.img_operating_step_04, R.drawable.img_operating_step_05, R.drawable.img_operating_step_06,
                R.drawable.img_operating_step_07};

        operatingList.setCanLoop(false);
        operatingList.setPages(new CBViewHolderCreator() {
            @Override
            public OperatingViewHolder createHolder() {
                return new OperatingViewHolder();
            }
        }, Arrays.asList(operatingImage));

        pointViewUtil.setSize(operatingImage.length);
        pointViewUtil.setSelectPointImage(R.drawable.img_selector_point);
        pointViewUtil.setPointView();

        operatingList.setPointViewVisible(true);
        operatingList.setOnPageChangeListener(this);
        operatingList.setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        operatingList.setcurrentitem(0);
        title.setText(operatingTitles[0]);
        pointViewUtil.setSelectPoint(0);
    }

    // ----------------------------------------------------
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    // ----------------------------------------------------
    @Override
    public void onPageSelected(int position) {
        title.setText(operatingTitles[position]);
        pointViewUtil.setSelectPoint(position);
    }

    // ----------------------------------------------------
    @Override
    public void onPageScrollStateChanged(int state) {

    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
}
