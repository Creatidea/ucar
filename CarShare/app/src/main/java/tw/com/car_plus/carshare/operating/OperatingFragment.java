package tw.com.car_plus.carshare.operating;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.operating.adapter.OperatingViewHolder;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.PointViewUtil;

/**
 * Created by winni on 2017/10/16.
 */

public class OperatingFragment extends CustomBaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.operating_list)
    ConvenientBanner operatingList;
    @BindView(R.id.enter)
    Button enter;
    @BindView(R.id.point_layout)
    LinearLayout pointLayout;

    // ----------------------------------------------------
    private OnEnterLoginListener onEnterLoginListener;
    private String operatingTitles[];
    private PointViewUtil pointViewUtil;

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_operating);
        operatingTitles = getResources().getStringArray(R.array.operating);
        pointViewUtil = new PointViewUtil(getActivity(), pointLayout);
        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        Integer operatingImage[] = new Integer[]{R.drawable.img_operating_step_01, R.drawable.img_operating_step_02, R.drawable.img_operating_step_03,
                R.drawable.img_operating_step_04, R.drawable.img_operating_step_05, R.drawable.img_operating_step_06,
                R.drawable.img_operating_step_07};

        operatingList.setCanLoop(false);
        operatingList.setPages(new CBViewHolderCreator() {
            @Override
            public OperatingViewHolder createHolder() {
                return new OperatingViewHolder();
            }
        }, Arrays.asList(operatingImage));

        pointViewUtil.setSize(operatingImage.length);
        pointViewUtil.setSelectPointImage(R.drawable.img_selector_point);
        pointViewUtil.setPointView();

        operatingList.setOnPageChangeListener(this);
        operatingList.setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        operatingList.setcurrentitem(0);
        title.setText(operatingTitles[0]);
        pointViewUtil.setSelectPoint(0);
    }

    // ----------------------------------------------------
    public void setEnterLogin(OnEnterLoginListener onEnterLoginListener) {
        this.onEnterLoginListener = onEnterLoginListener;
    }

    // ----------------------------------------------------
    @OnClick(R.id.enter)
    public void onViewClicked() {
        onEnterLoginListener.onEnter();
    }

    // ----------------------------------------------------
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    // ----------------------------------------------------
    @Override
    public void onPageSelected(int position) {
        title.setText(operatingTitles[position]);
        pointViewUtil.setSelectPoint(position);

        if (position == operatingTitles.length - 1) {
            enter.setVisibility(View.VISIBLE);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onPageScrollStateChanged(int state) {

    }

    // ----------------------------------------------------
    public interface OnEnterLoginListener {
        void onEnter();
    }

    // ----------------------------------------------------
}
