package tw.com.car_plus.carshare.operating.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.bumptech.glide.Glide;

import tw.com.car_plus.carshare.R;

/**
 * Created by winni on 2017/10/16.
 */

public class OperatingViewHolder implements Holder<Integer> {

    private ImageView operatingPic;

    @Override
    public View createView(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_operating, null);
        operatingPic = (ImageView) view.findViewById(R.id.image);
        return view;
    }

    // ----------------------------------------------------
    @Override
    public void UpdateUI(Context context, int position, Integer data) {
        Glide.with(context).load(data).into(operatingPic);
    }
}
