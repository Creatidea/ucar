package tw.com.car_plus.carshare.page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.neilchen.complextoolkit.util.json.JSONParser;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.RegisterFragment;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.return_car.ReturnCarFragment;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.pickup_car.PickUpTheCarFragment;
import tw.com.car_plus.carshare.rent.success.ReservationSuccessFragment;
import tw.com.car_plus.carshare.rent.success.StartRentCarFragment;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.Order;
import tw.com.car_plus.carshare.user.UserInfoActivity;
import tw.com.car_plus.carshare.util.DialogUtil;

import static tw.com.car_plus.carshare.index.UserStatus.FAIL_REVIEW;
import static tw.com.car_plus.carshare.index.UserStatus.NEW_MEMBER;
import static tw.com.car_plus.carshare.index.UserStatus.UNDER_REVIEW;
import static tw.com.car_plus.carshare.index.UserStatus.USER_NONE;

/**
 * Created by winni on 2017/5/10.
 * 登入後的page控管中心
 */

public class PageCenter {

    private Context context;
    private User user;
    private UserStatus userStatus;
    private JSONParser parser;
    private Gson gson;

    public PageCenter(Context context) {
        this.context = context;
        userStatus = new UserStatus();
        parser = new JSONParser();
        gson = new Gson();
    }

    // ----------------------------------------------------

    /**
     * 設定User
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        setUserStatus();
    }

    // ----------------------------------------------------

    /**
     * 設定使用者目前的會員狀態、註冊狀態、租車狀態
     */
    private void setUserStatus() {
        userStatus.setVerifyStatus(user.getVerifyStatus());
        userStatus.setRegisterStatus(user.getRegisterStatus());
        userStatus.setOperationStatus(user.getOperationStatus());
    }

    //--------------------------------------------------

    /**
     * 取得前往的Fragment
     *
     * @return
     */
    public Fragment getGoToFragment() {
        //新申請or舊用戶
        if (UserStatus.verifyStatus == NEW_MEMBER || UserStatus.verifyStatus == USER_NONE) {
            return new RegisterFragment();
        } else {

            if (UserStatus.verifyStatus == FAIL_REVIEW) {
                showGoToUserInfoDialog(R.string.user_fail_review, false);
            } else if (UserStatus.verifyStatus == UNDER_REVIEW) {
                new DialogUtil(context).setMessage(R.string.user_under_review).setConfirmButton(R.string.confirm, null).showDialog(false);
            } else {
                if (user.isNoCreditCard()) {
                    showGoToUserInfoDialog(R.string.need_credit_card, true);
                }
            }

            return goToRentCarFragment(UserStatus.operationStatus);
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示前往會員資料的畫面
     *
     * @param message
     * @param isGoToCreditCard
     */
    private void showGoToUserInfoDialog(int message, final boolean isGoToCreditCard) {

        new DialogUtil(context).setMessage(message).setConfirmButton(R.string.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, UserInfoActivity.class);
                if (isGoToCreditCard) {
                    intent.putExtra("isOpenCreditCard", isGoToCreditCard);
                }
                context.startActivity(intent);
            }
        }).showDialog(false);

    }

    // ----------------------------------------------------

    /**
     * 依照目前的狀前往態租車的各項畫面
     *
     * @param operationStatus
     */
    private Fragment goToRentCarFragment(@UserStatus.OperationStatus int operationStatus) {

        Fragment fragment = new MapModeFragment();

        switch (operationStatus) {
            case UserStatus.RENT_NONE:
                fragment = new MapModeFragment();
                break;
            case UserStatus.RESERVATION:
                fragment = new ReservationSuccessFragment();
                fragment.setArguments(getReservationData());
                break;
            case UserStatus.PICK_UP_CAR:
                fragment = new PickUpTheCarFragment();
                fragment.setArguments(getPickUpCarData());
                break;
            case UserStatus.DRIVE_CAR:
                fragment = new StartRentCarFragment();
                fragment.setArguments(getStartRentCarData());
                break;
            case UserStatus.RETURN_CAR:
                fragment = new ReturnCarFragment();
                fragment.setArguments(getReturnCarData());
                break;
        }

        return fragment;

    }

    //--------------------------------------------------

    /**
     * 取得預約成功的資料
     *
     * @return
     */
    private Bundle getReservationData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("reservationInfo", parser.getJSONData(gson.toJson(user.getPreOrder()), ReservationInfo.class));
        return bundle;
    }

    //--------------------------------------------------

    /**
     * 取得取車的資料
     *
     * @return
     */
    private Bundle getPickUpCarData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("order", parser.getJSONData(gson.toJson(user.getGetCar()), Order.class));
        return bundle;
    }

    //--------------------------------------------------

    /**
     * 取得開始租車的資料
     *
     * @return
     */
    private Bundle getStartRentCarData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("carFinish", parser.getJSONData(gson.toJson(user.getDriveCar()), CarFinish.class));
        return bundle;
    }

    //--------------------------------------------------

    /**
     * 取得還車的資料
     *
     * @return
     */
    private Bundle getReturnCarData() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromPage",true);
        bundle.putParcelable("info", parser.getJSONData(gson.toJson(user.getReturnCar()), ReturnCarInfo.class));
        return bundle;
    }

    //--------------------------------------------------

}
