package tw.com.car_plus.carshare.preferential;


import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.preferential.PreferentialConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.preferential.adapter.PreferentialAdapter;
import tw.com.car_plus.carshare.preferential.model.Preferential;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.PhotoViewActivity;

public class PreferentialActivity extends CustomBaseActivity implements PreferentialAdapter.OnImageClickListener {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private PreferentialConnect preferentialConnect;
    private PreferentialAdapter preferentialAdapter;
    private List<Preferential> preferentials;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_preferential);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_preferential);
        IndexActivity.activities.add(this);

        setData();

        preferentialConnect = new PreferentialConnect(this);

        if (getIntent().getParcelableExtra("") != null) {
            preferentialAdapter.setData((List<Preferential>) getIntent().getParcelableExtra(""));
        } else {
            preferentialConnect.sendPreferentialsInfo();
        }

        preferentialAdapter.setOnImageClickListener(this);
    }

    // ----------------------------------------------------
    private void setData() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        preferentialAdapter = new PreferentialAdapter(this);
        recyclerView.setAdapter(preferentialAdapter);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.PREFERENTIAL) {

            preferentials = (List<Preferential>) data.get("data");
            preferentialAdapter.setData(preferentials);
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
    @Override
    public void onOnClick(int position) {

        Intent intent = new Intent();
        intent.setClass(this, PhotoViewActivity.class);
        intent.putExtra("imagePath", preferentials.get(position).getPic());
        startActivity(intent);
    }

    // ----------------------------------------------------
}
