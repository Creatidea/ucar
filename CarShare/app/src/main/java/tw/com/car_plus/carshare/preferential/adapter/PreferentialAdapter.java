package tw.com.car_plus.carshare.preferential.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.preferential.model.Preferential;
import tw.com.car_plus.carshare.util.BaseRecyclerViewAdapter;
import tw.com.car_plus.carshare.util.PromotionDateUtil;

/**
 * Created by winni on 2017/6/1.
 */

public class PreferentialAdapter extends BaseRecyclerViewAdapter {

    private Context context;
    private PromotionDateUtil promotionDateUtil;
    private List<Preferential> preferentials;
    private OnImageClickListener onImageClickListener;

    public PreferentialAdapter(Context context) {
        this.context = context;
        promotionDateUtil = new PromotionDateUtil(context);
        preferentials = new ArrayList<>();
    }

    //--------------------------------------------------
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_preferential, parent, false));
    }

    //--------------------------------------------------
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {

            if (preferentials.get(position).getPic().length() > 0) {
                Glide.with(context).load(preferentials.get(position).getPic()).into(((ViewHolder) holder).banner);
                ((ViewHolder) holder).banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onImageClickListener != null) {
                            onImageClickListener.onOnClick(holder.getAdapterPosition());
                        }
                    }
                });
            } else {
                ((ViewHolder) holder).banner.setVisibility(View.GONE);
            }

            ((ViewHolder) holder).title.setText(preferentials.get(position).getTitle());
            ((ViewHolder) holder).date.setText(promotionDateUtil.getDate(preferentials.get(position).getStartTime(), preferentials.get(position)
                    .getEndTime()));

            ((ViewHolder) holder).info.setText(preferentials.get(position).getInfo());
            setApplicantView(preferentials.get(position).getSuitable(), ((ViewHolder) holder).applicant);
        }

    }

    //--------------------------------------------------
    @Override
    public int getItemCount() {
        return preferentials.size();
    }

    //--------------------------------------------------
    public void setData(List<Preferential> preferentials) {
        this.preferentials = preferentials;
        notifyDataSetChanged();
    }

    //--------------------------------------------------

    /**
     * 設定按下圖片的事件
     * @param onImageClickListener
     */
    public void setOnImageClickListener(OnImageClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    //--------------------------------------------------
    private void setApplicantView(List<String> strings, LinearLayout linearLayout) {

        TextView title;

        for (int i = 0; i < strings.size(); i++) {

            View view = LayoutInflater.from(context).inflate(R.layout.list_item_preferential_applicant, null);
            title = (TextView) view.findViewById(R.id.title);
            title.setText(strings.get(i));
            linearLayout.addView(view);
        }

    }

    //--------------------------------------------------
    public interface OnImageClickListener {
        void onOnClick(int position);
    }

    //--------------------------------------------------
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.banner)
        ImageView banner;
        @BindView(R.id.applicant)
        LinearLayout applicant;
        @BindView(R.id.info)
        TextView info;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            //列表item點下事件
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }
    //--------------------------------------------------
}
