package tw.com.car_plus.carshare.preferential.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by winni on 2017/6/1.
 */

public class Preferential implements Parcelable {


    /**
     * Title : 夏日趴趴購
     * Pic : http://220.128.149.246/FileDownload/GetPic?fileName=41e6946ee3a545ea947ebfba7574f487.png&uploadType=8
     * Info : 夏日趴趴購
     * StartTime :
     * EndTime :
     * Suitable : ["限柴油車、油電混合車、插電式油電混合PHEV(以電為主要動力供給能源，可充電)、柴電混合車","限首次註冊","限中興低碳停車場、測試點位、小巨蛋、微風停車場、行天宮停車場、測試停車場、台北西華飯店、美麗華停車場、大佳河濱公園停車場、裕隆汽車城(測試)
     * 、格上總部、格上濱江站、gps測試、南港展覽館"]
     */

    @SerializedName("Title")
    private String Title;
    @SerializedName("Pic")
    private String Pic;
    @SerializedName("Info")
    private String Info;
    @SerializedName("StartTime")
    private String StartTime;
    @SerializedName("EndTime")
    private String EndTime;
    @SerializedName("Suitable")
    private List<String> Suitable;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getPic() {
        return Pic;
    }

    public void setPic(String Pic) {
        this.Pic = Pic;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String Info) {
        this.Info = Info;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    public List<String> getSuitable() {
        return Suitable;
    }

    public void setSuitable(List<String> Suitable) {
        this.Suitable = Suitable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Title);
        dest.writeString(this.Pic);
        dest.writeString(this.Info);
        dest.writeString(this.StartTime);
        dest.writeString(this.EndTime);
        dest.writeStringList(this.Suitable);
    }

    public Preferential() {
    }

    protected Preferential(Parcel in) {
        this.Title = in.readString();
        this.Pic = in.readString();
        this.Info = in.readString();
        this.StartTime = in.readString();
        this.EndTime = in.readString();
        this.Suitable = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Preferential> CREATOR = new Parcelable.Creator<Preferential>() {
        @Override
        public Preferential createFromParcel(Parcel source) {
            return new Preferential(source);
        }

        @Override
        public Preferential[] newArray(int size) {
            return new Preferential[size];
        }
    };
}
