package tw.com.car_plus.carshare.price;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

public class RentPriceActivity extends CustomBaseActivity {

    // ----------------------------------------------------
    @Override
    public void init() {

        setContentView(R.layout.activity_rent_price);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_rent_price_actionbar);

        IndexActivity.activities.add(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

}
