package tw.com.car_plus.carshare.problems;

import android.widget.ExpandableListView;

import com.neilchen.complextoolkit.assets.AssetsOpenFile;
import com.neilchen.complextoolkit.util.json.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.problems.adapter.CommonProblemAdapter;
import tw.com.car_plus.carshare.problems.model.CommonProblems;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by jhen on 2017/10/17.
 */

public class CommonProblemsActivity extends CustomBaseActivity {

    @BindView(R.id.list)
    ExpandableListView list;

    private CommonProblemAdapter commonProblemAdapter;
    private List<CommonProblems> commonProblemsList;

    @Override
    public void init() {
        setContentView(R.layout.activity_common_problems);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_problems);

        IndexActivity.activities.add(this);
        commonProblemAdapter = new CommonProblemAdapter(this, getData());
        list.setAdapter(commonProblemAdapter);
    }

    // ----------------------------------------------------
    private List<CommonProblems> getData() {

        commonProblemsList = new ArrayList<>();

        String file = new AssetsOpenFile(this).setAssetsFile("common_problems.json");
        JSONParser jsonParser = new JSONParser();

        try {

            JSONArray jsonArray = new JSONArray(file);
            for (int i = 0; i < jsonArray.length(); i++) {
                CommonProblems commonProblems = jsonParser.getJSONData(jsonArray.getJSONObject(i).toString(), CommonProblems.class);
                commonProblemsList.add(commonProblems);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return commonProblemsList;
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------

}
