package tw.com.car_plus.carshare.problems.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.problems.model.CommonProblems;


/**
 * Created by jhen on 2017/10/17.
 */

public class CommonProblemAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<CommonProblems> commonProblemses;

    public CommonProblemAdapter(Context context, List<CommonProblems> commonProblemsList) {
        this.context = context;
        this.commonProblemses = commonProblemsList;
    }

    // ----------------------------------------------------
    @Override
    public int getGroupCount() {
        return commonProblemses.size();
    }

    // ----------------------------------------------------
    @Override
    public int getChildrenCount(int groupPosition) {
        return commonProblemses.get(groupPosition).getQuestion().size();
    }

    // ----------------------------------------------------
    @Override
    public Object getGroup(int groupPosition) {
        return commonProblemses.get(groupPosition);
    }

    // ----------------------------------------------------
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return commonProblemses.get(groupPosition).getQuestion().get(childPosition);
    }

    // ----------------------------------------------------
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }


    // ----------------------------------------------------
    @Override
    public boolean hasStableIds() {
        return false;
    }

    // ----------------------------------------------------
    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {

        final MainViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_problems, parent, false);
            holder = new MainViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (MainViewHolder) convertView.getTag();
        }

        CommonProblems commonProblems = commonProblemses.get(groupPosition);

        holder.title.setText(commonProblems.getTitle());

        if (groupPosition % 2 == 0) {
            holder.problemsLayout.setBackgroundResource(R.color.register_credit_card_bg);
        } else {
            holder.problemsLayout.setBackgroundResource(R.color.problems_title_bg);
        }

        if (isExpanded) {
            holder.view.setVisibility(View.GONE);
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.problems_question_title));
            holder.arrow.setImageResource(R.drawable.ic_problems_close);
        } else {
            holder.view.setVisibility(View.VISIBLE);
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.main_text_gray));
            holder.arrow.setImageResource(R.drawable.ic_problems_expand);
        }

        return convertView;
    }

    // ----------------------------------------------------
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        QAViewHolder qaViewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_problems_faq, parent, false);
            qaViewHolder = new QAViewHolder(convertView);
            convertView.setTag(qaViewHolder);
        } else {
            qaViewHolder = (QAViewHolder) convertView.getTag();
        }

        CommonProblems.QuestionBean questionBean = commonProblemses.get(groupPosition).getQuestion().get(childPosition);
        qaViewHolder.title.setText(String.format(context.getString(R.string.problems_question_title), questionBean.getTitle()));
        qaViewHolder.question.setText(questionBean.getAnswer());

        return convertView;
    }

    // ----------------------------------------------------
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    // ----------------------------------------------------
    static class MainViewHolder {

        @BindView(R.id.layout_problems)
        LinearLayout problemsLayout;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.arrow)
        ImageView arrow;
        @BindView(R.id.view)
        View view;

        MainViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
    static class QAViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.question)
        TextView question;

        QAViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
}
