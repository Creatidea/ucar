package tw.com.car_plus.carshare.problems.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jhen on 2017/10/17.
 */

public class CommonProblems {


    /**
     * title : 熱門問題
     * question : [{"title":"Car Sharing提供什麼服務？","answer":"透過整合汽車與行動裝置，創造一個智慧汽車租賃服務，將可有效提升汽車的使用率、解決空氣、減少停車空間需求及交通壅塞問題，達到共享經濟的理念。"},{"title":"Car
     * Sharing租賃目前狀況、營運範圍","answer":"Car Sharing服務於2016年11月底在台北市區上路，試營運規模為80輛，試營運期間的租還區域以信義、松山、士林、文山及新店、三峽為主，具體範圍查看Car Sharing APP內的地圖顯示。"},
     * {"title":"Car Sharing的營運時間","answer":"除颱風天及特殊狀況時不提供服務外，本系統提供24小時全電子自動無人化服務與電話客服。"}]
     */

    @SerializedName("title")
    private String title;
    @SerializedName("question")
    private List<QuestionBean> question;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionBean> getQuestion() {
        return question;
    }

    public void setQuestion(List<QuestionBean> question) {
        this.question = question;
    }

    public static class QuestionBean {
        /**
         * title : Car Sharing提供什麼服務？
         * answer : 透過整合汽車與行動裝置，創造一個智慧汽車租賃服務，將可有效提升汽車的使用率、解決空氣、減少停車空間需求及交通壅塞問題，達到共享經濟的理念。
         */

        @SerializedName("title")
        private String title;
        @SerializedName("answer")
        private String answer;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }
    }
}
