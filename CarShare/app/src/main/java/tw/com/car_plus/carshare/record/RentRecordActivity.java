package tw.com.car_plus.carshare.record;

import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/5/8.
 */

public class RentRecordActivity extends CustomBaseActivity {


    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_rent_record);
        ButterKnife.bind(this);

        setToolbar(R.drawable.ic_back, R.drawable.img_toolbar_rent_record, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        IndexActivity.activities.add(this);

        Bundle bundle = new Bundle();
        bundle.putBoolean("isCompanyAccount", isCompanyAccount());
        goToFragment(R.id.content_layout, new RentRecordFragment(), bundle);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------

}
