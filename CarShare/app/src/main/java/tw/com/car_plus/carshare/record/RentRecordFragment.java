package tw.com.car_plus.carshare.record;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.record.RecordConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.record.adapter.RentRecordAdapter;
import tw.com.car_plus.carshare.record.detail.RentRecordDetailFragment;
import tw.com.car_plus.carshare.record.model.RentRecord;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/5/8.
 */

public class RentRecordFragment extends CustomBaseFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.list_view)
    ListView listView;

    private RentRecordAdapter rentRecordAdapter;
    private RecordConnect recordConnect;
    private List<RentRecord> rentRecords;
    private boolean isCompanyAccount;

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_rent_record);
        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        recordConnect = new RecordConnect(activity);
        rentRecordAdapter = new RentRecordAdapter(activity);
        listView.setAdapter(rentRecordAdapter);
        listView.setOnItemClickListener(this);

        isCompanyAccount = getArguments().getBoolean("isCompanyAccount");
        recordConnect.getRecordData(isCompanyAccount);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.RENT_RECORD) {

            rentRecords = (List<RentRecord>) data.get("data");
            rentRecordAdapter.setData(rentRecords);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (rentRecords != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isCompanyAccount", isCompanyAccount);
            bundle.putInt("orderId", rentRecords.get(position).getOrderId());
            replaceFragment(R.id.content_layout, new RentRecordDetailFragment(), true, bundle);
        }
    }

    // ----------------------------------------------------


}
