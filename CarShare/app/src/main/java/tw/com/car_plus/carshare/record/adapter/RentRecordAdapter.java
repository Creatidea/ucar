package tw.com.car_plus.carshare.record.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.record.model.RentRecord;
import tw.com.car_plus.carshare.util.DateParser;

/**
 * Created by winni on 2017/5/8.
 */

public class RentRecordAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<RentRecord> rentRecords;
    private DateParser dateParser;

    public RentRecordAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.rentRecords = new ArrayList<>();
        dateParser = new DateParser();
    }

    // ----------------------------------------------------
    @Override
    public int getCount() {
        return rentRecords.size();
    }

    // ----------------------------------------------------
    @Override
    public Object getItem(int position) {
        return null;
    }

    // ----------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_rent_record, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        if (position % 2 == 0) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.rent_record_item_bg));
        } else {
            viewHolder.layout.setBackgroundColor(Color.WHITE);
        }

        viewHolder.date.setText(dateParser.getDate(rentRecords.get(position).getBookingOnUtc()));
        viewHolder.amount.setText(String.valueOf(rentRecords.get(position).getOrderAmount()));
        viewHolder.orderId.setText(rentRecords.get(position).getOrderNo());

        return convertView;
    }

    //--------------------------------------------------

    /**
     * 設定資料
     * @param rentRecords
     */
    public void setData(List<RentRecord> rentRecords) {
        this.rentRecords = rentRecords;
        notifyDataSetChanged();
    }

    //--------------------------------------------------
    class ViewHolder {
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.order_id)
        TextView orderId;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.layout)
        LinearLayout layout;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
