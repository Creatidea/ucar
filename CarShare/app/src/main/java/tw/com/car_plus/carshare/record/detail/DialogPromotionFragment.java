package tw.com.car_plus.carshare.record.detail;

import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.util.BaseDialogFragment;
import tw.com.car_plus.carshare.util.PromotionDateUtil;

/**
 * Created by winni on 2018/1/15.
 */

public class DialogPromotionFragment extends BaseDialogFragment {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.info)
    TextView info;

    private PromotionDateUtil promotionDateUtil;
    private String startDate, endDate;

    @Override
    protected void init() {

        setView(R.layout.dialog_preferential_info);

        Bundle bundle = getArguments();
        startDate = bundle.getString("startDate", "");
        endDate = bundle.getString("endDate", "");

        promotionDateUtil = new PromotionDateUtil(getActivity());

        title.setText(bundle.getString("title"));
        info.setText(bundle.getString("info"));
        date.setText(promotionDateUtil.getDate(startDate, endDate));

        setCancelable(false);

    }

    // ----------------------------------------------------
    @OnClick(R.id.close)
    public void onViewClicked() {
        dismiss();
    }


}
