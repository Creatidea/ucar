package tw.com.car_plus.carshare.record.detail;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.record.RecordConnect;
import tw.com.car_plus.carshare.record.detail.model.RentRecordDetail;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DateParser;

/**
 * Created by winni on 2017/5/31.
 */

public class RentRecordDetailFragment extends CustomBaseFragment {

    @BindView(R.id.transaction_date)
    TextView transactionDate;
    @BindView(R.id.order_no)
    TextView orderNo;
    @BindView(R.id.car_no)
    TextView carNo;
    @BindView(R.id.get_car_time)
    TextView getCarTime;
    @BindView(R.id.return_car_time)
    TextView returnCarTime;
    @BindView(R.id.get_car_location)
    TextView getCarLocation;
    @BindView(R.id.return_car_location)
    TextView returnCarLocation;
    @BindView(R.id.get_car_mileage)
    TextView getCarMileage;
    @BindView(R.id.return_car_mileage)
    TextView returnCarMileage;
    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.hour_unit)
    TextView hourUnit;
    @BindView(R.id.minute)
    TextView minute;
    @BindView(R.id.minute_unit)
    TextView minuteUnit;
    @BindView(R.id.time_rent)
    TextView timeRent;
    @BindView(R.id.mileage_price)
    TextView mileagePrice;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.bonus)
    TextView bonus;
    @BindView(R.id.layout_bonus)
    LinearLayout layoutBonus;
    @BindView(R.id.pay_money)
    TextView payMoney;
    @BindView(R.id.layout_invoice)
    RelativeLayout layoutInvoice;
    @BindView(R.id.invoice_number)
    TextView invoiceNumber;
    @BindView(R.id.layout_pay_promotion_info)
    RelativeLayout layoutPayPromotionInfo;
    @BindView(R.id.pay_promotion)
    TextView payPromotion;

    private RecordConnect recordConnect;
    private RentRecordDetail rentRecordDetail;
    private DateParser dateParser;

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_rent_record_detail);

        dateParser = new DateParser();
        recordConnect = new RecordConnect(activity);
        recordConnect.getRecordDetailData(getArguments().getInt("orderId"));

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(RentRecordDetail rentRecordDetail) {
        this.rentRecordDetail = rentRecordDetail;
        setData();
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    private void setData() {

        transactionDate.setText(dateParser.getDateTime(rentRecordDetail.getOrderTime()));
        orderNo.setText(rentRecordDetail.getOrderNo());
        carNo.setText(rentRecordDetail.getCarNo());
        getCarTime.setText(dateParser.getDateTime(rentRecordDetail.getRentSDate()));
        returnCarTime.setText(dateParser.getDateTime(rentRecordDetail.getRentEDate()));
        getCarLocation.setText(rentRecordDetail.getStartParkinglotName());
        returnCarLocation.setText(rentRecordDetail.getEndParkinglotName());
        showRentTime(rentRecordDetail.getUseHour(), rentRecordDetail.getUseMinute());
        getCarMileage.setText(String.valueOf(rentRecordDetail.getStartMeter()));
        returnCarMileage.setText(String.valueOf(rentRecordDetail.getEndMeter()));
        timeRent.setText(String.valueOf(rentRecordDetail.getRentByHour()));
        mileagePrice.setText(String.valueOf(rentRecordDetail.getMeterCost()));
        total.setText(String.valueOf(rentRecordDetail.getRentTotalAmount()));
        invoiceNumber.setText(rentRecordDetail.getInvoiceNo());

        payPromotion.setText(String.valueOf(rentRecordDetail.getRentTotalAmountByPromotion()));

        if (rentRecordDetail.getRentTotalAmountByPromotion() > 0) {
            layoutPayPromotionInfo.setVisibility(View.VISIBLE);
        }

        if (getArguments().getBoolean("isCompanyAccount")) {
            layoutInvoice.setVisibility(View.GONE);
            layoutBonus.setVisibility(View.GONE);
        } else {
            discount.setText(String.valueOf(rentRecordDetail.getUseBounce()));
            bonus.setText(String.valueOf(rentRecordDetail.getAddBounce()));
        }

        payMoney.setText(String.valueOf(rentRecordDetail.getAfterDiscount()));

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------

    /**
     * 顯示時租時間
     *
     * @param mHour
     * @param mMinute
     */
    private void showRentTime(int mHour, int mMinute) {

        if (mHour == 0) {
            hour.setVisibility(View.INVISIBLE);
            hourUnit.setVisibility(View.INVISIBLE);
        } else {
            hour.setText(String.valueOf(mHour));
        }

        minute.setText(String.valueOf(mMinute));
    }

    // ----------------------------------------------------
    @OnClick(R.id.layout_pay_promotion_info)
    public void onViewClicked() {

        RentRecordDetail.PromotionInfoDetailBean promotionInfoDetailBean = rentRecordDetail.getPromotionInfoDetail();

        Bundle bundle = new Bundle();
        bundle.putString("startDate", promotionInfoDetailBean.getStartTime());
        bundle.putString("endDate", promotionInfoDetailBean.getEndTime());
        bundle.putString("title", promotionInfoDetailBean.getTitle());
        bundle.putString("info", promotionInfoDetailBean.getInfo());

        showDialogFragment(new DialogPromotionFragment(), bundle);

    }
}
