package tw.com.car_plus.carshare.record.detail.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by winni on 2017/5/31.
 */

public class RentRecordDetail {


    /**
     * OrderId : 6520
     * OrderNo : S2018010910014
     * RentSDate : 2018-01-09T05:51:28.37
     * RentEDate : 2018-01-09T07:00:41.337
     * RentMemberName : 陳煒真
     * InvoiceType : 1
     * InvoiceNo : YM87739534
     * CarNo : 3083-NN
     * StartParkinglotName : 世大運(台北小巨蛋)
     * EndParkinglotName : 世大運(台北小巨蛋)
     * CurrentOrderStatus : 99
     * RentTotalAmount : 250
     * RentTotalAmountByPromotion : 175
     * UseBounce : 0
     * AddBounce : 1
     * AfterDiscount : 175
     * CompanyName : 創鈺國際
     * CompanySn : 28061479
     * GetCarPic : ["http://59.125.115.212/FileDownload/GetPic?fileName=63b65c04927c4159a5e7a111383da880.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=c08b0cfed69d4f3fa68518dff4f5de77.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=28b4c1bd7ae7490198c308ac0ac966aa.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=21aff11d96a84f7e8d81920a33fa2cfe.jpg&uploadType=5"]
     * ReturnCar : ["http://59.125.115.212/FileDownload/GetPic?fileName=3d5f25c3fd37432ebca870bda18853e3.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=f6678b595fc04d88bcc50fbddab0003d.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=995b04cf324045dea96a274f556b9c32.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=36d1df8e9ff84bdc92d92fc09c06bbdc.jpg&uploadType=5",
     * "http://59.125.115.212/FileDownload/GetPic?fileName=d393d8f718b245648ded6a7d42b35bdf.jpg&uploadType=5"]
     * GetCarSignature : http://59.125.115.212/FileDownload/GetPic?fileName=ba3820694ec945c0855d9732f645c899.jpg&uploadType=6
     * ReturnCarSignature : http://59.125.115.212/FileDownload/GetPic?fileName=d6285d500c334329818874ed42803bfc.jpg&uploadType=6
     * RentByHour : 200
     * RentByMileage : 3
     * Mileage : 0
     * OrderTime : 2018-01-09T05:51:28.37
     * UseHour : 1
     * UseMinute : 9
     * UseMeter : 0
     * StartMeter : 4
     * EndMeter : 4
     * MeterCost : 0
     * PromotionId : d77eedd8-2d09-4e1a-b7a3-4875ab421eac
     * PromotionInfoDetail : {"Title":"測試1","Info":"testetstastewsgf","StartTime":"","EndTime":""}
     */

    @SerializedName("OrderId")
    private int OrderId;
    @SerializedName("OrderNo")
    private String OrderNo;
    @SerializedName("RentSDate")
    private String RentSDate;
    @SerializedName("RentEDate")
    private String RentEDate;
    @SerializedName("RentMemberName")
    private String RentMemberName;
    @SerializedName("InvoiceType")
    private int InvoiceType;
    @SerializedName("InvoiceNo")
    private String InvoiceNo;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("StartParkinglotName")
    private String StartParkinglotName;
    @SerializedName("EndParkinglotName")
    private String EndParkinglotName;
    @SerializedName("CurrentOrderStatus")
    private int CurrentOrderStatus;
    @SerializedName("RentTotalAmount")
    private int RentTotalAmount;
    @SerializedName("RentTotalAmountByPromotion")
    private int RentTotalAmountByPromotion;
    @SerializedName("UseBounce")
    private int UseBounce;
    @SerializedName("AddBounce")
    private int AddBounce;
    @SerializedName("AfterDiscount")
    private int AfterDiscount;
    @SerializedName("CompanyName")
    private String CompanyName;
    @SerializedName("CompanySn")
    private String CompanySn;
    @SerializedName("GetCarSignature")
    private String GetCarSignature;
    @SerializedName("ReturnCarSignature")
    private String ReturnCarSignature;
    @SerializedName("RentByHour")
    private int RentByHour;
    @SerializedName("RentByMileage")
    private int RentByMileage;
    @SerializedName("Mileage")
    private int Mileage;
    @SerializedName("OrderTime")
    private String OrderTime;
    @SerializedName("UseHour")
    private int UseHour;
    @SerializedName("UseMinute")
    private int UseMinute;
    @SerializedName("UseMeter")
    private int UseMeter;
    @SerializedName("StartMeter")
    private int StartMeter;
    @SerializedName("EndMeter")
    private int EndMeter;
    @SerializedName("MeterCost")
    private int MeterCost;
    @SerializedName("PromotionId")
    private String PromotionId;
    @SerializedName("PromotionInfoDetail")
    private PromotionInfoDetailBean PromotionInfoDetail;
    @SerializedName("GetCarPic")
    private List<String> GetCarPic;
    @SerializedName("ReturnCar")
    private List<String> ReturnCar;

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String OrderNo) {
        this.OrderNo = OrderNo;
    }

    public String getRentSDate() {
        return RentSDate;
    }

    public void setRentSDate(String RentSDate) {
        this.RentSDate = RentSDate;
    }

    public String getRentEDate() {
        return RentEDate;
    }

    public void setRentEDate(String RentEDate) {
        this.RentEDate = RentEDate;
    }

    public String getRentMemberName() {
        return RentMemberName;
    }

    public void setRentMemberName(String RentMemberName) {
        this.RentMemberName = RentMemberName;
    }

    public int getInvoiceType() {
        return InvoiceType;
    }

    public void setInvoiceType(int InvoiceType) {
        this.InvoiceType = InvoiceType;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String InvoiceNo) {
        this.InvoiceNo = InvoiceNo;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getStartParkinglotName() {
        return StartParkinglotName;
    }

    public void setStartParkinglotName(String StartParkinglotName) {
        this.StartParkinglotName = StartParkinglotName;
    }

    public String getEndParkinglotName() {
        return EndParkinglotName;
    }

    public void setEndParkinglotName(String EndParkinglotName) {
        this.EndParkinglotName = EndParkinglotName;
    }

    public int getCurrentOrderStatus() {
        return CurrentOrderStatus;
    }

    public void setCurrentOrderStatus(int CurrentOrderStatus) {
        this.CurrentOrderStatus = CurrentOrderStatus;
    }

    public int getRentTotalAmount() {
        return RentTotalAmount;
    }

    public void setRentTotalAmount(int RentTotalAmount) {
        this.RentTotalAmount = RentTotalAmount;
    }

    public int getRentTotalAmountByPromotion() {
        return RentTotalAmountByPromotion;
    }

    public void setRentTotalAmountByPromotion(int RentTotalAmountByPromotion) {
        this.RentTotalAmountByPromotion = RentTotalAmountByPromotion;
    }

    public int getUseBounce() {
        return UseBounce;
    }

    public void setUseBounce(int UseBounce) {
        this.UseBounce = UseBounce;
    }

    public int getAddBounce() {
        return AddBounce;
    }

    public void setAddBounce(int AddBounce) {
        this.AddBounce = AddBounce;
    }

    public int getAfterDiscount() {
        return AfterDiscount;
    }

    public void setAfterDiscount(int AfterDiscount) {
        this.AfterDiscount = AfterDiscount;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    public String getCompanySn() {
        return CompanySn;
    }

    public void setCompanySn(String CompanySn) {
        this.CompanySn = CompanySn;
    }

    public String getGetCarSignature() {
        return GetCarSignature;
    }

    public void setGetCarSignature(String GetCarSignature) {
        this.GetCarSignature = GetCarSignature;
    }

    public String getReturnCarSignature() {
        return ReturnCarSignature;
    }

    public void setReturnCarSignature(String ReturnCarSignature) {
        this.ReturnCarSignature = ReturnCarSignature;
    }

    public int getRentByHour() {
        return RentByHour;
    }

    public void setRentByHour(int RentByHour) {
        this.RentByHour = RentByHour;
    }

    public int getRentByMileage() {
        return RentByMileage;
    }

    public void setRentByMileage(int RentByMileage) {
        this.RentByMileage = RentByMileage;
    }

    public int getMileage() {
        return Mileage;
    }

    public void setMileage(int Mileage) {
        this.Mileage = Mileage;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String OrderTime) {
        this.OrderTime = OrderTime;
    }

    public int getUseHour() {
        return UseHour;
    }

    public void setUseHour(int UseHour) {
        this.UseHour = UseHour;
    }

    public int getUseMinute() {
        return UseMinute;
    }

    public void setUseMinute(int UseMinute) {
        this.UseMinute = UseMinute;
    }

    public int getUseMeter() {
        return UseMeter;
    }

    public void setUseMeter(int UseMeter) {
        this.UseMeter = UseMeter;
    }

    public int getStartMeter() {
        return StartMeter;
    }

    public void setStartMeter(int StartMeter) {
        this.StartMeter = StartMeter;
    }

    public int getEndMeter() {
        return EndMeter;
    }

    public void setEndMeter(int EndMeter) {
        this.EndMeter = EndMeter;
    }

    public int getMeterCost() {
        return MeterCost;
    }

    public void setMeterCost(int MeterCost) {
        this.MeterCost = MeterCost;
    }

    public String getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(String PromotionId) {
        this.PromotionId = PromotionId;
    }

    public PromotionInfoDetailBean getPromotionInfoDetail() {
        return PromotionInfoDetail;
    }

    public void setPromotionInfoDetail(PromotionInfoDetailBean PromotionInfoDetail) {
        this.PromotionInfoDetail = PromotionInfoDetail;
    }

    public List<String> getGetCarPic() {
        return GetCarPic;
    }

    public void setGetCarPic(List<String> GetCarPic) {
        this.GetCarPic = GetCarPic;
    }

    public List<String> getReturnCar() {
        return ReturnCar;
    }

    public void setReturnCar(List<String> ReturnCar) {
        this.ReturnCar = ReturnCar;
    }

    public static class PromotionInfoDetailBean {
        /**
         * Title : 測試1
         * Info : testetstastewsgf
         * StartTime :
         * EndTime :
         */

        @SerializedName("Title")
        private String Title;
        @SerializedName("Info")
        private String Info;
        @SerializedName("StartTime")
        private String StartTime;
        @SerializedName("EndTime")
        private String EndTime;

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getInfo() {
            return Info;
        }

        public void setInfo(String Info) {
            this.Info = Info;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }
    }
}
