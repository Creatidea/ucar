package tw.com.car_plus.carshare.record.model;

/**
 * Created by winni on 2017/5/8.
 */

public class RentRecord {


    /**
     * OrderNo : S2017031400003
     * OrderAmount : 1326
     * BookingOnUtc : 2017-03-14T07:27:33.78
     */

    private int OrderId;
    private String OrderNo;
    private int OrderAmount;
    private String BookingOnUtc;

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String OrderNo) {
        this.OrderNo = OrderNo;
    }

    public int getOrderAmount() {
        return OrderAmount;
    }

    public void setOrderAmount(int OrderAmount) {
        this.OrderAmount = OrderAmount;
    }

    public String getBookingOnUtc() {
        return BookingOnUtc;
    }

    public void setBookingOnUtc(String BookingOnUtc) {
        this.BookingOnUtc = BookingOnUtc;
    }
}
