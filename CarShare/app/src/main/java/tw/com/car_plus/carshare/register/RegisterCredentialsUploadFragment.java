package tw.com.car_plus.carshare.register;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.util.camera.BaseCameraFragment;

/**
 * Created by winni on 2017/3/7.
 * 證件上傳
 */
@RuntimePermissions
public class RegisterCredentialsUploadFragment extends BaseCameraFragment implements View.OnClickListener {

    @BindView(R.id.layout_driver_license_positive)
    View driverLicensePositiveView;
    @BindView(R.id.layout_driver_license_negative)
    View driverLicenseNegativeView;
    @BindView(R.id.layout_id_card_positive)
    View idCardPositiveView;
    @BindView(R.id.layout_id_card_negative)
    View idCardNegativeView;
    @BindView(R.id.next)
    Button next;

    //駕照正面
    private final int DRIVER_LICENSE_POSITIVE = 22;
    //駕照反面
    private final int DRIVER_LICENSE_NEGATIVE = 33;
    //身份證正面
    private final int ID_CARD_POSITIVE = 44;
    //身分證反面
    private final int ID_CARD_NEGATIVE = 55;

    private UserAccountData userAccountData;
    private RegisterConnect registerConnect;
    private TextView driverLicensePositiveTitle, driverLicenseNegativeTitle, idCardPositiveTitle, idCardNegativeTitle;
    private ImageView driverLicensePositive, driverLicenseNegative, idCardPositive, idCardNegative;
    private String driverLicensePositivePath = "", driverLicenseNegativePath = "", idCardPositivePath = "", idCardNegativePath = "";
    private String driverLicensePositiveName = "", driverLicenseNegativeName = "", idCardPositiveName = "", idCardNegativeName = "";
    private int type;

    @Override
    protected void init() {

        setView(R.layout.fragment_register_credentials_upload);

        registerConnect = new RegisterConnect(activity);

        userAccountData = getArguments().getParcelable("UserAccountData");

        driverLicensePositiveTitle = (TextView) driverLicensePositiveView.findViewById(R.id.title);
        driverLicensePositive = (ImageView) driverLicensePositiveView.findViewById(R.id.image);

        setImageView(driverLicensePositive, driverLicensePositiveTitle, DRIVER_LICENSE_POSITIVE, getString(R.string
                .driver_license_positive), R.drawable.img_driverslicense_positive_register);

        driverLicenseNegativeTitle = (TextView) driverLicenseNegativeView.findViewById(R.id.title);
        driverLicenseNegative = (ImageView) driverLicenseNegativeView.findViewById(R.id.image);

        setImageView(driverLicenseNegative, driverLicenseNegativeTitle, DRIVER_LICENSE_NEGATIVE, getString(R.string
                .driver_license_negative), R.drawable.img_driverslicense_negative_register);

        idCardPositiveTitle = (TextView) idCardPositiveView.findViewById(R.id.title);
        idCardPositive = (ImageView) idCardPositiveView.findViewById(R.id.image);

        setImageView(idCardPositive, idCardPositiveTitle, ID_CARD_POSITIVE, getString(R.string
                .id_card_positive), R.drawable.img_id_card_positive_register);

        idCardNegativeTitle = (TextView) idCardNegativeView.findViewById(R.id.title);
        idCardNegative = (ImageView) idCardNegativeView.findViewById(R.id.image);

        setImageView(idCardNegative, idCardNegativeTitle, ID_CARD_NEGATIVE, getString(R.string
                .id_card_negative), R.drawable.img_id_card_negative_register);

        next.setOnClickListener(this);
    }

    // ----------------------------------------------------

    /**
     * 設定ImageView的相關資料
     *
     * @param imageView  imageview
     * @param textView   textview
     * @param id         imageview 要設定的d
     * @param title      textview 要設定的標題
     * @param background imageview 要設定的背景
     */
    private void setImageView(ImageView imageView, TextView textView, int id, String title, int background) {

        textView.setText(title);
        imageView.setId(id);
        imageView.setOnClickListener(this);
        Glide.with(this).load(background).into(imageView);

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        cleanPicture(driverLicensePositivePath);
        cleanPicture(driverLicenseNegativePath);
        cleanPicture(idCardPositivePath);
        cleanPicture(idCardNegativePath);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(UploadFile uploadFile) {

        switch (type) {
            case DRIVER_LICENSE_POSITIVE:
                loadingDialogManager.show();
                //上傳駕照反面
                type = DRIVER_LICENSE_NEGATIVE;
                driverLicensePositiveName = uploadFile.getFileName();
                fileUploadConnect.ConnectUploadFile(driverLicenseNegativePath);
                break;
            case DRIVER_LICENSE_NEGATIVE:
                //上傳身份證正面
                type = ID_CARD_POSITIVE;
                driverLicenseNegativeName = uploadFile.getFileName();
                fileUploadConnect.ConnectUploadFile(idCardPositivePath);
                break;
            case ID_CARD_POSITIVE:
                //上傳身分證反面
                type = ID_CARD_NEGATIVE;
                idCardPositiveName = uploadFile.getFileName();
                fileUploadConnect.ConnectUploadFile(idCardNegativePath);
                break;
            case ID_CARD_NEGATIVE:
                //將所有回來的檔名註冊證照
                loadingDialogManager.dismiss();
                idCardNegativeName = uploadFile.getFileName();
                registerConnect.connectCredentials(userAccountData, driverLicensePositiveName, driverLicenseNegativeName, idCardPositiveName,
                        idCardNegativeName);
                break;
        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }


    // ------------------------------------------------
    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case DRIVER_LICENSE_POSITIVE:
                cleanPicture(driverLicensePositivePath);
                RegisterCredentialsUploadFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, DRIVER_LICENSE_POSITIVE);
                break;
            case DRIVER_LICENSE_NEGATIVE:
                cleanPicture(driverLicenseNegativePath);
                RegisterCredentialsUploadFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, DRIVER_LICENSE_NEGATIVE);
                break;
            case ID_CARD_POSITIVE:
                cleanPicture(idCardPositivePath);
                RegisterCredentialsUploadFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, ID_CARD_POSITIVE);
                break;
            case ID_CARD_NEGATIVE:
                cleanPicture(idCardNegativePath);
                RegisterCredentialsUploadFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, ID_CARD_NEGATIVE);
                break;
            default:
                if (checkCredentialsData()) {

                    if (networkChecker.isNetworkAvailable()) {
                        //上傳駕照正面
                        type = DRIVER_LICENSE_POSITIVE;
                        fileUploadConnect.ConnectUploadFile(driverLicensePositivePath);
                    }

                } else {
                    Toast.makeText(activity, getString(R.string.check_credentials), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查是否都有拍照片
     *
     * @return
     */
    private boolean checkCredentialsData() {

        boolean isDriverLicensePositive = false, isDriverLicenseNegative = false, isIdCardPositive = false, isIdCardNegative =
                false;

        if (driverLicensePositivePath.length() != 0) {
            isDriverLicensePositive = true;
        }

        if (driverLicenseNegativePath.length() != 0) {
            isDriverLicenseNegative = true;
        }

        if (idCardPositivePath.length() != 0) {
            isIdCardPositive = true;
        }

        if (idCardNegativePath.length() != 0) {
            isIdCardNegative = true;
        }

        return isDriverLicensePositive && isDriverLicenseNegative && isIdCardPositive && isIdCardNegative;
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        RegisterCredentialsUploadFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意開啟相機與寫入資料的權限
     */
    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void goToTakePicture(int type) {
        takePictures(type);
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showDeniedForTakePicture() {
        Toast.makeText(activity, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            compressionPicture(new CompressionPictureListener() {
                @Override
                public void compressionListener() {

                    switch (requestCode) {
                        case DRIVER_LICENSE_POSITIVE:
                            driverLicensePositivePath = setImageView(driverLicensePositive);
                            break;
                        case DRIVER_LICENSE_NEGATIVE:
                            driverLicenseNegativePath = setImageView(driverLicenseNegative);
                            break;
                        case ID_CARD_POSITIVE:
                            idCardPositivePath = setImageView(idCardPositive);
                            break;
                        case ID_CARD_NEGATIVE:
                            idCardNegativePath = setImageView(idCardNegative);
                            break;
                    }
                }
            });
        }
    }

}
