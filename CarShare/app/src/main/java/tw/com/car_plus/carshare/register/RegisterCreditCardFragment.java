package tw.com.car_plus.carshare.register;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.LayoutInflater;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.CreditCardConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.CreditCards;
import tw.com.car_plus.carshare.register.model.CreditCardsData;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/3/8.
 * 註冊信用卡
 */

public class RegisterCreditCardFragment extends CustomBaseFragment {

    @BindView(R.id.layout_code_1)
    View layoutCode1;
    @BindView(R.id.layout_code_2)
    View layoutCode2;
    @BindView(R.id.layout_code_3)
    View layoutCode3;
    @BindView(R.id.layout_code_4)
    View layoutCode4;
    @BindView(R.id.card_security_code)
    EditText cardSecurityCode;
    @BindView(R.id.birthday_title)
    TextView dateTitle;
    @BindView(R.id.birthday)
    EditText date;
    @BindView(R.id.next)
    Button next;

    //信用卡第一組卡號
    private final int CODE_1 = 22;
    //信用卡第二組卡號
    private final int CODE_2 = 33;
    //信用卡第三組卡號
    private final int CODE_3 = 44;
    //信用卡第四組卡號
    private final int CODE_4 = 55;

    private static final int MAX_MONTH = 12;
    private static final int MIN_MONTH = 1;
    private static final int MAX_YEAR = 2099;

    private CreditCardConnect creditCardConnect;
    private UserAccountData userAccountData;
    private EditText code1, code2, code3, code4;
    private int validityYear, validityMonth;
    private int from = 0;

    @Override
    protected void init() {

        setView(R.layout.fragment_register_credit_card);

        userAccountData = getArguments().getParcelable("UserAccountData");
        creditCardConnect = new CreditCardConnect(activity);
        from = getArguments().getInt("from");

        code1 = (EditText) layoutCode1.findViewById(R.id.card_number);
        setEditTextData(code1, CODE_1, true);
        code2 = (EditText) layoutCode2.findViewById(R.id.card_number);
        setEditTextData(code2, CODE_2, true);
        code3 = (EditText) layoutCode3.findViewById(R.id.card_number);
        setEditTextData(code3, CODE_3, true);
        code4 = (EditText) layoutCode4.findViewById(R.id.card_number);
        setEditTextData(code4, CODE_4, false);

        dateTitle.setText(getString(R.string.validity_period));
        date.setHint(getString(R.string.validity_period_hint));

        if (from == EventCenter.USER) {
            next.setText(getString(R.string.confirm));
        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(CreditCardsData creditCardsData) {

        if (from == EventCenter.REGISTER) {

            if (creditCardsData != null) {
                EventCenter.getInstance().sendSuccessEvent(userAccountData);
            }

        } else if (from == EventCenter.USER) {
            creditCardConnect.getUserCreditCard();
            activity.onBackPressed();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.calendar, R.id.next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.calendar:
                showDateDialog();
                break;
            case R.id.next:

                if (checkHaveData(code1, code2, code3, code4, cardSecurityCode, date)) {
                    if (checkData()) {
                        creditCardConnect.connectRegisterCreditCard(getCreditCardsData());
                    }
                } else {
                    EventCenter.getInstance().sendSuccessEvent(userAccountData);
                }

                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查資料
     *
     * @return
     */
    private boolean checkData() {

        boolean isCode1, isCode2, isCode3, isCode4, isValidityDate = false, isSecurityCode = false;

        isCode1 = checkCode(code1);
        isCode2 = checkCode(code2);
        isCode3 = checkCode(code3);
        isCode4 = checkCode(code4);

        if (date.getText().toString().length() != 0) {
            isValidityDate = true;
        } else {
            date.setError(getString(R.string.format_error));
        }

        if (cardSecurityCode.getText().toString().length() == 3) {
            isSecurityCode = true;
        } else {
            cardSecurityCode.setError(getString(R.string.format_error));
        }

        return isCode1 && isCode2 && isCode3 && isCode4 && isValidityDate && isSecurityCode;

    }

    // ----------------------------------------------------

    /**
     * 檢查有欄位填寫就設定為必填
     *
     * @param editTexts
     * @return
     */
    private boolean checkHaveData(EditText... editTexts) {

        for (EditText editText : editTexts) {
            if (editText.getText().toString().length() > 0) {
                return true;
            }
        }
        return false;
    }

    // ----------------------------------------------------

    /**
     * 檢查信用卡卡號
     *
     * @param text
     * @return
     */
    private boolean checkCode(EditText text) {
        if (text.getText().toString().length() == 4) {
            return true;
        } else {
            text.setError(getString(R.string.format_error));
            return false;
        }
    }

    // ----------------------------------------------------

    /**
     * 取得信用卡資料
     *
     * @return
     */
    private CreditCards getCreditCardsData() {

        CreditCards creditCards = new CreditCards();

        List<CreditCards.CreditCardsBean> creditCardsList = new ArrayList<>();

        CreditCards.CreditCardsBean creditCardsBean = new CreditCards.CreditCardsBean();
        creditCardsBean.setCardNo(String.format(getString(R.string.credit_card_no), code1.getText().toString(), code2.getText().toString(), code3
                .getText().toString(), code4.getText().toString()));
        creditCardsBean.setGtMonth(String.format("%02d", validityMonth));
        creditCardsBean.setGtYear(String.valueOf(validityYear));
        creditCardsBean.setSecurityCode(cardSecurityCode.getText().toString());
        creditCardsList.add(creditCardsBean);

        creditCards.setAcctId(userAccountData.getAcctId());
        creditCards.setCreditCards(creditCardsList);

        return creditCards;
    }

    // ----------------------------------------------------

    /**
     * 設定EditText的資料
     *
     * @param editText
     * @param tag
     */
    private void setEditTextData(EditText editText, int tag, boolean isAddTextChangedListener) {
        editText.setTag(tag);
        if (isAddTextChangedListener) {
            editText.addTextChangedListener(new GenericTextWatcher(editText));
        }
    }


    // ----------------------------------------------------
    //Declaration
    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            String text = editable.toString();

            if (text.length() == 4) {

                switch ((int) view.getTag()) {
                    case CODE_1:
                        code2.requestFocus();
                        break;
                    case CODE_2:
                        code3.requestFocus();
                        break;
                    case CODE_3:
                        code4.requestFocus();
                        break;
                }
            }

        }
    }

    // ----------------------------------------------------

    /**
     * 顯示選擇有效期限的dialog
     */
    private void showDateDialog() {

        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dailog_credit_card_date, null);

        final NumberPicker yearPicker = (NumberPicker) dialogView.findViewById(R.id.picker_year);
        final NumberPicker monthPicker = (NumberPicker) dialogView.findViewById(R.id.picker_month);

        monthPicker.setMinValue(MIN_MONTH);
        monthPicker.setMaxValue(MAX_MONTH);
        monthPicker.setValue(mm);

        yearPicker.setMinValue(yy);
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(yy);

        dialogBuilder.setTitle(getString(R.string.validity_period_title));
        dialogBuilder.setView(dialogView)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String format = String.format(getString(R.string.validity_period_format), monthPicker.getValue(), yearPicker.getValue());
                        date.setText(format);
                        validityYear = yearPicker.getValue();
                        validityMonth = monthPicker.getValue();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNeutralButton(R.string.clean, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        date.setText("");
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

}
