package tw.com.car_plus.carshare.register;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.login.LoginFragment;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.register.provision.RegisterAgreeProvisionFragment;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.UserUtil;

import static tw.com.car_plus.carshare.index.UserStatus.REGISTER_NONE;
import static tw.com.car_plus.carshare.index.UserStatus.USER_CREDENTIALS;
import static tw.com.car_plus.carshare.index.UserStatus.USER_CREDIT_CARD;
import static tw.com.car_plus.carshare.index.UserStatus.USER_IDENTIFYING_CODE;
import static tw.com.car_plus.carshare.index.UserStatus.USER_INFO;

/**
 * Created by winni on 2017/3/2.
 * 註冊
 */

@RuntimePermissions
public class RegisterFragment extends CustomBaseFragment {

    @BindView(R.id.step_2)
    ImageView step2;
    @BindView(R.id.step_3)
    ImageView step3;
    @BindView(R.id.step_4)
    ImageView step4;
    @BindView(R.id.step_5)
    ImageView step5;

    private static final int PAGE_USER_INFO = 200;
    private static final int PAGE_IDENTIFYING = 201;
    private static final int PAGE_IMAGE_UPLOAD = 202;
    private static final int PAGE_CREDIT_CARD = 203;
    private static final int PAGE_PROVISION = 204;

    private SharedPreferenceUtil sp;
    private UserUtil userUtil;
    private ImageView toolbarImage;
    @RegisterPage
    private int registerPage;
    private boolean isAgree = false;

    // ----------------------------------------------------

    /**
     * 註冊頁面
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PAGE_USER_INFO, PAGE_IDENTIFYING, PAGE_IMAGE_UPLOAD, PAGE_CREDIT_CARD, PAGE_PROVISION})
    public @interface RegisterPage {
    }

    // ----------------------------------------------------
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.toolbarImage = ((IndexActivity) context).toolbarImage;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_register);

        RegisterFragmentPermissionsDispatcher.goToReceiveSmsWithCheck(this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            isAgree = true;
        }

        sp = new SharedPreferenceUtil(activity);
        userUtil = new UserUtil(activity, sp);
        startRegisterPage(UserStatus.registerStatus);
    }

    // ----------------------------------------------------

    /**
     * 根據已完成的註冊階段，前往註冊的開始畫面
     *
     * @param registerStatus
     */
    private void startRegisterPage(@UserStatus.RegisterStatus int registerStatus) {

        UserAccountData userAccountData = null;

        if (registerStatus == REGISTER_NONE) {
            registerPage = PAGE_USER_INFO;

        } else {

            userAccountData = new UserAccountData();
            userAccountData.setAcctId(userUtil.getUser().getAcctId());
            userAccountData.setLoginId(userUtil.getUser().getLoginId());

            switch (registerStatus) {
                case USER_INFO:
                    setImageViewSelected(step2);
                    registerPage = PAGE_IDENTIFYING;
                    break;
                case USER_IDENTIFYING_CODE:
                    setImageViewSelected(step2, step3);
                    registerPage = PAGE_IMAGE_UPLOAD;
                    break;
                case USER_CREDENTIALS:

                    if (isCompanyAccount(userUtil.getUser())) {
                        setImageViewSelected(step2, step3, step4, step5);
                        registerPage = PAGE_PROVISION;
                    } else {
                        setImageViewSelected(step2, step3, step4);
                        registerPage = PAGE_CREDIT_CARD;
                    }

                    break;
                case USER_CREDIT_CARD:
                    setImageViewSelected(step2, step3, step4, step5);
                    registerPage = PAGE_PROVISION;
                    break;
            }

            new DialogUtil(activity).setMessage(R.string.register_error).setConfirmButton(R.string.confirm, null).showDialog(false);

        }

        goToFragment(registerPage, userAccountData);

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    //--------------------------------------------------
    @Subscribe
    public void onSuccessEvent(UserAccountData userAccountData) {

        if (userAccountData != null) {

            if (registerPage != PAGE_PROVISION) {

                switch (registerPage) {
                    case PAGE_USER_INFO:

                        if (userUtil.getUser().getLoadMemberDataResult() != null) {
                            registerPage = PAGE_IMAGE_UPLOAD;
                            setImageViewSelected(step2, step3);
                        } else {
                            registerPage = PAGE_IDENTIFYING;
                            step2.setSelected(true);
                        }

                        break;
                    case PAGE_IDENTIFYING:
                        registerPage = PAGE_IMAGE_UPLOAD;
                        step3.setSelected(true);
                        break;
                    case PAGE_IMAGE_UPLOAD:
                        registerPage = PAGE_CREDIT_CARD;
                        step4.setSelected(true);
                        break;
                    case PAGE_CREDIT_CARD:
                        registerPage = PAGE_PROVISION;
                        step5.setSelected(true);
                        break;
                }

                goToFragment(registerPage, userAccountData);

            } else {
                goToPage();
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.REGISTER) {

            User user = (User) data.get("data");
            userUtil.setUser(user);
            userUtil.setAccountInfo();
            sp.setUserData(user);

            replaceFragment(new MapModeFragment(), false);
        }

    }

    // ----------------------------------------------------

    /**
     * 完成註冊時前往的頁面
     */
    private void goToPage() {

        User user = sp.getUserData();

        //註冊未完成或舊用戶
        if (UserStatus.verifyStatus == UserStatus.NEW_MEMBER || user.getLoadMemberDataResult() != null) {

            if (isCompanyAccount(user)) {
                new LoginConnect(activity).connectLoginCompany(EventCenter.REGISTER, user.getLoginId(), user.getPassword(), user
                        .getCompanyAccount().getCompanyId());
            } else {
                new LoginConnect(activity).connectLogin(EventCenter.REGISTER, user.getLoginId(), user.getPassword());
            }

        } else {
            replaceFragment(new LoginFragment(), false);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        RegisterFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意讀取簡訊與監聽簡訊的權限
     */
    @NeedsPermission({Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS})
    void goToReceiveSms() {
        isAgree = true;
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS})
    void showDeniedForReceiveSms() {
        isAgree = false;
    }

    //--------------------------------------------------

    /**
     * 前往下一個步驟的fragment
     *
     * @param registerPage
     */
    private void goToFragment(@RegisterPage int registerPage, UserAccountData userAccountData) {

        Fragment fragment = null;

        Bundle bundle = new Bundle();

        switch (registerPage) {
            case PAGE_USER_INFO:
                toolbarImage.setImageResource(R.drawable.img_toolbar_register);
                fragment = new RegisterInfoFragment();
                break;
            case PAGE_IDENTIFYING:
                toolbarImage.setImageResource(R.drawable.img_toolbar_identifying_code);
                bundle.putBoolean("IsAgree", isAgree);
                fragment = new RegisterIdentifyingCodeFragment();
                break;
            case PAGE_IMAGE_UPLOAD:
                toolbarImage.setImageResource(R.drawable.img_toolbar_upload);
                fragment = new RegisterCredentialsUploadFragment();
                break;
            case PAGE_CREDIT_CARD:
                toolbarImage.setImageResource(R.drawable.img_toolbar_credit_card);
                bundle.putInt("from", EventCenter.REGISTER);
                fragment = new RegisterCreditCardFragment();
                break;
            case PAGE_PROVISION:
                toolbarImage.setImageResource(R.drawable.img_toolbar_provision);
                fragment = new RegisterAgreeProvisionFragment();
                break;
        }

        if (userAccountData != null) {
            bundle.putParcelable("UserAccountData", userAccountData);
        }

        replaceFragment(R.id.container_frame_layout, fragment, false, bundle);

    }

    // -------------------------------------------------------

    /**
     * 將ImageView的select設定為true
     *
     * @param imageViews
     */
    private void setImageViewSelected(ImageView... imageViews) {
        for (ImageView imageView : imageViews) {
            imageView.setSelected(true);
        }
    }

    //--------------------------------------------------

}
