package tw.com.car_plus.carshare.register;

import android.content.IntentFilter;
import android.view.View;

import android.widget.EditText;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.net.NetworkChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.SMSBroadcastReceiver;

/**
 * Created by winni on 2017/3/6.
 * 確認驗證碼
 */

public class RegisterIdentifyingCodeFragment extends CustomBaseFragment {

    @BindView(R.id.identifying_code)
    EditText identifyingCode;

    private NetworkChecker networkChecker;
    private SMSBroadcastReceiver broadcast;
    private RegisterConnect registerConnect;
    private UserAccountData userAccountData;
    private boolean isAgree;

    @Override
    protected void init() {
        setView(R.layout.fragment_register_identifying_code);
        networkChecker = NetworkChecker.getInstance(activity);
        registerConnect = new RegisterConnect(activity);

        userAccountData = getArguments().getParcelable("UserAccountData");
        isAgree = getArguments().getBoolean("IsAgree", false);

        if (userAccountData != null) {
            registerConnect.connectVerifyCode(userAccountData.getAcctId(), userAccountData.getLoginId());
        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Boolean isSuccess) {
        if (isSuccess && isAgree) {
            startListener();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcast != null) {
            closeListener();
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.send, R.id.again})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                if (identifyingCode.getText().toString().length() > 0) {
                    if (networkChecker.isNetworkAvailable()) {
                        registerConnect.connectCheckEffectiveCode(userAccountData, identifyingCode.getText().toString());
                    }
                } else {
                    identifyingCode.setError(getString(R.string.format_error));
                }
                break;
            case R.id.again:
                registerConnect.connectVerifyCode(userAccountData.getAcctId(), userAccountData.getLoginId());
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 啟動監聽
     */
    private void startListener() {

        broadcast = new SMSBroadcastReceiver();
        broadcast.setOnReceivedMessageListener(new SMSBroadcastReceiver.MessageListener() {
            @Override
            public void OnReceived(String code) {
                identifyingCode.setText(code);
                identifyingCode.setSelection(identifyingCode.getText().toString().length());
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter.setPriority(1000);
        activity.registerReceiver(broadcast, filter);

    }

    // ----------------------------------------------------------------

    /**
     * 關閉Receiver監聽事件
     */
    public void closeListener() {
        activity.unregisterReceiver(broadcast);
    }
}
