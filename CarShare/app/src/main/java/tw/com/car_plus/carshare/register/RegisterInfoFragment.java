package tw.com.car_plus.carshare.register;

import android.app.DatePickerDialog;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.net.NetworkChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.adapter.CustomSpinnerAdapter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.register.model.RegisterInfo;
import tw.com.car_plus.carshare.util.CheckDataUtil;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.address.AddressUtil;
import tw.com.car_plus.carshare.util.view.CustomSpinner;

/**
 * Created by winni on 2017/3/2.
 * 會員資訊
 */

public class RegisterInfoFragment extends CustomBaseFragment implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener,
        View.OnTouchListener {

    @BindView(R.id.account)
    EditText account;
    @BindView(R.id.password_title)
    TextView passwordTitle;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.password_check)
    EditText passwordCheck;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.birthday)
    EditText birthday;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.is_same)
    CheckBox isSame;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.census_address)
    View censusAddress;
    @BindView(R.id.address)
    View address;

    private final String ADDRESS_CENSUS_CITY = "AddressCensusCity";
    private final String ADDRESS_CENSUS_AREA = "AddressCensusArea";
    private final String ADDRESS_CITY = "AddressCity";
    private final String ADDRESS_AREA = "AddressArea";

    private NetworkChecker networkChecker;
    private RegisterConnect registerConnect;
    private CustomSpinnerAdapter customSpinnerAdapter;
    private CheckDataUtil checkDataUtil;
    private AddressUtil addressUtil;
    private SharedPreferenceUtil sp;
    private User.LoadMemberDataResultBean loadMemberDataResultBean;
    private EditText addressCensusStreets, addressStreets;
    private CustomSpinner addressCensusCity, addressCensusArea, addressCity, addressArea;
    private TextView addressCityTitle, addressAreaTitle, addressStreetsTitle, addressCensusHint, addressHint;
    private int censusCityId = -1, censusAreaId = -1, cityId = -1, areaId = -1;
    private List<City> cities;
    private List<Area> censusAreas, areas;
    //是否為戶籍地址
    private boolean isCensus = true;
    private boolean isSetUserData = false;
    private String strAddress = "";
    private boolean isLoadCity = false;

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_register_info);

        networkChecker = NetworkChecker.getInstance(activity);
        registerConnect = new RegisterConnect(activity);
        checkDataUtil = new CheckDataUtil();
        addressUtil = new AddressUtil(activity);
        sp = new SharedPreferenceUtil(activity);

        isSame.setOnCheckedChangeListener(this);
        setAddressView();
    }

    // ----------------------------------------------------
    private void setAddressView() {

        addressCensusCity = (CustomSpinner) censusAddress.findViewById(R.id.address_city);
        setSpinnerData(addressCensusCity, ADDRESS_CENSUS_CITY, true);

        addressCensusArea = (CustomSpinner) censusAddress.findViewById(R.id.address_area);
        setSpinnerData(addressCensusArea, ADDRESS_CENSUS_AREA, false);

        addressCity = (CustomSpinner) address.findViewById(R.id.address_city);
        setSpinnerData(addressCity, ADDRESS_CITY, true);

        addressArea = (CustomSpinner) address.findViewById(R.id.address_area);
        setSpinnerData(addressArea, ADDRESS_AREA, false);

        addressCensusHint = (TextView) censusAddress.findViewById(R.id.address_area_hint);
        addressCensusStreets = (EditText) censusAddress.findViewById(R.id.address_streets);
        addressHint = (TextView) address.findViewById(R.id.address_area_hint);
        addressStreets = (EditText) address.findViewById(R.id.address_streets);
        addressCityTitle = (TextView) address.findViewById(R.id.address_city_title);
        addressAreaTitle = (TextView) address.findViewById(R.id.address_area_title);
        addressStreetsTitle = (TextView) address.findViewById(R.id.address_streets_title);

        addressCityTitle.setText(getString(R.string.address_city));
        addressAreaTitle.setText(getString(R.string.address_area));
        addressStreetsTitle.setText(getString(R.string.address_streets));

        setUserData();
    }

    // ----------------------------------------------------

    /**
     * 設定使用者資料
     */
    private void setUserData() {

        User user = sp.getUserData();

        if (user != null && user.getLoadMemberDataResult() != null) {
            loadMemberDataResultBean = user.getLoadMemberDataResult();
            isSetUserData = true;

            account.setText(loadMemberDataResultBean.getLoginID());
            account.setEnabled(false);
            password.setText(user.getPassword());
            name.setText(loadMemberDataResultBean.getAcctName());
            birthday.setText(loadMemberDataResultBean.getBirth());
            phone.setText(loadMemberDataResultBean.getMaincell());
            email.setText(loadMemberDataResultBean.getEmail());
            addressCensusStreets.setText(loadMemberDataResultBean.getHHRAddr());
            addressStreets.setText(loadMemberDataResultBean.getAddr());
            censusCityId = loadMemberDataResultBean.getHHRCityID();
            cityId = loadMemberDataResultBean.getCityID();
            censusAreaId = loadMemberDataResultBean.getHHRAreaID();
            areaId = loadMemberDataResultBean.getAreaID();

        } else {

            addressCensusArea.setOnItemSelectedListener(this);
            addressArea.setOnItemSelectedListener(this);
            addressCensusCity.setOnItemSelectedListener(this);
            addressCity.setOnItemSelectedListener(this);
        }
    }

    // ----------------------------------------------------

    /**
     * 設定spinner的資料
     *
     * @param spinner
     * @param tag       spinner的tag
     * @param isOnTouch 是否需要監聽OnTouch事件
     */
    private void setSpinnerData(CustomSpinner spinner, String tag, boolean isOnTouch) {
        spinner.setTag(tag);
        if (isOnTouch) {
            spinner.setOnTouchListener(this);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        if (!isLoadCity) {
            addressUtil.loadCity();
        }

        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.calendar, R.id.next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.calendar:
                showDateDialog();
                break;
            case R.id.next:
                if (checkData() && networkChecker.isNetworkAvailable()) {
                    registerConnect.connectSendUserInfo(getRegisterInfo());
                }
                break;
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.CITY) {
            isLoadCity = true;

            cities = (List<City>) data.get("data");
            addressUtil.setCities(cities);
            customSpinnerAdapter = new CustomSpinnerAdapter(activity, cities, null);
            addressCensusCity.setAdapter(customSpinnerAdapter);
            addressCity.setAdapter(customSpinnerAdapter);

            if (loadMemberDataResultBean != null && isSetUserData) {

                addressCensusCity.setSelection(addressUtil.getCityPosition(loadMemberDataResultBean.getHHRCityID()), false);
                addressCity.setSelection(addressUtil.getCityPosition(loadMemberDataResultBean.getCityID()), false);
                addressUtil.loadArea(loadMemberDataResultBean.getHHRCityID());
                addressCensusCity.setOnItemSelectedListener(this);
                addressCity.setOnItemSelectedListener(this);

            }

        } else if ((int) data.get("type") == EventCenter.AREA) {

            if (isCensus) {
                addressCensusHint.setVisibility(View.GONE);
                censusAreas = (List<Area>) data.get("data");
                customSpinnerAdapter = new CustomSpinnerAdapter(activity, null, censusAreas);
                addressCensusArea.setAdapter(customSpinnerAdapter);

                if (loadMemberDataResultBean != null && isSetUserData) {
                    addressCensusArea.setSelection(addressUtil.getAreaPosition(censusAreas, loadMemberDataResultBean.getHHRAreaID()), false);
                    addressCensusArea.setOnItemSelectedListener(this);

                    addressUtil.loadArea(loadMemberDataResultBean.getCityID());
                    isCensus = false;
                }

            } else {
                addressHint.setVisibility(View.GONE);
                areas = (List<Area>) data.get("data");
                customSpinnerAdapter = new CustomSpinnerAdapter(activity, null, areas);
                addressArea.setAdapter(customSpinnerAdapter);

                if (loadMemberDataResultBean != null && isSetUserData) {
                    addressArea.setSelection(addressUtil.getAreaPosition(areas, loadMemberDataResultBean.getAreaID()), false);
                    addressArea.setOnItemSelectedListener(this);
                    isSetUserData = false;
                    isCensus = true;
                }
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    //--------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (networkChecker.isNetworkAvailable()) {

            String tag = (String) parent.getTag();

            switch (tag) {
                case ADDRESS_CENSUS_CITY:
                    censusCityId = cities.get(position).getCityId();
                    addressUtil.loadArea(cities.get(position).getCityId());
                    break;
                case ADDRESS_CENSUS_AREA:
                    censusAreaId = censusAreas.get(position).getAreaId();
                    break;
                case ADDRESS_CITY:
                    cityId = cities.get(position).getCityId();
                    addressUtil.loadArea(cities.get(position).getCityId());
                    break;
                case ADDRESS_AREA:
                    areaId = areas.get(position).getAreaId();
                    break;
            }
        }

    }

    //--------------------------------------------------
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //--------------------------------------------------
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        String tag = (String) v.getTag();

        switch (tag) {
            case ADDRESS_CENSUS_CITY:
                isCensus = true;
                break;
            case ADDRESS_CITY:
                isCensus = false;
                break;
        }

        return false;
    }

    //--------------------------------------------------
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            address.setVisibility(View.GONE);
            cityId = censusCityId;
            areaId = censusAreaId;
            strAddress = addressCensusStreets.getText().toString();
        } else {
            address.setVisibility(View.VISIBLE);
            cityId = -1;
            areaId = -1;
            customSpinnerAdapter = new CustomSpinnerAdapter(activity, cities, null);
            addressCity.setAdapter(customSpinnerAdapter);
            customSpinnerAdapter = new CustomSpinnerAdapter(activity, null, new ArrayList<Area>());
            addressArea.setAdapter(customSpinnerAdapter);
            addressHint.setVisibility(View.VISIBLE);
            addressStreets.setText("");
        }

    }

    //--------------------------------------------------

    /**
     * 顯示日期的Dialog
     */
    private void showDateDialog() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                String format = String.format(getString(R.string.date_format), year, month + 1, day);
                birthday.setText(format);
            }

        }, mYear, mMonth, mDay);

        c.set(mYear - 20, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    //--------------------------------------------------

    /**
     * 檢查資料
     *
     * @return
     */
    private boolean checkData() {

        boolean isAccount = false, isPassword = false, isPasswordCheck = false, isName = false, isBirthday = false, isPhone = false, isEmail =
                false, isCensusCity = false, isCensusArea = false, isCensusStreets = false, isStreets = true, isCity = true,
                isArea = true;

        if (birthday.getText().toString().length() > 0) {
            isBirthday = true;
        } else {
            birthday.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkPassword(6, 16, password.getText().toString())) {
            isPassword = true;
        } else {
            password.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkPassword(6, 16, passwordCheck.getText().toString()) && passwordCheck.getText().toString().equals(password.getText()
                .toString())) {
            isPasswordCheck = true;
        } else {
            passwordCheck.setError(getString(R.string.format_error));
        }

        if (name.getText().toString().length() > 0) {
            isName = true;
        } else {
            name.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkAccount(account.getText().toString())) {
            isAccount = true;
        } else {
            account.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkEmail(email.getText().toString())) {
            isEmail = true;
        } else {
            email.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkPhone(phone.getText().toString())) {
            isPhone = true;
        } else {
            phone.setError(getString(R.string.format_error));
        }

        if (checkDataUtil.checkCity(censusCityId)) {
            isCensusCity = true;
        }

        if (checkDataUtil.checkArea(censusAreaId)) {
            isCensusArea = true;
        }

        if (checkDataUtil.checkStreets(addressCensusStreets.getText().toString())) {
            isCensusStreets = true;
        } else {
            addressCensusStreets.setError(getString(R.string.format_error));
        }

        if (!isSame.isChecked()) {
            if (!checkDataUtil.checkStreets(addressStreets.getText().toString())) {
                isStreets = false;
                addressStreets.setError(getString(R.string.format_error));
            } else {
                strAddress = addressStreets.getText().toString();
            }

            if (!checkDataUtil.checkCity(cityId)) {
                isCity = false;
            }

            if (!checkDataUtil.checkArea(areaId)) {
                isArea = false;
            }
        }

        return isAccount && isPassword && isPasswordCheck && isName && isBirthday && isPhone && isEmail && isCensusStreets &&
                isCensusCity && isCensusArea && isStreets && isCity && isArea;
    }

    //--------------------------------------------------

    /**
     * 取得註冊的資料
     *
     * @return
     */
    private RegisterInfo getRegisterInfo() {

        RegisterInfo registerInfo = new RegisterInfo();
        registerInfo.setLoginId(account.getText().toString());
        registerInfo.setPwd(password.getText().toString());
        registerInfo.setCheckPwd(passwordCheck.getText().toString());
        registerInfo.setChtName(name.getText().toString());
        registerInfo.setBirthday(birthday.getText().toString());
        registerInfo.setCellPhone(phone.getText().toString());
        registerInfo.setEmail(email.getText().toString());
        registerInfo.setHhrCityId(censusCityId);
        registerInfo.setHhrAreaId(censusAreaId);
        registerInfo.setHhrAddr(addressCensusStreets.getText().toString());
        registerInfo.setMailingCityId(cityId);
        registerInfo.setMailingAreaId(areaId);
        registerInfo.setMailingAddr(strAddress);
        registerInfo.setSubscriptionEDM("N");
        registerInfo.setReceiveSMS("N");

        return registerInfo;
    }

    //--------------------------------------------------
}
