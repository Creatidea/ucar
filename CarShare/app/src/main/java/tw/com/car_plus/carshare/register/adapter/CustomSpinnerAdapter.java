package tw.com.car_plus.carshare.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;

/**
 * Created by winni on 2017/3/3.
 */

public class CustomSpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<City> cities;
    private List<Area> areas;

    public CustomSpinnerAdapter(Context context, List<City> cities, List<Area> areas) {
        this.context = context;
        this.cities = cities;
        this.areas = areas;
    }

    //--------------------------------------------------
    @Override
    public int getCount() {

        if (cities != null) {
            return cities.size();
        } else if (areas != null) {
            return areas.size();
        } else {
            return 0;
        }
    }

    //--------------------------------------------------
    @Override
    public Object getItem(int position) {
        return null;
    }

    //--------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    //--------------------------------------------------
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.spinner_item, null);
        if (convertView != null) {
            TextView title = (TextView) convertView.findViewById(R.id.title);

            if (cities != null) {
                title.setText(cities.get(position).getCityName());
            } else if (areas != null) {
                title.setText(areas.get(position).getAreaName());
            }
        }
        return convertView;
    }

}
