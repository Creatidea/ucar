package tw.com.car_plus.carshare.register.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/3/2.
 */

public class Area implements Parcelable {


    /**
     * CityId : 3
     * AreaId : 20
     * AreaName : 萬里區
     */

    private int CityId;
    private int AreaId;
    private String AreaName;

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int CityId) {
        this.CityId = CityId;
    }

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int AreaId) {
        this.AreaId = AreaId;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String AreaName) {
        this.AreaName = AreaName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CityId);
        dest.writeInt(this.AreaId);
        dest.writeString(this.AreaName);
    }

    public Area() {
    }

    protected Area(Parcel in) {
        this.CityId = in.readInt();
        this.AreaId = in.readInt();
        this.AreaName = in.readString();
    }

    public static final Parcelable.Creator<Area> CREATOR = new Parcelable.Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel source) {
            return new Area(source);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };
}
