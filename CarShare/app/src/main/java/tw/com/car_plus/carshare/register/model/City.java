package tw.com.car_plus.carshare.register.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/3/2.
 */

public class City implements Parcelable {


    /**
     * CityId : 2
     * CityName : 基隆市
     */

    private int CityId;
    private String CityName;

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int CityId) {
        this.CityId = CityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String CityName) {
        this.CityName = CityName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CityId);
        dest.writeString(this.CityName);
    }

    public City() {
    }

    protected City(Parcel in) {
        this.CityId = in.readInt();
        this.CityName = in.readString();
    }

    public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {
        @Override
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };
}
