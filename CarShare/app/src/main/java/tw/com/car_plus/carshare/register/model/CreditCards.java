package tw.com.car_plus.carshare.register.model;

import java.util.List;

/**
 * Created by winni on 2017/3/8.
 */

public class CreditCards {


    /**
     * AcctId : string
     * CreditCards : [{"CardNo":"string","GtYear":"string","GtMonth":"string","SecurityCode":"string"}]
     */

    private String AcctId;
    private List<CreditCardsBean> CreditCards;

    public String getAcctId() {
        return AcctId;
    }

    public void setAcctId(String AcctId) {
        this.AcctId = AcctId;
    }

    public List<CreditCardsBean> getCreditCards() {
        return CreditCards;
    }

    public void setCreditCards(List<CreditCardsBean> CreditCards) {
        this.CreditCards = CreditCards;
    }

    public static class CreditCardsBean {
        /**
         * CardNo : string
         * GtYear : string
         * GtMonth : string
         * SecurityCode : string
         */

        private String CardNo;
        private String GtYear;
        private String GtMonth;
        private String SecurityCode;

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String CardNo) {
            this.CardNo = CardNo;
        }

        public String getGtYear() {
            return GtYear;
        }

        public void setGtYear(String GtYear) {
            this.GtYear = GtYear;
        }

        public String getGtMonth() {
            return GtMonth;
        }

        public void setGtMonth(String GtMonth) {
            this.GtMonth = GtMonth;
        }

        public String getSecurityCode() {
            return SecurityCode;
        }

        public void setSecurityCode(String SecurityCode) {
            this.SecurityCode = SecurityCode;
        }
    }
}
