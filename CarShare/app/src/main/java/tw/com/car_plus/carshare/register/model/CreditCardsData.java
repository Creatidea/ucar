package tw.com.car_plus.carshare.register.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by winni on 2017/3/8.
 */

public class CreditCardsData {


    /**
     * Errors : []
     * Status : true
     * CreditCards : [{"SerialNo":"TeU/aLdclAUXQ1BautfteA==","IsValid":true,"CardNo":"3560-5807-0000-2402"}]
     * AcctId : 194511
     */

    private String AcctId;
    private List<CreditCardsBean> CreditCards;

    public String getAcctId() {
        return AcctId;
    }

    public void setAcctId(String AcctId) {
        this.AcctId = AcctId;
    }

    public List<CreditCardsBean> getCreditCards() {
        return CreditCards;
    }

    public void setCreditCards(List<CreditCardsBean> CreditCards) {
        this.CreditCards = CreditCards;
    }

    public static class CreditCardsBean implements Parcelable {
        /**
         * SerialNo : TeU/aLdclAUXQ1BautfteA==
         * IsValid : true
         * CardNo : 3560-5807-0000-2402
         */

        private String SerialNo;
        private boolean IsValid;
        private String CardNo;
        private String GtYear;
        private String GtMonth;
        private String SecurityCode;
        private boolean IsDefault;

        public String getSerialNo() {
            return SerialNo;
        }

        public void setSerialNo(String SerialNo) {
            this.SerialNo = SerialNo;
        }

        public boolean isIsValid() {
            return IsValid;
        }

        public void setIsValid(boolean IsValid) {
            this.IsValid = IsValid;
        }

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String CardNo) {
            this.CardNo = CardNo;
        }

        public String getGtYear() {
            return GtYear;
        }

        public void setGtYear(String gtYear) {
            GtYear = gtYear;
        }

        public String getGtMonth() {
            return GtMonth;
        }

        public void setGtMonth(String gtMonth) {
            GtMonth = gtMonth;
        }

        public String getSecurityCode() {
            return SecurityCode;
        }

        public void setSecurityCode(String securityCode) {
            SecurityCode = securityCode;
        }

        public boolean isDefault() {
            return IsDefault;
        }

        public void setDefault(boolean aDefault) {
            IsDefault = aDefault;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.SerialNo);
            dest.writeByte(this.IsValid ? (byte) 1 : (byte) 0);
            dest.writeString(this.CardNo);
            dest.writeString(this.GtYear);
            dest.writeString(this.GtMonth);
            dest.writeString(this.SecurityCode);
            dest.writeByte(this.IsDefault ? (byte) 1 : (byte) 0);
        }

        public CreditCardsBean() {
        }

        protected CreditCardsBean(Parcel in) {
            this.SerialNo = in.readString();
            this.IsValid = in.readByte() != 0;
            this.CardNo = in.readString();
            this.GtYear = in.readString();
            this.GtMonth = in.readString();
            this.SecurityCode = in.readString();
            this.IsDefault = in.readByte() != 0;
        }

        public static final Parcelable.Creator<CreditCardsBean> CREATOR = new Parcelable.Creator<CreditCardsBean>() {
            @Override
            public CreditCardsBean createFromParcel(Parcel source) {
                return new CreditCardsBean(source);
            }

            @Override
            public CreditCardsBean[] newArray(int size) {
                return new CreditCardsBean[size];
            }
        };
    }
}
