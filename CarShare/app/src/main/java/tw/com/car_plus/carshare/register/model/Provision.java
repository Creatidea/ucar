package tw.com.car_plus.carshare.register.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/3/9.
 */

public class Provision implements Parcelable {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
    }

    public Provision() {
    }

    protected Provision(Parcel in) {
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Provision> CREATOR = new Parcelable.Creator<Provision>() {
        @Override
        public Provision createFromParcel(Parcel source) {
            return new Provision(source);
        }

        @Override
        public Provision[] newArray(int size) {
            return new Provision[size];
        }
    };
}
