package tw.com.car_plus.carshare.register.model;

/**
 * Created by winni on 2017/4/27.
 */

public class RegisterInfo {

    private int AcctId;
    private String LoginId;
    private String Pwd;
    private String CheckPwd;
    private String ChtName;
    private String Birthday;
    private String CellPhone;
    private String Email;
    private String Jurisdiction;
    private int HhrCityId;
    private int HhrAreaId;
    private String HhrAddr;
    private String Tel;
    private String TelAreaNumber;
    private String TelExtension;
    private int MailingCityId;
    private int MailingAreaId;
    private String MailingAddr;
    private String SubscriptionEDM;
    private String ReceiveSMS;

    public int getAcctId() {
        return AcctId;
    }

    public void setAcctId(int acctId) {
        AcctId = acctId;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getPwd() {
        return Pwd;
    }

    public void setPwd(String pwd) {
        Pwd = pwd;
    }

    public String getCheckPwd() {
        return CheckPwd;
    }

    public void setCheckPwd(String checkPwd) {
        CheckPwd = checkPwd;
    }

    public String getChtName() {
        return ChtName;
    }

    public void setChtName(String chtName) {
        ChtName = chtName;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getJurisdiction() {
        return Jurisdiction;
    }

    public void setJurisdiction(String jurisdiction) {
        Jurisdiction = jurisdiction;
    }

    public int getHhrCityId() {
        return HhrCityId;
    }

    public void setHhrCityId(int hhrCityId) {
        HhrCityId = hhrCityId;
    }

    public int getHhrAreaId() {
        return HhrAreaId;
    }

    public void setHhrAreaId(int hhrAreaId) {
        HhrAreaId = hhrAreaId;
    }

    public String getHhrAddr() {
        return HhrAddr;
    }

    public void setHhrAddr(String hhrAddr) {
        HhrAddr = hhrAddr;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getTelAreaNumber() {
        return TelAreaNumber;
    }

    public void setTelAreaNumber(String telAreaNumber) {
        TelAreaNumber = telAreaNumber;
    }

    public String getTelExtension() {
        return TelExtension;
    }

    public void setTelExtension(String telExtension) {
        TelExtension = telExtension;
    }

    public int getMailingCityId() {
        return MailingCityId;
    }

    public void setMailingCityId(int mailingCityId) {
        MailingCityId = mailingCityId;
    }

    public int getMailingAreaId() {
        return MailingAreaId;
    }

    public void setMailingAreaId(int mailingAreaId) {
        MailingAreaId = mailingAreaId;
    }

    public String getMailingAddr() {
        return MailingAddr;
    }

    public void setMailingAddr(String mailingAddr) {
        MailingAddr = mailingAddr;
    }

    public String getSubscriptionEDM() {
        return SubscriptionEDM;
    }

    public void setSubscriptionEDM(String subscriptionEDM) {
        SubscriptionEDM = subscriptionEDM;
    }

    public String getReceiveSMS() {
        return ReceiveSMS;
    }

    public void setReceiveSMS(String receiveSMS) {
        ReceiveSMS = receiveSMS;
    }
}
