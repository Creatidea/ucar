package tw.com.car_plus.carshare.register.model;

import java.util.List;

/**
 * Created by winni on 2017/3/7.
 */

public class UploadFile {

    /**
     * FileName :
     * Status : false
     * Errors : [{"ErrorCode":-100,"ErrorMsg":"無上傳檔案"}]
     */

    private String FileName;
    private boolean Status;
    private List<ErrorsBean> Errors;

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public List<ErrorsBean> getErrors() {
        return Errors;
    }

    public void setErrors(List<ErrorsBean> Errors) {
        this.Errors = Errors;
    }

    public static class ErrorsBean {
        /**
         * ErrorCode : -100
         * ErrorMsg : 無上傳檔案
         */

        private int ErrorCode;
        private String ErrorMsg;

        public int getErrorCode() {
            return ErrorCode;
        }

        public void setErrorCode(int ErrorCode) {
            this.ErrorCode = ErrorCode;
        }

        public String getErrorMsg() {
            return ErrorMsg;
        }

        public void setErrorMsg(String ErrorMsg) {
            this.ErrorMsg = ErrorMsg;
        }
    }
}
