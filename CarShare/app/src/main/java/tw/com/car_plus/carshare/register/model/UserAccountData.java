package tw.com.car_plus.carshare.register.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/3/9.
 */

public class UserAccountData implements Parcelable {

    private String acctId;
    private String loginId;

    public String getAcctId() {
        return acctId;
    }

    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.acctId);
        dest.writeString(this.loginId);
    }

    public UserAccountData() {
    }

    protected UserAccountData(Parcel in) {
        this.acctId = in.readString();
        this.loginId = in.readString();
    }

    public static final Parcelable.Creator<UserAccountData> CREATOR = new Parcelable.Creator<UserAccountData>() {
        @Override
        public UserAccountData createFromParcel(Parcel source) {
            return new UserAccountData(source);
        }

        @Override
        public UserAccountData[] newArray(int size) {
            return new UserAccountData[size];
        }
    };
}
