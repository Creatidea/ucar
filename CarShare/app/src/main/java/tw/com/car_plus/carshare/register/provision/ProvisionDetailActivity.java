package tw.com.car_plus.carshare.register.provision;

import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.Provision;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/3/9.
 * 隱私權詳細頁
 */

public class ProvisionDetailActivity extends CustomBaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.content)
    TextView content;

    private Provision provision;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_provision_detail);
        ButterKnife.bind(this);

        provision = getIntent().getParcelableExtra("Provision");

        setBackToolBar(R.drawable.img_toolbar_provision_detail);
        title.setText(provision.getTitle());
    }

    // ----------------------------------------------------
}
