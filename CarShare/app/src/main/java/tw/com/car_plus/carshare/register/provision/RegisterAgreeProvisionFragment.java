package tw.com.car_plus.carshare.register.provision;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.net.NetworkChecker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.register.model.Provision;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.register.provision.adapter.ProvisionAdapter;
import tw.com.car_plus.carshare.util.BaseRecyclerViewAdapter;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/3/9.
 * 隱私權條款
 */

public class RegisterAgreeProvisionFragment extends CustomBaseFragment implements BaseRecyclerViewAdapter.onItemClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.is_agree)
    CheckBox isAgree;

    private NetworkChecker networkChecker;
    private UserAccountData userAccountData;
    private RegisterConnect registerConnect;
    private ProvisionAdapter provisionAdapter;
    private List<Provision> provision;

    @Override
    protected void init() {

        setView(R.layout.fragment_register_agree_provision);
        userAccountData = getArguments().getParcelable("UserAccountData");
        registerConnect = new RegisterConnect(activity);
        networkChecker = NetworkChecker.getInstance(activity);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        // TODO: 2017/4/11 目前是假資料需接上真的api資料 
        provision = getProvisionLis();

        provisionAdapter = new ProvisionAdapter(activity, provision);
        recyclerView.setAdapter(provisionAdapter);
        provisionAdapter.setOnItemClickListener(this);
    }

    // ----------------------------------------------------
    private List<Provision> getProvisionLis() {
        List<Provision> provisionList = new ArrayList<>();
        Provision provision = new Provision();
        provision.setTitle("Ucar會員條款");
        provisionList.add(provision);
        return provisionList;
    }

    // ----------------------------------------------------
    @OnClick(R.id.send)
    public void onClick() {

        if (isAgree.isChecked()) {
            if (networkChecker.isNetworkAvailable()) {
                registerConnect.connectApplyVerify(userAccountData);
            }
        } else {
            Toast.makeText(activity, getString(R.string.check_provision), Toast.LENGTH_LONG).show();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent();
        intent.setClass(activity, ProvisionDetailActivity.class);
        intent.putExtra("Provision", provision.get(position));
        startActivity(intent);
    }
}
