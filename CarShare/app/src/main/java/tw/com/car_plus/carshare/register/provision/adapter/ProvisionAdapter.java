package tw.com.car_plus.carshare.register.provision.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.Provision;
import tw.com.car_plus.carshare.util.BaseRecyclerViewAdapter;

/**
 * Created by winni on 2017/3/9.
 */

public class ProvisionAdapter extends BaseRecyclerViewAdapter {

    private Context context;
    private List<Provision> provisions;

    public ProvisionAdapter(Context context, List<Provision> provisions) {
        this.context = context;
        this.provisions = provisions;
    }

    // ----------------------------------------------------
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_provision_register, parent, false));
    }

    // ----------------------------------------------------
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).title.setText(provisions.get(position).getTitle());

        if (position == 0) {
            ((ViewHolder) holder).gradient.setVisibility(View.GONE);
        } else {
            ((ViewHolder) holder).gradient.setVisibility(View.VISIBLE);
        }
    }

    // ----------------------------------------------------
    @Override
    public int getItemCount() {
        return provisions.size();
    }

    // ----------------------------------------------------
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.gradient)
        View gradient;
        @BindView(R.id.title)
        TextView title;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            //列表item點下事件
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }
}
