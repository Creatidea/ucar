package tw.com.car_plus.carshare.rent;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.neilchen.complextoolkit.util.map.MapSetting;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.rent.adapter.AreaAdapter;
import tw.com.car_plus.carshare.rent.adapter.ListModeAdapter;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.reservation_car.ReservationCarActivity;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.UserUtil;

/**
 * Created by neil on 2017/3/16.
 */

public class ListModeFragment extends CustomBaseFragment implements AdapterView.OnItemSelectedListener, ExpandableListView.OnChildClickListener,
        ListModeAdapter.OnParkingClickListener, ListModeAdapter.OnNaviClickListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    // ----------------------------------------------------
    @BindView(R.id.spinner_distance)
    Spinner spinnerDistance;
    @BindView(R.id.spinner_area)
    Spinner spinnerArea;
    @BindView(R.id.list)
    ExpandableListView list;
    @BindView(R.id.list_mode)
    Button listMode;

    // ----------------------------------------------------
    private List<Parking> parkings;
    private List<Area> areas;
    private List<String> distancies;

    private SharedPreferenceUtil sp;
    private RentConnect connect;
    private RegisterConnect registerConnect;
    private Location userLocation;
    private ListModeAdapter adapter;
    private MapSetting mapSetting;
    private UserUtil userUtil;
    private DialogUtil dialogUtil;
    private LinearLayout changeLayout;
    private int cityId;

    // ----------------------------------------------------
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.changeLayout = ((IndexActivity) context).toolbarChange;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_list_rental);

        cityId = getArguments().getInt("cityId");

        mapSetting = new MapSetting(getActivity());
        mapSetting.initApiClient(this);
        mapSetting.apiClient.connect();

        connect = new RentConnect(activity);
        registerConnect = new RegisterConnect(activity);

        sp = new SharedPreferenceUtil(activity);

        userUtil = new UserUtil(activity, sp);
        dialogUtil = new DialogUtil(activity);

        listMode.setBackgroundResource(R.drawable.bookmark_b);
        changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userUtil.changeAccount(EventCenter.INDEX);
            }
        });

        setKMSpinner();
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.PARKING) {

            parkings = (List<Parking>) data.get("data");
            setListData();

        } else if ((int) data.get("type") == EventCenter.AREA) {
            //區域
            areas = (List<Area>) data.get("data");
            setAreaSpinner();
        } else if ((int) data.get("type") == EventCenter.INDEX) {

            User user = (User) data.get("data");
            userUtil.setUser(user);
            userUtil.setAccountInfo();
            sp.setUserData(userUtil.getUser());
            EventCenter.getInstance().sendSetToolbarChange();
            dialogUtil.setMessage(String.format(getString(R.string.change_account), sp.getUserData().getCompanyAccount() != null ? getString(R
                    .string.company) : getString(R.string.personal))).setConfirmButton(R.string.confirm, null).showDialog(true);

        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    private void setListData() {

        setParkingData();
        adapter = new ListModeAdapter(parkings);
        adapter.setOnNaviClickListener(this);
        adapter.setOnParkingClickListener(this);

        list.setAdapter(adapter);
        list.setOnChildClickListener(this);

        if (parkings.size() > 0) {
            list.expandGroup(0);
        }
    }

    // ----------------------------------------------------

    /**
     * 將把可以按下停車場資訊跟導航的項目加上去
     */
    private void setParkingData() {

        for (int i = 0; i < parkings.size(); i++) {
            List<Parking.CarGroupBean> carGroupBeen = parkings.get(i).getCarGroup();
            carGroupBeen.add(0, new Parking.CarGroupBean());
        }
    }

    // ----------------------------------------------------
    private void setKMSpinner() {

        distancies = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.search_range)));
        distancies.add(0, getString(R.string.list_mode_range));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.list_map_mode, R.id.item, distancies);

        spinnerDistance.setAdapter(adapter);
        spinnerDistance.setOnItemSelectedListener(this);
    }

    // ----------------------------------------------------
    private void setAreaSpinner() {

        Area area = new Area();
        area.setAreaName(getString(R.string.list_mode_area));
        areas.add(0, area);

        AreaAdapter adapter = new AreaAdapter(areas);
        spinnerArea.setAdapter(adapter);
        spinnerArea.setOnItemSelectedListener(this);
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.map_mode, R.id.qrcode_mode})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.map_mode:
                replaceFragment(new MapModeFragment(), false);
                break;
            case R.id.qrcode_mode:
                Bundle qrcodeBundle = new Bundle();
                qrcodeBundle.putInt("cityId", cityId);
                qrcodeBundle.putParcelable("location", userLocation);
                replaceFragment(new QRCodeModeFragment(), false, qrcodeBundle);
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {

            case R.id.spinner_distance:
                if (position != 0) {
                    spinnerArea.setSelection(0);
                    chooseDistance(position);
                }
                break;
            case R.id.spinner_area:
                if (position != 0) {
                    spinnerDistance.setSelection(0);
                    loadData(RentConnect.NONE, areas.get(position).getAreaId());
                }
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // ----------------------------------------------------
    private void chooseDistance(int position) {

        switch (position) {
            case 1:
                //1km
                loadData(RentConnect.ONE_KM, 0);
                break;
            case 2:
                //5km
                loadData(RentConnect.FIVE_KM, 0);
                break;
            case 3:
                //10km
                loadData(RentConnect.TEN_KM, 0);
                break;
            case 4:
                //15km
                loadData(RentConnect.FIFTEEN_KM, 0);
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent intent = new Intent();
        intent.setClass(activity, ReservationCarActivity.class);
        intent.putExtra("carId", parkings.get(groupPosition).getCarGroup().get(childPosition).getCarId());
        intent.putExtra("userLocation", userLocation);
        startActivity(intent);
        return false;
    }

    // ----------------------------------------------------

    /**
     * 讀取資料
     */
    private void loadData(@RentConnect.DistanceMode int distance, int areaId) {
        connect.loadRentInfo(userLocation, distance, areaId);
    }

    // ----------------------------------------------------
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mapSetting.startLocationUpdates(this, 1, 1);
    }

    // ----------------------------------------------------
    @Override
    public void onConnectionSuspended(int i) {

    }

    // ----------------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        try {
            userLocation = location;
            connect.loadRentInfo(userLocation, RentConnect.FIFTEEN_KM, 0);
            registerConnect.connectGetArea(cityId);

            mapSetting.onStop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------
    @Override
    public void onNaviClick(int position) {

        String lat = parkings.get(position).getLatitude();
        String lon = parkings.get(position).getLongitude();

        Uri gmmIntentUri = Uri.parse(String.format("google.navigation:q=%s,%s", lat, lon));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    // ----------------------------------------------------
    @Override
    public void onParkingClick(int groupPosition) {
        Intent intent = new Intent();
        intent.setClass(activity, ParkingDetailActivity.class);
        intent.putExtra("parking", parkings.get(groupPosition));
        intent.putExtra("userLocation", userLocation);
        startActivity(intent);
    }

    // ----------------------------------------------------
}
