package tw.com.car_plus.carshare.rent;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;
import com.neilchen.complextoolkit.util.map.MapSetting;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.preferential.PreferentialConnect;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.connect.setting.SettingConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.preferential.PreferentialActivity;
import tw.com.car_plus.carshare.preferential.model.Preferential;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.rent.adapter.AreaAdapter;
import tw.com.car_plus.carshare.rent.adapter.CarViewHolder;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.reservation_car.ReservationCarActivity;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.MapUtils;
import tw.com.car_plus.carshare.util.PromotionDateUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.UserSignInChecker;
import tw.com.car_plus.carshare.util.UserUtil;
import tw.com.car_plus.carshare.util.address.AddressUtil;

/**
 * Created by neil on 2017/3/14.
 */

@RuntimePermissions
public class MapModeFragment extends CustomBaseFragment implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleMap
        .OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, OnItemClickListener, GoogleMap.OnMapClickListener {

    // ----------------------------------------------------
    @BindView(R.id.map_mode)
    Button mapMode;
    @BindView(R.id.car_list)
    ConvenientBanner carList;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.view_loading)
    View viewLoading;
    @BindView(R.id.btn_call)
    Button btnCall;
    @BindView(R.id.layout_connection)
    View layoutConnection;

    public static Location parkingLocation;

    // ----------------------------------------------------
    private final int REMIND_CAR = 0;

    private View view;
    private SharedPreferenceUtil sp;
    private LoadingDialogManager loadingDialogManager;
    private AddressUtil addressUtil;
    private MapSetting mapSetting;
    private RentConnect connect;
    private SettingConnect settingConnect;
    private PreferentialConnect preferentialConnect;
    private Location userLocation;
    private MapUtils mapUtils;
    private Marker oldMarker;
    private UserUtil userUtil;
    private DialogUtil dialogUtil;
    private LinearLayout changeLayout;
    private ImageView toolbarImage;
    private int currentDistance;
    private int currentAreaId;
    private List<Parking> parkings;
    private Map<String, Parking> mapData;
    private List<String> remindParkingData;
    private UserSignInChecker signInChecker;
    private static boolean isShowPreferantial = false;

    // ----------------------------------------------------
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.toolbarImage = ((IndexActivity) context).toolbarImage;
            this.changeLayout = ((IndexActivity) context).toolbarChange;
        }
    }

    // ----------------------------------------------------
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MapsInitializer.initialize(getActivity());

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_map_rental, container, false);
            ButterKnife.bind(this, view);
            MapModeFragmentPermissionsDispatcher.startLocationWithCheck(this);
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        ButterKnife.bind(this, view);
        return view;
    }

    // ----------------------------------------------------
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarImage.setImageResource(R.drawable.img_toolbar_map);
    }

    // ----------------------------------------------------
    protected void init() {

        signInChecker = new UserSignInChecker(activity);

        preferentialConnect = new PreferentialConnect(activity);
        addressUtil = new AddressUtil(activity);
        mapSetting = new MapSetting(this, R.id.map);
        connect = new RentConnect(activity);
        loadingDialogManager = new LoadingDialogManager(activity);
        settingConnect = new SettingConnect(activity);

        if (dialogUtil == null) {
            dialogUtil = new DialogUtil(activity);
        }

        sp = new SharedPreferenceUtil(activity);
        userUtil = new UserUtil(activity, sp);

        mapSetting.initApiClient(this);
        mapSetting.apiClient.connect();
        loadingDialogManager.show();

        mapMode.setBackgroundResource(R.drawable.bookmark_b);
        addressUtil.loadCity();

        changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userUtil.changeAccount(EventCenter.INDEX);
            }
        });
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.PARKING) {

            //data init.
            dataInit();

            parkings = (List<Parking>) data.get("data");
            mapSetting.moveCamera(userLocation, 16, false);

            if (!sp.getAcctId().isEmpty() && sp.getUserData().getCompanyAccount() == null) {
                // 讀取提醒停車場資訊
                new SettingConnect(activity).loadRemindPraking();
            } else {
                addMarkerOnMap();
                // 優惠資訊
                preferentialConnect.sendPreferentialsInfo();
            }

        } else if ((int) data.get("type") == EventCenter.REMIND_PARKING) {

            remindParkingData = (List<String>) data.get("data");
            addMarkerOnMap();
            // 取得提醒停車場資訊
            preferentialConnect.sendPreferentialsInfo();

        } else if ((int) data.get("type") == EventCenter.CITY) {
            //城市
            addressUtil.setCities((List<City>) data.get("data"));
        } else if ((int) data.get("type") == EventCenter.AREA) {
            //區域
            addressUtil.setAreas((List<Area>) data.get("data"));
            showAreaData();
        } else if ((int) data.get("type") == EventCenter.INDEX) {

            User user = (User) data.get("data");
            userUtil.setUser(user);
            userUtil.setAccountInfo();
            sp.setUserData(user);

            EventCenter.getInstance().sendSetToolbarChange();
            dialogUtil.setMessage(String.format(getString(R.string.change_account), sp.getUserData().getCompanyAccount() != null ? getString(R
                    .string.company) : getString(R.string.personal))).setConfirmButton(R.string.confirm, null).showDialog(true);

        } else if ((int) data.get("type") == EventCenter.PREFERENTIAL) {

            List<Preferential> preferentialList = (List<Preferential>) data.get("data");
            if (preferentialList.size() > 0 && !isShowPreferantial) {
                showPreferantialDialog(preferentialList);
            }

        } else if ((int) data.get("type") == EventCenter.SETTING_REMIND_PARKING) {
            String parkingName = (String) data.get("data");
            Toast.makeText(activity, getString(R.string.setting_success), Toast.LENGTH_LONG).show();
            boolean isRemind = mapData.get(parkingName).isRemind();
            mapData.get(parkingName).setRemind(!isRemind);
            setMarkerColor(oldMarker, mapData.get(parkingName).isRemind() ? R.drawable.pin_deepblue_alert : R.drawable.pin_deepblue);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    private void dataInit() {

        viewLoading.setVisibility(View.GONE);
        list.setVisibility(View.GONE);
        carList.setVisibility(View.GONE);
        oldMarker = null;
    }

    // ----------------------------------------------------

    /**
     * 添加大頭釘
     */
    private void addMarkerOnMap() {

        mapSetting.mMap.clear();
        mapData = new HashMap<>();
        Bitmap icon;

        for (int count = 0; count < parkings.size(); count++) {

            if (remindParkingData != null && remindParkingData.indexOf(parkings.get(count).getName()) > -1) {
                parkings.get(count).setRemind(true);
                icon = mapUtils.getBitmapMarker(R.drawable.pin_orange_alert, String.valueOf(parkings.get(count).getCarGroup().size()));
            } else {
                icon = mapUtils.getBitmapMarker(R.drawable.pin_orange, String.valueOf(parkings.get(count).getCarGroup().size()));
                parkings.get(count).setRemind(false);
            }

            mapData.put(parkings.get(count).getName(), parkings.get(count));
            LatLng point = new LatLng(Double.parseDouble(parkings.get(count).getLatitude()), Double.parseDouble(parkings.get(count).getLongitude()));

            mapSetting.mMap.addMarker(new MarkerOptions()
                    .position(point)
                    .title(parkings.get(count).getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(icon)));
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        if (mapSetting != null) {

            if (parkingLocation != null) {
                mapSetting.moveCamera(parkingLocation, false);
                if (userLocation != null) {
                    loadData(RentConnect.FIFTEEN_KM, 0);
                }
            } else if (userLocation != null) {
                mapSetting.moveCamera(userLocation);
            }

            parkingLocation = null;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onStop() {
        super.onStop();
    }

    // ----------------------------------------------------
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        loadingDialogManager.dismiss();

        mapSetting.mMap.setOnMarkerClickListener(this);
        mapSetting.mMap.setOnInfoWindowClickListener(this);
        mapSetting.mMap.setOnMapClickListener(this);
        mapSetting.startLocationUpdates(this, 1, 1);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mapSetting.mMap.setMyLocationEnabled(true);
            UiSettings uiSettings = mapSetting.mMap.getUiSettings();
            uiSettings.setMyLocationButtonEnabled(false);

            try {
                userLocation = mapSetting.getUserLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // ----------------------------------------------------
    @Override
    public void onConnectionSuspended(int i) {

    }

    // ----------------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        try {
            mapUtils = new MapUtils(getActivity());
            userLocation = location;
            loadData(RentConnect.FIFTEEN_KM, 0);
            mapSetting.onStop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.list_mode, R.id.qrcode_mode, R.id.search, R.id.refresh, R.id.my_location, R.id.btn_call})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.list_mode:
                // 判斷是否有登入
                if (!signInChecker.isSignIn()) {
                    return;
                }

                if (checkPermission()) {
                    Bundle listBundle = new Bundle();
                    listBundle.putInt("cityId", addressUtil.getCityId(userLocation));
                    replaceFragment(new ListModeFragment(), false, listBundle);
                }

                break;
            case R.id.qrcode_mode:
                // 判斷是否有登入
                if (!signInChecker.isSignIn()) {
                    return;
                }

                if (checkPermission()) {
                    Bundle qrcodeBundle = new Bundle();
                    qrcodeBundle.putInt("cityId", addressUtil.getCityId(userLocation));
                    qrcodeBundle.putParcelable("location", userLocation);
                    replaceFragment(new QRCodeModeFragment(), false, qrcodeBundle);
                }
                break;
            case R.id.search:
                showSearchDialog();
                break;
            case R.id.refresh:
                if (checkPermission()) {
                    loadData(currentDistance, currentAreaId);
                }
                break;
            case R.id.my_location:
                if (checkPermission()) {
                    mapSetting.moveCamera(userLocation, false);
                }
                break;
            //撥出客服電話
            case R.id.btn_call:
                MapModeFragmentPermissionsDispatcher.startCallPhoneWithCheck(this);
                break;
        }
    }

    // ----------------------------------------------------
    private void showSearchDialog() {

        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.choose_search))
                .setItems(getResources().getStringArray(R.array.rent), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        switch (position) {
                            case 0:
                                //範圍
                                if (checkPermission()) {
                                    list.setVisibility(View.VISIBLE);
                                    showRangeData();
                                }
                                break;
                            case 1:
                                //地區
                                if (checkPermission()) {
                                    addressUtil.loadArea(addressUtil.getCityId(userLocation));
                                }
                                break;
                            case 2:
                                //取消
                                dialog.dismiss();
                                break;
                        }
                    }
                }).show();
    }

    // ----------------------------------------------------

    /**
     * 顯示範圍資料
     */
    private void showRangeData() {

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.list_map_mode, R.id.item, getResources().getStringArray(R.array
                .search_range));
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        //1km
                        loadData(RentConnect.ONE_KM, 0);
                        break;
                    case 1:
                        //5km
                        loadData(RentConnect.FIVE_KM, 0);
                        break;
                    case 2:
                        //10km
                        loadData(RentConnect.TEN_KM, 0);
                        break;
                    case 3:
                        //15km
                        loadData(RentConnect.FIFTEEN_KM, 0);
                        break;
                }
            }
        });

    }

    // ----------------------------------------------------

    /**
     * 顯示地區資料
     */
    private void showAreaData() {

        list.setVisibility(View.VISIBLE);
        AreaAdapter adapter = new AreaAdapter(addressUtil.getAreas());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (checkPermission()) {
                    loadData(RentConnect.NONE, addressUtil.getAreas().get(position).getAreaId());
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 讀取資料
     */
    private void loadData(@RentConnect.DistanceMode int distance, int areaId) {

        currentDistance = distance;
        currentAreaId = areaId;
        connect.loadRentInfo(userLocation, currentDistance, currentAreaId);
    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void startLocation() {
        init();
    }

    // ----------------------------------------------------
    @NeedsPermission(Manifest.permission.CALL_PHONE)
    void startCallPhone() {
        getActivity().startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.map_mode_connection_phone))));
    }

    // ----------------------------------------------------
    //請求授權
    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocationPermission(final PermissionRequest request) {
        request.proceed();
    }

    //請求授權
    @OnShowRationale(Manifest.permission.CALL_PHONE)
    void showRationaleForCallPhonePermission(final PermissionRequest request) {
        request.proceed();
    }

    // ----------------------------------------------------
    // location 授權被拒
    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showDeniedForLocationPermission() {
        Toast.makeText(getActivity(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    // CALL PHONE 授權被拒
    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void showDeniedForCallPhonePermission() {
        Toast.makeText(getActivity(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @OnNeverAskAgain({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CALL_PHONE})
    void goToSettingPermission() {

        dialogUtil = new DialogUtil(activity);
        dialogUtil.setMessage(R.string.go_setting_permission).setCancelButton(R.string.cancel, null).setConfirmButton(R.string.confirm, new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
                settings.addCategory(Intent.CATEGORY_DEFAULT);
                settings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(settings);
            }
        }).showDialog(false);

    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MapModeFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // ----------------------------------------------------
    @Override
    public void onInfoWindowClick(Marker marker) {

        if (checkPermission()) {
            //停車場資訊
            Intent intent = new Intent();
            intent.setClass(activity, ParkingDetailActivity.class);
            intent.putExtra("parking", mapData.get(marker.getTitle()));
            intent.putExtra("userLocation", userLocation);
            startActivity(intent);
        }
    }

    // ----------------------------------------------------
    @Override
    public boolean onMarkerClick(final Marker marker) {

        // 判斷是否有登入
        if (!signInChecker.isSignIn()) {
            return false;
        }

        //將上一次的還原
        if (oldMarker != null) {
            setMarkerColor(oldMarker, mapData.get(oldMarker.getTitle()).isRemind() ? R.drawable.pin_orange_alert : R.drawable.pin_orange);
        }

        //更換顏色
        setMarkerColor(marker, mapData.get(marker.getTitle()).isRemind() ? R.drawable.pin_deepblue_alert : R.drawable.pin_deepblue);
        oldMarker = marker;

        Parking parking = mapData.get(marker.getTitle());

        //車輛數=0的時候
        if (parking.getCarGroup().size() == REMIND_CAR) {

            carList.setVisibility(View.GONE);

            //如果是企業版
            if (sp.getUserData().getCompanyAccount() != null) {
                layoutConnection.setVisibility(View.VISIBLE);
            } else {
                setRemindParking(parking.isRemind(), parking.getParkinglotId(), parking.getName());
            }

        } else {

            layoutConnection.setVisibility(View.GONE);
            carList.setVisibility(View.VISIBLE);

            carList.setPages(new CBViewHolderCreator() {
                @Override
                public CarViewHolder createHolder() {
                    return new CarViewHolder(mapData.get(marker.getTitle()).getCarGroup().size());
                }
            }, mapData.get(marker.getTitle()).getCarGroup());

            carList.setCanLoop(false);
            carList.setOnItemClickListener(this);
            carList.getViewPager().setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            carList.getViewPager().setPadding(dpToPx(5), 0, dpToPx(5), 0);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            carList.getViewPager().setPageMargin(dpToPx(-20));

            if (parking.isRemind()) {
                setRemindParking(parking.isRemind(), parking.getParkinglotId(), parking.getName());
            }

        }
        return false;
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(int position) {
        if (checkPermission()) {
            //車輛資訊
            Intent intent = new Intent();
            intent.setClass(activity, ReservationCarActivity.class);
            intent.putExtra("carId", mapData.get(oldMarker.getTitle()).getCarGroup().get(position).getCarId());
            intent.putExtra("userLocation", userLocation);
            startActivity(intent);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onMapClick(LatLng latLng) {

        //隱藏下方 car list
        carList.setVisibility(View.GONE);
        layoutConnection.setVisibility(View.GONE);
        //還原成未點選顏色
        if (oldMarker != null) {
            setMarkerColor(oldMarker, mapData.get(oldMarker.getTitle()).isRemind() ? R.drawable.pin_orange_alert : R.drawable.pin_orange);
        }
        //隱藏下方搜尋選擇列表
        list.setVisibility(View.GONE);
    }

    // ----------------------------------------------------

    /**
     * 更換大頭釘顏色
     */
    private void setMarkerColor(Marker marker, int resource) {

        marker.setIcon(BitmapDescriptorFactory.fromBitmap(mapUtils.getBitmapMarker(resource, String.valueOf(mapData.get(marker.getTitle())
                .getCarGroup().size()))));
    }


    // ----------------------------------------------------

    /**
     * 檢查是否同意權限以及是否有取得到使用者目前位置
     *
     * @return
     */
    private boolean checkPermission() {

        if (mapSetting != null) {
            if (userLocation != null) {
                return true;
            } else {
                Toast.makeText(activity, getString(R.string.need_location_info), Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            MapModeFragmentPermissionsDispatcher.startLocationWithCheck(this);
            return false;
        }
    }

    //--------------------------------------------------

    /**
     * dp轉成px
     *
     * @param dp
     * @return
     */
    private int dpToPx(int dp) {
        return Math.round((float) dp * getResources().getDisplayMetrics().density);
    }

    // ----------------------------------------------------
    private void showPreferantialDialog(List<Preferential> preferentialList) {

        isShowPreferantial = true;

        Preferential preferential = preferentialList.get(0);

        final Dialog mDialog = new Dialog(activity);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.dialog_preferential);

        LinearLayout applicant = (LinearLayout) mDialog.findViewById(R.id.applicant);
        TextView title = (TextView) mDialog.findViewById(R.id.title);
        TextView date = (TextView) mDialog.findViewById(R.id.date);
        ImageView banner = (ImageView) mDialog.findViewById(R.id.banner);
        final TextView info = (TextView) mDialog.findViewById(R.id.info);

        Button close = (Button) mDialog.findViewById(R.id.close);
        Button more = (Button) mDialog.findViewById(R.id.more);

        title.setText(preferential.getTitle());
        date.setText(new PromotionDateUtil(activity).getDate(preferential.getStartTime(), preferential.getEndTime()));

        if (preferential.getPic().length() > 0) {
            Glide.with(activity).load(preferential.getPic()).into(banner);
        } else {
            banner.setVisibility(View.GONE);
        }

        info.setText(preferential.getInfo());

        for (int i = 0; i < preferential.getSuitable().size(); i++) {

            View view = LayoutInflater.from(activity).inflate(R.layout.list_item_preferential_applicant, null);
            title = (TextView) view.findViewById(R.id.title);
            title.setText(preferential.getSuitable().get(i));
            applicant.addView(view);
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(activity, PreferentialActivity.class);
                startActivity(intent);
            }
        });

        // 點邊取消
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();

    }

    // ----------------------------------------------------

    /**
     * 設定
     *
     * @param isSetting
     */
    private void setRemindParking(final boolean isSetting, final int parkingId, final String parkingName) {

        String title, message;

        if (isSetting) {
            title = getString(R.string.cancel_notification_title);
            message = getString(R.string.cancel_notification_message);
        } else {
            title = getString(R.string.setting_notification_title);
            message = getString(R.string.setting_notification_message);
        }

        SettingNotificationDialog dialog = new SettingNotificationDialog(activity, title, message);
        dialog.setOnSettingClickListener(new SettingNotificationDialog.OnSettingListener() {
            @Override
            public void onSetting() {
                settingConnect.setSubscribeParking(parkingId, parkingName, !isSetting);
            }
        });
        dialog.show();

    }

    // ----------------------------------------------------
}
