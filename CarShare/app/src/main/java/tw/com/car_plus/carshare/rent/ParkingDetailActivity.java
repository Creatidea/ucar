package tw.com.car_plus.carshare.rent;

import android.location.Location;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.neilchen.complextoolkit.util.map.MapSetting;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/3/17.
 * 停車場資訊
 */

public class ParkingDetailActivity extends CustomBaseActivity {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.rental_count)
    TextView rentalCount;
    @BindView(R.id.charging_count)
    TextView chargingCount;
    @BindView(R.id.space_count)
    TextView spaceCount;
    @BindView(R.id.information)
    TextView information;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.banner)
    ImageView banner;

    private MapSetting mapSetting;
    private Parking parking;
    private Location userLocation, parkingLocation;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_parking_detail);
        ButterKnife.bind(this);

        IndexActivity.activities.add(this);

        setBackToolBar(R.drawable.img_toolbar_parking_info);

        mapSetting = new MapSetting(this);
        userLocation = getIntent().getParcelableExtra("userLocation");
        parking = getIntent().getParcelableExtra("parking");

        name.setText(parking.getName());
        address.setText(parking.getAddress());
        rentalCount.setText(String.valueOf(parking.getTotalCanGetCarCount()));
        chargingCount.setText(String.valueOf(parking.getChargeSiteCount()));
        spaceCount.setText(String.valueOf(parking.getParkingSpaceCount()));
        information.setText(parking.getSpaceInfo());
        location.setText(parking.getLocationDesc());
        type.setText(getParkingType(parking.getParkinglotType()));

        Glide.with(this)
                .load(String.format(ConnectInfo.IMAGE_PATH, parking.getParkinglotPicUrl()))
                .into(banner);

        parkingLocation = new Location("Location");
        parkingLocation.setLatitude(Double.valueOf(parking.getLatitude()));
        parkingLocation.setLongitude(Double.valueOf(parking.getLongitude()));

        DecimalFormat df = new DecimalFormat("0.00");
        String strDistance = df.format(mapSetting.get2PointDistance(userLocation, parkingLocation) / 1000);
        distance.setText(String.format(getString(R.string.distance), strDistance));
    }

    // ----------------------------------------------------
    private String getParkingType(int type) {

        if (type == 1) {
            return getString(R.string.flat);
        } else {
            return getString(R.string.stereoscopic);
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.gps, R.id.back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gps:
                startGoogleMapNavigation(parking.getLatitude(), parking.getLongitude());
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
}
