package tw.com.car_plus.carshare.rent;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.rent.reservation_car.ReservationCarActivity;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.UserUtil;

/**
 * Created by winni on 2017/3/14.
 */
@RuntimePermissions
public class QRCodeModeFragment extends CustomBaseFragment implements BarcodeCallback {

    @BindView(R.id.decorated_barcode_view)
    DecoratedBarcodeView decoratedBarcodeView;
    @BindView(R.id.qrcode_mode)
    Button qrcodeMode;

    private SharedPreferenceUtil sp;
    private UserUtil userUtil;
    private DialogUtil dialogUtil;
    private BeepManager beepManager;
    private LinearLayout changeLayout;
    private Location userLocation;
    private boolean isOpenInfo = false;


    // ----------------------------------------------------
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.changeLayout = ((IndexActivity) context).toolbarChange;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_qrcode_rental);

        userLocation = getArguments().getParcelable("location");

        dialogUtil = new DialogUtil(activity);
        sp = new SharedPreferenceUtil(activity);

        userUtil = new UserUtil(activity, sp);

        qrcodeMode.setBackgroundResource(R.drawable.bookmark_b);
        changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userUtil.changeAccount(EventCenter.INDEX);
            }
        });

        QRCodeModeFragmentPermissionsDispatcher.goToScanQRCodeWithCheck(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.INDEX) {

            User user = (User) data.get("data");
            userUtil.setUser(user);
            userUtil.setAccountInfo();
            sp.setUserData(user);

            EventCenter.getInstance().sendSetToolbarChange();
            dialogUtil.setMessage(String.format(getString(R.string.change_account), sp.getUserData().getCompanyAccount() != null ? getString(R
                    .string.company) : getString(R.string.personal))).setConfirmButton(R.string.confirm, null).showDialog(true);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        decoratedBarcodeView.resume();
        isOpenInfo = false;
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        decoratedBarcodeView.pause();
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        QRCodeModeFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意開啟相機
     */
    @NeedsPermission({Manifest.permission.CAMERA})
    void goToScanQRCode() {
        decoratedBarcodeView.decodeContinuous(this);
        decoratedBarcodeView.setStatusText("");
        beepManager = new BeepManager(activity);
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.CAMERA})
    void showDeniedForScanQRCode() {
        Toast.makeText(activity, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Override
    public void barcodeResult(BarcodeResult result) {

        if (result.getText() != null) {

            String lastText = result.getText();

            try {

                if (!isOpenInfo) {
                    Intent intent = new Intent();
                    intent.setClass(activity, ReservationCarActivity.class);
                    intent.putExtra("carId", Integer.valueOf(lastText));
                    intent.putExtra("userLocation", userLocation);
                    startActivity(intent);
                    beepManager.playBeepSoundAndVibrate();
                    isOpenInfo = true;
                }

            } catch (Exception e) {

            }
        }
    }

    // ----------------------------------------------------
    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    // ----------------------------------------------------
    @OnClick({R.id.map_mode, R.id.list_mode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.map_mode:
                replaceFragment(new MapModeFragment(), false);
                break;
            case R.id.list_mode:
                Bundle listBundle = new Bundle();
                listBundle.putInt("cityId", getArguments().getInt("cityId"));
                replaceFragment(new ListModeFragment(), false, listBundle);
                break;
        }
    }

}
