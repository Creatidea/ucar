package tw.com.car_plus.carshare.rent;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import tw.com.car_plus.carshare.R;

/**
 * Created by neil on 2017/11/30.
 */

public class SettingNotificationDialog extends Dialog implements View.OnClickListener {

    Button cancel;
    Button confirm;


    // ----------------------------------------------------
    private Context context;
    private OnSettingListener listener;

    // ----------------------------------------------------
    public SettingNotificationDialog(@NonNull Context context, String strTitle, String strMessage) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_notification);
        this.context = context;

        cancel = (Button) findViewById(R.id.cancel);
        confirm = (Button) findViewById(R.id.confirm);
        TextView title = (TextView) findViewById(R.id.title);
        TextView message = (TextView) findViewById(R.id.message);
        title.setText(strTitle);
        message.setText(strMessage);

        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);
    }

    // ----------------------------------------------------
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.confirm:
                if (listener != null) {
                    listener.onSetting();
                }
                break;
        }

        dismiss();
    }

    // ----------------------------------------------------
    public interface OnSettingListener {
        void onSetting();
    }

    // ----------------------------------------------------
    public void setOnSettingClickListener(OnSettingListener listener) {
        this.listener = listener;
    }

    // ----------------------------------------------------
}
