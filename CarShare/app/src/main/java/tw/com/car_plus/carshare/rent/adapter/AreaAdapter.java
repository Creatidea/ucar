package tw.com.car_plus.carshare.rent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.Area;

/**
 * Created by neil on 2017/3/16.
 */

public class AreaAdapter extends BaseAdapter {

    // ----------------------------------------------------
    private List<Area> areas;

    // ----------------------------------------------------
    public AreaAdapter(List<Area> areas) {
        this.areas = areas;
    }

    // ----------------------------------------------------
    @Override
    public int getCount() {
        return areas.size();
    }

    // ----------------------------------------------------
    @Override
    public Object getItem(int position) {
        return position;
    }

    // ----------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemViewHolder holder;

        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_map_mode, parent, false);
            holder = new ItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }

        holder.item.setText(areas.get(position).getAreaName());

        return convertView;
    }

    // ----------------------------------------------------
    static class ItemViewHolder {

        @BindView(R.id.item)
        TextView item;

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
}
