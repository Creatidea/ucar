package tw.com.car_plus.carshare.rent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.bumptech.glide.Glide;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType;

/**
 * Created by neil on 2017/3/16.
 */

public class CarViewHolder implements Holder<Parking.CarGroupBean> {

    // ----------------------------------------------------
    private int dataSize;
    private ImageView carPic;
    private TextView count, name;
    private TextView power, distance, price;
    private TextView titleEnergy;

    // ----------------------------------------------------
    public CarViewHolder(int dataSize) {
        this.dataSize = dataSize;
    }

    // ----------------------------------------------------
    @Override
    public View createView(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.module_car_rental, null);

        carPic = (ImageView) view.findViewById(R.id.car_image);
        count = (TextView) view.findViewById(R.id.list_count);
        name = (TextView) view.findViewById(R.id.car_name);
        titleEnergy = (TextView) view.findViewById(R.id.title_energy);
        power = (TextView) view.findViewById(R.id.power);
        distance = (TextView) view.findViewById(R.id.car_distance);
        price = (TextView) view.findViewById(R.id.price);

        return view;
    }

    // ----------------------------------------------------
    @Override
    public void UpdateUI(Context context, int position, Parking.CarGroupBean data) {

        Glide.with(context).load(String.format(ConnectInfo.IMAGE_PATH, data.getCarPicUrl())).into(carPic);
        count.setText((position + 1) + "/" + dataSize);
        name.setText(String.format(context.getResources().getString(R.string.car_name), data.getCarSeries(), data.getCarNo()));
        power.setText(data.getCurrentElectri());
        distance.setText(data.getAvailableTrip());
        price.setText(String.valueOf(data.getRentByFuelKm()));

        if (data.getCarEnergyType() == CarEnergyType.ELECTRIC_CAR) {
            titleEnergy.setText(context.getString(R.string.map_mode_power_electric));
        } else {
            titleEnergy.setText(context.getString(R.string.map_mode_power));
        }

    }
    // ----------------------------------------------------
}
