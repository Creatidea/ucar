package tw.com.car_plus.carshare.rent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;

/**
 * Created by neil on 2017/4/19.
 */

public class CityAdapter extends BaseAdapter {

    // ----------------------------------------------------
    private List<City> cities;

    // ----------------------------------------------------
    public CityAdapter(List<City> cities) {
        this.cities = cities;
    }

    // ----------------------------------------------------
    @Override
    public int getCount() {
        return cities.size();
    }

    // ----------------------------------------------------
    @Override
    public Object getItem(int position) {
        return position;
    }

    // ----------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AreaAdapter.ItemViewHolder holder;

        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_map_mode, parent, false);
            holder = new AreaAdapter.ItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (AreaAdapter.ItemViewHolder) convertView.getTag();
        }

        holder.item.setText(cities.get(position).getCityName());

        return convertView;
    }

    // ----------------------------------------------------
    static class ItemViewHolder {

        @BindView(R.id.item)
        TextView item;

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
}
