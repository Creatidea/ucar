package tw.com.car_plus.carshare.rent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType;

/**
 * Created by neil on 2017/3/16.
 */

public class ListModeAdapter extends BaseExpandableListAdapter {

    // ----------------------------------------------------
    private final int ITEM_0 = 0;
    private final int ITEM_1 = 1;

    private List<Parking> parkings;
    private OnNaviClickListener naviClickListener;
    private OnParkingClickListener parkingClickListener;

    // ----------------------------------------------------
    public ListModeAdapter(List<Parking> parkings) {
        this.parkings = parkings;
    }

    // ----------------------------------------------------
    @Override
    public int getGroupCount() {
        return parkings.size();
    }

    // ----------------------------------------------------
    @Override
    public int getChildrenCount(int groupPosition) {
        return parkings.get(groupPosition).getCarGroup().size();
    }

    // ----------------------------------------------------
    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    // ----------------------------------------------------
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    // ----------------------------------------------------
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public int getChildType(int groupPosition, int childPosition) {

        if (parkings.get(groupPosition).getCarGroup().get(childPosition).getCarNo() != null) {
            return ITEM_1;
        } else {
            return ITEM_0;
        }
    }

    // ----------------------------------------------------
    @Override
    public int getChildTypeCount() {
        return 2;
    }

    // ----------------------------------------------------
    @Override
    public boolean hasStableIds() {
        return false;
    }

    // ----------------------------------------------------
    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {

        final MainViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_main_list_rental, parent, false);
            holder = new MainViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (MainViewHolder) convertView.getTag();
        }

        Parking parking = parkings.get(groupPosition);

        holder.title.setText(String.format(parent.getContext().getString(R.string.list_mode_main_title), parking.getName(), parking.getCarGroup()
                .size() - 1));

        if (isExpanded) {
            holder.arrow.setImageResource(R.drawable.ic_close);
        } else {
            holder.arrow.setImageResource(R.drawable.ic_expand);
        }

        return convertView;
    }

    // ----------------------------------------------------
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        SubViewHolderOne subViewHolderOne;
        SubViewHolderTwo subViewHolderTwo;

        if (convertView == null) {

            if (parkings.get(groupPosition).getCarGroup().get(childPosition).getCarNo() != null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sub_list_rental, parent, false);
                subViewHolderOne = new SubViewHolderOne(convertView);
                convertView.setTag(subViewHolderOne);
                setViewOne(parent, subViewHolderOne, groupPosition, childPosition);
            } else {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sub_action_list_rental, parent, false);
                subViewHolderTwo = new SubViewHolderTwo(convertView);
                convertView.setTag(subViewHolderTwo);
                setViewTwo(subViewHolderTwo, groupPosition);
            }

        } else {
            if (parkings.get(groupPosition).getCarGroup().get(childPosition).getCarNo() != null) {
                subViewHolderOne = (SubViewHolderOne) convertView.getTag();
                setViewOne(parent, subViewHolderOne, groupPosition, childPosition);
            } else {
                subViewHolderTwo = (SubViewHolderTwo) convertView.getTag();
                setViewTwo(subViewHolderTwo, groupPosition);
            }
        }

        return convertView;
    }

    // ----------------------------------------------------
    private void setViewOne(ViewGroup parent, SubViewHolderOne subViewHolder, int groupPosition, int childPosition) {

        Parking.CarGroupBean car = parkings.get(groupPosition).getCarGroup().get(childPosition);

        Glide.with(parent.getContext()).load(String.format(ConnectInfo.IMAGE_PATH, car.getCarPicUrl())).into(subViewHolder.carImage);
        subViewHolder.carName.setText(String.format(parent.getContext().getResources().getString(R.string.car_name), car.getCarSeries(), car
                .getCarNo()));
        subViewHolder.carDistance.setText(car.getAvailableTrip());
        subViewHolder.power.setText(car.getCurrentElectri());
        subViewHolder.price.setText(String.valueOf(car.getRentByFuelKm()));
        subViewHolder.listCount.setText(childPosition + "/" + (parkings.get(groupPosition).getCarGroup().size() - 1));

        if (car.getCarEnergyType() == CarEnergyType.ELECTRIC_CAR) {
            subViewHolder.titleEnergy.setText(parent.getContext().getString(R.string.map_mode_power_electric));
        } else {
            subViewHolder.titleEnergy.setText(parent.getContext().getString(R.string.map_mode_power));
        }
    }

    // ----------------------------------------------------
    private void setViewTwo(SubViewHolderTwo subViewHolderTwo, final int groupPosition) {

        subViewHolderTwo.layoutParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parkingClickListener != null) {
                    parkingClickListener.onParkingClick(groupPosition);
                }
            }
        });

        subViewHolderTwo.layoutNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (naviClickListener != null) {
                    naviClickListener.onNaviClick(groupPosition);
                }
            }
        });

    }

    // ----------------------------------------------------
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    // ----------------------------------------------------
    static class MainViewHolder {

        @BindView(R.id.arrow)
        ImageView arrow;
        @BindView(R.id.title)
        TextView title;

        public MainViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
    static class SubViewHolderOne {

        @BindView(R.id.list_count)
        TextView listCount;
        @BindView(R.id.car_image)
        ImageView carImage;
        @BindView(R.id.car_name)
        TextView carName;
        @BindView(R.id.power)
        TextView power;
        @BindView(R.id.car_distance)
        TextView carDistance;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.title_energy)
        TextView titleEnergy;

        public SubViewHolderOne(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
    static class SubViewHolderTwo {

        @BindView(R.id.layout_parking)
        RelativeLayout layoutParking;
        @BindView(R.id.layout_navigation)
        RelativeLayout layoutNavigation;

        SubViewHolderTwo(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------

    /**
     * 設定按下導航的監聽事件
     *
     * @param naviClickListener
     */
    public void setOnNaviClickListener(OnNaviClickListener naviClickListener) {
        this.naviClickListener = naviClickListener;
    }

    // ----------------------------------------------------

    /**
     * 按下前往停車場資訊的監聽事件
     *
     * @param parkingClickListener
     */
    public void setOnParkingClickListener(OnParkingClickListener parkingClickListener) {
        this.parkingClickListener = parkingClickListener;
    }

    // ----------------------------------------------------
    public interface OnNaviClickListener {
        void onNaviClick(int position);
    }

    // ----------------------------------------------------
    public interface OnParkingClickListener {
        void onParkingClick(int groupPosition);
    }

    // ----------------------------------------------------
}
