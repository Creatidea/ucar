package tw.com.car_plus.carshare.rent.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jhen on 2018/1/18.
 */

public class CancelErrorOrder {


    /**
     * Status : false
     * Errors : [{"ErrorCode":-100,"ErrorMsg":"此停車場已訂閱。"}]
     */

    @SerializedName("Status")
    private boolean Status;
    @SerializedName("Errors")
    private List<ErrorsBean> Errors;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public List<ErrorsBean> getErrors() {
        return Errors;
    }

    public void setErrors(List<ErrorsBean> Errors) {
        this.Errors = Errors;
    }

    public static class ErrorsBean {
        /**
         * ErrorCode : -100
         * ErrorMsg : 此停車場已訂閱。
         */

        @SerializedName("ErrorCode")
        private int ErrorCode;
        @SerializedName("ErrorMsg")
        private String ErrorMsg;

        public int getErrorCode() {
            return ErrorCode;
        }

        public void setErrorCode(int ErrorCode) {
            this.ErrorCode = ErrorCode;
        }

        public String getErrorMsg() {
            return ErrorMsg;
        }

        public void setErrorMsg(String ErrorMsg) {
            this.ErrorMsg = ErrorMsg;
        }
    }
}
