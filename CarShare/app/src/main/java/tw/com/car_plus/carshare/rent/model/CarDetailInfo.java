package tw.com.car_plus.carshare.rent.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by winni on 2017/3/15.
 */

public class CarDetailInfo implements Parcelable {


    /**
     * CarType : 汽油
     * CarFuelType : 1
     * AvailableTrip :
     * SeatCount : 5
     * RentByQuarter : 50
     * RentLimitByDay : 3000
     * RentByNight : 600
     * NightTimeStart : 20
     * NightTimeEnd : 8
     * ParkinglotId : 1
     * ParkinglotName : 小巨蛋
     * ParkinglotAddress : 臺北市中正區南京東路四段一號
     * ParkinglotLon : 121.54953069999999
     * ParkinglotLat : 25.0519742
     * CarId : 26
     * CarNo : RBL-3313
     * CarBrand : 納智捷(Luxgen)(國產)
     * CarSeries : S3
     * CarPicUrl : /FileDownload/GetPic?fileName=cd2d8340581f441ca74d99c48e98ef41.png&uploadType=3
     * CurrentElectri :
     * MaxMileage : 250
     * RentByFuelKm : 1
     * RentByHour : 200
     */

    @SerializedName("CarType")
    private String CarType;
    @SerializedName("CarFuelType")
    private String CarFuelType;
    @SerializedName("CarEnergyType")
    private int CarEnergyType;
    @SerializedName("CarVigType")
    private int CarVigType;
    @SerializedName("AvailableTrip")
    private String AvailableTrip;
    @SerializedName("SeatCount")
    private int SeatCount;
    @SerializedName("RentByQuarter")
    private int RentByQuarter;
    @SerializedName("RentLimitByDay")
    private int RentLimitByDay;
    @SerializedName("RentByNight")
    private int RentByNight;
    @SerializedName("NightTimeStart")
    private int NightTimeStart;
    @SerializedName("NightTimeEnd")
    private int NightTimeEnd;
    @SerializedName("ParkinglotId")
    private int ParkinglotId;
    @SerializedName("ParkinglotName")
    private String ParkinglotName;
    @SerializedName("ParkinglotAddress")
    private String ParkinglotAddress;
    @SerializedName("ParkinglotLon")
    private String ParkinglotLon;
    @SerializedName("ParkinglotLat")
    private String ParkinglotLat;
    @SerializedName("CarId")
    private int CarId;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("CarBrand")
    private String CarBrand;
    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarPicUrl")
    private String CarPicUrl;
    @SerializedName("CurrentElectri")
    private String CurrentElectri;
    @SerializedName("MaxMileage")
    private String MaxMileage;
    @SerializedName("RentByFuelKm")
    private int RentByFuelKm;
    @SerializedName("RentByHour")
    private int RentByHour;
    @SerializedName("ParkinglotFloorDesc")
    private String ParkinglotFloorDesc;


    public String getCarType() {
        return CarType;
    }

    public void setCarType(String CarType) {
        this.CarType = CarType;
    }

    public int getCarEnergyType() {
        return CarEnergyType;
    }

    public void setCarEnergyType(int carEnergyType) {
        CarEnergyType = carEnergyType;
    }

    public int getCarVigType() {
        return CarVigType;
    }

    public void setCarVigType(int carVigType) {
        CarVigType = carVigType;
    }

    public String getCarFuelType() {
        return CarFuelType;
    }

    public void setCarFuelType(String CarFuelType) {
        this.CarFuelType = CarFuelType;
    }

    public String getAvailableTrip() {
        return AvailableTrip;
    }

    public void setAvailableTrip(String AvailableTrip) {
        this.AvailableTrip = AvailableTrip;
    }

    public int getSeatCount() {
        return SeatCount;
    }

    public void setSeatCount(int SeatCount) {
        this.SeatCount = SeatCount;
    }

    public int getRentByQuarter() {
        return RentByQuarter;
    }

    public void setRentByQuarter(int RentByQuarter) {
        this.RentByQuarter = RentByQuarter;
    }

    public int getRentLimitByDay() {
        return RentLimitByDay;
    }

    public void setRentLimitByDay(int RentLimitByDay) {
        this.RentLimitByDay = RentLimitByDay;
    }

    public int getRentByNight() {
        return RentByNight;
    }

    public void setRentByNight(int RentByNight) {
        this.RentByNight = RentByNight;
    }

    public int getNightTimeStart() {
        return NightTimeStart;
    }

    public void setNightTimeStart(int NightTimeStart) {
        this.NightTimeStart = NightTimeStart;
    }

    public int getNightTimeEnd() {
        return NightTimeEnd;
    }

    public void setNightTimeEnd(int NightTimeEnd) {
        this.NightTimeEnd = NightTimeEnd;
    }

    public int getParkinglotId() {
        return ParkinglotId;
    }

    public void setParkinglotId(int ParkinglotId) {
        this.ParkinglotId = ParkinglotId;
    }

    public String getParkinglotName() {
        return ParkinglotName;
    }

    public void setParkinglotName(String ParkinglotName) {
        this.ParkinglotName = ParkinglotName;
    }

    public String getParkinglotAddress() {
        return ParkinglotAddress;
    }

    public void setParkinglotAddress(String ParkinglotAddress) {
        this.ParkinglotAddress = ParkinglotAddress;
    }

    public String getParkinglotLon() {
        return ParkinglotLon;
    }

    public void setParkinglotLon(String ParkinglotLon) {
        this.ParkinglotLon = ParkinglotLon;
    }

    public String getParkinglotLat() {
        return ParkinglotLat;
    }

    public void setParkinglotLat(String ParkinglotLat) {
        this.ParkinglotLat = ParkinglotLat;
    }

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int CarId) {
        this.CarId = CarId;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String CarBrand) {
        this.CarBrand = CarBrand;
    }

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarPicUrl() {
        return CarPicUrl;
    }

    public void setCarPicUrl(String CarPicUrl) {
        this.CarPicUrl = CarPicUrl;
    }

    public String getCurrentElectri() {
        return CurrentElectri;
    }

    public void setCurrentElectri(String CurrentElectri) {
        this.CurrentElectri = CurrentElectri;
    }

    public String getMaxMileage() {
        return MaxMileage;
    }

    public void setMaxMileage(String MaxMileage) {
        this.MaxMileage = MaxMileage;
    }

    public int getRentByFuelKm() {
        return RentByFuelKm;
    }

    public void setRentByFuelKm(int RentByFuelKm) {
        this.RentByFuelKm = RentByFuelKm;
    }

    public int getRentByHour() {
        return RentByHour;
    }

    public void setRentByHour(int RentByHour) {
        this.RentByHour = RentByHour;
    }

    public String getParkinglotFloorDesc() {
        return ParkinglotFloorDesc;
    }

    public void setParkinglotFloorDesc(String parkinglotFloorDesc) {
        ParkinglotFloorDesc = parkinglotFloorDesc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.CarType);
        dest.writeString(this.CarFuelType);
        dest.writeInt(this.CarEnergyType);
        dest.writeInt(this.CarVigType);
        dest.writeString(this.AvailableTrip);
        dest.writeInt(this.SeatCount);
        dest.writeInt(this.RentByQuarter);
        dest.writeInt(this.RentLimitByDay);
        dest.writeInt(this.RentByNight);
        dest.writeInt(this.NightTimeStart);
        dest.writeInt(this.NightTimeEnd);
        dest.writeInt(this.ParkinglotId);
        dest.writeString(this.ParkinglotName);
        dest.writeString(this.ParkinglotAddress);
        dest.writeString(this.ParkinglotLon);
        dest.writeString(this.ParkinglotLat);
        dest.writeInt(this.CarId);
        dest.writeString(this.CarNo);
        dest.writeString(this.CarBrand);
        dest.writeString(this.CarSeries);
        dest.writeString(this.CarPicUrl);
        dest.writeString(this.CurrentElectri);
        dest.writeString(this.MaxMileage);
        dest.writeInt(this.RentByFuelKm);
        dest.writeInt(this.RentByHour);
        dest.writeString(this.ParkinglotFloorDesc);
    }

    public CarDetailInfo() {
    }

    protected CarDetailInfo(Parcel in) {
        this.CarType = in.readString();
        this.CarFuelType = in.readString();
        this.CarEnergyType = in.readInt();
        this.CarVigType = in.readInt();
        this.AvailableTrip = in.readString();
        this.SeatCount = in.readInt();
        this.RentByQuarter = in.readInt();
        this.RentLimitByDay = in.readInt();
        this.RentByNight = in.readInt();
        this.NightTimeStart = in.readInt();
        this.NightTimeEnd = in.readInt();
        this.ParkinglotId = in.readInt();
        this.ParkinglotName = in.readString();
        this.ParkinglotAddress = in.readString();
        this.ParkinglotLon = in.readString();
        this.ParkinglotLat = in.readString();
        this.CarId = in.readInt();
        this.CarNo = in.readString();
        this.CarBrand = in.readString();
        this.CarSeries = in.readString();
        this.CarPicUrl = in.readString();
        this.CurrentElectri = in.readString();
        this.MaxMileage = in.readString();
        this.RentByFuelKm = in.readInt();
        this.RentByHour = in.readInt();
        this.ParkinglotFloorDesc = in.readString();
    }

    public static final Parcelable.Creator<CarDetailInfo> CREATOR = new Parcelable.Creator<CarDetailInfo>() {
        @Override
        public CarDetailInfo createFromParcel(Parcel source) {
            return new CarDetailInfo(source);
        }

        @Override
        public CarDetailInfo[] newArray(int size) {
            return new CarDetailInfo[size];
        }
    };
}
