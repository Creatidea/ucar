package tw.com.car_plus.carshare.rent.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;
import java.util.List;

/**
 * Created by neil on 2017/3/14.
 */

public class Parking implements Parcelable, Comparator<Parking> {

    /**
     * ParkinglotId : 1
     * Name : 小巨蛋
     * Address : 臺北市中正區南京東路四段一號
     * Longitude : 121.54953069999999
     * Latitude : 25.0519742
     * TotalCarCount : 9
     * TotalCanGetCarCount : 5
     * ParkingSpaceCount : 4
     * ChargeSiteCount : 0
     * ParkinglotPicUrl : /FileDownload/GetPic?fileName=4dafd8b32d374c9baee02ecff942b2fa.jpg&uploadType=2
     * SpaceInfo : 地下一樓
     * LocationDesc : 台北小巨蛋停車場
     * PaymodeDesc : 免費
     * ParkinglotType : 1
     * TimeDesc : 24H
     * CarGroup : [{"CarId":24,"CarNo":"RBL-1203","CarBrand":"納智捷(Luxgen)(國產) ","CarSeries":"S3",
     * "CarPicUrl":"/FileDownload/GetPic?fileName=cd2d8340581f441ca74d99c48e98ef41.png&uploadType=3","CurrentElectri":"","MaxMileage":"250",
     * "RentByFuelKm":1,"RentByHour":200}]
     * Beacons : [{"BeaconId":1,"BeaconNo":"ion123"}]
     */

    @SerializedName("ParkinglotId")
    private int ParkinglotId;
    @SerializedName("Name")
    private String Name;
    @SerializedName("Address")
    private String Address;
    @SerializedName("Longitude")
    private String Longitude;
    @SerializedName("Latitude")
    private String Latitude;
    @SerializedName("TotalCarCount")
    private int TotalCarCount;
    @SerializedName("TotalCanGetCarCount")
    private int TotalCanGetCarCount;
    @SerializedName("ParkingSpaceCount")
    private int ParkingSpaceCount;
    @SerializedName("ChargeSiteCount")
    private int ChargeSiteCount;
    @SerializedName("ParkinglotPicUrl")
    private String ParkinglotPicUrl;
    @SerializedName("SpaceInfo")
    private String SpaceInfo;
    @SerializedName("LocationDesc")
    private String LocationDesc;
    @SerializedName("PaymodeDesc")
    private String PaymodeDesc;
    @SerializedName("ParkinglotType")
    private int ParkinglotType;
    @SerializedName("TimeDesc")
    private String TimeDesc;
    @SerializedName("CarGroup")
    private List<CarGroupBean> CarGroup;
    @SerializedName("Beacons")
    private List<BeaconsBean> Beacons;
    private Integer distance;
    private boolean isRemind;

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public int getParkinglotId() {
        return ParkinglotId;
    }

    public void setParkinglotId(int ParkinglotId) {
        this.ParkinglotId = ParkinglotId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public int getTotalCarCount() {
        return TotalCarCount;
    }

    public void setTotalCarCount(int TotalCarCount) {
        this.TotalCarCount = TotalCarCount;
    }

    public int getTotalCanGetCarCount() {
        return TotalCanGetCarCount;
    }

    public void setTotalCanGetCarCount(int TotalCanGetCarCount) {
        this.TotalCanGetCarCount = TotalCanGetCarCount;
    }

    public int getParkingSpaceCount() {
        return ParkingSpaceCount;
    }

    public void setParkingSpaceCount(int ParkingSpaceCount) {
        this.ParkingSpaceCount = ParkingSpaceCount;
    }

    public int getChargeSiteCount() {
        return ChargeSiteCount;
    }

    public void setChargeSiteCount(int ChargeSiteCount) {
        this.ChargeSiteCount = ChargeSiteCount;
    }

    public String getParkinglotPicUrl() {
        return ParkinglotPicUrl;
    }

    public void setParkinglotPicUrl(String ParkinglotPicUrl) {
        this.ParkinglotPicUrl = ParkinglotPicUrl;
    }

    public String getSpaceInfo() {
        return SpaceInfo;
    }

    public void setSpaceInfo(String SpaceInfo) {
        this.SpaceInfo = SpaceInfo;
    }

    public String getLocationDesc() {
        return LocationDesc;
    }

    public void setLocationDesc(String LocationDesc) {
        this.LocationDesc = LocationDesc;
    }

    public String getPaymodeDesc() {
        return PaymodeDesc;
    }

    public void setPaymodeDesc(String PaymodeDesc) {
        this.PaymodeDesc = PaymodeDesc;
    }

    public int getParkinglotType() {
        return ParkinglotType;
    }

    public void setParkinglotType(int ParkinglotType) {
        this.ParkinglotType = ParkinglotType;
    }

    public String getTimeDesc() {
        return TimeDesc;
    }

    public void setTimeDesc(String TimeDesc) {
        this.TimeDesc = TimeDesc;
    }

    public List<CarGroupBean> getCarGroup() {
        return CarGroup;
    }

    public void setCarGroup(List<CarGroupBean> CarGroup) {
        this.CarGroup = CarGroup;
    }

    public List<BeaconsBean> getBeacons() {
        return Beacons;
    }

    public void setBeacons(List<BeaconsBean> Beacons) {
        this.Beacons = Beacons;
    }

    public boolean isRemind() {
        return isRemind;
    }

    public void setRemind(boolean remind) {
        isRemind = remind;
    }

    @Override
    public int compare(Parking o1, Parking o2) {
        return o1.getDistance().compareTo(o2.getDistance());
    }


    public static class CarGroupBean implements Parcelable {
        /**
         * CarId : 24
         * CarNo : RBL-1203
         * CarBrand : 納智捷(Luxgen)(國產)
         * CarSeries : S3
         * CarPicUrl : /FileDownload/GetPic?fileName=cd2d8340581f441ca74d99c48e98ef41.png&uploadType=3
         * CurrentElectri :
         * MaxMileage : 250
         * RentByFuelKm : 1
         * RentByHour : 200
         */

        @SerializedName("CarId")
        private int CarId;
        @SerializedName("CarNo")
        private String CarNo;
        @SerializedName("CarFuelType")
        private String CarFuelType;
        @SerializedName("CarEnergyType")
        private int CarEnergyType;
        @SerializedName("CarVigType")
        private int CarVigType;
        @SerializedName("CarBrand")
        private String CarBrand;
        @SerializedName("CarSeries")
        private String CarSeries;
        @SerializedName("CarPicUrl")
        private String CarPicUrl;
        @SerializedName("CurrentElectri")
        private String CurrentElectri;
        @SerializedName("MaxMileage")
        private String MaxMileage;
        @SerializedName("AvailableTrip")
        private String AvailableTrip;
        @SerializedName("RentByFuelKm")
        private int RentByFuelKm;
        @SerializedName("RentByHour")
        private int RentByHour;

        public int getCarId() {
            return CarId;
        }

        public void setCarId(int CarId) {
            this.CarId = CarId;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getCarFuelType() {
            return CarFuelType;
        }

        public void setCarFuelType(String carFuelType) {
            CarFuelType = carFuelType;
        }

        public int getCarEnergyType() {
            return CarEnergyType;
        }

        public void setCarEnergyType(int carEnergyType) {
            CarEnergyType = carEnergyType;
        }

        public int getCarVigType() {
            return CarVigType;
        }

        public void setCarVigType(int carVigType) {
            CarVigType = carVigType;
        }

        public String getCarBrand() {
            return CarBrand;
        }

        public void setCarBrand(String CarBrand) {
            this.CarBrand = CarBrand;
        }

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarPicUrl() {
            return CarPicUrl;
        }

        public void setCarPicUrl(String CarPicUrl) {
            this.CarPicUrl = CarPicUrl;
        }

        public String getCurrentElectri() {
            return CurrentElectri;
        }

        public void setCurrentElectri(String CurrentElectri) {
            this.CurrentElectri = CurrentElectri;
        }

        public String getMaxMileage() {
            return MaxMileage;
        }

        public void setMaxMileage(String MaxMileage) {
            this.MaxMileage = MaxMileage;
        }

        public String getAvailableTrip() {
            return AvailableTrip;
        }

        public void setAvailableTrip(String availableTrip) {
            AvailableTrip = availableTrip;
        }

        public int getRentByFuelKm() {
            return RentByFuelKm;
        }

        public void setRentByFuelKm(int RentByFuelKm) {
            this.RentByFuelKm = RentByFuelKm;
        }

        public int getRentByHour() {
            return RentByHour;
        }

        public void setRentByHour(int RentByHour) {
            this.RentByHour = RentByHour;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.CarId);
            dest.writeString(this.CarNo);
            dest.writeString(this.CarFuelType);
            dest.writeInt(this.CarEnergyType);
            dest.writeInt(this.CarVigType);
            dest.writeString(this.CarBrand);
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarPicUrl);
            dest.writeString(this.CurrentElectri);
            dest.writeString(this.AvailableTrip);
            dest.writeString(this.MaxMileage);
            dest.writeInt(this.RentByFuelKm);
            dest.writeInt(this.RentByHour);
        }

        public CarGroupBean() {
        }

        protected CarGroupBean(Parcel in) {
            this.CarId = in.readInt();
            this.CarNo = in.readString();
            this.CarFuelType = in.readString();
            this.CarEnergyType = in.readInt();
            this.CarVigType = in.readInt();
            this.CarBrand = in.readString();
            this.CarSeries = in.readString();
            this.CarPicUrl = in.readString();
            this.CurrentElectri = in.readString();
            this.MaxMileage = in.readString();
            this.AvailableTrip = in.readString();
            this.RentByFuelKm = in.readInt();
            this.RentByHour = in.readInt();
        }

        public static final Creator<CarGroupBean> CREATOR = new Creator<CarGroupBean>() {
            @Override
            public CarGroupBean createFromParcel(Parcel source) {
                return new CarGroupBean(source);
            }

            @Override
            public CarGroupBean[] newArray(int size) {
                return new CarGroupBean[size];
            }
        };
    }

    public static class BeaconsBean implements Parcelable {
        /**
         * BeaconId : 1
         * BeaconNo : ion123
         */

        @SerializedName("BeaconId")
        private int BeaconId;
        @SerializedName("BeaconNo")
        private String BeaconNo;

        public int getBeaconId() {
            return BeaconId;
        }

        public void setBeaconId(int BeaconId) {
            this.BeaconId = BeaconId;
        }

        public String getBeaconNo() {
            return BeaconNo;
        }

        public void setBeaconNo(String BeaconNo) {
            this.BeaconNo = BeaconNo;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.BeaconId);
            dest.writeString(this.BeaconNo);
        }

        public BeaconsBean() {
        }

        protected BeaconsBean(Parcel in) {
            this.BeaconId = in.readInt();
            this.BeaconNo = in.readString();
        }

        public static final Creator<BeaconsBean> CREATOR = new Creator<BeaconsBean>() {
            @Override
            public BeaconsBean createFromParcel(Parcel source) {
                return new BeaconsBean(source);
            }

            @Override
            public BeaconsBean[] newArray(int size) {
                return new BeaconsBean[size];
            }
        };
    }

    public Parking() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ParkinglotId);
        dest.writeString(this.Name);
        dest.writeString(this.Address);
        dest.writeString(this.Longitude);
        dest.writeString(this.Latitude);
        dest.writeInt(this.TotalCarCount);
        dest.writeInt(this.TotalCanGetCarCount);
        dest.writeInt(this.ParkingSpaceCount);
        dest.writeInt(this.ChargeSiteCount);
        dest.writeString(this.ParkinglotPicUrl);
        dest.writeString(this.SpaceInfo);
        dest.writeString(this.LocationDesc);
        dest.writeString(this.PaymodeDesc);
        dest.writeInt(this.ParkinglotType);
        dest.writeString(this.TimeDesc);
        dest.writeTypedList(this.CarGroup);
        dest.writeTypedList(this.Beacons);
        dest.writeValue(this.distance);
    }

    protected Parking(Parcel in) {
        this.ParkinglotId = in.readInt();
        this.Name = in.readString();
        this.Address = in.readString();
        this.Longitude = in.readString();
        this.Latitude = in.readString();
        this.TotalCarCount = in.readInt();
        this.TotalCanGetCarCount = in.readInt();
        this.ParkingSpaceCount = in.readInt();
        this.ChargeSiteCount = in.readInt();
        this.ParkinglotPicUrl = in.readString();
        this.SpaceInfo = in.readString();
        this.LocationDesc = in.readString();
        this.PaymodeDesc = in.readString();
        this.ParkinglotType = in.readInt();
        this.TimeDesc = in.readString();
        this.CarGroup = in.createTypedArrayList(CarGroupBean.CREATOR);
        this.Beacons = in.createTypedArrayList(BeaconsBean.CREATOR);
        this.distance = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Parking> CREATOR = new Creator<Parking>() {
        @Override
        public Parking createFromParcel(Parcel source) {
            return new Parking(source);
        }

        @Override
        public Parking[] newArray(int size) {
            return new Parking[size];
        }
    };
}
