package tw.com.car_plus.carshare.rent.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by winni on 2017/3/16.
 * 預約成功資訊
 */

public class ReservationInfo implements Parcelable {


    /**
     * OrderId : 4858
     * BookingOnUtc : 2017-03-16T08:20:06.0531978Z
     * PreorderExtensionNum : 0
     * ReservationStartOnUtc : 2017-03-16T18:37:00Z
     * PreorderExpireOnUtc : 2017-03-16T08:50:06.1938269Z
     * ParkinglotName : 小巨蛋
     * ParkinglotLongitude : 121.54953069999999
     * ParkinglotLatitude : 25.0519742
     * CarBrand : 納智捷(Luxgen)(國產)
     * CarSeries : S3
     * CarNo : RBL-1203
     * CarPic : /FileDownload/GetPic?fileName=cd2d8340581f441ca74d99c48e98ef41.png&uploadType=3
     * CarEnergyType : 2
     */

    @SerializedName("OrderId")
    private int OrderId;
    @SerializedName("BookingOnUtc")
    private String BookingOnUtc;
    @SerializedName("PreorderExtensionNum")
    private int PreorderExtensionNum;
    @SerializedName("ReservationStartOnUtc")
    private String ReservationStartOnUtc;
    @SerializedName(value = "PreorderExpireOnUtc", alternate = {"PreOrderExpireOnUtc"})
    private String PreorderExpireOnUtc;
    @SerializedName("ParkinglotName")
    private String ParkinglotName;
    @SerializedName("ParkinglotLongitude")
    private String ParkinglotLongitude;
    @SerializedName("ParkinglotLatitude")
    private String ParkinglotLatitude;
    @SerializedName("CarBrand")
    private String CarBrand;
    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("CarPic")
    private String CarPic;
    @SerializedName("CarEnergyType")
    private int CarEnergyType;

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public String getBookingOnUtc() {
        return BookingOnUtc;
    }

    public void setBookingOnUtc(String BookingOnUtc) {
        this.BookingOnUtc = BookingOnUtc;
    }

    public int getPreorderExtensionNum() {
        return PreorderExtensionNum;
    }

    public void setPreorderExtensionNum(int PreorderExtensionNum) {
        this.PreorderExtensionNum = PreorderExtensionNum;
    }

    public String getReservationStartOnUtc() {
        return ReservationStartOnUtc;
    }

    public void setReservationStartOnUtc(String ReservationStartOnUtc) {
        this.ReservationStartOnUtc = ReservationStartOnUtc;
    }

    public String getPreorderExpireOnUtc() {
        return PreorderExpireOnUtc;
    }

    public void setPreorderExpireOnUtc(String PreorderExpireOnUtc) {
        this.PreorderExpireOnUtc = PreorderExpireOnUtc;
    }

    public String getParkinglotName() {
        return ParkinglotName;
    }

    public void setParkinglotName(String ParkinglotName) {
        this.ParkinglotName = ParkinglotName;
    }

    public String getParkinglotLongitude() {
        return ParkinglotLongitude;
    }

    public void setParkinglotLongitude(String ParkinglotLongitude) {
        this.ParkinglotLongitude = ParkinglotLongitude;
    }

    public String getParkinglotLatitude() {
        return ParkinglotLatitude;
    }

    public void setParkinglotLatitude(String ParkinglotLatitude) {
        this.ParkinglotLatitude = ParkinglotLatitude;
    }

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String CarBrand) {
        this.CarBrand = CarBrand;
    }

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getCarPic() {
        return CarPic;
    }

    public void setCarPic(String CarPic) {
        this.CarPic = CarPic;
    }

    public int getCarEnergyType() {
        return CarEnergyType;
    }

    public void setCarEnergyType(int carEnergyType) {
        CarEnergyType = carEnergyType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.OrderId);
        dest.writeString(this.BookingOnUtc);
        dest.writeInt(this.PreorderExtensionNum);
        dest.writeString(this.ReservationStartOnUtc);
        dest.writeString(this.PreorderExpireOnUtc);
        dest.writeString(this.ParkinglotName);
        dest.writeString(this.ParkinglotLongitude);
        dest.writeString(this.ParkinglotLatitude);
        dest.writeString(this.CarBrand);
        dest.writeString(this.CarSeries);
        dest.writeString(this.CarNo);
        dest.writeString(this.CarPic);
        dest.writeInt(this.CarEnergyType);
    }

    public ReservationInfo() {
    }

    protected ReservationInfo(Parcel in) {
        this.OrderId = in.readInt();
        this.BookingOnUtc = in.readString();
        this.PreorderExtensionNum = in.readInt();
        this.ReservationStartOnUtc = in.readString();
        this.PreorderExpireOnUtc = in.readString();
        this.ParkinglotName = in.readString();
        this.ParkinglotLongitude = in.readString();
        this.ParkinglotLatitude = in.readString();
        this.CarBrand = in.readString();
        this.CarSeries = in.readString();
        this.CarNo = in.readString();
        this.CarPic = in.readString();
        this.CarEnergyType = in.readInt();
    }

    public static final Parcelable.Creator<ReservationInfo> CREATOR = new Parcelable.Creator<ReservationInfo>() {
        @Override
        public ReservationInfo createFromParcel(Parcel source) {
            return new ReservationInfo(source);
        }

        @Override
        public ReservationInfo[] newArray(int size) {
            return new ReservationInfo[size];
        }
    };
}
