package tw.com.car_plus.carshare.rent.pickup_car;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.util.camera.BaseCameraActivity;

/**
 * Created by winni on 2017/3/18.
 */
@RuntimePermissions
public class CheckCarActivity extends BaseCameraActivity implements View.OnClickListener {

    @BindView(R.id.scrollview)
    ScrollView scrollview;
    //派車單（企業版用）
    @BindView(R.id.layout_image_0)
    View carInvoiceView;
    //左前
    @BindView(R.id.layout_image_1)
    View leftFrontView;
    //右前
    @BindView(R.id.layout_image_2)
    View rightFrontView;
    //右後
    @BindView(R.id.layout_image_3)
    View rightBackView;
    //左後
    @BindView(R.id.layout_image_4)
    View leftBackView;
    //增加的
    @BindView(R.id.layout_image_5)
    View moreView;


    @BindView(R.id.add)
    Button add;
    @BindView(R.id.complete)
    Button complete;

    //左前
    private final int LEFT_FRONT = 22;
    //右前
    private final int RIGHT_FRONT = 33;
    //右後
    private final int RIGHT_BACK = 44;
    //左後
    private final int LEFT_BACK = 55;
    //增加
    private final int MORE = 66;
    //派車單（企業版用）
    private final int CAR_INVOICE = 77;

    private TextView leftFrontTitle, rightFrontTitle, rightBackTitle, leftBackTitle, moreTitle, carInvoiceTitle;
    private TextView carInvoiceNumber, leftFrontNumber, rightFrontNumber, rightBackNumber, leftBackNumber, moreNumber;
    private ImageView leftFront, rightFront, rightBack, leftBack, more, carInvoice;
    private String leftFrontPath = "", rightFrontPath = "", rightBackPath = "", leftBackPath = "", morePath = "", carInvoicePath = "";
    private ArrayList<String> fileNames;
    private List<String> imagePath;
    private ArrayList<TextView> numberArray;
    private int position = 0;
    private boolean isShowDialog = false;

    @Override
    public void init() {

        setContentView(R.layout.activity_check_car);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_check_car);

        numberArray = new ArrayList<>();

        add.setOnClickListener(this);
        complete.setOnClickListener(this);
        //派車單
        carInvoiceTitle = ButterKnife.findById(carInvoiceView, R.id.title);
        carInvoice = ButterKnife.findById(carInvoiceView, R.id.image);
        carInvoiceNumber = ButterKnife.findById(carInvoiceView, R.id.number);

        if (isCompanyAccount()) {
            carInvoiceView.setVisibility(View.VISIBLE);
            setImageData(carInvoiceTitle, carInvoiceNumber, carInvoice, getString(R.string.car_invoice), R.drawable.img_car_invoice, CAR_INVOICE);
        }
        //左前
        leftFrontTitle = ButterKnife.findById(leftFrontView, R.id.title);
        leftFront = ButterKnife.findById(leftFrontView, R.id.image);
        leftFrontNumber = ButterKnife.findById(leftFrontView, R.id.number);
        setImageData(leftFrontTitle, leftFrontNumber, leftFront, getString(R.string.left_front), R.drawable.img_car_left_front, LEFT_FRONT);

        //右前
        rightFrontTitle = ButterKnife.findById(rightFrontView, R.id.title);
        rightFront = ButterKnife.findById(rightFrontView, R.id.image);
        rightFrontNumber = ButterKnife.findById(rightFrontView, R.id.number);
        setImageData(rightFrontTitle, rightFrontNumber, rightFront, getString(R.string.right_front), R.drawable.img_car_right_front, RIGHT_FRONT);

        //右後
        rightBackTitle = ButterKnife.findById(rightBackView, R.id.title);
        rightBack = ButterKnife.findById(rightBackView, R.id.image);
        rightBackNumber = ButterKnife.findById(rightBackView, R.id.number);
        setImageData(rightBackTitle, rightBackNumber, rightBack, getString(R.string.right_back), R.drawable.img_car_right_back, RIGHT_BACK);

        //左後
        leftBackTitle = ButterKnife.findById(leftBackView, R.id.title);
        leftBack = ButterKnife.findById(leftBackView, R.id.image);
        leftBackNumber = ButterKnife.findById(leftBackView, R.id.number);
        setImageData(leftBackTitle, leftBackNumber, leftBack, getString(R.string.left_back), R.drawable.img_car_left_back, LEFT_BACK);

        //其他
        moreTitle = ButterKnife.findById(moreView, R.id.title);
        more = ButterKnife.findById(moreView, R.id.image);
        moreNumber = ButterKnife.findById(moreView, R.id.number);
        setImageData(moreTitle, moreNumber, more, "", R.drawable.img_car, MORE);

        setImageNumber();

        imagePath = new ArrayList<>();
    }

    // ----------------------------------------------------
    private void setImageData(TextView title, TextView number, ImageView imageView, String strTitle, int background, int id) {

        title.setText(strTitle);
        imageView.setId(id);
        imageView.setOnClickListener(this);
        Glide.with(this).load(background).into(imageView);
        numberArray.add(number);

    }

    // ----------------------------------------------------

    /**
     * 設定圖片編號
     */
    private void setImageNumber() {
        for (int i = 0; i < numberArray.size(); i++) {
            numberArray.get(i).setText(String.valueOf(i + 1));
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(UploadFile uploadFile) {

        isShowDialog = true;
        loadingDialogManager.show(getString(R.string.upload_picture));

        fileNames.add(uploadFile.getFileName());
        position++;

        if (fileNames.size() != imagePath.size()) {
            fileUploadConnect.ConnectUploadFile(imagePath.get(position));
        } else {
            cleanPicture(imagePath);
            Toast.makeText(this, getString(R.string.upload_finish), Toast.LENGTH_LONG).show();
            Intent intent = new Intent();
            intent.putStringArrayListExtra("photo", fileNames);
            setResult(RESULT_OK, intent);
            loadingDialogManager.dismiss();
            finish();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {

        if (isShowDialog) {
            loadingDialogManager.dismiss();
            isShowDialog = false;
        }

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_CANCELED) {

            compressionPicture(new CompressionPictureListener() {
                @Override
                public void compressionListener() {

                    switch (requestCode) {
                        //派車單
                        case CAR_INVOICE:
                            carInvoicePath = setImageView(carInvoice);
                            focusOnView(leftFrontView);
                            break;
                        //左前
                        case LEFT_FRONT:
                            leftFrontPath = setImageView(leftFront);
                            focusOnView(rightFrontView);
                            break;
                        //右前
                        case RIGHT_FRONT:
                            rightFrontPath = setImageView(rightFront);
                            focusOnView(rightBackView);
                            break;
                        //右後
                        case RIGHT_BACK:
                            rightBackPath = setImageView(rightBack);
                            focusOnView(leftBackView);
                            break;
                        //左後
                        case LEFT_BACK:
                            leftBackPath = setImageView(leftBack);
                            if (moreView.isShown()) {
                                focusOnView(moreView);
                            }
                            break;
                        //其他
                        case MORE:
                            morePath = setImageView(more);
                            break;
                    }
                }
            });
        }
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CheckCarActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意開啟相機與寫入資料的權限
     */
    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void goToTakePicture(int type) {
        takePictures(type);
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showDeniedForTakePicture() {
        Toast.makeText(this, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add:
                moreView.setVisibility(View.VISIBLE);
                add.setVisibility(View.GONE);
                break;
            case R.id.complete:
                position = 0;
                imagePath.clear();
                checkUploadImage();
                break;
            default:
                clickImage(view.getId());
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 確認上傳車子拍照的資料
     */
    private void checkUploadImage() {

        if (checkCarPicture()) {
            if (networkChecker.isNetworkAvailable()) {
                //表示有照片
                fileNames = new ArrayList<>();
                fileUploadConnect.ConnectUploadFile(imagePath.get(position));
            }
        } else {
            //其他-> 沒完全拍完照片 toast給使用者：請確認是否已拍完所有照片。
            Toast.makeText(this, getString(R.string.check_car_toast_take_picture), Toast.LENGTH_SHORT).show();
        }
    }


    // ----------------------------------------------------
    //檢查是否有拍照
    private boolean checkCarPicture() {

        boolean isLeftFrontPath, isRightFront, isRightBackPath, isLeftBackPath, isMorPath, isCarInvoicePath = true;

        isLeftFrontPath = checkHavePicture(leftFrontPath);
        isRightFront = checkHavePicture(rightFrontPath);
        isRightBackPath = checkHavePicture(rightBackPath);
        isLeftBackPath = checkHavePicture(leftBackPath);

        if (isCompanyAccount()) {
            isCarInvoicePath = checkHavePicture(carInvoicePath);
        }

        if (checkHavePicture(morePath)) {
            isMorPath = checkHavePicture(morePath);
            return isLeftFrontPath && isRightFront && isRightBackPath && isLeftBackPath && isMorPath && isCarInvoicePath;
        } else {
            return isLeftFrontPath && isRightFront && isRightBackPath && isLeftBackPath && isCarInvoicePath;
        }

    }

    // ----------------------------------------------------

    /**
     * 檢查是否有拍照
     *
     * @param path
     * @return
     */
    private boolean checkHavePicture(String path) {
        if (path.length() != 0) {
            imagePath.add(path);
            return true;
        } else {
            return false;
        }
    }

    // ----------------------------------------------------

    /**
     * image的按下事件
     *
     * @param id
     */
    private void clickImage(int id) {

        int type = 0;

        switch (id) {
            case LEFT_FRONT:
                type = LEFT_FRONT;
                break;
            case RIGHT_FRONT:
                type = RIGHT_FRONT;
                break;
            case RIGHT_BACK:
                type = RIGHT_BACK;
                break;
            case LEFT_BACK:
                type = LEFT_BACK;
                break;
            case MORE:
                type = MORE;
                break;
            case CAR_INVOICE:
                type = CAR_INVOICE;
                break;
        }


        CheckCarActivityPermissionsDispatcher.goToTakePictureWithCheck(this, type);
    }

    // ----------------------------------------------------

    /**
     * 將移動至指定的view
     *
     * @param view
     */
    private void focusOnView(final View view) {

        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.scrollTo(0, view.getTop() - 150);
            }
        });
    }

}
