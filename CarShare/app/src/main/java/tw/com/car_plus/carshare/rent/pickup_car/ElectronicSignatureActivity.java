package tw.com.car_plus.carshare.rent.pickup_car;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.FileUploadConnect;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/3/18.
 * 電子簽名
 */

public class ElectronicSignatureActivity extends CustomBaseActivity implements SignaturePad.OnSignedListener {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;

    // ----------------------------------------------------
    private FileUploadConnect connect;
    private boolean isEmpty = true;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_electronic_signature);
        ButterKnife.bind(this);

        connect = new FileUploadConnect(this);

        setToolbar(R.drawable.ic_back, R.drawable.img_toolbar_electronic_signature, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(getString(R.string.signature_title));
        icon.setImageResource(R.drawable.ic_signature);
        signaturePad.setOnSignedListener(this);

    }

    // ----------------------------------------------------
    @OnClick({R.id.clean, R.id.send})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.clean:
                signaturePad.clear();
                break;
            case R.id.send:
                if (isEmpty) {
                    Toast.makeText(this, getString(R.string.not_signature), Toast.LENGTH_SHORT).show();
                } else {
                    connect.ConnectUploadFile(signaturePad.getSignatureBitmap());
                }
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onStartSigning() {
    }

    @Override
    public void onSigned() {
        isEmpty = false;
    }

    @Override
    public void onClear() {
        isEmpty = true;
    }

    // ----------------------------------------------------
    @Subscribe
    public void errorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Subscribe
    public void uploadFileSuccess(UploadFile uploadFile) {
        Intent intent = new Intent();
        intent.putExtra("signatureUrl", uploadFile.getFileName());
        setResult(RESULT_OK, intent);
        finish();
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
}
