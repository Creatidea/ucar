package tw.com.car_plus.carshare.rent.pickup_car;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.ble.BleConnect;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.rent.model.ErrorMessage;
import tw.com.car_plus.carshare.rent.success.StartRentCarFragment;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.Order;
import tw.com.car_plus.carshare.service.CountDownService;
import tw.com.car_plus.carshare.service.model.Time;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DateParser;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.db.DBUtil;
import tw.com.car_plus.carshare.util.db.SecretKeyData;
import tw.com.car_plus.carshare.util.security.HaitecSecurityRentStatus;
import tw.com.car_plus.carshare.util.security.SecurityUtil;

import static android.app.Activity.RESULT_OK;

/**
 * Created by neil on 2017/3/17.
 * 開始取車
 */

public class PickUpTheCarFragment extends CustomBaseFragment {

    @BindView(R.id.countdown)
    TextView countdown;
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.take_picture_car)
    ImageView takePictureCar;
    @BindView(R.id.rental_rule)
    ImageView rentalRule;
    @BindView(R.id.electronic_signature)
    ImageView electronicSignature;

    // ----------------------------------------------------
    private static final long FIVE_MINUTE_TIME = 5 * 60 * 1000;
    private RentConnect connect;
    private BleConnect bleConnect;
    private DateParser parser;
    private DialogUtil dialogUtil;
    private LoadingDialogManager manager;
    private Intent intent;
    private SecurityUtil securityUtil;
    private SharedPreferenceUtil sp;

    private boolean isPhoto = false;
    private boolean isContract = false;
    private boolean isSignature = false;
    private Order order;
    private String signatureUrl;
    private List<String> carPhotos;
    private int orderId;
    private int commandType;
    private boolean isSendMessage = false;
    private boolean isTimeUp = false;
    private boolean isGetCarError = false;
    private boolean isGetKey = false;


    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_pick_up_car);

        EventCenter.getInstance().sendSetToolbarImageIndex(R.drawable.img_toolbar_pick_up_car);
        parser = new DateParser();
        dialogUtil = new DialogUtil(activity);
        securityUtil = new SecurityUtil(activity);
        manager = new LoadingDialogManager(activity);
        sp = new SharedPreferenceUtil(activity);
        // General use.
        connect = new RentConnect(activity);
        bleConnect = new BleConnect(activity);
        carPhotos = new ArrayList<>();

        if (getArguments().getParcelable("order") != null) {
            order = getArguments().getParcelable("order");
            setOrderData(order);
            orderId = order.getOrderId();
        } else {
            orderId = getArguments().getInt("orderId");
            connect.loadPickUpTheCar(orderId);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Order order) {
        this.order = order;
        setOrderData(order);
    }

    // ----------------------------------------------------
    @Subscribe
    public void errorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        connect.cancelPickUpCar(orderId, isTimeUp);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onTime(Time time) {

        if (time.isFinish()) {
            Toast.makeText(getActivity(), time.getMessage(), Toast.LENGTH_SHORT).show();
            isTimeUp = true;
            goCancelPickUpCar();
        } else {
            countdown.setText(time.getCountDownTime());
            sendMessage(time);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onCarFinish(final CarFinish finish) {

        if (finish.isStatus()) {

            if (CarVigType.isGeneralVIG(finish.getCarVigType())) {
                manager.show(activity.getString(R.string.ordering));

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        manager.dismiss();
                        goToStartRentCar(finish);
                    }
                }, 20000);

            } else {
                goToStartRentCar(finish);
            }

        } else {
            Toast.makeText(activity, getString(R.string.get_car_error), Toast.LENGTH_LONG).show();
            manager.show();
            commandType = SecurityUtil.CANCEL;
            securityUtil.sendCommandToRentServer(commandType, CarVigType.getVIGType(order.getCarVigType()), order.getCarId(), orderId,
                    parser.getDateTime(order.getGetCarOnUtc()), "");
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessCancel(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.CANCEL_CAR) {

            Boolean isSuccess = (Boolean) data.get("data");

            if (isSuccess) {
                stopCountDownService();
                replaceFragment(new MapModeFragment(), false);
            }

        } else if ((int) data.get("type") == EventCenter.GET_CAR_FINISH) {

            final Boolean isSuccess = (Boolean) data.get("data");

            activity.runOnUiThread(new Runnable() {
                public void run() {
                    manager.dismiss();
                    if (isSuccess) {
                        connect.getCarFinish(orderId, signatureUrl, carPhotos);
                    } else {
                        Toast.makeText(activity, getString(R.string.get_car_error), Toast.LENGTH_LONG).show();
                        connect.cancelPickUpCar(orderId, isTimeUp);
                    }
                }
            });

        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_ERROR_HAITEC) {

            ErrorMessage errorMessage = (ErrorMessage) data.get("data");

            if (commandType == SecurityUtil.RENT_CAR) {
                manager.dismiss();
                showPickUpErrorMessage(errorMessage);
                connect.cancelPickUpCar(orderId, isTimeUp);
            } else if (commandType == SecurityUtil.CANCEL) {
                checkHaitecCancelPickUpCar(false);
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.GET_GAR_FINISH_ERROR) {

            isGetCarError = true;
            String message = (String) data.get("data");
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            isSignature = false;
            electronicSignature.setSelected(false);

        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void onGetKey(Map<String, Object> data) {

        // Generic only.
        if ((int) data.get("type") == EventCenter.SERVER_KEY) {
            // save key & user information.
            DBUtil.getInstance().saveSecurityKey(getInfo((String) data.get("data")));
            isGetKey = true;
            connect.getCarFinish(orderId, signatureUrl, carPhotos);

        } else if ((int) data.get("type") == EventCenter.SERVER_KEY_ERROR) {
            Toast.makeText(activity, activity.getString(R.string.pick_up_car_error), Toast.LENGTH_LONG).show();
            isGetKey = false;
            isTimeUp = false;
            goCancelPickUpCar();

        } else if ((int) data.get("type") == EventCenter.CHECK_MOBILETRON_VIG_KEY) {

            boolean status = (boolean) data.get("data");

            if (status) {
                DBUtil.getInstance().openDb(activity);
            } else {

                dialogUtil.setMessage(activity.getString(R.string.pick_up_car_error)).setConfirmButton(activity.getString(R.string.confirm),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isTimeUp = false;
                                goCancelPickUpCar();
                            }

                        }).showDialog(false);

            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onCancelCar(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.CANCEL_CAR_FINISH) {

            final boolean isSuccess = (boolean) data.get("data");
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    checkHaitecCancelPickUpCar(isSuccess);
                }
            });

        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_MOBILETRON) {
            final boolean isSuccess = (boolean) data.get("data");

            if (manager.dialog.isShowing()) {
                manager.dismiss();
            }
            connect.cancelPickUpCar(orderId, isTimeUp);
        }
    }

    // ----------------------------------------------------

    /**
     * get user & key information.
     */
    private SecretKeyData getInfo(String key) {

        SecretKeyData info = new SecretKeyData();
        info.setAccountId(new SharedPreferenceUtil(activity).getAcctId());
        info.setCarId(order.getCarId());
        info.setOrderId(order.getOrderId());
        info.setPrivateKey(key);
        info.setKeyCreateTime(new Timestamp(System.currentTimeMillis()));

        return info;
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case EventCenter.PHOTO:
                    //車體拍照
                    isPhoto = lockType(takePictureCar);
                    if (data != null) {
                        carPhotos = data.getStringArrayListExtra("photo");
                    }
                    break;
                case EventCenter.CONTRACT:
                    //契約
                    isContract = lockType(rentalRule);
                    break;
                case EventCenter.SIGNATURE:
                    //簽名
                    isSignature = lockType(electronicSignature);
                    if (data != null) {
                        signatureUrl = data.getStringExtra("signatureUrl");
                    }
                    break;
            }

            checkAllPass();
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopCountDownService();
    }

    // ----------------------------------------------------
    @OnClick({R.id.layout_take_picture_car, R.id.layout_rental_rule, R.id.signature, R.id.cancel})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.layout_take_picture_car:
                //拍照
                if (!isPhoto) {
                    goToActivity(EventCenter.PHOTO, CheckCarActivity.class);
                }
                break;
            case R.id.layout_rental_rule:
                //契約
                if (!isContract && isPhoto) {
                    goToActivity(EventCenter.CONTRACT, RentalRuleActivity.class);
                }
                break;
            case R.id.signature:
                //簽名
                if (!isSignature && isPhoto && isContract) {
                    goToActivity(EventCenter.SIGNATURE, ElectronicSignatureActivity.class);
                }
                break;
            case R.id.cancel:
                //取消租車
                dialogUtil.setMessage(R.string.is_cancel_pick_up_car).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isTimeUp = false;
                        goCancelPickUpCar();
                    }
                }).setCancelButton(R.string.cancel, null).showDialog(false);
                break;
        }

    }

    // ----------------------------------------------------

    /**
     * 前往開始租車頁面
     *
     * @param finish
     */
    private void goToStartRentCar(CarFinish finish) {

        Toast.makeText(activity,activity.getString(R.string.pick_up_car_success),Toast.LENGTH_LONG).show();

        Bundle bundle = new Bundle();
        bundle.putParcelable("carFinish", finish);
        replaceFragment(new StartRentCarFragment(), false, bundle);
    }

    // ----------------------------------------------------

    /**
     * 設定取車的資料
     *
     * @param orderData
     */
    private void setOrderData(Order orderData) {

        //先判斷是否有延長時間
        if (orderData.getGetCarExpireOnUtc().length() > 0) {
            startCountDownService(parser.getMillise(orderData.getGetCarExpireOnUtc()));
        } else {
            //代表沒有延長時間
            startCountDownService(parser.getMillise(orderData.getGetCarOnUtc()));
        }

        Glide.with(getActivity()).load(String.format(ConnectInfo.IMAGE_PATH, orderData.getCarPic())).into(carImage);

        if (!CarVigType.isGeneralVIG(orderData.getCarVigType())) {
            bleConnect.checkMobiletronVIGKey(orderData.getOrderId(), sp.getAcctId());
        } else {
            bleConnect.wakeToVIG(order.getCarId(), null);
        }

    }

    // ----------------------------------------------------
    private void checkAllPass() {
        //代表都已經成功
        if (isPhoto & isSignature & isContract) {
            //  check is General or Generic.
            if (!CarVigType.isGeneralVIG(order.getCarVigType())) {
                if (isGetKey) {
                    connect.getCarFinish(orderId, signatureUrl, carPhotos);
                } else {
                    securityUtil.loadServerKey(order.getOrderId());
                }
            } else {

                if (isGetCarError) {
                    connect.getCarFinish(orderId, signatureUrl, carPhotos);
                } else {
                    manager.show();
                    commandType = SecurityUtil.RENT_CAR;
                    securityUtil.sendCommandToRentServer(commandType, CarVigType.HAITEC, order.getCarId(), orderId,
                            parser.getDateTime(order.getGetCarOnUtc()), "");
                }

            }

            getActivity().stopService(new Intent(getActivity(), CountDownService.class));
        }
    }

    // ----------------------------------------------------
    private boolean lockType(ImageView btn) {
        //防止使用者再次點選
        btn.setSelected(true);
        return true;
    }

    // ----------------------------------------------------

    /**
     * 開始倒數計時的Service
     *
     * @param millies
     */
    private void startCountDownService(long millies) {

        intent = new Intent();
        intent.setClass(getActivity(), CountDownService.class);
        intent.putExtra("time", millies);

        getActivity().startService(intent);
    }

    // ----------------------------------------------------

    /**
     * 停止倒數的Service
     */
    private void stopCountDownService() {
        if (intent != null) {
            getActivity().stopService(intent);
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示取車錯誤的資訊
     *
     * @param errorMessage
     */
    private void showPickUpErrorMessage(ErrorMessage errorMessage) {

        if (errorMessage.getErrorCode() == -1) {
            Toast.makeText(activity, activity.getString(R.string.get_car_error), Toast.LENGTH_LONG).show();
        } else {
            String message = errorMessage.getErrorMessage() != null ? errorMessage.getErrorMessage() : activity.getString(R.string.get_car_error_vig);
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查取消是否成功
     *
     * @param isSuccess
     */
    private void checkHaitecCancelPickUpCar(boolean isSuccess) {

        if (manager.dialog.isShowing()) {
            manager.dismiss();
        }

        if (!isSuccess) {
            Toast.makeText(activity, activity.getString(R.string.pick_up_car_error), Toast.LENGTH_LONG).show();
        } else {
            new BleConnect(activity).wakeToVIG(order.getCarId(), null);
            stopCountDownService();
            replaceFragment(new MapModeFragment(), false);
        }
    }

    // ----------------------------------------------------

    /**
     * 前往取消取車
     */
    private void goCancelPickUpCar() {

        if (CarVigType.isGeneralVIG(order.getCarVigType())) {
            //取消租車
            connect.cancelPickUpCar(getArguments().getInt("orderId"), isTimeUp);
        } else {
            manager.show();
            commandType = SecurityUtil.CANCEL;
            securityUtil.sendCommandToRentServer(commandType, CarVigType.MOBILETRON, order.getCarId(), orderId, parser.getDateTime(order
                    .getGetCarOnUtc()), "");
        }
    }

    // ----------------------------------------------------

    /**
     * 發送簡訊喚醒
     */
    private void sendMessage(Time time) {

        if (CarVigType.isGeneralVIG(order.getCarVigType()) && !isSendMessage && time.getLongTime() < FIVE_MINUTE_TIME) {
            bleConnect.wakeToVIG(order.getCarId(), null);
            isSendMessage = true;
        }
    }

    // ----------------------------------------------------
}
