package tw.com.car_plus.carshare.rent.pickup_car;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/3/18.
 */

public class RentalRuleActivity extends CustomBaseActivity {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.content)
    TextView content;
    @BindView(R.id.is_agree)
    CheckBox isAgree;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_rental_rule);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbat_rent_rule);
        icon.setImageResource(R.drawable.ic_rental_rule);
        title.setText(getText(R.string.car_contract));
        content.setText(getProvisions());
    }

    // ----------------------------------------------------
    private String getProvisions() {


        String provisions = "本契約條款已於簽立前經承租人審閱完成。出租人（甲方）並應於簽約前，將契約內容逐條向承租人（乙方）說明，雙方謹簽訂書面契約，以憑信守。\n\n" +
                "第一條:\n" +
                "契約當事人：出租人（以下簡稱甲方）及承租人（以下簡稱乙方）玆為出租租賃車輛(以下簡稱本車輛)乙事，雙方同意訂立本契約書。\n\n" +
                "第二條:\n" +
                "本車輛及隨車配件、出車前車況圖、出車里程、租賃期間、租金計算及付款方式詳如正面所載，乙方簽妥本契約後，視為租賃車輛交付與驗收完畢。惟本車輛及隨車附件之所有權仍歸屬甲方，本契約僅係將本車輛及隨車附件在約定期間內租與乙方使用，乙方並不取得其他任何權利。在租賃期間內，乙方並非甲方任何目的之代理人，有關本車輛之任何零件或附件之修護或更換須經甲方事前核准。乙方於租賃期間內應依本合約給付甲方全部之租金與相關費用。如乙方為二人或二人以上時，應負連帶給付責任。\n\n" +
                "第三條:\n" +
                "除強制汽車責任險，租金附加下列保險。除雙方另有約定，若因乙方之故意或過失其應負責賠償損失之金額超過保險理賠金額及範圍者(包含財損、精神、工作等損失)，由乙方自行負擔與甲方無涉：\n" +
                "1.\t第三人責任保險：\t保險金額每一個人之傷害最高200萬元、每一意外事故之傷害最高400萬元、每一意外事故之財損最高50萬元。\n" +
                "2.\t駕駛人傷害保險：\t保險金額100萬元。\n" +
                "3.\t乘客責任保險：保險金額每人100萬元(超載部分除外)。\n" +
                "4.\t乙式車體損失險。\n" +
                "5.\t汽車竊盜損失險。（不含零件、配件被竊損失險)\n\n";

        return provisions;

    }

    // ----------------------------------------------------
    @OnClick({R.id.not_agree, R.id.agree})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.not_agree:
                finish();
                break;
            case R.id.agree:
                if (isAgree.isChecked()) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(this, getString(R.string.check_car_contract), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
