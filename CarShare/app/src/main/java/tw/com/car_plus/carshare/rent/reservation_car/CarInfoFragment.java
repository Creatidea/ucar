package tw.com.car_plus.carshare.rent.reservation_car;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.neilchen.complextoolkit.util.map.MapSetting;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.rent.model.CarDetailInfo;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;

/**
 * Created by winni on 2017/3/15.
 * 車輛資訊
 */

public class CarInfoFragment extends CustomBaseFragment {


    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.car_name)
    TextView carName;
    @BindView(R.id.car_type)
    TextView carType;
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.title_energy)
    TextView titleEnergy;
    @BindView(R.id.car_power)
    TextView carPower;
    @BindView(R.id.car_distance)
    TextView carDistance;
    @BindView(R.id.car_seat)
    TextView carSeat;
    @BindView(R.id.car_time_price)
    TextView carTimePrice;
    @BindView(R.id.car_night_rent)
    TextView carNightRent;
    @BindView(R.id.car_price)
    TextView carPrice;

    private RentConnect rentConnect;
    private CarDetailInfo carDetailInfo;
    private DialogUtil dialogUtil;
    private MapSetting mapSetting;
    private Location userLocation, parkingLocation;
    private int carId;

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_car_info);
        carId = getArguments().getInt("carId");
        userLocation = getArguments().getParcelable("userLocation");
        rentConnect = new RentConnect(activity);
        mapSetting = new MapSetting(activity);
        dialogUtil = new DialogUtil(activity);
        rentConnect.getCarInfoConnect(carId);
    }

    // ----------------------------------------------------
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventCenter.getInstance().sendSetToolbarImage(R.drawable.img_toolbar_car_info);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(CarDetailInfo carDetailInfo) {

        this.carDetailInfo = carDetailInfo;

        name.setText(carDetailInfo.getParkinglotName());
        address.setText(carDetailInfo.getParkinglotAddress());
        carName.setText(carDetailInfo.getCarSeries() + "  " + carDetailInfo.getCarNo());
        carType.setText(carDetailInfo.getCarType());

        if (carDetailInfo.getCarEnergyType() == CarEnergyType.ELECTRIC_CAR) {
            titleEnergy.setText(getString(R.string.map_mode_power_electric));
        } else {
            titleEnergy.setText(getString(R.string.map_mode_power));
        }

        carPower.setText(carDetailInfo.getCurrentElectri());
        carDistance.setText(carDetailInfo.getAvailableTrip());
        carSeat.setText(String.valueOf(carDetailInfo.getSeatCount()));
        //因是需要顯示半小時的時租價格因此乘2
        carTimePrice.setText(String.valueOf(carDetailInfo.getRentByQuarter() * 2));
        carNightRent.setText(String.valueOf(carDetailInfo.getRentByNight()));
        carPrice.setText(String.valueOf(carDetailInfo.getRentByFuelKm()));

        Glide.with(this)
                .load(String.format(ConnectInfo.IMAGE_PATH, carDetailInfo.getCarPicUrl()))
                .into(carImage);

        parkingLocation = getLocation(Double.valueOf(carDetailInfo.getParkinglotLat()), Double.valueOf(carDetailInfo.getParkinglotLon()));

        DecimalFormat df = new DecimalFormat("0.00");
        String strDistance = df.format(mapSetting.get2PointDistance(userLocation, parkingLocation) / 1000);
        distance.setText(String.format(getString(R.string.distance), strDistance));


    }

    // ----------------------------------------------------

    /**
     * 取得location
     *
     * @param lat 緯度
     * @param lng 經度
     * @return
     */
    private Location getLocation(double lat, double lng) {
        Location location = new Location("Location");
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        EventCenter.getInstance().sendSetToolbarImage(R.drawable.img_toolbar_car_info);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.gps, R.id.appointment, R.id.layout_parking, R.id.layout_bonus})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gps:
                if (carDetailInfo != null) {
                    Uri gmmIntentUri = Uri.parse(String.format("google.navigation:q=%s,%s", carDetailInfo.getParkinglotLat(), carDetailInfo
                            .getParkinglotLon()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
                break;
            case R.id.appointment:
                if (carDetailInfo != null) {
                    checkCanAppointment();
                }
                break;
            case R.id.layout_parking:
                dialogUtil.setMessage(carDetailInfo.getParkinglotFloorDesc()).setConfirmButton(R.string.confirm, null).showTitleDialog(getActivity
                        ().getString(R.string.parking_information), true);
                break;
            case R.id.layout_bonus:
                // TODO: 2017/3/15 優惠資訊之後需要接api傳過來的值
                dialogUtil.setMessage(R.string.preferential_description).setConfirmButton(R.string.confirm, null).showTitleDialog(getActivity
                        ().getString(R.string.car_bonus), true);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查是否可以租車
     */
    private void checkCanAppointment() {

        switch (UserStatus.verifyStatus) {

            case UserStatus.UNDER_REVIEW:
                dialogUtil.setMessage(R.string.user_under_review).setConfirmButton(R.string.confirm, null).showDialog(true);
                break;
            case UserStatus.FAIL_REVIEW:
                dialogUtil.setMessage(R.string.user_fail_review).setConfirmButton(R.string.confirm, null).showDialog(true);
                break;
            case UserStatus.BLACKLIST:
                dialogUtil.setMessage(R.string.blacklist).setConfirmButton(R.string.confirm, null).showDialog(true);
                break;
            case UserStatus.PASS_REVIEW:

                if (!getArguments().getBoolean("isOtherRent")) {

                    Bundle bundle = new Bundle();
                    bundle.putParcelable("carDetailInfo", carDetailInfo);
                    bundle.putString("distance", distance.getText().toString());
                    replaceFragment(R.id.container_frame_layout, new ReservationDetailFragment(), true, bundle);
                } else {
                    Toast.makeText(activity, getString(R.string.have_rent_car), Toast.LENGTH_LONG).show();
                }

                break;
        }

    }

}
