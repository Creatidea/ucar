package tw.com.car_plus.carshare.rent.reservation_car;

import android.location.Location;
import android.os.Bundle;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/3/16.
 */

public class ReservationCarActivity extends CustomBaseActivity {

    private int carId;
    private Location userLocation;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_reservation_car);
        ButterKnife.bind(this);

        IndexActivity.activities.add(this);

        userLocation = getIntent().getParcelableExtra("userLocation");
        carId = getIntent().getIntExtra("carId", 0);

        setToolbar(R.drawable.ic_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Bundle bundle = new Bundle();
        bundle.putParcelable("userLocation", userLocation);
        bundle.putInt("carId", carId);
        bundle.putBoolean("isOtherRent", sp.getUserData().isOtherRent());
        goToFragment(R.id.container_frame_layout, new CarInfoFragment(), bundle);

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Boolean isSuccess) {
        if (isSuccess) {
            finish();
        }
    }

    //--------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {
        if ((int) data.get("type") == EventCenter.TOOLBAR_IMAGE) {
            int resource = (int) data.get("data");
            toolbarImage.setImageResource(resource);
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }
}
