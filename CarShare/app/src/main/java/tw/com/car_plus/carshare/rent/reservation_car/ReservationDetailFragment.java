package tw.com.car_plus.carshare.rent.reservation_car;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.bumptech.glide.Glide;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.model.CancelErrorOrder;
import tw.com.car_plus.carshare.rent.model.CarDetailInfo;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;

/**
 * Created by winni on 2017/3/16.
 * 預約明細
 */

public class ReservationDetailFragment extends CustomBaseFragment {


    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.car_series)
    TextView carSeries;
    @BindView(R.id.car_no)
    TextView carNo;
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.power)
    TextView power;
    @BindView(R.id.car_distance)
    TextView carDistance;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.pick_up_car_time)
    View layoutPickUpCarTime;
    @BindView(R.id.return_car_time)
    View layoutReturnCarTime;

    private final String PICK_UP_CAR_TIME = "PickUpCarTime";
    private final String RETURN_CAR_TIME = "ReturnCarTime";

    private RentConnect rentConnect;
    private ReturnCarConnect returnCarConnect;
    private CarDetailInfo carDetailInfo;
    private Calendar pickUpCarCalendar, returnCarCalendar;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.TAIWAN);
    private DialogUtil dialogUtil;
    private SublimePicker sublimePicker;
    private SublimeOptions sublimeOptions;
    private CarVigType carVigType;
    private ReservationInfo reservationInfo;
    private LoadingDialogManager loadingDialogManager;
    private TextView pickUpCarTimeTitle, returnCarTimeTitle;
    private EditText pickUpCarTime, returnCarTime;
    private ImageView pickCarTimeCalendar, returnCarTimeCalendar;
    private ImageOnClickListener imageListener = new ImageOnClickListener();
    private Dialog dialog;
    private long minTime, maxTime;

    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_reservation_detail);

        carDetailInfo = getArguments().getParcelable("carDetailInfo");
        EventCenter.getInstance().sendSetToolbarImage(R.drawable.img_toolbar_reservation_detail);

        rentConnect = new RentConnect(activity);
        returnCarConnect = new ReturnCarConnect(activity);
        sublimePicker = new SublimePicker(activity);
        sublimeOptions = new SublimeOptions();
        dialogUtil = new DialogUtil(activity);
        carVigType = new CarVigType();
        dialog = new Dialog(activity);
        loadingDialogManager = new LoadingDialogManager(activity);

        pickUpCarCalendar = Calendar.getInstance();
        returnCarCalendar = Calendar.getInstance();

        pickUpCarTimeTitle = (TextView) layoutPickUpCarTime.findViewById(R.id.birthday_title);
        pickUpCarTime = (EditText) layoutPickUpCarTime.findViewById(R.id.birthday);
        pickCarTimeCalendar = (ImageView) layoutPickUpCarTime.findViewById(R.id.calendar);

        returnCarTimeTitle = (TextView) layoutReturnCarTime.findViewById(R.id.birthday_title);
        returnCarTime = (EditText) layoutReturnCarTime.findViewById(R.id.birthday);
        returnCarTimeCalendar = (ImageView) layoutReturnCarTime.findViewById(R.id.calendar);

        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        carVigType.setCarVigType(carDetailInfo.getCarVigType());

        minTime = pickUpCarCalendar.getTimeInMillis();
        pickUpCarCalendar.add(Calendar.MINUTE, 30);
        maxTime = pickUpCarCalendar.getTimeInMillis();
        returnCarCalendar.add(Calendar.HOUR, 1);
        pickUpCarTime.setText(getStringDate(pickUpCarCalendar.getTime()));
        returnCarTime.setText(getStringDate(returnCarCalendar.getTime()));

        rentConnect.estimateSimpleAmountConnect(pickUpCarTime.getText().toString(), returnCarTime.getText().toString(), carDetailInfo);

        name.setText(carDetailInfo.getParkinglotName());
        address.setText(carDetailInfo.getParkinglotAddress());
        carSeries.setText(carDetailInfo.getCarSeries());
        carNo.setText(carDetailInfo.getCarNo());
        power.setText(carDetailInfo.getCurrentElectri());
        carDistance.setText(carDetailInfo.getAvailableTrip());
        price.setText(String.valueOf(carDetailInfo.getRentByFuelKm()));
        distance.setText(getArguments().getString("distance"));

        Glide.with(this)
                .load(String.format(ConnectInfo.IMAGE_PATH, carDetailInfo.getCarPicUrl()))
                .into(carImage);

        setDateView(pickUpCarTimeTitle, pickCarTimeCalendar, PICK_UP_CAR_TIME, getString(R.string.reservation_pick_up_car));
        setDateView(returnCarTimeTitle, returnCarTimeCalendar, RETURN_CAR_TIME, getString(R.string.reservation_return_car));

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setCancelable(false);

        sublimeOptions.setCanPickDateRange(false);
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.RESERVATION) {

            reservationInfo = (ReservationInfo) data.get("data");
            loadingDialogManager.show(activity.getString(R.string.wait_key_create));
            new SecurityUtil(activity).sendCommandToRentServer(SecurityUtil.RENT_CAR, CarVigType.MOBILETRON, carDetailInfo.getCarId(), reservationInfo
                    .getOrderId(), pickUpCarTime.getText().toString(), returnCarTime.getText().toString());

        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_MOBILETRON) {

            boolean isSuccess = (boolean) data.get("data");

            loadingDialogManager.dismiss();

            if (isSuccess) {
                EventCenter.getInstance().sendReservationSuccess(true);
                EventCenter.getInstance().sendReservationInfoToIndex(reservationInfo);

            } else {

                dialogUtil.setMessage(activity.getString(R.string.pick_up_car_error)).setConfirmButton(activity.getString(R.string.confirm), new
                        View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                returnCarConnect.sendCancelErrorOrder(reservationInfo.getOrderId());
                            }
                        }).showDialog(false);
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(CancelErrorOrder cancelErrorOrder) {

        if (cancelErrorOrder != null) {

            if (!cancelErrorOrder.isStatus()) {
                List<CancelErrorOrder.ErrorsBean> errorsBeen = cancelErrorOrder.getErrors();
                if (errorsBeen.size() > 0) {
                    Toast.makeText(activity, errorsBeen.get(0).getErrorMsg(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Integer amounts) {
        amount.setText(String.valueOf(amounts));
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.gps, R.id.cancel, R.id.confirm, R.id.layout_info})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gps:
                Uri gmmIntentUri = Uri.parse(String.format("google.navigation:q=%s,%s", carDetailInfo.getParkinglotLat(), carDetailInfo
                        .getParkinglotLon()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.confirm:

                dialogUtil.setMessage(R.string.is_reservation).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rentConnect.reservationCarConnect(carVigType.getCarVigType(), carDetailInfo.getCarId(), pickUpCarTime.getText().toString
                                (), returnCarTime.getText().toString());
                    }
                }).setCancelButton(R.string.cancel, null).showDialog(false);

                break;
            case R.id.layout_info:
                dialogUtil.setMessage(activity.getString(R.string.reservation_car_remind)).setConfirmButton(R.string.confirm, null).showDialog(false);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示picker
     *
     * @param calendar
     * @param sublimeListenerAdapter
     */
    private void showSublimePicker(final Calendar calendar, SublimeListenerAdapter sublimeListenerAdapter) {

        int displayOptions = 0;
        displayOptions |= SublimeOptions.ACTIVATE_DATE_PICKER;
        displayOptions |= SublimeOptions.ACTIVATE_TIME_PICKER;

        sublimePicker = (SublimePicker) activity.getLayoutInflater().inflate(R.layout.sublime_picker, null);
        sublimeOptions.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        sublimeOptions.setDisplayOptions(displayOptions);
        sublimeOptions.setDateParams(calendar);
        sublimeOptions.setTimeParams(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        sublimePicker.initializePicker(sublimeOptions, sublimeListenerAdapter);

        dialog.setContentView(sublimePicker);
        dialog.show();
    }

    // ----------------------------------------------------

    /**
     * 設定日期的view data
     *
     * @param title     textview
     * @param imageView imageview
     * @param tag       tag
     * @param strTitle  標題
     */
    private void setDateView(TextView title, ImageView imageView, String tag, String strTitle) {
        title.setText(strTitle);
        imageView.setTag(tag);
        imageView.setOnClickListener(imageListener);
    }

    // ----------------------------------------------------

    /**
     * 取得將Date轉成字串日期
     *
     * @param date
     * @return
     */
    private String getStringDate(Date date) {
        return sdf.format(date);
    }

    // ----------------------------------------------------

    /**
     * 取得由picker回傳回來的日期時間轉成Calendar
     *
     * @param selectedDate
     * @param hour         時
     * @param minute       分
     * @return
     */
    private Calendar getCalendar(SelectedDate selectedDate, int hour, int minute) {
        Calendar calendar = selectedDate.getStartDate();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        return calendar;
    }

    // ----------------------------------------------------

    /**
     * 取車時間、還車時間的日曆按下監聽
     */
    private class ImageOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            String tag = (String) v.getTag();
            switch (tag) {
                case PICK_UP_CAR_TIME:

                    showSublimePicker(pickUpCarCalendar, new SublimeListenerAdapter() {
                        @Override
                        public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker, SelectedDate selectedDate, int hourOfDay, int
                                minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                            Calendar calendar = getCalendar(selectedDate, hourOfDay, minute);

                            if (minTime <= calendar.getTimeInMillis() && calendar.getTimeInMillis() <= maxTime) {
                                pickUpCarTime.setText(getStringDate(calendar.getTime()));
                                pickUpCarCalendar = calendar;
                                rentConnect.estimateSimpleAmountConnect(pickUpCarTime.getText().toString(), returnCarTime.getText().toString(),
                                        carDetailInfo);
                                dialog.dismiss();
                            } else {
                                Toast.makeText(activity, getString(R.string.pick_up_car_time_limit), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled() {
                            dialog.dismiss();
                        }
                    });
                    break;

                case RETURN_CAR_TIME:

                    showSublimePicker(returnCarCalendar, new SublimeListenerAdapter() {
                        @Override
                        public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker, SelectedDate selectedDate, int hourOfDay, int
                                minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                            Calendar calendar = getCalendar(selectedDate, hourOfDay, minute);
                            returnCarTime.setText(getStringDate(calendar.getTime()));
                            returnCarCalendar = calendar;
                            rentConnect.estimateSimpleAmountConnect(pickUpCarTime.getText().toString(), returnCarTime.getText().toString(),
                                    carDetailInfo);
                            dialog.dismiss();
                        }

                        @Override
                        public void onCancelled() {
                            dialog.dismiss();
                        }
                    });
                    break;
            }
        }
    }

}
