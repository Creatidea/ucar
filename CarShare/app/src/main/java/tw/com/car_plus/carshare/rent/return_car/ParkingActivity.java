package tw.com.car_plus.carshare.rent.return_car;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.neilchen.complextoolkit.util.map.MapSetting;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.rent.adapter.AreaAdapter;
import tw.com.car_plus.carshare.rent.model.Parking;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.MapUtils;
import tw.com.car_plus.carshare.util.address.AddressUtil;

@RuntimePermissions
public class ParkingActivity extends CustomBaseActivity implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleMap
        .OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.parking_name)
    TextView parkingName;
    @BindView(R.id.parking_address)
    TextView parkingAddress;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.parking_info_layout)
    LinearLayout parkingInfoLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.view_loading)
    View viewLoading;

    // ----------------------------------------------------
    //可停車範圍
    private final int PARKING_RANGE = 1000;

    private RentConnect connect;
    private MapSetting mapSetting;
    private Location userLocation;
    private MapUtils mapUtils;
    private AddressUtil addressUtil;
    private DialogUtil dialogUtil;
    private Marker oldMarker;
    private List<Parking> parkings;
    private Map<String, Parking> mapData;
    private int currentDistance;
    private int currentAreaId;
    private boolean isFirst = true;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_parking);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_choose_parking);
        ParkingActivityPermissionsDispatcher.startLocationWithCheck(this);
    }

    // ----------------------------------------------------
    private void startData() {

        connect = new RentConnect(this);
        addressUtil = new AddressUtil(this);
        dialogUtil = new DialogUtil(this);
        mapSetting = new MapSetting(this, R.id.map);
        mapSetting.initApiClient(this);
        mapSetting.apiClient.connect();
        addressUtil.loadCity();

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.PARKING) {

            viewLoading.setVisibility(View.GONE);
            parkings = (List<Parking>) data.get("data");
            addMarkerOnMap();
            mapSetting.moveCamera(userLocation, 16, false);
            showRecommendParking();

        } else if ((int) data.get("type") == EventCenter.CITY) {
            //城市
            addressUtil.setCities((List<City>) data.get("data"));
        } else if ((int) data.get("type") == EventCenter.AREA) {
            //區域
            addressUtil.setAreas((List<Area>) data.get("data"));
            showAreaData();
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示推薦停車場(找距離使用者最近的)
     */
    private void showRecommendParking() {

        Location parkingLocation = new Location("parking");
        for (Parking p : parkings) {

            parkingLocation.setLatitude(Double.parseDouble(p.getLatitude()));
            parkingLocation.setLongitude(Double.parseDouble(p.getLongitude()));
            p.setDistance((int) userLocation.distanceTo(parkingLocation));
        }

        message.setVisibility(View.VISIBLE);
        message.setText(String.format(getString(R.string.return_car_map_nearby_parking), parkings.get(0).getName()));
    }

    // ----------------------------------------------------

    /**
     * 顯示地區資料
     */
    private void showAreaData() {

        list.setVisibility(View.VISIBLE);
        AreaAdapter adapter = new AreaAdapter(addressUtil.getAreas());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (checkPermission()) {
                    loadData(RentConnect.NONE, addressUtil.getAreas().get(position).getAreaId());
                }
            }
        });
    }
    // ----------------------------------------------------

    /**
     * 添加大頭釘
     */
    private void addMarkerOnMap() {

        mapSetting.mMap.clear();
        mapData = new HashMap<>();

        for (int count = 0; count < parkings.size(); count++) {

            mapData.put(parkings.get(count).getName(), parkings.get(count));

            Bitmap icon = mapUtils.getBitmapMarker(R.drawable.pin_light_blue_parking, String.valueOf(parkings.get(count).getParkingSpaceCount()));
            LatLng point = new LatLng(Double.parseDouble(parkings.get(count).getLatitude()), Double.parseDouble(parkings.get(count).getLongitude()));

            mapSetting.mMap.addMarker(new MarkerOptions()
                    .position(point)
                    .title(parkings.get(count).getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(icon)));
        }
    }

    // ----------------------------------------------------
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mapSetting.mMap.setOnMarkerClickListener(this);
        mapSetting.mMap.setOnInfoWindowClickListener(this);
        mapSetting.mMap.setOnMapClickListener(this);
        mapSetting.startLocationUpdates(this, 1, 1);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mapSetting.mMap.setMyLocationEnabled(true);
            UiSettings uiSettings = mapSetting.mMap.getUiSettings();
            uiSettings.setMyLocationButtonEnabled(false);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onConnectionSuspended(int i) {

    }

    // ----------------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        try {
            mapUtils = new MapUtils(this);
            userLocation = location;
            if (isFirst) {
                loadData(RentConnect.FIFTEEN_KM, 0);
                isFirst = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------

    /**
     * 讀取資料
     */
    private void loadData(@RentConnect.DistanceMode int distance, int areaId) {

        currentDistance = distance;
        currentAreaId = areaId;
        connect.loadRentInfo(userLocation, currentDistance, currentAreaId);
    }

    // ----------------------------------------------------
    @Override
    public void onInfoWindowClick(Marker marker) {
        checkParkingDistance(marker.getTitle());
    }

    // ----------------------------------------------------
    private void checkParkingDistance(String parkingName) {

        if (mapData.get(parkingName).getDistance() < PARKING_RANGE) {

            showDialog(String.format(getString(R.string.return_car_map_dialog), parkingName), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("parkingId", mapData.get(oldMarker.getTitle()).getParkinglotId());
                    intent.putExtra("carInfo", getIntent().getParcelableExtra("carInfo"));
                    setResult(RESULT_OK, intent);
                    ParkingActivity.this.finish();
                }
            });

        } else {
            dialogUtil.setMessage(R.string.return_car_map_fail_range).setConfirmButton(R.string.ok, null).showDialog(true);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onMapClick(LatLng latLng) {

        //還原成未點選顏色
        if (oldMarker != null) {
            setMarkerColor(oldMarker, R.drawable.pin_light_blue_parking);
        }
        //隱藏下方搜尋選擇列表
        list.setVisibility(View.GONE);
        //隱藏車輛資訊視窗
        parkingInfoLayout.setVisibility(View.GONE);
    }

    // ----------------------------------------------------
    @Override
    public boolean onMarkerClick(Marker marker) {

        changePinColor(marker);

        parkingInfoLayout.setVisibility(View.VISIBLE);
        parkingName.setText(mapData.get(marker.getTitle()).getName());
        parkingAddress.setText(mapData.get(marker.getTitle()).getAddress());


        Location parkingLocation = new Location("Location");
        parkingLocation.setLatitude(Double.valueOf(mapData.get(marker.getTitle()).getLatitude()));
        parkingLocation.setLongitude(Double.valueOf(mapData.get(marker.getTitle()).getLongitude()));

        DecimalFormat df = new DecimalFormat("0.00");
        String strDistance = df.format(mapSetting.get2PointDistance(userLocation, parkingLocation) / 1000);
        distance.setText(String.format(getString(R.string.distance), strDistance));


        return false;
    }

    // ----------------------------------------------------
    private void changePinColor(Marker marker) {

        //將上一次的還原
        if (oldMarker != null) {
            setMarkerColor(oldMarker, R.drawable.pin_light_blue_parking);
        }
        //更換顏色
        setMarkerColor(marker, R.drawable.pin_deepblue_parking);
        oldMarker = marker;
    }

    // ----------------------------------------------------
    @OnClick({R.id.search, R.id.refresh, R.id.my_location, R.id.layout_gps, R.id.return_car})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.search:
                showSearchDialog();
                break;
            case R.id.refresh:
                if (checkPermission()) {
                    loadData(currentDistance, currentAreaId);
                }
                break;
            case R.id.my_location:
                if (checkPermission()) {
                    mapSetting.moveCamera(userLocation, false);
                }
                break;
            case R.id.layout_gps:
                startGoogleMapNavigation(String.valueOf(oldMarker.getPosition().latitude), String.valueOf(oldMarker.getPosition().longitude));
                break;
            case R.id.return_car:
                checkParkingDistance(oldMarker.getTitle());
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onStop() {
        super.onStop();
        mapSetting.onStop();
    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void startLocation() {
        startData();
    }

    // ----------------------------------------------------
    //請求授權
    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocationPermission(final PermissionRequest request) {
        request.proceed();
    }

    // ----------------------------------------------------
    // location 授權被拒
    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showDeniedForLocationPermission() {
        Toast.makeText(this, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @OnNeverAskAgain({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void goToSettingPermission() {

        showDialog(getString(R.string.go_setting_permission), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                settings.addCategory(Intent.CATEGORY_DEFAULT);
                settings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(settings);
            }
        });
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ParkingActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    // ----------------------------------------------------

    /**
     * 更換大頭釘顏色
     */
    private void setMarkerColor(Marker marker, int resource) {

        marker.setIcon(BitmapDescriptorFactory.fromBitmap(mapUtils.getBitmapMarker(resource, String.valueOf(mapData.get(marker.getTitle())
                .getParkingSpaceCount()))));
    }

    // ----------------------------------------------------
    private void showSearchDialog() {

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.choose_search))
                .setItems(getResources().getStringArray(R.array.rent), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        switch (position) {
                            case 0:
                                //範圍
                                if (checkPermission()) {
                                    list.setVisibility(View.VISIBLE);
                                    showRangeData();
                                }
                                break;
                            case 1:
                                //地區
                                if (checkPermission()) {
                                    addressUtil.loadArea(addressUtil.getCityId(userLocation));
                                }

                                break;
                            case 2:
                                //取消
                                dialog.dismiss();
                                break;
                        }
                    }
                }).show();
    }

    // ----------------------------------------------------

    /**
     * 顯示範圍資料
     */
    private void showRangeData() {

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_map_mode, R.id.item, getResources().getStringArray(R.array
                .search_range));
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        //1km
                        loadData(RentConnect.ONE_KM, 0);
                        break;
                    case 1:
                        //5km
                        loadData(RentConnect.FIVE_KM, 0);
                        break;
                    case 2:
                        //10km
                        loadData(RentConnect.TEN_KM, 0);
                        break;
                    case 3:
                        //15km
                        loadData(RentConnect.FIFTEEN_KM, 0);
                        break;
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog
     *
     * @param message  訊息
     * @param listener 確認按鈕的按下事件
     */
    private void showDialog(String message, View.OnClickListener listener) {
        dialogUtil.setMessage(message).setCancelButton(R.string.cancel, null).setConfirmButton(R.string.confirm, listener).showDialog(true);
    }

    // ----------------------------------------------------

    /**
     * 檢查是否同意權限以及是否有取得到使用者目前位置
     *
     * @return
     */
    private boolean checkPermission() {

        if (mapSetting != null) {
            if (userLocation != null) {
                return true;
            } else {
                Toast.makeText(this, getString(R.string.need_location_info), Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            ParkingActivityPermissionsDispatcher.startLocationWithCheck(this);
            return false;
        }
    }

    // ----------------------------------------------------
}
