package tw.com.car_plus.carshare.rent.return_car;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.ble.BleConnect;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.record.detail.DialogPromotionFragment;
import tw.com.car_plus.carshare.rent.return_car.model.BillInfo;
import tw.com.car_plus.carshare.rent.return_car.model.Bonus;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.util.CheckDataUtil;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.DateParser;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.db.DBUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;

/**
 * Created by neil on 2017/4/18.
 */

public class PayActivity extends CustomBaseActivity implements RadioGroup.OnCheckedChangeListener {

    // ----------------------------------------------------
    @BindView(R.id.price_layout)
    View priceLayout;
    @BindView(R.id.bonus_layout)
    View bonusLayout;
    @BindView(R.id.invoice_layout)
    View invoiceLayout;

    @BindView(R.id.bonus_pay_layout)
    View bonusPayLayout;
    @BindView(R.id.invoice_pay_layout)
    View invoicePayLayout;
    @BindView(R.id.credit_card_layout)
    LinearLayout creditCardLayout;
    @BindView(R.id.credit_card_line)
    View creditCardLine;
    @BindView(R.id.layout_add_bonus)
    LinearLayout addBonusLayout;
    @BindView(R.id.car_series)
    TextView carSeries;
    @BindView(R.id.car_number)
    TextView carNumber;
    @BindView(R.id.credit_card_number)
    TextView creditCardNumber;
    @BindView(R.id.get_car_time)
    TextView getCarTime;
    @BindView(R.id.return_car_time)
    TextView returnCarTime;
    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.hour_unit)
    TextView hourUnit;
    @BindView(R.id.minute)
    TextView minute;
    @BindView(R.id.rent_price)
    TextView rentPrice;
    @BindView(R.id.dollar)
    TextView dollar;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.distance_price)
    TextView distancePrice;
    @BindView(R.id.e_tag_price)
    TextView eTagPrice;
    @BindView(R.id.now_bonus)
    TextView nowBonus;
    @BindView(R.id.discounted_max)
    TextView discountedMax;
    @BindView(R.id.current_bonus)
    TextView currentBonus;
    @BindView(R.id.remainder_bonus)
    TextView remainderBonus;
    @BindView(R.id.total_price)
    TextView totalPrice;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.bonus_agree)
    CheckBox bonusAgree;
    @BindView(R.id.total_agree)
    CheckBox totalAgree;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.agree_donate)
    CheckBox agreeDonate;
    @BindView(R.id.carriers_agree)
    CheckBox carriersAgree;
    @BindView(R.id.carriers)
    EditText carriers;
    @BindView(R.id.layout_invoice_2)
    RelativeLayout layoutInvoice2;
    @BindView(R.id.layout_carriers)
    LinearLayout layoutCarriers;
    @BindView(R.id.company_name)
    EditText companyName;
    @BindView(R.id.company_number)
    EditText companyNumber;
    @BindView(R.id.layout_company)
    RelativeLayout layoutCompany;
    @BindView(R.id.radio_donate)
    RadioButton radioDonate;
    @BindView(R.id.radio_invoice_2)
    RadioButton radioInvoice2;
    @BindView(R.id.radio_invoice_3)
    RadioButton radioInvoice3;
    @BindView(R.id.promotion_price)
    TextView promotionPrice;
    @BindView(R.id.layout_promotion)
    LinearLayout layoutPromotion;

    public static final int INVOICE_TYPE_NONE = -1;
    public static final int INVOICE_TYPE_1 = 1;
    public static final int INVOICE_TYPE_2 = 2;
    public static final int INVOICE_TYPE_3 = 3;


    // ----------------------------------------------------

    /**
     * 發票類型
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({INVOICE_TYPE_NONE, INVOICE_TYPE_1, INVOICE_TYPE_2, INVOICE_TYPE_3})
    public @interface InvoiceType {
    }

    // ----------------------------------------------------
    private ImageView priceIcon, bonusIcon, invoiceIcon;
    private TextView priceTitle, bonusTitle, invoiceTitle;

    private LoadingDialogManager loadingDialogManager;
    private ReturnCarConnect connect;
    private ReturnCarInfo info;
    private BillInfo billInfo;
    private DateParser dateParser;
    private CheckDataUtil checkDataUtil;
    private DialogUtil dialogUtil;
    @InvoiceType
    private int invoiceType = INVOICE_TYPE_NONE;
    private SharedPreferenceUtil sp;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);

        info = getIntent().getParcelableExtra("info");
        connect = new ReturnCarConnect(this);
        dateParser = new DateParser();
        checkDataUtil = new CheckDataUtil();
        dialogUtil = new DialogUtil(this);
        sp = new SharedPreferenceUtil(this);

        setBackToolBar(R.drawable.img_toolbar_pay);
        setLayoutData();

        if (isCompanyAccount()) {
            connect.loadBillInfo(ConnectInfo.GET_BILL_INFO_COMPANY, info.getOrderId());
            setViewGone(bonusLayout, invoiceLayout, bonusPayLayout, invoicePayLayout, currentBonus, remainderBonus, creditCardLayout,
                    creditCardLine, addBonusLayout);
        } else {
            initInvoiceChoose();
            connect.loadBillInfo(ConnectInfo.GET_BILL_INFO, info.getOrderId());
        }
    }
    // ----------------------------------------------------

    /***
     * 上次成功付款所選的發票類型選擇 todo _noel
     */
    private void initInvoiceChoose() {
        radioGroup.setOnCheckedChangeListener(this);
        invoiceType = sp.getInvoiceType();
        radioGroup.clearCheck();
        switch (invoiceType) {
            case INVOICE_TYPE_1:
                radioDonate.setChecked(true);
                break;
            case INVOICE_TYPE_2:
                radioInvoice2.setChecked(true);
                break;
            case INVOICE_TYPE_3:
                radioInvoice3.setChecked(true);
                break;

        }
    }

    // ----------------------------------------------------
    private void setLayoutData() {

        //price title bar.
        priceIcon = (ImageView) priceLayout.findViewById(R.id.icon);
        priceTitle = (TextView) priceLayout.findViewById(R.id.title);

        priceIcon.setImageResource(R.drawable.ic_dollar);
        priceTitle.setText(getString(R.string.pay_consumption_title));

        //bonus title bar.
        bonusIcon = (ImageView) bonusLayout.findViewById(R.id.icon);
        bonusTitle = (TextView) bonusLayout.findViewById(R.id.title);

        bonusIcon.setImageResource(R.drawable.ic_bonus);
        bonusTitle.setText(getString(R.string.pay_bonus_title));

        //invoice title bar.
        invoiceIcon = (ImageView) invoiceLayout.findViewById(R.id.icon);
        invoiceTitle = (TextView) invoiceLayout.findViewById(R.id.title);

        String strInvoiceTitle = getString(R.string.pay_invoice_title);
        SpannableString spannableString = new SpannableString(strInvoiceTitle);
        spannableString.setSpan(new ForegroundColorSpan(Color.RED), 6, strInvoiceTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        invoiceIcon.setImageResource(R.drawable.ic_invoice);
        invoiceTitle.setText(spannableString);

        radioGroup.check(R.id.radio_donate);
        radioGroup.setOnCheckedChangeListener(this);

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(BillInfo info) {
        billInfo = info;
        setInfoData();
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Bonus bonus) {

        //本次紅利
        currentBonus.setText(String.valueOf(bonus.getOrderBonus()));
        //目前紅利
        nowBonus.setText(String.valueOf(bonus.getCurrentBonus()));
        //可折MAX
        discountedMax.setText(String.valueOf(bonus.getMaxAmountLimit()));
        //剩餘紅利
        remainderBonus.setText(String.valueOf(bonus.getRemainBonus()));
        //應付總額
        totalPrice.setText(String.valueOf(bonus.getTotalAmount()));
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.RETURN_CAR_FINISH) {

            final boolean isSuccess = (boolean) data.get("data");
            runOnUiThread(new Runnable() {
                public void run() {
                    sp.setSecurityStatus(false);
                    checkReturnCar(isSuccess);
                }
            });

        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_MOBILETRON) {
            final boolean isSuccess = (boolean) data.get("data");
            checkReturnCar(isSuccess);

        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_ERROR_HAITEC) {
            sp.setSecurityStatus(false);
            checkReturnCar(false);
        }
    }

    // ----------------------------------------------------
    private void setInfoData() {

        BillInfo.CalAmountResultBean calAmountResultBean = billInfo.getCalAmountResult();

        //車輛型號
        carSeries.setText(calAmountResultBean.getCarSeries());
        //車輛號碼
        carNumber.setText(calAmountResultBean.getCarNo());
        //取車時間
        getCarTime.setText(dateParser.getDateTime(calAmountResultBean.getRentStartOnUtc()));
        //還車時間
        returnCarTime.setText(dateParser.getDateTime(calAmountResultBean.getRentEndOnUtc()));
        //時租
        showRentTime(billInfo.getCalAmountResult().getTimeForHour(), calAmountResultBean.getTimeForMinute());
        //時間租金
        rentPrice.setText(String.valueOf(calAmountResultBean.getTimeAmount()));
        //里程
        distance.setText(String.valueOf(calAmountResultBean.getOdoMeter()));
        //里程租金
        distancePrice.setText(String.valueOf(calAmountResultBean.getOdoMeterAmount()));
        //eTag金額
        eTagPrice.setText(String.valueOf(calAmountResultBean.getETagAmount()));
        //小計
        price.setText(String.valueOf(calAmountResultBean.getTotalAmount()));

        if (!isCompanyAccount()) {

            BillInfo.CalAmountResultBean.CreditCardsBean cardInfo;
            //信用卡號碼
            if ((cardInfo = getDefaultCreditCard()) != null) {
                creditCardNumber.setText(cardInfo.getCardNo());
            }

            BillInfo.BonusCalculateResultBean bonusCalculateResultBean = billInfo.getBonusCalculateResult();

            //目前紅利
            nowBonus.setText(String.valueOf(bonusCalculateResultBean.getCurrentBonus()));
            //可折MAX
            discountedMax.setText(String.valueOf(bonusCalculateResultBean.getMaxAmountLimit()));
            //本次紅利
            currentBonus.setText(String.valueOf(bonusCalculateResultBean.getOrderBonus()));
            //剩餘紅利
            remainderBonus.setText(String.valueOf(bonusCalculateResultBean.getRemainBonus()));

            if (calAmountResultBean.isIsPromotion()) {
                layoutPromotion.setVisibility(View.VISIBLE);
                promotionPrice.setText(String.valueOf(calAmountResultBean.getPromotionTotalAmount()));
                //應付總額
                totalPrice.setText(String.valueOf(calAmountResultBean.getPromotionTotalAmount()));
            } else {
                //應付總額
                totalPrice.setText(String.valueOf(calAmountResultBean.getTotalAmount()));
            }

        } else {
            invoiceType = INVOICE_TYPE_1;
            //應付總額
            totalPrice.setText(String.valueOf(calAmountResultBean.getTotalAmount()));
        }


    }

    // ----------------------------------------------------

    /**
     * 取得預設信用卡
     */
    private BillInfo.CalAmountResultBean.CreditCardsBean getDefaultCreditCard() {

        List<BillInfo.CalAmountResultBean.CreditCardsBean> data = billInfo.getCalAmountResult().getCreditCards();

        for (int count = 0; count < data.size(); count++) {
            if (data.get(count).isIsDefault()) {
                return data.get(count);
            }
        }
        return null;
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.bonus_agree, R.id.double_check, R.id.carriers_agree, R.id.layout_pay_promotion_info})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.bonus_agree:

                int price;

                if (billInfo.getCalAmountResult().isIsPromotion()) {
                    price = billInfo.getCalAmountResult().getPromotionTotalAmount();
                } else {
                    price = billInfo.getCalAmountResult().getTotalAmount();
                }

                connect.loadBonusInfo(price, bonusAgree.isChecked());

                break;
            case R.id.double_check:

                dialogUtil.setMessage(R.string.is_pay).setCancelButton(R.string.cancel, null).setConfirmButton(R.string.confirm, new View
                        .OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!totalAgree.isChecked()) {
                            Toast.makeText(PayActivity.this, getString(R.string.pay_not_agree), Toast.LENGTH_SHORT).show();
                        } else if (totalAgree.isChecked() && completeWithInvoiceCheck(invoiceType)) {

                            Bundle bundle = new Bundle();
                            bundle.putParcelable("carInfo", info);
                            bundle.putParcelable("billInfo", billInfo);
                            bundle.putStringArrayList("carPic", getIntent().getStringArrayListExtra("carPic"));
                            bundle.putBoolean("isUesBonus", bonusAgree.isChecked());
                            bundle.putInt("parkingId", getIntent().getIntExtra("parkingId", 0));
                            bundle.putInt("invoiceType", invoiceType);
                            bundle.putBoolean("isCompany", isCompanyAccount());
                            bundle.putString("companyName", companyName.getText().toString());
                            bundle.putString("companySn", companyNumber.getText().toString());
                            bundle.putBoolean("isUseCarrier", carriersAgree.isChecked());
                            bundle.putString("carrierId", carriers.getText().toString());
                            goToSignatureDialog(bundle);
                        }
                    }

                }).showDialog(false);

                break;
            case R.id.carriers_agree:
                checkAgreeCarriers(carriersAgree.isChecked());
                break;
            case R.id.layout_pay_promotion_info:

                BillInfo.CalAmountResultBean.PromotionInfoDetailBean promotionInfoDetailBean = billInfo.getCalAmountResult().getPromotionInfoDetail();

                Bundle bundle = new Bundle();
                bundle.putString("startDate", promotionInfoDetailBean.getStartTime());
                bundle.putString("endDate", promotionInfoDetailBean.getEndTime());
                bundle.putString("title", promotionInfoDetailBean.getTitle());
                bundle.putString("info", promotionInfoDetailBean.getInfo());

                showDialogFragment(new DialogPromotionFragment(), bundle);
                break;
        }
    }

    // ----------------------------------------------------
    private void goToSignatureDialog(Bundle bundle) {

        final SignatureDialog signatureDialog = new SignatureDialog();
        signatureDialog.setArguments(bundle);
        signatureDialog.setOnDismissListener(new SignatureDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                signatureDialog.dismiss();

                runOnUiThread(new Runnable() {
                    public void run() {

                        loadingDialogManager = new LoadingDialogManager(PayActivity.this);
                        loadingDialogManager.show();
                        new SecurityUtil(PayActivity.this).sendCommandToRentServer(SecurityUtil.RETURN_CAR, CarVigType.getVIGType(info
                                .getCarVigType()), info.getCarId(), info.getOrderId(), "", "");
                    }
                });
            }

            @Override
            public void onFailDismiss(String errorMessage) {
                showPaymentInfoDialog(errorMessage, false);
            }
        });

        //跳Dialog頁
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        signatureDialog.show(ft, "dialog");
    }

    // ----------------------------------------------------
    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        carriers.setError(null);
        companyName.setError(null);
        companyNumber.setError(null);

        switch (checkedId) {
            case R.id.radio_donate:
                invoiceType = INVOICE_TYPE_1;
                agreeDonate.setVisibility(View.VISIBLE);
                setViewGone(layoutInvoice2, layoutCompany);
                break;
            case R.id.radio_invoice_2:
                invoiceType = INVOICE_TYPE_2;
                showInvoiceData(true);
                break;
            case R.id.radio_invoice_3:
                invoiceType = INVOICE_TYPE_3;
                showInvoiceData(false);
                break;
        }

    }

    // ----------------------------------------------------

    /**
     * 顯示二連發票還是三連發票
     *
     * @param isType2
     */
    private void showInvoiceData(boolean isType2) {

        agreeDonate.setVisibility(View.GONE);

        if (isType2) {
            layoutCompany.setVisibility(View.GONE);
            layoutInvoice2.setVisibility(View.VISIBLE);
            checkAgreeCarriers(carriersAgree.isChecked());
        } else {
            layoutInvoice2.setVisibility(View.GONE);
            layoutCompany.setVisibility(View.VISIBLE);
        }
    }

    // ----------------------------------------------------

    /**
     * 確認是否同意存入載具
     *
     * @param isAgree
     */
    private void checkAgreeCarriers(boolean isAgree) {
        if (isAgree) {
            layoutCarriers.setVisibility(View.VISIBLE);
        } else {
            layoutCarriers.setVisibility(View.GONE);
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示付款成功的Dialog
     */
    private void showPaymentInfoDialog(String message, final boolean isSuccess) {

        dialogUtil.setMessage(message).setConfirmButton(R.string.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSuccess) {
                    setResult(RESULT_OK);
                }
                finish();
            }
        }).showDialog(false);

    }

    // ----------------------------------------------------

    /**
     * 檢查發票填入的資訊 todo _noel
     */
    private boolean completeWithInvoiceCheck(int invoiceType) {

        switch (invoiceType) {
            case INVOICE_TYPE_1:
                // 捐贈發票
                return true;
            case INVOICE_TYPE_2:
                // 載具
                return !carriersAgree.isChecked() || isKeyIn(carriers);
            case INVOICE_TYPE_3:
                // 統編
                return isKeyIn(companyName) & isKeyIn(companyNumber);
            default:
                return true;
        }
    }

    // ----------------------------------------------------

    /***
     * 是否輸入字串
     * @param editText
     * @return
     */
    private boolean isKeyIn(EditText editText) {

        boolean isData = (editText.getText().toString().length() > 0);

        if (!isData) {
            editText.setError(getString(R.string.format_error));
        }

        return isData;
    }
    // ----------------------------------------------------

    /***
     * email格式是否正確
     * @param email
     * @return
     */
    private boolean isEmailKeyIn(EditText email) {

        boolean isCorrect = checkDataUtil.checkEmail(email.getText().toString());

        if (!isCorrect) {
            email.setError(getString(R.string.format_error));
        }

        return isCorrect;
    }

    // ----------------------------------------------------

    /**
     * 顯示時租時間
     *
     * @param mHour
     * @param mMinute
     */
    private void showRentTime(int mHour, int mMinute) {

        if (mHour == 0) {
            hour.setVisibility(View.INVISIBLE);
            hourUnit.setVisibility(View.INVISIBLE);
        } else {
            hour.setText(String.valueOf(mHour));
        }

        minute.setText(String.valueOf(mMinute));
    }

    // ----------------------------------------------------

    /**
     * 檢查還車是否成功
     *
     * @param isSuccess
     */
    private void checkReturnCar(boolean isSuccess) {

        if (loadingDialogManager != null && loadingDialogManager.dialog.isShowing()) {
            loadingDialogManager.dismiss();
        }

        if (isSuccess) {

            if (CarVigType.isGeneralVIG(info.getCarVigType())) {
                new BleConnect(this).wakeToVIG(info.getCarId(), null);
            } else {
                DBUtil.getInstance().openDb(this);
                DBUtil.getInstance().deleteSecurityKey(sp.getAcctId());
            }
        }

        if (!isCompanyAccount()) {
            showPaymentInfoDialog(String.format(getString(R.string.pay_amount), totalPrice.getText().toString()), true);
            sp.setInvoiceType(invoiceType);
        } else {
            showPaymentInfoDialog(getString(R.string.transaction_success), true);
        }

    }

    // ----------------------------------------------------
    private void setViewGone(View... viewGone) {

        for (View view : viewGone) {
            view.setVisibility(View.GONE);
        }
    }

    // ----------------------------------------------------
}
