package tw.com.car_plus.carshare.rent.return_car;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.rent.return_car.check.CheckListActivity;
import tw.com.car_plus.carshare.rent.return_car.check.CheckLockActivity;
import tw.com.car_plus.carshare.rent.return_car.check.ReturnCheckCarActivity;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.success.StartRentCarFragment;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.service.CountDownService;
import tw.com.car_plus.carshare.service.model.Time;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DateParser;

import static android.app.Activity.RESULT_OK;

/**
 * Created by winni on 2017/4/14.
 * 開始還車
 */

public class ReturnCarFragment extends CustomBaseFragment {

    @BindView(R.id.countdown)
    TextView countdown;
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.ic_check)
    ImageView icCheck;
    @BindView(R.id.ic_pic)
    ImageView icPic;
    @BindView(R.id.ic_lock)
    ImageView icLock;
    @BindView(R.id.ic_signature)
    ImageView icSignature;
    @BindView(R.id.countdown_title)
    TextView countdownTitle;

    // ----------------------------------------------------
    private DateParser parser;
    private ReturnCarInfo info;
    private List<String> carPhotos;
    private ReturnCarConnect connect;
    private Intent serviceIntent;
    private boolean isCheck = false, isPhoto = false, isLock = false, isSignature = false;

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_return_car);

        EventCenter.getInstance().sendSetToolbarImageIndex(R.drawable.img_toolbar_return_car);

        parser = new DateParser();
        connect = new ReturnCarConnect(activity);

        info = getArguments().getParcelable("info");

        if (getArguments().getBoolean("isFromPage")) {
            connect.loadCancelCar(info.getOrderId());
        } else {
            countdownTitle.setText(getString(R.string.return_car_title));
            Glide.with(getActivity()).load(String.format(ConnectInfo.IMAGE_PATH, info.getCarPic())).into(carImage);
            startCountDownService(parser.getMillise(info.getReturnCarExpireOnUtc()));
        }
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case EventCenter.RETURN_CAR_CHECK:
                    isCheck = lockType(icCheck);
                    break;
                case EventCenter.RETURN_CAR_PHOTO:
                    isPhoto = lockType(icPic);
                    if (data != null) {
                        carPhotos = data.getStringArrayListExtra("photo");
                    }
                    break;
                case EventCenter.RETURN_CAR_LOCk:
                    isLock = lockType(icLock);
                    break;
                case EventCenter.RETURN_CAR_SIGNATURE:
                    isSignature = lockType(icSignature);
                    stopCountDownService();
                    replaceFragment(new MapModeFragment(), false);
                    break;
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnConnectSuccess(CarFinish carFinish) {

        //停止該Service
        stopCountDownService();

        Bundle bundle = new Bundle();
        bundle.putParcelable("carFinish", carFinish);
        replaceFragment(new StartRentCarFragment(), false, bundle);
    }

    // ----------------------------------------------------
    @Subscribe
    public void errorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }


    // ----------------------------------------------------
    @Subscribe
    public void onTime(Time time) {

        if (time.isFinish()) {
            connect.loadCancelCar(info.getOrderId());
            Toast.makeText(getActivity(), time.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            countdown.setText(time.getCountDownTime());
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopCountDownService();
    }

    // ----------------------------------------------------
    @OnClick({R.id.ic_check, R.id.ic_pic, R.id.ic_lock, R.id.ic_signature, R.id.cancel, R.id.request})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //檢查清單
            case R.id.ic_check:
                if (!isCheck) {
                    Intent checkIntent = new Intent();
                    checkIntent.putExtra("orderId", info.getOrderId());
                    checkIntent.putExtra("type", info.getCarEnergyType());
                    goToActivity(checkIntent, EventCenter.RETURN_CAR_CHECK, CheckListActivity.class);
                }
                break;
            //拍照
            case R.id.ic_pic:
                if (!isPhoto && isCheck) {
                    goToActivity(EventCenter.RETURN_CAR_PHOTO, ReturnCheckCarActivity.class);
                }
                break;
            //上鎖
            case R.id.ic_lock:
                if (!isLock && isCheck && isPhoto) {
                    Intent intent = new Intent();
                    intent.putExtra("vigType", getArguments().getInt("vigType"));
                    intent.putExtra("vigId", getArguments().getString("vigId"));
                    intent.putExtra("macAddress", getArguments().getString("macAddress"));
                    goToActivity(intent, EventCenter.RETURN_CAR_LOCk, CheckLockActivity.class);
                }
                break;
            //付款
            case R.id.ic_signature:
                if (!isSignature && isCheck && isPhoto && isLock) {
                    Intent payIntent = new Intent();
                    payIntent.putExtra("info", info);
                    payIntent.putStringArrayListExtra("carPic", new ArrayList<>(carPhotos));
                    payIntent.putExtra("parkingId", getArguments().getInt("parkingId"));
                    goToActivity(payIntent, EventCenter.RETURN_CAR_SIGNATURE, PayActivity.class);

                }
                break;
            //取消還車
            case R.id.cancel:
                connect.loadCancelCar(info.getOrderId());
                break;
            //問題回報
            case R.id.request:
                // TODO: 2017/4/18 這邊不知道連去哪！
                break;
        }
    }

    // ----------------------------------------------------
    private boolean lockType(ImageView btn) {
        //防止使用者再次點選
        btn.setSelected(true);
        return true;
    }

    // ----------------------------------------------------
    private void startCountDownService(long millies) {

        serviceIntent = new Intent();
        serviceIntent.setClass(getActivity(), CountDownService.class);
        serviceIntent.putExtra("time", millies);

        getActivity().startService(serviceIntent);
    }

    // ----------------------------------------------------
    private void stopCountDownService() {
        if (serviceIntent != null) {
            getActivity().stopService(serviceIntent);
        }
    }

    // ----------------------------------------------------
}
