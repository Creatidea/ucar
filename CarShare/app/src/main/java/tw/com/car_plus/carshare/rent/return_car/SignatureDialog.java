package tw.com.car_plus.carshare.rent.return_car;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.FileUploadConnect;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.rent.return_car.model.BillInfo;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.util.BaseDialogFragment;
import tw.com.car_plus.carshare.util.DialogUtil;

/**
 * Created by neil on 2017/4/19.
 */

public class SignatureDialog extends BaseDialogFragment implements SignaturePad.OnSignedListener {

    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;

    // ----------------------------------------------------

    private boolean isEmpty = true;
    private FileUploadConnect connect;
    private ReturnCarConnect returnCarConnect;
    private ReturnCarInfo returnCarInfo;
    private BillInfo billInfo;
    private OnDismissListener listener;
    private ArrayList<String> carPic;
    private boolean isUseBonus;
    private boolean isCompany;
    private String companyName;
    private String companySn;
    private String email;
    private boolean isUseCarrier;
    private String carrierId;


    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.dialog_signature);

        connect = new FileUploadConnect(getActivity());
        signaturePad.setOnSignedListener(this);
        returnCarInfo = getArguments().getParcelable("carInfo");
        billInfo = getArguments().getParcelable("billInfo");
        carPic = getArguments().getStringArrayList("carPic");
        isUseBonus = getArguments().getBoolean("isUesBonus");
        isCompany = getArguments().getBoolean("isCompany");
        companyName = getArguments().getString("companyName");
        companySn = getArguments().getString("companySn");
        isUseCarrier = getArguments().getBoolean("isUseCarrier");
        carrierId = getArguments().getString("carrierId");
    }

    // ----------------------------------------------------
    @Subscribe
    public void uploadFileSuccess(UploadFile uploadFile) {

        returnCarConnect = new ReturnCarConnect(getActivity());

        if (isCompany) {
            returnCarConnect.confirmPaymentCompany(uploadFile.getFileName(), carPic, getArguments().getInt("parkingId"), returnCarInfo.getCarId(),
                    returnCarInfo.getOrderId());
        } else {
            returnCarConnect.confirmPayment(uploadFile.getFileName(), carPic, getDefaultCreditCard().getSerialNo(), isUseBonus, getArguments()
                            .getInt("invoiceType"), getArguments().getInt("parkingId"), returnCarInfo.getCarId(), returnCarInfo.getOrderId(),
                    companyName, companySn, isUseCarrier, carrierId);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onConnectSuccess(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.BANK_PAYMENT) {

            String message = (String) data.get("data");
            Log.e("Success", message);

            if (message.equals(getString(R.string.transaction_success))) {
                onTransactionFinish(true, "");
            } else {
                onTransactionFinish(false, message);
            }

        } else if ((int) data.get("type") == EventCenter.BANK_PAYMENT_URL) {
            returnCarConnect.loadBankPaymentHtml((String) data.get("data"));

        } else if ((int) data.get("type") == EventCenter.BANK_PAYMENT_COMPANY) {
            onTransactionFinish(true, "");
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onConnectSuccess(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onStartSigning() {
    }

    @Override
    public void onSigned() {
        isEmpty = false;
    }

    @Override
    public void onClear() {
        isEmpty = true;
    }


    // ----------------------------------------------------

    /**
     * 成功後結束
     */
    private void onTransactionFinish(boolean isSuccess, String errorMessage) {

        if (listener != null) {

            if (isSuccess) {
                listener.onDismiss();
            } else {
                listener.onFailDismiss(errorMessage);
            }

        } else {
            dismiss();
        }
    }

    // ----------------------------------------------------
    private void showDoubleCheck() {

        new DialogUtil(getActivity()).setMessage(R.string.pay_confirm_payment).setCancelButton(R.string.cancel, null).setConfirmButton(R.string
                .confirm, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                connect.ConnectUploadFile(signaturePad.getSignatureBitmap());
            }

        }).showDialog(true);

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.clean, R.id.send})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.clean:
                signaturePad.clear();
                dismiss();
                break;
            case R.id.send:
                if (isEmpty) {
                    Toast.makeText(getActivity(), getString(R.string.not_signature), Toast.LENGTH_SHORT).show();
                } else {
                    showDoubleCheck();
                }
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 取得預設信用卡
     */
    private BillInfo.CalAmountResultBean.CreditCardsBean getDefaultCreditCard() {

        List<BillInfo.CalAmountResultBean.CreditCardsBean> data = billInfo.getCalAmountResult().getCreditCards();

        for (int count = 0; count < data.size(); count++) {
            if (data.get(count).isIsDefault()) {
                return data.get(count);
            }
        }
        return null;
    }

    // ----------------------------------------------------
    public void setOnDismissListener(OnDismissListener listener) {
        this.listener = listener;
    }

    // ----------------------------------------------------
    public interface OnDismissListener {

        void onDismiss();

        void onFailDismiss(String errorMessage);
    }

    // ----------------------------------------------------
}
