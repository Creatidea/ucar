package tw.com.car_plus.carshare.rent.return_car.check;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.rent.return_car.check.adapter.CheckListAdapter;
import tw.com.car_plus.carshare.rent.return_car.check.model.CheckList;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType.CAR;
import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType.ELECTRIC_CAR;

/**
 * Created by winni on 2017/4/14.
 */

public class CheckListActivity extends CustomBaseActivity implements AdapterView.OnItemClickListener {


    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.list)
    ListView list;

    private CheckListAdapter checkListAdapter;
    private ReturnCarConnect returnCarConnect;
    private CarEnergyType carEnergyType;
    private ArrayList<CheckList> checkLists;
    private String checkList[];
    private int orderId;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_check_list);
        ButterKnife.bind(this);

        orderId = getIntent().getIntExtra("orderId", 0);
        returnCarConnect = new ReturnCarConnect(this);
        carEnergyType = new CarEnergyType();
        checkListAdapter = new CheckListAdapter(this);
        checkLists = new ArrayList<>();
        carEnergyType.setCarEnergyType(getIntent().getIntExtra("type", 1));
        checkList = getResources().getStringArray(getCheckListResourcesId(carEnergyType.getCarEnergyType()));

        setBackToolBar(R.drawable.img_toolbar_check_list);
        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        icon.setImageResource(R.drawable.ic_navigation_02);
        title.setText(getString(R.string.check_list_step_title));
        checkListAdapter.setData(getCheck());
        list.setAdapter(checkListAdapter);
        list.setOnItemClickListener(this);
    }

    // ----------------------------------------------------

    /**
     * 取得顯示需給使用者檢查的項目
     *
     * @param type
     * @return
     */
    private int getCheckListResourcesId(@CarEnergyType.CarEnergy int type) {
        int id = R.array.electricity_car;

        switch (type) {
            case ELECTRIC_CAR:
                id = R.array.electricity_car;
                break;
            case CAR:
                id = R.array.oil_car;
                break;
        }

        return id;
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Boolean isEngineStatus) {

        if (!isEngineStatus) {
            checkLists.get(1).setCheck(true);
            checkListAdapter.setData(checkLists);
        } else {
            Toast.makeText(this, getString(R.string.check_engine_status), Toast.LENGTH_LONG).show();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void errorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------

    /**
     * 取得檢查清單的列表
     *
     * @return
     */
    private ArrayList<CheckList> getCheck() {

        for (int i = 0; i < checkList.length; i++) {
            CheckList checkListBean = new CheckList();
            checkListBean.setTitle(checkList[i]);
            checkListBean.setCheck(false);
            checkLists.add(checkListBean);
        }

        return checkLists;
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (position == 1) {
            if (!checkLists.get(position).isCheck()) {
                returnCarConnect.checkEngineStatus(orderId);
            }
        } else {
            checkLists.get(position).setCheck(!checkLists.get(position).isCheck());
            checkListAdapter.setData(checkLists);
        }

    }

    // ----------------------------------------------------
    @OnClick({R.id.clean, R.id.send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.clean:
                finish();
                break;
            case R.id.send:
                if (checkData()) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(this, getString(R.string.check_list_error), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查資料
     *
     * @return
     */
    private boolean checkData() {

        boolean isCheck = false;

        for (CheckList checkList : checkLists) {

            if (!checkList.isCheck()) {
                isCheck = false;
                break;
            } else {
                isCheck = true;
            }
        }

        return isCheck;
    }

    // ----------------------------------------------------

}
