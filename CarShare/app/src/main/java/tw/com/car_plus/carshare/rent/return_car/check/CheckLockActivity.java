package tw.com.car_plus.carshare.rent.return_car.check;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neilchen.complextoolkit.bluetooth.BlueTooth;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.BuildConfig;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.ble.BLEScanUtil;
import tw.com.car_plus.carshare.util.command.CarControllerGatt;
import tw.com.car_plus.carshare.util.security.AESUtil;
import tw.com.haitec.ae.project.carsharing.CommandCallback;

/**
 * Created by winni on 2017/4/17.
 */

@RuntimePermissions
public class CheckLockActivity extends CustomBaseActivity implements CarControllerGatt.OnConnectListener, BLEScanUtil.OnScanBleListener {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;

    private static final int REQUEST_ENABLE_BT = 99;
    private static final int REQUEST_ENABLE_LOCATION = 999;

    private CarControllerGatt carControllerGatt;
    private LoadingDialogManager loadingDialogManager;
    private DialogUtil dialogUtil;
    private BLEScanUtil bleScanUtil;
    private BlueTooth blueTooth;
    private CarVigType carVigType;
    private TimeOutThread timeOutThread = null;
    private SendToBleQueue sendToBleQueue = null;
    private SendDataTimeOutThread sendDataTimeOutThread = null;
    private ArrayBlockingQueue<byte[]> saveBleDataArray, bleDataArray;
    private byte[] writeByte = null;
    private String mDeviceAddress = "";
    //isSuccess:是否成功接收到vig回傳的值，isLock：確認是否成功上鎖，isShowDisconnectDialog：是否執行過斷線的Dialog訊息，isSecurity：華創的資安連線是否成功
    private boolean isSuccess = false, isLock = false, isShowDisconnectDialog = false, isSecurity = false;
    private int vigType;
    //傳送封包
    private boolean isLoop = true;
    //監聽傳送封包後的Timeout
    private boolean isLoopDataTime = true;
    //監聽執行Command後的Timeout
    private boolean isLoopTime = true;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_lock);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_lock);

        vigType = getIntent().getIntExtra("vigType", 1);

        loadingDialogManager = new LoadingDialogManager(this);
        blueTooth = new BlueTooth(this);
        dialogUtil = new DialogUtil(this);
        bleScanUtil = new BLEScanUtil(this, vigType);
        carVigType = new CarVigType();
        carVigType.setCarVigType(vigType);
        carControllerGatt = new CarControllerGatt(this, carVigType.getCarVigType());
        carControllerGatt.setVigId(getIntent().getStringExtra("vigId"));
        mDeviceAddress = getIntent().getStringExtra("macAddress");

        saveBleDataArray = new ArrayBlockingQueue<>(100);
        bleDataArray = new ArrayBlockingQueue<>(100);

        resetBle();

        icon.setImageResource(R.drawable.ic_password);
        title.setText(getString(R.string.return_car_lock));

        carControllerGatt.setDeviceAddress(mDeviceAddress);
        carControllerGatt.onStartBluetoothLeService();
        carControllerGatt.setOnConnectListener(this);
        carControllerGatt.registerGATT();

        if (!CarVigType.isGeneralVIG(vigType)) {
            sendToBleQueue = new SendToBleQueue();
            sendToBleQueue.start();
        }

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (CarVigType.isGeneralVIG(vigType)) {
            carControllerGatt.disconnectSecurity();
        } else {
            isLoop = false;
        }

        carControllerGatt.disconnect();
        carControllerGatt.onDestroy();
    }

    // ----------------------------------------------------
    @OnClick({R.id.img_lock, R.id.lock})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_lock:
                CheckLockActivityPermissionsDispatcher.startConnectBleWithCheck(this);
                break;
            case R.id.lock:
                if (BuildConfig.IS_INTERNAL) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    if (isLock) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        goLockedCar();
                    }
                }
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_ENABLE_BT:
                    if (bleScanUtil.isLocationEnable()) {
                        bleScanUtil.scanBle(mDeviceAddress, this);
                    } else {
                        goToOpenLocation();
                    }
                    break;
            }

        } else {

            if (requestCode == REQUEST_ENABLE_LOCATION) {

                if (bleScanUtil.isOpenBle()) {
                    bleScanUtil.scanBle(mDeviceAddress, this);
                } else {
                    Toast.makeText(this, getString(R.string.not_open_location), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.SECURITY_STATUS) {

            final int status = (int) data.get("data");
            isSecurity = (status == CommandCallback.CONNECTING_STATUS_SUCCESS);

            if (isSecurity) {
                isSuccess = false;
                startAndCloseTimeOutThread(true);
                carControllerGatt.writeCommandToSecurity(CarControllerGatt.DOOR_LOCK);

            } else {
                connectSecurityStatus(status);
            }

        } else if ((int) data.get("type") == EventCenter.MAIN_SECURITY_READ) {

            final String value = (String) data.get("data");
            isSuccess = true;
            commandFinish(value);
            startAndCloseTimeOutThread(false);

        } else if ((int) data.get("type") == EventCenter.BLUETOOTH_ERROR) {

            runOnUiThread(new Runnable() {

                public void run() {
                    closeDialog();
                    Toast.makeText(CheckLockActivity.this, getString(R.string.ble_restart), Toast.LENGTH_LONG).show();
                    startAndCloseSendDataTimeOutThread(false);
                    startAndCloseTimeOutThread(false);
                }
            });

        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessMobiletronVig(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.READ_VIG_DATA) {

            String value = (String) data.get("data");
            value = value.replace(AESUtil.HEADER, "");

            if (value.contains("error")) {
                //強制移除監看command的Thread
                startAndCloseSendDataTimeOutThread(false);
                startAndCloseTimeOutThread(false);
                closeDialog();
                Toast.makeText(this, getString(R.string.try_again), Toast.LENGTH_LONG).show();
            }

        } else if ((int) data.get("type") == EventCenter.READ_VIG_DATA_COMMAND) {
            isSuccess = true;
            String value = (String) data.get("data");
            commandFinish(value);
            //強制移除監看command的Thread
            startAndCloseSendDataTimeOutThread(false);
            startAndCloseTimeOutThread(false);
        }

    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void startConnectBle() {

        if (carControllerGatt.isConnect()) {
            isSuccess = false;

            if (CarVigType.isGeneralVIG(vigType)) {

                if (isSecurity) {
                    startAndCloseTimeOutThread(true);
                    carControllerGatt.writeCommandToSecurity(CarControllerGatt.DOOR_LOCK);
                } else {
                    carControllerGatt.connectSecurity();
                }

            } else {
                setMobiletronCommand();
            }

        } else {
            bleScanUtil.scanBle(mDeviceAddress, this);
        }
    }

    // ----------------------------------------------------
    // location 授權被拒
    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showDeniedForLocationPermission() {
        Toast.makeText(this, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CheckLockActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // ----------------------------------------------------
    @Override
    /**
     * 藍芽連線成功後
     */
    public void connected() {

        Toast.makeText(this, getString(R.string.ble_connect), Toast.LENGTH_LONG).show();

        if (CarVigType.isGeneralVIG(vigType)) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    carControllerGatt.connectSecurity();
                }
            }, 3000);

        } else {
            setMobiletronCommand();
        }

    }

    // ----------------------------------------------------
    @Override
    /**
     * 藍芽斷線後
     */
    public void disconnected() {

        closeDialog();

        carControllerGatt.disconnect();

        if (!CarVigType.isGeneralVIG(vigType)) {
            bleDataArray.clear();
            saveBleDataArray.clear();
            writeByte = null;
        } else {
            isSecurity = false;
        }

        if (!isShowDisconnectDialog) {
            dialogUtil.setMessage(R.string.connect_bluetooth).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckLockActivityPermissionsDispatcher.startConnectBleWithCheck(CheckLockActivity.this);
                    isShowDisconnectDialog = false;
                }

            }).setCancelButton(getString(R.string.cancel), null).showDialog(false);

            isShowDisconnectDialog = true;
        }
    }

    // ----------------------------------------------------
    @Override
    public void OnNotSupportedBle() {
        Toast.makeText(this, getString(R.string.not_supported_ble), Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void OnNotOpenBle() {
        Toast.makeText(this, getString(R.string.not_open_ble), Toast.LENGTH_LONG).show();
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    // ----------------------------------------------------
    @Override
    public void OnStopScanBle(boolean isSearch) {

        if (isSearch) {
            loadingDialogManager.show(getString(R.string.connect_key));
            carControllerGatt.connect();
        } else {
            Toast.makeText(this, getString(R.string.not_search), Toast.LENGTH_LONG).show();
            dialogUtil.setMessage(R.string.search).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckLockActivityPermissionsDispatcher.startConnectBleWithCheck(CheckLockActivity.this);
                }
            }).setCancelButton(getString(R.string.cancel), null).showDialog(false);
        }
    }

    // ----------------------------------------------------
    @Override
    public void OnBLESignalLow() {
        Toast.makeText(this, getString(R.string.ble_signal), Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void OnNotOpenLocation() {
        Toast.makeText(this, getString(R.string.not_open_location), Toast.LENGTH_LONG).show();
        goToOpenLocation();

    }

    // ----------------------------------------------------

    /**
     * 前往上鎖
     */
    private void goLockedCar() {

        dialogUtil.setMessage(R.string.ask_locked)
                .setCancelButton(R.string.cancel, null)
                .setConfirmButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckLockActivityPermissionsDispatcher.startConnectBleWithCheck(CheckLockActivity.this);
                    }

                }).showDialog(false);
    }

    // ----------------------------------------------------

    /**
     * 監測是否有TimeOut的問題
     */
    private class TimeOutThread extends Thread {

        public TimeOutThread() {

        }

        @Override
        public void run() {

            try {

                while (isLoopTime) {

                    if (CarVigType.isGeneralVIG(vigType)) {
                        sleep(10000);
                    } else {
                        sleep(5000);
                    }

                    if (timeOutThread != null && !isSuccess) {

                        commandFinish("");
                        startAndCloseTimeOutThread(false);

                        if (isLoopDataTime) {
                            startAndCloseSendDataTimeOutThread(false);
                        }
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 監測傳送封包完是否有TimeOut的問題
     */
    private class SendDataTimeOutThread extends Thread {

        public SendDataTimeOutThread() {
        }

        @Override
        public void run() {

            try {

                while (isLoopDataTime) {

                    sleep(5000);

                    if (sendDataTimeOutThread != null) {

                        runOnUiThread(new Runnable() {

                            public void run() {
                                closeDialog();
                                startAndCloseSendDataTimeOutThread(false);
                            }
                        });
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 完成傳送指令後動做
     *
     * @param value
     */
    private void commandFinish(final String value) {

        runOnUiThread(new Runnable() {
            public void run() {

                closeDialog();

                if (value.length() > 0) {

                    switch (value) {
                        case "ID:2,nack":
                            isLock = false;
                            Toast.makeText(CheckLockActivity.this, getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            break;
                        case "ID:2,ack":
                            isLock = true;
                            Toast.makeText(CheckLockActivity.this, getString(R.string.door_lock_success), Toast.LENGTH_LONG).show();
                            break;
                    }

                } else {
                    Toast.makeText(CheckLockActivity.this, getString(R.string.try_again), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 建立資安連線
     */
    private void connectSecurityStatus(final int type) {

        runOnUiThread(new Runnable() {

            public void run() {

                closeDialog();

                if (type != CommandCallback.CONNECTING_STATUS_SUCCESS) {
                    switch (type) {
                        //資安連線認證失敗(VIG尚未得到KEY)
                        case CommandCallback.CONNECTING_STATUS_AUTH_FAILED:
                            Toast.makeText(CheckLockActivity.this, getString(R.string.connect_error), Toast.LENGTH_LONG).show();
                            break;
                        //資安連線被中斷(藍芽連線不穩定)
                        case CommandCallback.CONNECTING_STATUS_TERMINATED:
                            Toast.makeText(CheckLockActivity.this, getString(R.string.connect_ble_error), Toast.LENGTH_LONG).show();
                            break;
                        //資安連線逾時
                        case CommandCallback.CONNECTING_STATUS_TIMEOUT:
                            Toast.makeText(CheckLockActivity.this, getString(R.string.connect_timeout), Toast.LENGTH_LONG).show();
                            break;
                        //資安連線失敗
                        case CommandCallback.CONNECTING_STATUS_FAILED:
                            carControllerGatt.reSetCommunicationAgent();
                            Toast.makeText(CheckLockActivity.this, getString(R.string.connect_error), Toast.LENGTH_LONG).show();
                            break;
                        default:
                            //do something break;
                    }

                    carControllerGatt.disconnect();
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 設定車王電的秘文資料
     */
    private void setMobiletronCommand() {

        loadingDialogManager.show(getString(R.string.send_command_lock));

        try {

            saveBleDataArray = carControllerGatt.getMobiletronCommandData(CarControllerGatt.DOOR_LOCK, sp.getAcctId());
            bleDataArray = saveBleDataArray;

        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (BadPaddingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        }

    }

    // ----------------------------------------------------

    /**
     * 持續傳送BleData的資料
     */
    private class SendToBleQueue extends Thread {

        public SendToBleQueue() {

        }

        @Override
        public void run() {

            while (isLoop) {

                if (bleDataArray.size() > 0) {

                    try {

                        byte[] data = bleDataArray.peek();
                        if (data == null) {
                            sleep(5);
                            continue;
                        }

                        writeByte = data;
                        writeToBle(data);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 寫入資料到Ble
     *
     * @param bytes
     */
    private void writeToBle(final byte[] bytes) {

        if (carControllerGatt.isConnect()) {

            if (carControllerGatt.writeCommand(bytes) == CarControllerGatt.WRITE_STATUS_SUCCESS) {

                bleDataArray.remove(writeByte);

                if (bleDataArray.size() == 0) {
                    startAndCloseSendDataTimeOutThread(true);
                    startAndCloseTimeOutThread(true);
                }
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 開啟或關閉傳送車王封包後的TimeoutThread
     *
     * @param isStart
     */
    private void startAndCloseSendDataTimeOutThread(boolean isStart) {

        if (isStart) {
            sendDataTimeOutThread = new SendDataTimeOutThread();
            sendDataTimeOutThread.start();
            isLoopDataTime = true;
        } else {
            isLoopDataTime = false;
            sendDataTimeOutThread = null;
        }
    }

    // ----------------------------------------------------

    /**
     * 開啟或關閉 車王版、華創版傳送完指令後的Thread
     *
     * @param isStart
     */
    private void startAndCloseTimeOutThread(boolean isStart) {

        if (isStart) {
            timeOutThread = new TimeOutThread();
            timeOutThread.start();
            isLoopTime = true;
        } else {
            isLoopTime = false;
            timeOutThread = null;
        }
    }

    // ----------------------------------------------------

    /**
     * 關閉LoadingDialog
     */
    private void closeDialog() {
        if (loadingDialogManager.dialog.isShowing()) {
            loadingDialogManager.dismiss();
        }
    }

    // ----------------------------------------------------

    /**
     * 重新連線藍芽
     */
    private void resetBle() {
        if (blueTooth.isOpen(false)) {
            blueTooth.turnOffBlueTooth();
            blueTooth.turnOnBlueTooth();
        }
    }

    // ----------------------------------------------------

    /**
     * 前往開啟定位的頁面
     */
    private void goToOpenLocation() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, REQUEST_ENABLE_LOCATION);
    }


    // ----------------------------------------------------
}
