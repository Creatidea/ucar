package tw.com.car_plus.carshare.rent.return_car.check;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.util.camera.BaseCameraActivity;

/**
 * Created by winni on 2017/3/18.
 */
@RuntimePermissions
public class ReturnCheckCarActivity extends BaseCameraActivity implements View.OnClickListener {

    @BindView(R.id.scrollview)
    ScrollView scrollview;
    //左前
    @BindView(R.id.layout_image_1)
    View leftFrontView;
    //車位
    @BindView(R.id.layout_image_2)
    View carPlaceView;
    //右前
    @BindView(R.id.layout_image_3)
    View rightFrontView;
    //右後
    @BindView(R.id.layout_image_4)
    View rightBackView;
    //左後
    @BindView(R.id.layout_image_5)
    View leftBackView;
    //增加的
    @BindView(R.id.layout_image_6)
    View moreView;
    @BindView(R.id.add)
    Button add;
    @BindView(R.id.complete)
    Button complete;

    //左前
    private final int LEFT_FRONT = 11;
    //車位
    private final int CAR_PLACE = 22;
    //右前
    private final int RIGHT_FRONT = 33;
    //右後
    private final int RIGHT_BACK = 44;
    //左後
    private final int LEFT_BACK = 55;
    //增加
    private final int MORE = 66;


    private TextView leftFrontTitle, carPlaceTitle, rightFrontTitle, rightBackTitle, leftBackTitle, moreTitle;
    private TextView leftFrontNumber, carPlaceNumber, rightFrontNumber, rightBackNumber, leftBackNumber, moreNumber;
    private ImageView leftFront, carPlace, rightFront, rightBack, leftBack, more;
    private String leftFrontPath = "", carPlacePath = "", rightFrontPath = "", rightBackPath = "", leftBackPath = "", morePath = "";
    private ArrayList<String> fileNames;
    private List<String> imagePath;
    private int position = 0;
    private boolean isShowDialog = false;


    @Override
    public void init() {

        setContentView(R.layout.activity_check_car);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_toolbar_check_car);

        add.setOnClickListener(this);
        complete.setOnClickListener(this);

        leftFrontTitle = (TextView) leftFrontView.findViewById(R.id.title);
        leftFront = (ImageView) leftFrontView.findViewById(R.id.image);
        leftFrontNumber = (TextView) leftFrontView.findViewById(R.id.number);
        setImageData(leftFrontTitle, leftFrontNumber, leftFront, getString(R.string.left_front), R.drawable.img_car_left_front, LEFT_FRONT, "1");

        carPlaceTitle = (TextView) carPlaceView.findViewById(R.id.title);
        carPlace = (ImageView) carPlaceView.findViewById(R.id.image);
        carPlaceNumber = (TextView) carPlaceView.findViewById(R.id.number);
        setImageData(carPlaceTitle, carPlaceNumber, carPlace, getString(R.string.car_place), R.drawable.img_car_place, CAR_PLACE, "2");

        rightFrontTitle = (TextView) rightFrontView.findViewById(R.id.title);
        rightFront = (ImageView) rightFrontView.findViewById(R.id.image);
        rightFrontNumber = (TextView) rightFrontView.findViewById(R.id.number);
        setImageData(rightFrontTitle, rightFrontNumber, rightFront, getString(R.string.right_front), R.drawable.img_car_right_front, RIGHT_FRONT,
                "3");

        rightBackTitle = (TextView) rightBackView.findViewById(R.id.title);
        rightBack = (ImageView) rightBackView.findViewById(R.id.image);
        rightBackNumber = (TextView) rightBackView.findViewById(R.id.number);
        setImageData(rightBackTitle, rightBackNumber, rightBack, getString(R.string.right_back), R.drawable.img_car_right_back, RIGHT_BACK, "4");

        leftBackView.setVisibility(View.VISIBLE);
        leftBackTitle = (TextView) leftBackView.findViewById(R.id.title);
        leftBack = (ImageView) leftBackView.findViewById(R.id.image);
        leftBackNumber = (TextView) leftBackView.findViewById(R.id.number);
        setImageData(leftBackTitle, leftBackNumber, leftBack, getString(R.string.left_back), R.drawable.img_car_left_back, LEFT_BACK, "5");

        moreTitle = (TextView) moreView.findViewById(R.id.title);
        more = (ImageView) moreView.findViewById(R.id.image);
        moreNumber = (TextView) moreView.findViewById(R.id.number);
        setImageData(moreTitle, moreNumber, more, "", R.drawable.img_car, MORE, "6");

        imagePath = new ArrayList<>();

    }

    // ----------------------------------------------------
    private void setImageData(TextView title, TextView number, ImageView imageView, String strTitle, int background, int id, String strNumber) {
        title.setText(strTitle);
        imageView.setId(id);
        imageView.setOnClickListener(this);
        Glide.with(this).load(background).into(imageView);
        number.setText(strNumber);
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(UploadFile uploadFile) {

        isShowDialog = true;
        loadingDialogManager.show(getString(R.string.upload_picture));

        fileNames.add(uploadFile.getFileName());
        position++;

        if (fileNames.size() != imagePath.size()) {
            fileUploadConnect.ConnectUploadFile(imagePath.get(position));
        } else {
            cleanPicture(imagePath);
            Toast.makeText(this, getString(R.string.upload_finish), Toast.LENGTH_LONG).show();
            Intent intent = new Intent();
            intent.putStringArrayListExtra("photo", fileNames);
            setResult(RESULT_OK, intent);
            loadingDialogManager.dismiss();
            finish();
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        if (isShowDialog) {
            loadingDialogManager.dismiss();
            isShowDialog = false;
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_CANCELED) {

            compressionPicture(new CompressionPictureListener() {
                @Override
                public void compressionListener() {

                    switch (requestCode) {
                        case LEFT_FRONT:
                            leftFrontPath = setImageView(leftFront);
                            focusOnView(carPlaceView);
                            break;
                        case CAR_PLACE:
                            carPlacePath = setImageView(carPlace);
                            focusOnView(rightFrontView);
                            break;
                        case RIGHT_FRONT:
                            rightFrontPath = setImageView(rightFront);
                            focusOnView(rightBackView);
                            break;
                        case RIGHT_BACK:
                            rightBackPath = setImageView(rightBack);
                            focusOnView(leftBackView);
                            break;
                        case LEFT_BACK:
                            leftBackPath = setImageView(leftBack);
                            if (moreView.isShown()) {
                                focusOnView(carPlaceView);
                            }
                            break;
                        case MORE:
                            morePath = setImageView(more);
                            break;
                    }
                }
            });
        }
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ReturnCheckCarActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意開啟相機與寫入資料的權限
     */
    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void goToTakePicture(int type) {
        takePictures(type);
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showDeniedForTakePicture() {
        Toast.makeText(this, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add:
                moreView.setVisibility(View.VISIBLE);
                add.setVisibility(View.GONE);
                break;
            case R.id.complete:
                position = 0;
                imagePath.clear();

                if (checkCarPicture()) {
                    if (networkChecker.isNetworkAvailable()) {
                        //表示有照片
                        fileNames = new ArrayList<>();
                        fileUploadConnect.ConnectUploadFile(imagePath.get(position));
                    }
                } else {
                    Toast.makeText(this, getString(R.string.check_car_picture), Toast.LENGTH_LONG).show();
                }
                break;
            default:
                clickImage(view.getId());
                break;
        }
    }

    // ----------------------------------------------------
    //檢查是否有拍照
    private boolean checkCarPicture() {

        boolean isLeftFrontPath, isCarPlace, isRightFront, isRightBackPath, isLeftBackPath, isMorPath;

        isLeftFrontPath = checkHavePicture(leftFrontPath);
        isCarPlace = checkHavePicture(carPlacePath);
        isRightFront = checkHavePicture(rightFrontPath);
        isRightBackPath = checkHavePicture(rightBackPath);
        isLeftBackPath = checkHavePicture(leftBackPath);
        isMorPath = checkHavePicture(morePath);

        return isLeftFrontPath && isRightFront && isCarPlace && isRightBackPath && isLeftBackPath;

    }

    // ----------------------------------------------------

    /**
     * 檢查是否有拍照
     *
     * @param path
     * @return
     */
    private boolean checkHavePicture(String path) {
        if (path.length() != 0) {
            imagePath.add(path);
            return true;
        } else {
            return false;
        }
    }

    // ----------------------------------------------------

    /**
     * image的按下事件
     *
     * @param id
     */
    private void clickImage(int id) {

        int type = 0;
        switch (id) {
            case LEFT_FRONT:
                type = LEFT_FRONT;
                break;
            case CAR_PLACE:
                type = CAR_PLACE;
                break;
            case RIGHT_FRONT:
                type = RIGHT_FRONT;
                break;
            case RIGHT_BACK:
                type = RIGHT_BACK;
                break;
            case LEFT_BACK:
                type = LEFT_BACK;
                break;
            case MORE:
                type = MORE;
                break;
        }

        ReturnCheckCarActivityPermissionsDispatcher.goToTakePictureWithCheck(this, type);
    }

    // ----------------------------------------------------

    /**
     * 將移動至指定的view
     *
     * @param view
     */
    private void focusOnView(final View view) {

        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.scrollTo(0, view.getTop() - 150);
            }
        });
    }
}
