package tw.com.car_plus.carshare.rent.return_car.check.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.rent.return_car.check.model.CheckList;

/**
 * Created by winni on 2017/4/14.
 */

public class CheckListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<CheckList> checkLists;

    // ----------------------------------------------------
    public CheckListAdapter(Context context) {
        checkLists = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    // ----------------------------------------------------
    @Override
    public int getCount() {
        return checkLists.size();
    }

    // ----------------------------------------------------
    @Override
    public Object getItem(int position) {
        return null;
    }

    // ----------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // ----------------------------------------------------
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_check_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.item.setText(checkLists.get(position).getTitle());
        viewHolder.check.setSelected(checkLists.get(position).isCheck());

        return convertView;
    }

    // ----------------------------------------------------

    /**
     * 設定資料
     *
     * @param checkLists
     */
    public void setData(ArrayList<CheckList> checkLists) {
        this.checkLists = checkLists;
        notifyDataSetChanged();
    }

    // ----------------------------------------------------
    class ViewHolder {
        @BindView(R.id.item)
        TextView item;
        @BindView(R.id.check)
        ImageView check;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    // ----------------------------------------------------
}
