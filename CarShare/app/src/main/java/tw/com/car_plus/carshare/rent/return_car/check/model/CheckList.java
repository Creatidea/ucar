package tw.com.car_plus.carshare.rent.return_car.check.model;

/**
 * Created by winni on 2017/4/14.
 */

public class CheckList {

    private String title;
    private boolean isCheck;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
