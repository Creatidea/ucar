package tw.com.car_plus.carshare.rent.return_car.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by winni on 2017/4/17.
 */

public class BillInfo implements Parcelable {


    /**
     * OrderNo : S2017041800014
     * CalAmountResult : {"CarSeries":"S3","CarNo":"RBT-6220","RentStartOnUtc":"2017-04-18T07:40:30.267Z",
     * "RentEndOnUtc":"2017-04-18T07:42:26.9311852Z","CreditCards":[{"SerialNo":"0lAqEoccPzzvyWN6Wgdo2Q==","CardNo":"3560-****-****-2402",
     * "IsDefault":true}],"TotalAmount":100,"OdoMeter":0,"OdoMeterAmount":0,"TimeForHour":0,"TimeForMinute":2,"TimeAmount":100,"Status":true,
     * "IsPromotion":true,"PromotionTimeAmount":111,"PromotionTotalAmount":111,"PromotionInfoDetail":{"Title":"測試1","Info":"testetstastewsgf",
     * "StartTime":"","EndTime":""}}
     * BonusCalculateResult : {"OrderBonus":1,"RemainBonus":51,"CurrentBonus":50,"MaxBonusLimit":5,"MaxAmountLimit":10,"TotalAmount":100,"Status":false}
     */

    @SerializedName("OrderNo")
    private String OrderNo;
    @SerializedName("CalAmountResult")
    private CalAmountResultBean CalAmountResult;
    @SerializedName("BonusCalculateResult")
    private BonusCalculateResultBean BonusCalculateResult;

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String OrderNo) {
        this.OrderNo = OrderNo;
    }

    public CalAmountResultBean getCalAmountResult() {
        return CalAmountResult;
    }

    public void setCalAmountResult(CalAmountResultBean CalAmountResult) {
        this.CalAmountResult = CalAmountResult;
    }

    public BonusCalculateResultBean getBonusCalculateResult() {
        return BonusCalculateResult;
    }

    public void setBonusCalculateResult(BonusCalculateResultBean BonusCalculateResult) {
        this.BonusCalculateResult = BonusCalculateResult;
    }

    public static class CalAmountResultBean implements Parcelable {
        /**
         * CarSeries : S3
         * CarNo : RBT-6220
         * RentStartOnUtc : 2017-04-18T07:40:30.267Z
         * RentEndOnUtc : 2017-04-18T07:42:26.9311852Z
         * CreditCards : [{"SerialNo":"0lAqEoccPzzvyWN6Wgdo2Q==","CardNo":"3560-****-****-2402","IsDefault":true}]
         * TotalAmount : 100
         * OdoMeter : 0
         * OdoMeterAmount : 0
         * TimeForHour : 0
         * TimeForMinute : 2
         * TimeAmount : 100
         * Status : true
         * IsPromotion : true
         * PromotionTimeAmount : 111
         * PromotionTotalAmount : 111
         * PromotionInfoDetail : {"Title":"測試1","Info":"testetstastewsgf","StartTime":"","EndTime":""}
         */

        @SerializedName("CarSeries")
        private String CarSeries;
        @SerializedName("CarNo")
        private String CarNo;
        @SerializedName("RentStartOnUtc")
        private String RentStartOnUtc;
        @SerializedName("RentEndOnUtc")
        private String RentEndOnUtc;
        @SerializedName("TotalAmount")
        private int TotalAmount;
        @SerializedName("OdoMeter")
        private int OdoMeter;
        @SerializedName("OdoMeterAmount")
        private int OdoMeterAmount;
        @SerializedName("ETagAmount")
        private int ETagAmount;
        @SerializedName("TimeForHour")
        private int TimeForHour;
        @SerializedName("TimeForMinute")
        private int TimeForMinute;
        @SerializedName("TimeAmount")
        private int TimeAmount;
        @SerializedName("Status")
        private boolean Status;
        @SerializedName("IsPromotion")
        private boolean IsPromotion;
        @SerializedName("PromotionTimeAmount")
        private int PromotionTimeAmount;
        @SerializedName("PromotionTotalAmount")
        private int PromotionTotalAmount;
        @SerializedName("PromotionInfoDetail")
        private PromotionInfoDetailBean PromotionInfoDetail;
        @SerializedName("CreditCards")
        private List<CreditCardsBean> CreditCards;

        public String getCarSeries() {
            return CarSeries;
        }

        public void setCarSeries(String CarSeries) {
            this.CarSeries = CarSeries;
        }

        public String getCarNo() {
            return CarNo;
        }

        public void setCarNo(String CarNo) {
            this.CarNo = CarNo;
        }

        public String getRentStartOnUtc() {
            return RentStartOnUtc;
        }

        public void setRentStartOnUtc(String RentStartOnUtc) {
            this.RentStartOnUtc = RentStartOnUtc;
        }

        public String getRentEndOnUtc() {
            return RentEndOnUtc;
        }

        public void setRentEndOnUtc(String RentEndOnUtc) {
            this.RentEndOnUtc = RentEndOnUtc;
        }

        public int getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(int TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public int getOdoMeter() {
            return OdoMeter;
        }

        public void setOdoMeter(int OdoMeter) {
            this.OdoMeter = OdoMeter;
        }

        public int getOdoMeterAmount() {
            return OdoMeterAmount;
        }

        public void setOdoMeterAmount(int OdoMeterAmount) {
            this.OdoMeterAmount = OdoMeterAmount;
        }

        public int getETagAmount() {
            return ETagAmount;
        }

        public void setETagAmount(int ETagAmount) {
            this.ETagAmount = ETagAmount;
        }

        public int getTimeForHour() {
            return TimeForHour;
        }

        public void setTimeForHour(int TimeForHour) {
            this.TimeForHour = TimeForHour;
        }

        public int getTimeForMinute() {
            return TimeForMinute;
        }

        public void setTimeForMinute(int TimeForMinute) {
            this.TimeForMinute = TimeForMinute;
        }

        public int getTimeAmount() {
            return TimeAmount;
        }

        public void setTimeAmount(int TimeAmount) {
            this.TimeAmount = TimeAmount;
        }

        public boolean isStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public boolean isIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(boolean IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public int getPromotionTimeAmount() {
            return PromotionTimeAmount;
        }

        public void setPromotionTimeAmount(int PromotionTimeAmount) {
            this.PromotionTimeAmount = PromotionTimeAmount;
        }

        public int getPromotionTotalAmount() {
            return PromotionTotalAmount;
        }

        public void setPromotionTotalAmount(int PromotionTotalAmount) {
            this.PromotionTotalAmount = PromotionTotalAmount;
        }

        public PromotionInfoDetailBean getPromotionInfoDetail() {
            return PromotionInfoDetail;
        }

        public void setPromotionInfoDetail(PromotionInfoDetailBean PromotionInfoDetail) {
            this.PromotionInfoDetail = PromotionInfoDetail;
        }

        public List<CreditCardsBean> getCreditCards() {
            return CreditCards;
        }

        public void setCreditCards(List<CreditCardsBean> CreditCards) {
            this.CreditCards = CreditCards;
        }

        public static class PromotionInfoDetailBean implements Parcelable {
            /**
             * Title : 測試1
             * Info : testetstastewsgf
             * StartTime :
             * EndTime :
             */

            @SerializedName("Title")
            private String Title;
            @SerializedName("Info")
            private String Info;
            @SerializedName("StartTime")
            private String StartTime;
            @SerializedName("EndTime")
            private String EndTime;

            public String getTitle() {
                return Title;
            }

            public void setTitle(String Title) {
                this.Title = Title;
            }

            public String getInfo() {
                return Info;
            }

            public void setInfo(String Info) {
                this.Info = Info;
            }

            public String getStartTime() {
                return StartTime;
            }

            public void setStartTime(String StartTime) {
                this.StartTime = StartTime;
            }

            public String getEndTime() {
                return EndTime;
            }

            public void setEndTime(String EndTime) {
                this.EndTime = EndTime;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.Title);
                dest.writeString(this.Info);
                dest.writeString(this.StartTime);
                dest.writeString(this.EndTime);
            }

            public PromotionInfoDetailBean() {
            }

            protected PromotionInfoDetailBean(Parcel in) {
                this.Title = in.readString();
                this.Info = in.readString();
                this.StartTime = in.readString();
                this.EndTime = in.readString();
            }

            public static final Creator<PromotionInfoDetailBean> CREATOR = new Creator<PromotionInfoDetailBean>() {
                @Override
                public PromotionInfoDetailBean createFromParcel(Parcel source) {
                    return new PromotionInfoDetailBean(source);
                }

                @Override
                public PromotionInfoDetailBean[] newArray(int size) {
                    return new PromotionInfoDetailBean[size];
                }
            };
        }

        public static class CreditCardsBean implements Parcelable {
            /**
             * SerialNo : 0lAqEoccPzzvyWN6Wgdo2Q==
             * CardNo : 3560-****-****-2402
             * IsDefault : true
             */

            @SerializedName("SerialNo")
            private String SerialNo;
            @SerializedName("CardNo")
            private String CardNo;
            @SerializedName("IsDefault")
            private boolean IsDefault;

            public String getSerialNo() {
                return SerialNo;
            }

            public void setSerialNo(String SerialNo) {
                this.SerialNo = SerialNo;
            }

            public String getCardNo() {
                return CardNo;
            }

            public void setCardNo(String CardNo) {
                this.CardNo = CardNo;
            }

            public boolean isIsDefault() {
                return IsDefault;
            }

            public void setIsDefault(boolean IsDefault) {
                this.IsDefault = IsDefault;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.SerialNo);
                dest.writeString(this.CardNo);
                dest.writeByte(this.IsDefault ? (byte) 1 : (byte) 0);
            }

            public CreditCardsBean() {
            }

            protected CreditCardsBean(Parcel in) {
                this.SerialNo = in.readString();
                this.CardNo = in.readString();
                this.IsDefault = in.readByte() != 0;
            }

            public static final Creator<CreditCardsBean> CREATOR = new Creator<CreditCardsBean>() {
                @Override
                public CreditCardsBean createFromParcel(Parcel source) {
                    return new CreditCardsBean(source);
                }

                @Override
                public CreditCardsBean[] newArray(int size) {
                    return new CreditCardsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.CarSeries);
            dest.writeString(this.CarNo);
            dest.writeString(this.RentStartOnUtc);
            dest.writeString(this.RentEndOnUtc);
            dest.writeInt(this.TotalAmount);
            dest.writeInt(this.OdoMeter);
            dest.writeInt(this.OdoMeterAmount);
            dest.writeInt(this.ETagAmount);
            dest.writeInt(this.TimeForHour);
            dest.writeInt(this.TimeForMinute);
            dest.writeInt(this.TimeAmount);
            dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
            dest.writeByte(this.IsPromotion ? (byte) 1 : (byte) 0);
            dest.writeInt(this.PromotionTimeAmount);
            dest.writeInt(this.PromotionTotalAmount);
            dest.writeParcelable(this.PromotionInfoDetail, flags);
            dest.writeList(this.CreditCards);
        }

        public CalAmountResultBean() {
        }

        protected CalAmountResultBean(Parcel in) {
            this.CarSeries = in.readString();
            this.CarNo = in.readString();
            this.RentStartOnUtc = in.readString();
            this.RentEndOnUtc = in.readString();
            this.TotalAmount = in.readInt();
            this.OdoMeter = in.readInt();
            this.OdoMeterAmount = in.readInt();
            this.ETagAmount=in.readInt();
            this.TimeForHour = in.readInt();
            this.TimeForMinute = in.readInt();
            this.TimeAmount = in.readInt();
            this.Status = in.readByte() != 0;
            this.IsPromotion = in.readByte() != 0;
            this.PromotionTimeAmount = in.readInt();
            this.PromotionTotalAmount = in.readInt();
            this.PromotionInfoDetail = in.readParcelable(PromotionInfoDetailBean.class.getClassLoader());
            this.CreditCards = new ArrayList<CreditCardsBean>();
            in.readList(this.CreditCards, CreditCardsBean.class.getClassLoader());
        }

        public static final Creator<CalAmountResultBean> CREATOR = new Creator<CalAmountResultBean>() {
            @Override
            public CalAmountResultBean createFromParcel(Parcel source) {
                return new CalAmountResultBean(source);
            }

            @Override
            public CalAmountResultBean[] newArray(int size) {
                return new CalAmountResultBean[size];
            }
        };
    }

    public static class BonusCalculateResultBean implements Parcelable {
        /**
         * OrderBonus : 1
         * RemainBonus : 51
         * CurrentBonus : 50
         * MaxBonusLimit : 5
         * TotalAmount : 100
         * MaxAmountLimit : 10
         * Status : false
         */

        @SerializedName("OrderBonus")
        private int OrderBonus;
        @SerializedName("RemainBonus")
        private int RemainBonus;
        @SerializedName("CurrentBonus")
        private int CurrentBonus;
        @SerializedName("MaxBonusLimit")
        private int MaxBonusLimit;
        @SerializedName("MaxAmountLimit")
        private int MaxAmountLimit;
        @SerializedName("TotalAmount")
        private int TotalAmount;
        @SerializedName("Status")
        private boolean Status;

        public int getOrderBonus() {
            return OrderBonus;
        }

        public void setOrderBonus(int OrderBonus) {
            this.OrderBonus = OrderBonus;
        }

        public int getRemainBonus() {
            return RemainBonus;
        }

        public void setRemainBonus(int RemainBonus) {
            this.RemainBonus = RemainBonus;
        }

        public int getCurrentBonus() {
            return CurrentBonus;
        }

        public void setCurrentBonus(int CurrentBonus) {
            this.CurrentBonus = CurrentBonus;
        }

        public int getMaxBonusLimit() {
            return MaxBonusLimit;
        }

        public void setMaxBonusLimit(int MaxBonusLimit) {
            this.MaxBonusLimit = MaxBonusLimit;
        }

        public int getMaxAmountLimit() {
            return MaxAmountLimit;
        }

        public void setMaxAmountLimit(int maxAmountLimit) {
            MaxAmountLimit = maxAmountLimit;
        }

        public int getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(int TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public boolean isStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.OrderBonus);
            dest.writeInt(this.RemainBonus);
            dest.writeInt(this.CurrentBonus);
            dest.writeInt(this.MaxBonusLimit);
            dest.writeInt(this.MaxAmountLimit);
            dest.writeInt(this.TotalAmount);
            dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
        }

        public BonusCalculateResultBean() {
        }

        protected BonusCalculateResultBean(Parcel in) {
            this.OrderBonus = in.readInt();
            this.RemainBonus = in.readInt();
            this.CurrentBonus = in.readInt();
            this.MaxBonusLimit = in.readInt();
            this.MaxAmountLimit = in.readInt();
            this.TotalAmount = in.readInt();
            this.Status = in.readByte() != 0;
        }

        public static final Creator<BonusCalculateResultBean> CREATOR = new Creator<BonusCalculateResultBean>() {
            @Override
            public BonusCalculateResultBean createFromParcel(Parcel source) {
                return new BonusCalculateResultBean(source);
            }

            @Override
            public BonusCalculateResultBean[] newArray(int size) {
                return new BonusCalculateResultBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.OrderNo);
        dest.writeParcelable(this.CalAmountResult, flags);
        dest.writeParcelable(this.BonusCalculateResult, flags);
    }

    public BillInfo() {
    }

    protected BillInfo(Parcel in) {
        this.OrderNo = in.readString();
        this.CalAmountResult = in.readParcelable(CalAmountResultBean.class.getClassLoader());
        this.BonusCalculateResult = in.readParcelable(BonusCalculateResultBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<BillInfo> CREATOR = new Parcelable.Creator<BillInfo>() {
        @Override
        public BillInfo createFromParcel(Parcel source) {
            return new BillInfo(source);
        }

        @Override
        public BillInfo[] newArray(int size) {
            return new BillInfo[size];
        }
    };
}
