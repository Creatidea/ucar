package tw.com.car_plus.carshare.rent.return_car.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neil on 2017/4/19.
 */

public class Bonus {

    /**
     * OrderBonus : 20
     * RemainBonus : 239
     * CurrentBonus : 219
     * MaxBonusLimit : 50
     * TotalAmount : 2050
     */

    @SerializedName("OrderBonus")
    private int OrderBonus;
    @SerializedName("RemainBonus")
    private int RemainBonus;
    @SerializedName("CurrentBonus")
    private int CurrentBonus;
    @SerializedName("MaxBonusLimit")
    private int MaxBonusLimit;
    @SerializedName("TotalAmount")
    private int TotalAmount;
    @SerializedName("MaxAmountLimit")
    private int MaxAmountLimit;

    public int getMaxAmountLimit() {
        return MaxAmountLimit;
    }

    public void setMaxAmountLimit(int maxAmountLimit) {
        MaxAmountLimit = maxAmountLimit;
    }

    public int getOrderBonus() {
        return OrderBonus;
    }

    public void setOrderBonus(int OrderBonus) {
        this.OrderBonus = OrderBonus;
    }

    public int getRemainBonus() {
        return RemainBonus;
    }

    public void setRemainBonus(int RemainBonus) {
        this.RemainBonus = RemainBonus;
    }

    public int getCurrentBonus() {
        return CurrentBonus;
    }

    public void setCurrentBonus(int CurrentBonus) {
        this.CurrentBonus = CurrentBonus;
    }

    public int getMaxBonusLimit() {
        return MaxBonusLimit;
    }

    public void setMaxBonusLimit(int MaxBonusLimit) {
        this.MaxBonusLimit = MaxBonusLimit;
    }

    public int getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(int TotalAmount) {
        this.TotalAmount = TotalAmount;
    }
}
