package tw.com.car_plus.carshare.rent.return_car.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neil on 2017/4/17.
 */

public class ReturnCarInfo implements Parcelable {

    /**
     * StartReturnCarOnUtc : 2017-04-17T10:18:34.9334235Z
     * ReturnCarExpireOnUtc : 2017-04-17T10:28:34.9334235Z
     * CarId : 39
     * CarSeries : Luxgen S3
     * CarNo : RBT-6210
     * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
     * OrderId : 4951
     * CarEnergyType : 2
     * Status : true
     */

    @SerializedName("StartReturnCarOnUtc")
    private String StartReturnCarOnUtc;
    @SerializedName("ReturnCarExpireOnUtc")
    private String ReturnCarExpireOnUtc;
    @SerializedName("CarId")
    private int CarId;
    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("CarPic")
    private String CarPic;
    @SerializedName("OrderId")
    private int OrderId;
    @SerializedName("CarEnergyType")
    private int CarEnergyType;
    @SerializedName("CarVigType")
    private int CarVigType;
    @SerializedName("Status")
    private boolean Status;

    public String getStartReturnCarOnUtc() {
        return StartReturnCarOnUtc;
    }

    public void setStartReturnCarOnUtc(String StartReturnCarOnUtc) {
        this.StartReturnCarOnUtc = StartReturnCarOnUtc;
    }

    public String getReturnCarExpireOnUtc() {
        return ReturnCarExpireOnUtc;
    }

    public void setReturnCarExpireOnUtc(String ReturnCarExpireOnUtc) {
        this.ReturnCarExpireOnUtc = ReturnCarExpireOnUtc;
    }

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int CarId) {
        this.CarId = CarId;
    }

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getCarPic() {
        return CarPic;
    }

    public void setCarPic(String CarPic) {
        this.CarPic = CarPic;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public int getCarEnergyType() {
        return CarEnergyType;
    }

    public void setCarEnergyType(int carEnergyType) {
        CarEnergyType = carEnergyType;
    }

    public int getCarVigType() {
        return CarVigType;
    }

    public void setCarVigType(int carVigType) {
        CarVigType = carVigType;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.StartReturnCarOnUtc);
        dest.writeString(this.ReturnCarExpireOnUtc);
        dest.writeInt(this.CarId);
        dest.writeString(this.CarSeries);
        dest.writeString(this.CarNo);
        dest.writeString(this.CarPic);
        dest.writeInt(this.OrderId);
        dest.writeInt(this.CarEnergyType);
        dest.writeInt(this.CarVigType);
        dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
    }

    public ReturnCarInfo() {
    }

    protected ReturnCarInfo(Parcel in) {
        this.StartReturnCarOnUtc = in.readString();
        this.ReturnCarExpireOnUtc = in.readString();
        this.CarId = in.readInt();
        this.CarSeries = in.readString();
        this.CarNo = in.readString();
        this.CarPic = in.readString();
        this.OrderId = in.readInt();
        this.CarEnergyType = in.readInt();
        this.CarVigType = in.readInt();
        this.Status = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ReturnCarInfo> CREATOR = new Parcelable.Creator<ReturnCarInfo>() {
        @Override
        public ReturnCarInfo createFromParcel(Parcel source) {
            return new ReturnCarInfo(source);
        }

        @Override
        public ReturnCarInfo[] newArray(int size) {
            return new ReturnCarInfo[size];
        }
    };
}
