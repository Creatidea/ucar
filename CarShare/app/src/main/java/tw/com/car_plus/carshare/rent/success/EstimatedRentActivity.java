package tw.com.car_plus.carshare.rent.success;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.record.detail.DialogPromotionFragment;
import tw.com.car_plus.carshare.rent.success.model.EstimateAmount;
import tw.com.car_plus.carshare.util.CustomBaseActivity;
import tw.com.car_plus.carshare.util.DateParser;

/**
 * Created by winni on 2017/3/20.
 * 估租金
 */

public class EstimatedRentActivity extends CustomBaseActivity {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.car_series)
    TextView carSeries;
    @BindView(R.id.car_number)
    TextView carNumber;
    @BindView(R.id.credit_card_number)
    TextView creditCardNumber;
    @BindView(R.id.get_car_time)
    TextView getCarTime;
    @BindView(R.id.return_car_time)
    TextView returnCarTime;
    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.hour_unit)
    TextView hourUnit;
    @BindView(R.id.minute)
    TextView minute;
    @BindView(R.id.rent_price)
    TextView rentPrice;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.distance_price)
    TextView distancePrice;
    @BindView(R.id.e_tag_price)
    TextView eTagPrice;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.promotion_price)
    TextView promotionPrice;
    @BindView(R.id.layout_promotion)
    LinearLayout layoutPromotion;
    
    private DateParser dateParser;
    private EstimateAmount estimateAmount;


    // ----------------------------------------------------
    @Override
    public void init() {

        setContentView(R.layout.activity_estimated_rent);
        ButterKnife.bind(this);
        dateParser = new DateParser();

        setBackToolBar(R.drawable.img_toolbar_estimated_amount);
        icon.setImageResource(R.drawable.ic_dollar);
        title.setText(getString(R.string.consumer_details));

        new RentConnect(this).estimateAmountConnect(getIntent().getIntExtra("orderId", 0));
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(EstimateAmount estimateAmount) {

        this.estimateAmount = estimateAmount;

        carSeries.setText(estimateAmount.getCarSeries());
        carNumber.setText(estimateAmount.getCarNo());

        if (estimateAmount.getCreditCards().size() > 0) {
            creditCardNumber.setText(estimateAmount.getCreditCards().get(0).getCardNo());
        }

        getCarTime.setText(dateParser.getDateTime(estimateAmount.getRentStartOnUtc()));
        returnCarTime.setText(dateParser.getDateTime(estimateAmount.getRentEndOnUtc()));
        showRentTime(estimateAmount.getTimeForHour(), estimateAmount.getTimeForMinute());
        rentPrice.setText(String.valueOf(estimateAmount.getTimeAmount()));
        distance.setText(String.valueOf(estimateAmount.getOdoMeter()));
        distancePrice.setText(String.valueOf(estimateAmount.getOdoMeterAmount()));
        eTagPrice.setText(String.valueOf(estimateAmount.getETagAmount()));
        price.setText(String.valueOf(estimateAmount.getTotalAmount()));

        if (estimateAmount.isPromotion()) {
            layoutPromotion.setVisibility(View.VISIBLE);
            promotionPrice.setText(String.valueOf(estimateAmount.getPromotionTotalAmount()));
        }

    }

    // ----------------------------------------------------

    /**
     * 顯示時租時間
     *
     * @param mHour
     * @param mMinute
     */
    private void showRentTime(int mHour, int mMinute) {

        if (mHour == 0) {
            hour.setVisibility(View.INVISIBLE);
            hourUnit.setVisibility(View.INVISIBLE);
        } else {
            hour.setText(String.valueOf(mHour));
        }

        minute.setText(String.valueOf(mMinute));
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.layout_pay_promotion_info, R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_pay_promotion_info:

                EstimateAmount.PromotionInfoDetailBean promotionInfoDetailBean = estimateAmount.getPromotionInfoDetail();

                Bundle bundle = new Bundle();
                bundle.putString("startDate", promotionInfoDetailBean.getStartTime());
                bundle.putString("endDate", promotionInfoDetailBean.getEndTime());
                bundle.putString("title", promotionInfoDetailBean.getTitle());
                bundle.putString("info", promotionInfoDetailBean.getInfo());

                showDialogFragment(new DialogPromotionFragment(), bundle);

                break;
            case R.id.back:
                finish();
                break;
        }
    }

    // ----------------------------------------------------
}
