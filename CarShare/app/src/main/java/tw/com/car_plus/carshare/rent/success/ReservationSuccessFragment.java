package tw.com.car_plus.carshare.rent.success;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.ConnectInfo;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.rent.model.ReservationInfo;
import tw.com.car_plus.carshare.rent.pickup_car.PickUpTheCarFragment;
import tw.com.car_plus.carshare.rent.success.model.Postpone;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DateParser;
import tw.com.car_plus.carshare.util.DialogUtil;

/**
 * Created by neil on 2017/3/17.
 * 預約成功
 */

public class ReservationSuccessFragment extends CustomBaseFragment {

    @BindView(R.id.car_series)
    TextView carSeries;
    @BindView(R.id.car_no)
    TextView carNo;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.layout_postpone)
    RelativeLayout layoutPostpone;

    private final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
    private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm aaa", Locale.TAIWAN);

    private DateParser dateParser;
    private ReservationInfo reservationInfo;
    private RentConnect rentConnect;
    private DialogUtil dialogUtil;
    private ImageView toolbarImage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            this.toolbarImage = ((IndexActivity) context).toolbarImage;
        }
    }

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_reservation_success);
        reservationInfo = getArguments().getParcelable("reservationInfo");
        rentConnect = new RentConnect(activity);
        dialogUtil = new DialogUtil(activity);
        dateParser = new DateParser();
        toolbarImage.setImageResource(R.drawable.img_toolbar_reservation_success);

        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        carSeries.setText(reservationInfo.getCarSeries());
        carNo.setText(reservationInfo.getCarNo());

        Glide.with(this)
                .load(String.format(ConnectInfo.IMAGE_PATH, reservationInfo.getCarPic()))
                .into(carImage);

        name.setText(reservationInfo.getParkinglotName());
        time.setText(getTimeTo12hour(dateParser.getDateTime(reservationInfo.getPreorderExpireOnUtc())));

    }

    // ----------------------------------------------------
    @OnClick({R.id.gps, R.id.postpone, R.id.pick_up_car})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gps:
                if (reservationInfo != null) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + reservationInfo.getParkinglotLatitude() + "," + reservationInfo
                            .getParkinglotLongitude());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
                break;
            case R.id.postpone:
                rentConnect.prolongPickUpCarConnect(reservationInfo.getOrderId());
                break;
            case R.id.pick_up_car:

                dialogUtil.setMessage(R.string.is_pick_up_car).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("orderId", reservationInfo.getOrderId());
                        replaceFragment(new PickUpTheCarFragment(), false, bundle);
                    }
                }).setCancelButton(R.string.cancel, null).showDialog(false);

                break;
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Postpone postpones) {
        time.setText(getTimeTo12hour(dateParser.getDateTime(postpones.getExpireOnUtc())));
        layoutPostpone.setVisibility(View.GONE);
    }

    // ----------------------------------------------------

    /**
     * 將原本24小時制的轉成12小時制
     *
     * @param strTime
     * @return
     */
    private String getTimeTo12hour(String strTime) {

        try {

            Date date = FORMAT.parse(strTime);
            return TIME_FORMAT.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------


}
