package tw.com.car_plus.carshare.rent.success;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.neilchen.complextoolkit.bluetooth.BlueTooth;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.BaseConnect;
import tw.com.car_plus.carshare.connect.ble.BleConnect;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.connect.return_car.ReturnCarConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.MapModeFragment;
import tw.com.car_plus.carshare.rent.model.CancelErrorOrder;
import tw.com.car_plus.carshare.rent.model.ErrorMessage;
import tw.com.car_plus.carshare.rent.return_car.ParkingActivity;
import tw.com.car_plus.carshare.rent.return_car.ReturnCarFragment;
import tw.com.car_plus.carshare.rent.return_car.model.ReturnCarInfo;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.rent.success.charge.ChargingStationActivity;
import tw.com.car_plus.carshare.rent.success.model.CarFinish;
import tw.com.car_plus.carshare.rent.success.model.CheckVig;
import tw.com.car_plus.carshare.rent.success.model.Mileage;
import tw.com.car_plus.carshare.rent.success.model.SpecialReturn;
import tw.com.car_plus.carshare.service.TimeMeasuringService;
import tw.com.car_plus.carshare.service.model.Time;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DateParser;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.ble.BLEScanUtil;
import tw.com.car_plus.carshare.util.command.CarControllerGatt;
import tw.com.car_plus.carshare.util.security.AESUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;
import tw.com.haitec.ae.project.carsharing.CommandCallback;

import static android.app.Activity.RESULT_OK;
import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType.CAR;
import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarEnergyType.ELECTRIC_CAR;

/**
 * Created by neil on 2017/3/18.
 * 開始租車
 */
@RuntimePermissions
public class StartRentCarFragment extends CustomBaseFragment implements CarControllerGatt.OnConnectListener, BLEScanUtil.OnScanBleListener {

    @BindView(R.id.charging)
    Button charging;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.day)
    TextView day;
    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.minute)
    TextView minute;
    @BindView(R.id.second)
    TextView second;
    @BindView(R.id.mileage)
    TextView mileage;
    @BindView(R.id.locked)
    ImageView locked;
    @BindView(R.id.unlock)
    ImageView unlock;

    // ----------------------------------------------------
    private final String TAG = "StartRentCarFragment";
    private static final int REQUEST_ENABLE_BT = 99;
    private static final int REQUEST_ENABLE_LOCATION = 999;

    private RentConnect rentConnect;
    private ReturnCarConnect returnCarConnect;
    private BleConnect bleConnect;
    private SharedPreferenceUtil sp;
    private CarFinish carFinish;
    private DateParser dateParser;
    private CarEnergyType carEnergyType;
    private CarVigType carVigType;
    private CarControllerGatt carControllerGatt;
    private LoadingDialogManager loadingDialogManager;
    private BLEScanUtil bleScanUtil;
    private DialogUtil dialogUtil;
    private Intent serviceIntent;
    private BlueTooth blueTooth;
    private TimeOutThread timeOutThread = null;
    private SendToBleQueue sendToBleQueue = null;
    private SendDataTimeOutThread sendDataTimeOutThread = null;
    private ArrayBlockingQueue<byte[]> saveBleDataArray, bleDataArray;
    private AlertDialog vigAD;
    private Long startRentTime;
    private int parkingId;
    //車子傳送指令類型(上鎖/開鎖)
    private int type = CarControllerGatt.NONE;
    private String mDeviceAddress = "";
    //isSuccess:是否成功接收到vig回傳的值，isShowDisconnectDialog：是否執行過斷線的Dialog訊息，isSecurity：華創的資安連線是否成功，isVIGCheck：華創的vig是否已有key
    private boolean isSuccess = false, isShowDisconnectDialog = false, isSecurity = false, isLoop = true, isVIGCheck = false;
    //isLoopDataTime：監聽傳送封包的Timeout，isLoopTime：監聽執行Command的Timeout
    private boolean isLoopDataTime = true, isLoopTime = true;
    private int count = 0, countTimeOut = 0;
    private byte[] writeByte;
    private int vigType;


    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_rental_car);

        EventCenter.getInstance().sendSetToolbarImageIndex(R.drawable.img_toolbar_start_rent);

        blueTooth = new BlueTooth(activity);
        resetBle();

        carFinish = getArguments().getParcelable("carFinish");
        loadingDialogManager = new LoadingDialogManager(activity);
        bleConnect = new BleConnect(activity);
        returnCarConnect = new ReturnCarConnect(activity);
        sp = new SharedPreferenceUtil(activity);
        dateParser = new DateParser();
        carEnergyType = new CarEnergyType();
        carVigType = new CarVigType();
        rentConnect = new RentConnect(activity);
        dialogUtil = new DialogUtil(activity);
        bleScanUtil = new BLEScanUtil(activity, carFinish.getCarVigType());
        saveBleDataArray = new ArrayBlockingQueue<>(100);
        bleDataArray = new ArrayBlockingQueue<>(100);
        vigType = carFinish.getCarVigType();
        carVigType.setCarVigType(carFinish.getCarVigType());

        carControllerGatt = new CarControllerGatt(activity, carVigType.getCarVigType());
        carControllerGatt.setVigId(carFinish.getVigId());
        mDeviceAddress = carControllerGatt.getSecurityUtil().getMacAddress(carFinish.getMacAddr());
        carControllerGatt.setDeviceAddress(mDeviceAddress);

        carControllerGatt.onStartBluetoothLeService();
        carControllerGatt.setOnConnectListener(this);
        carControllerGatt.registerGATT();

        description.setText(String.format(getString(R.string.start_rental_time), carFinish.getCarNo(), dateParser.getDateTime(carFinish
                .getRentStartOnUtc())));

        carEnergyType.setCarEnergyType(carFinish.getCarEnergyType());
        changeChargingImage(carEnergyType.getCarEnergyType());
        locked.setSelected(true);

        if (!CarVigType.isGeneralVIG(vigType)) {

            sendToBleQueue = new SendToBleQueue();
            sendToBleQueue.start();

            startRentTime = dateParser.getMillise(carFinish.getRentStartOnUtc());
            // start timer.
            startTimeMeasuringService(startRentTime);
            // if is general type,then unlock VIG check event.
            isVIGCheck = true;

        } else {
            // check VIG has key.
            goToCheckVig();
        }

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimeMeasuringService();

        if (CarVigType.isGeneralVIG(vigType)) {
            carControllerGatt.disconnectSecurity();
        } else {
            isLoop = false;
        }

        carControllerGatt.disconnect();
        carControllerGatt.onDestroy();
    }

    // ----------------------------------------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case EventCenter.RETURN_CAR_MAP:
                    if (data != null) {
                        parkingId = data.getIntExtra("parkingId", 0);
                        returnCarConnect.loadReturnCarInfo(((CarFinish) data.getParcelableExtra("carInfo")).getOrderId());
                    }
                    break;
                case REQUEST_ENABLE_BT:
                    if (bleScanUtil.isLocationEnable()) {
                        bleScanUtil.scanBle(mDeviceAddress, this);
                    } else {
                        goToOpenLocation();
                    }
                    break;
                case REQUEST_ENABLE_LOCATION:
                    bleScanUtil.scanBle(mDeviceAddress, this);
                    break;
            }

        } else {

            if (requestCode == REQUEST_ENABLE_LOCATION) {

                if (bleScanUtil.isOpenBle()) {
                    bleScanUtil.scanBle(mDeviceAddress, this);
                } else {
                    Toast.makeText(activity, getString(R.string.not_open_location), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onTime(Time time) {

        if (!time.getDay().equals("00")) {
            day.setText(time.getDay());
        }

        if (!time.getHour().equals("00")) {
            hour.setText(time.getHour());
        }

        if (!time.getMinute().equals("00")) {
            minute.setText(time.getMinute());
        }

        second.setText(time.getSec());
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(ReturnCarInfo info) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("info", info);
        bundle.putInt("parkingId", parkingId);
        bundle.putString("vigId", carFinish.getVigId());
        bundle.putInt("vigType", vigType);
        bundle.putString("macAddress", mDeviceAddress);
        replaceFragment(new ReturnCarFragment(), false, bundle);
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(Mileage mileageBean) {
        mileage.setText(String.format("%03d", mileageBean.getOdoMeter()));
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(SpecialReturn specialReturn) {

        if (specialReturn.isIsSpecialReturn()) {
            parkingId = specialReturn.getParkinglotId();
            returnCarConnect.loadReturnCarInfo(carFinish.getOrderId());
        } else {
            Intent parkingIntent = new Intent();
            parkingIntent.putExtra("carInfo", carFinish);
            goToActivity(parkingIntent, EventCenter.RETURN_CAR_MAP, ParkingActivity.class);
        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(CancelErrorOrder cancelErrorOrder) {

        if (cancelErrorOrder != null) {

            if (cancelErrorOrder.isStatus()) {

                dialogUtil.setMessage(R.string.cancel_rent_car_success).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        replaceFragment(new MapModeFragment(), false);
                    }
                }).showDialog(false);

            } else {
                List<CancelErrorOrder.ErrorsBean> errorsBeen = cancelErrorOrder.getErrors();
                if (errorsBeen.size() > 0) {
                    Toast.makeText(activity, errorsBeen.get(0).getErrorMsg(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessMobiletronVig(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.READ_VIG_DATA) {

            String value = (String) data.get("data");
            value = value.replace(AESUtil.HEADER, "");

            if (value.contains("error")) {

                //強制移除監看command的Thread
                startAndCloseSendDataTimeOutThread(false);
                startAndCloseTimeOutThread(false);
                closeDialog();
                Toast.makeText(activity, activity.getString(R.string.try_again), Toast.LENGTH_LONG).show();
            }

        } else if ((int) data.get("type") == EventCenter.READ_VIG_DATA_COMMAND) {

            isSuccess = true;
            //強制移除監看command的Thread
            startAndCloseSendDataTimeOutThread(false);
            startAndCloseTimeOutThread(false);

            String value = (String) data.get("data");
            commandFinish(value);
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void OnSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.SECURITY_STATUS) {

            final int status = (int) data.get("data");

            isSecurity = (status == CommandCallback.CONNECTING_STATUS_SUCCESS);
            sp.setSecurityStatus(isSecurity);

            if (isSecurity) {
                count = 0;
                countTimeOut = 0;
                if (type != CarControllerGatt.NONE) {
                    goSendCommandToSecurity(type);
                }
            } else {
                connectSecurityStatus(status);
            }

        } else if ((int) data.get("type") == EventCenter.MAIN_SECURITY_READ) {

            final String value = (String) data.get("data");
            isSuccess = true;
            commandFinish(value);
            startAndCloseTimeOutThread(false);

        } else if ((int) data.get("type") == EventCenter.BLUETOOTH_ERROR) {

            activity.runOnUiThread(new Runnable() {

                public void run() {
                    closeDialog();
                    Toast.makeText(activity, activity.getString(R.string.ble_restart), Toast.LENGTH_LONG).show();
                    startAndCloseSendDataTimeOutThread(false);
                    startAndCloseTimeOutThread(false);
                }
            });

        } else if ((int) data.get("type") == EventCenter.CHECK_VIG_KEY) {

            closeDialog();

            CheckVig vig = (CheckVig) data.get("data");
            // check VIG has key.
            if (isVIGCheck = vig.isKeyInVig()) {
                startRentTime = dateParser.getMillise(vig.getRentStartOnUtc());
                description.setText(String.format(getString(R.string.start_rental_time), carFinish.getCarNo(), dateParser.getDateTime(vig
                        .getRentStartOnUtc())));
                // start timer.
                startTimeMeasuringService(startRentTime);
            } else {
                showCheckVIGDialog();
            }
        } else if ((int) data.get("type") == EventCenter.CANCEL_CAR_FINISH) {
            loadingDialogManager.dismiss();
            returnCarConnect.sendCancelErrorOrder(carFinish.getOrderId());
        } else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_ERROR_HAITEC) {
            loadingDialogManager.dismiss();
            ErrorMessage errorMessage = (ErrorMessage) data.get("data");
            if (errorMessage.getErrorCode() == -1) {
                Toast.makeText(activity, activity.getString(R.string.cancel_car_error), Toast.LENGTH_LONG).show();
            } else {
                String message = errorMessage.getErrorMessage() != null ? errorMessage.getErrorMessage() : activity.getString(R.string
                        .get_car_error_vig);
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    // ----------------------------------------------------
    @OnClick({R.id.charging, R.id.estimated_rent, R.id.update_mileage, R.id.return_car, R.id.unlock, R.id.locked, R.id.call})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.charging:
                goToCharging(carEnergyType.getCarEnergyType());
                break;
            case R.id.estimated_rent:
                Intent intent = new Intent();
                intent.putExtra("orderId", carFinish.getOrderId());
                intent.setClass(activity, EstimatedRentActivity.class);
                startActivity(intent);
                break;
            case R.id.update_mileage:
                rentConnect.updateMileage(carFinish.getOrderId());
                break;
            case R.id.return_car:
                returnCarConnect.getCheckSpecialReturnCar(carFinish.getOrderId());
                break;
            case R.id.unlock:
                if (isVIGCheck) {
                    //解鎖
                    goToCommand(CarControllerGatt.DOOR_UNLOCK);
                } else {
                    goToCheckVig();
                }
                break;
            case R.id.locked:
                if (isVIGCheck) {
                    //上鎖
                    goToCommand(CarControllerGatt.DOOR_LOCK);
                } else {
                    goToCheckVig();
                }
                break;
            case R.id.call:
                StartRentCarFragmentPermissionsDispatcher.startCallPhoneWithCheck(this);
                break;
        }
    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void startConnectBle(int commandType) {
        type = commandType;

        if (carControllerGatt.isConnect()) {
            isSuccess = false;

            if (CarVigType.isGeneralVIG(vigType)) {

                if (isSecurity) {
                    goSendCommandToSecurity(commandType);
                } else {
                    carControllerGatt.connectSecurity();
                }

            } else {
                setMobiletronCommand(commandType);
            }

        } else {
            bleScanUtil.scanBle(mDeviceAddress, this);
        }
    }

    // ----------------------------------------------------
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void startGetChargingStation() {
        Intent intent = new Intent();
        intent.setClass(activity, ChargingStationActivity.class);
        startActivity(intent);
    }

    // ----------------------------------------------------
    @NeedsPermission(Manifest.permission.CALL_PHONE)
    void startCallPhone() {
        getActivity().startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.map_mode_connection_phone))));
    }

    // ----------------------------------------------------
    //請求授權
    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocationPermission(final PermissionRequest request) {
        request.proceed();
    }

    // ----------------------------------------------------
    //請求授權(撥打電話)
    @OnShowRationale(Manifest.permission.CALL_PHONE)
    void showRationaleForCallPhonePermission(final PermissionRequest request) {
        request.proceed();
    }

    // ----------------------------------------------------
    // location 授權被拒
    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showDeniedForLocationPermission() {
        Toast.makeText(getActivity(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    // CALL PHONE 授權被拒
    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void showDeniedForCallPhonePermission() {
        Toast.makeText(getActivity(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        StartRentCarFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // ----------------------------------------------------

    /**
     * 藍芽連線成功，將連線資安
     */
    @Override
    public void connected() {

        Toast.makeText(activity, activity.getString(R.string.ble_connect), Toast.LENGTH_LONG).show();

        if (CarVigType.isGeneralVIG(vigType)) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    carControllerGatt.connectSecurity();
                }
            }, 3000);

        } else {
            closeDialog();
            setMobiletronCommand(type);
        }

    }

    // ----------------------------------------------------
    @Override
    public void disconnected() {

        closeDialog();

        carControllerGatt.disconnect();

        if (!CarVigType.isGeneralVIG(carFinish.getCarVigType())) {
            bleDataArray.clear();
            saveBleDataArray.clear();
            writeByte = null;
        } else {
            isSecurity = false;
        }

        if (!isShowDisconnectDialog) {
            dialogUtil.setMessage(R.string.connect_bluetooth).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetBle();
                    setCommandType();
                    goToCommand(type);
                    isShowDisconnectDialog = false;
                }

            }).setCancelButton(activity.getString(R.string.cancel), null).showDialog(false);

            isShowDisconnectDialog = true;
        }
    }

    // ----------------------------------------------------
    @Override
    public void OnNotSupportedBle() {
        Toast.makeText(activity, getString(R.string.not_supported_ble), Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void OnNotOpenBle() {
        Toast.makeText(activity, getString(R.string.not_open_ble), Toast.LENGTH_LONG).show();
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    // ----------------------------------------------------
    @Override
    public void OnStopScanBle(boolean isSearch) {

        if (isSearch) {
            loadingDialogManager.show(getString(R.string.connect_key));
            carControllerGatt.connect();
        } else {
            Toast.makeText(activity, getString(R.string.not_search), Toast.LENGTH_LONG).show();
            dialogUtil.setMessage(R.string.search).setConfirmButton(R.string.confirm, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToCommand(type);
                }
            }).setCancelButton(activity.getString(R.string.cancel), null).showDialog(false);
        }
    }

    // ----------------------------------------------------
    @Override
    public void OnBLESignalLow() {
        Toast.makeText(activity, getString(R.string.ble_signal), Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void OnNotOpenLocation() {
        Toast.makeText(activity, getString(R.string.not_open_location), Toast.LENGTH_LONG).show();
        goToOpenLocation();
    }

    // ----------------------------------------------------

    /**
     * 前往加油or充電
     *
     * @param type
     */
    private void goToCharging(@CarEnergyType.CarEnergy int type) {

        switch (type) {
            case ELECTRIC_CAR:
                StartRentCarFragmentPermissionsDispatcher.startGetChargingStationWithCheck(this);
                break;
            case CAR:
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=中油加油站,台塑加油站");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 根據車子類型更換加油的圖示以及文字
     *
     * @param type
     */
    private void changeChargingImage(@CarEnergyType.CarEnergy int type) {

        String title = "";
        int background = 0;

        switch (type) {
            case ELECTRIC_CAR:
                background = R.drawable.btn_charging;
                title = getString(R.string.charging);
                break;
            case CAR:
                background = R.drawable.btn_fuel_up;
                title = getString(R.string.fuel_up);
                break;
        }

        charging.setText(title);
        charging.setBackgroundResource(background);

    }

    // ----------------------------------------------------

    /**
     * 清除開關鎖按鈕的Select
     */
    private void cleanDoorSelect() {
        unlock.setSelected(false);
        locked.setSelected(false);
    }

    // ----------------------------------------------------
    private void startTimeMeasuringService(long millies) {
        serviceIntent = new Intent();
        serviceIntent.setClass(activity, TimeMeasuringService.class);
        serviceIntent.putExtra("time", millies);
        activity.startService(serviceIntent);
    }

    // ----------------------------------------------------
    private void stopTimeMeasuringService() {
        if (serviceIntent != null) {
            activity.stopService(serviceIntent);
        }
    }

    // ----------------------------------------------------

    /**
     * 建立資安連線
     */
    private void connectSecurityStatus(final int type) {

        activity.runOnUiThread(new Runnable() {

            public void run() {

                closeDialog();

                if (type != CommandCallback.CONNECTING_STATUS_SUCCESS) {

                    if (type == CommandCallback.CONNECTING_STATUS_TIMEOUT) {
                        //資安連線逾時
                        countTimeOut++;
                        repeatSendCommand(countTimeOut);
                    } else {
                        count++;
                        switch (type) {
                            //資安連線認證失敗(VIG尚未得到KEY)
                            case CommandCallback.CONNECTING_STATUS_AUTH_FAILED:
                                Toast.makeText(activity, getString(R.string.connect_error), Toast.LENGTH_LONG).show();
                                break;
                            //資安連線被中斷(藍芽連線不穩定)
                            case CommandCallback.CONNECTING_STATUS_TERMINATED:
                                Toast.makeText(activity, getString(R.string.connect_ble_error), Toast.LENGTH_LONG).show();
                                break;
                            //資安連線失敗
                            case CommandCallback.CONNECTING_STATUS_FAILED:
                                Toast.makeText(activity, getString(R.string.connect_error), Toast.LENGTH_LONG).show();
                                break;
                            default:
                                //do something break;
                        }
                    }
                    carControllerGatt.disconnect();
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 重複傳送指令
     *
     * @param count
     */
    private void repeatSendCommand(int count) {

        if (count == 3) {
            Toast.makeText(activity, getString(R.string.connect_timeout), Toast.LENGTH_LONG).show();
            countTimeOut = 0;
        } else {
            setCommandType();
            goToCommand(type);
        }
    }

    // ----------------------------------------------------

    /**
     * 完成傳送指令後的動作
     *
     * @param value
     */
    private void commandFinish(final String value) {

        activity.runOnUiThread(new Runnable() {

            public void run() {

                closeDialog();

                if (value.length() > 0) {

                    switch (value) {
                        case "ID:1,ack":
                            cleanDoorSelect();
                            unlock.setSelected(true);
                            Toast.makeText(activity, getString(R.string.door_unlock_success), Toast.LENGTH_LONG).show();
                            break;
                        case "ID:2,ack":
                            cleanDoorSelect();
                            locked.setSelected(true);
                            Toast.makeText(activity, getString(R.string.door_lock_success), Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Toast.makeText(activity, getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            break;
                    }

                    type = CarControllerGatt.NONE;

                } else {
                    Toast.makeText(activity, getString(R.string.try_again), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 前往下開解鎖的指令
     *
     * @param type
     */
    private void goToCommand(int type) {

        this.type = type;

        if (CarVigType.isGeneralVIG(vigType)) {

            if (count > 5 && !isSecurity) {
                count = 0;
                sendWakeMessage();
            } else {
                StartRentCarFragmentPermissionsDispatcher.startConnectBleWithCheck(StartRentCarFragment.this, type);
            }
        } else {
            StartRentCarFragmentPermissionsDispatcher.startConnectBleWithCheck(StartRentCarFragment.this, type);
        }

    }

    // ----------------------------------------------------

    /**
     * 將加密過的指令寫入Security
     *
     * @param commandType
     */
    private void goSendCommandToSecurity(int commandType) {
        startAndCloseTimeOutThread(true);
        carControllerGatt.writeCommandToSecurity(commandType);
    }

    // ----------------------------------------------------

    /**
     * 發起喚起簡訊
     */
    private void sendWakeMessage() {

        bleConnect.wakeToVIG(carFinish.getCarId(), new BaseConnect.IStatus() {
            @Override
            public void statusEvent() {
                loadingDialogManager.show(getString(R.string.create_key));
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setCommandType();
                        goToCommand(type);
                        closeDialog();
                    }
                }, 20000);
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 設定指令
     */
    private void setCommandType() {

        if (locked.isSelected()) {
            type = CarControllerGatt.DOOR_UNLOCK;
        } else {
            type = CarControllerGatt.DOOR_LOCK;
        }
    }

    // ----------------------------------------------------

    /**
     * 設定車王電的秘文資料
     */
    private void setMobiletronCommand(int commandType) {

        if (type == CarControllerGatt.DOOR_UNLOCK) {
            loadingDialogManager.show(activity.getString(R.string.send_command_unlock));
        } else {
            loadingDialogManager.show(activity.getString(R.string.send_command_lock));
        }

        try {

            saveBleDataArray = carControllerGatt.getMobiletronCommandData(commandType, sp.getAcctId());
            bleDataArray = saveBleDataArray;

        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (BadPaddingException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            loadingDialogManager.dismiss();
        }

    }


    // ----------------------------------------------------

    /**
     * 監測是否有TimeOut的問題
     */
    private class TimeOutThread extends Thread {

        public TimeOutThread() {
        }

        @Override
        public void run() {

            try {

                while (isLoopTime) {

                    if (CarVigType.isGeneralVIG(vigType)) {
                        sleep(10000);
                    } else {
                        sleep(5000);
                    }

                    if (timeOutThread != null && !isSuccess) {

                        commandFinish("");
                        startAndCloseTimeOutThread(false);

                        if (isLoopDataTime) {
                            startAndCloseSendDataTimeOutThread(false);
                        }
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 監測是否有TimeOut的問題
     */
    private class SendDataTimeOutThread extends Thread {

        public SendDataTimeOutThread() {
        }

        @Override
        public void run() {

            try {

                while (isLoopDataTime) {

                    sleep(5000);

                    if (sendDataTimeOutThread != null) {

                        activity.runOnUiThread(new Runnable() {

                            public void run() {

                                closeDialog();
                                startAndCloseSendDataTimeOutThread(false);
                            }
                        });
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 持續傳送BleData的資料
     */
    private class SendToBleQueue extends Thread {

        public SendToBleQueue() {

        }

        @Override
        public void run() {

            while (isLoop) {

                if (bleDataArray.size() > 0) {

                    try {

                        byte[] data = bleDataArray.peek();
                        if (data == null) {
                            sleep(5);
                            continue;
                        }

                        writeByte = data;
                        writeToBle(data);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 寫入資料到Ble
     *
     * @param bytes
     */
    private void writeToBle(final byte[] bytes) {

        if (carControllerGatt.isConnect()) {

            if (carControllerGatt.writeCommand(bytes) == CarControllerGatt.WRITE_STATUS_SUCCESS) {

                bleDataArray.remove(writeByte);

                if (bleDataArray.size() == 0) {
                    startAndCloseSendDataTimeOutThread(true);
                    startAndCloseTimeOutThread(true);
                }
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 開啟或關閉傳送車王封包後的TimeoutThread
     *
     * @param isStart
     */
    private void startAndCloseSendDataTimeOutThread(boolean isStart) {

        if (isStart) {
            sendDataTimeOutThread = new SendDataTimeOutThread();
            sendDataTimeOutThread.start();
            isLoopDataTime = true;
        } else {
            isLoopDataTime = false;
            sendDataTimeOutThread = null;
        }
    }

    // ----------------------------------------------------

    /**
     * 開啟或關閉 車王版、華創版傳送完指令後的Thread
     *
     * @param isStart
     */
    private void startAndCloseTimeOutThread(boolean isStart) {

        if (isStart) {
            timeOutThread = new TimeOutThread();
            timeOutThread.start();
            isLoopTime = true;
        } else {
            isLoopTime = false;
            timeOutThread = null;
        }
    }

    // ----------------------------------------------------

    /**
     * 前往檢查Vig
     */
    private void goToCheckVig() {
        loadingDialogManager.show(activity.getString(R.string.check_vig_key));
        bleConnect.checkHaitecVIGKey(carFinish.getOrderId());
    }

    // ----------------------------------------------------

    /**
     * if VigInKey is false show dialog.
     */
    private void showCheckVIGDialog() {

        AlertDialog.Builder vigDialog;

        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_check_vig_key, null);
        Button cancel, again;
        cancel = (Button) view.findViewById(R.id.cancel);
        again = (Button) view.findViewById(R.id.again);

        again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // try again.
                goToCheckVig();
                vigAD.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogUtil.setMessage(R.string.check_cancel_rent_car).setConfirmButton(R.string.yes, new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        vigAD.dismiss();
                        loadingDialogManager.show();
                        new SecurityUtil(activity).sendCommandToRentServer(SecurityUtil.CANCEL, CarVigType.getVIGType(carFinish.getCarVigType()),
                                carFinish.getCarId(), carFinish.getOrderId(), "", "");
                    }
                }).setCancelButton(R.string.no, null).showDialog(false);
            }
        });

        vigDialog = new AlertDialog.Builder(activity).setView(view);
        vigDialog.setCancelable(false);
        vigAD = vigDialog.show();
    }

    // ----------------------------------------------------

    /**
     * 重新連線藍芽
     */
    private void resetBle() {
        if (blueTooth.isOpen(false)) {
            blueTooth.turnOffBlueTooth();
            blueTooth.turnOnBlueTooth();
        }
    }

    // ----------------------------------------------------

    /**
     * 關閉LoadingDialog
     */
    private void closeDialog() {
        if (loadingDialogManager.dialog.isShowing()) {
            loadingDialogManager.dismiss();
        }
    }

    // ----------------------------------------------------

    /**
     * 前往開啟定位的頁面
     */
    private void goToOpenLocation() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, REQUEST_ENABLE_LOCATION);
    }

    // ----------------------------------------------------
}
