package tw.com.car_plus.carshare.rent.success.car_structure_type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by winni on 2017/5/15.
 * 車子引擎類型
 */

public class CarEnergyType {

    /**
     * 電動車
     */
    public static final int ELECTRIC_CAR = 1;
    /**
     * 燃油車
     */
    public static final int CAR = 2;

    //車子引擎類型
    @CarEnergyType.CarEnergy
    private static int carEnergyType = CarEnergyType.ELECTRIC_CAR;

    // ----------------------------------------------------
    public CarEnergyType() {

    }

    // ----------------------------------------------------

    /**
     * 車子引擎類型
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ELECTRIC_CAR, CAR})
    public @interface CarEnergy {
    }


    // ----------------------------------------------------

    /**
     * 設定車子引擎類型
     *
     * @param type
     * @return
     */
    public void setCarEnergyType(int type) {

        switch (type) {
            case ELECTRIC_CAR:
                carEnergyType = ELECTRIC_CAR;
                break;
            case CAR:
                carEnergyType = CAR;
                break;

        }
    }

    // ----------------------------------------------------

    /**
     * 取得車子引擎類型
     *
     * @return
     */
    @CarEnergy
    public int getCarEnergyType() {
        return carEnergyType;
    }


    // ----------------------------------------------------

}
