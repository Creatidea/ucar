package tw.com.car_plus.carshare.rent.success.car_structure_type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by winni on 2017/5/15.
 * 車子Vig類型
 */

public class CarVigType {

    /**
     * 一般型(華創版) General
     */
    public static final int HAITEC = 1;
    /**
     * 泛用型(車王電版) Generic
     */
    public static final int MOBILETRON = 2;


    //車子引擎類型
    @CarVigType.CarVig
    private static int carVigType = CarVigType.HAITEC;

    // ----------------------------------------------------
    public CarVigType() {

    }

    // ----------------------------------------------------

    /**
     * 車子Vig類型
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({HAITEC, MOBILETRON})
    public @interface CarVig {
    }


    // ----------------------------------------------------

    /**
     * 設定車子Vig類型
     *
     * @param type
     * @return
     */
    public void setCarVigType(int type) {

        switch (type) {
            case HAITEC:
                carVigType = HAITEC;
                break;
            case MOBILETRON:
                carVigType = MOBILETRON;
                break;

        }
    }

    // ----------------------------------------------------

    /**
     * 取得車子Vig類型
     *
     * @return
     */
    @CarVig
    public int getCarVigType() {
        return carVigType;
    }

    // ----------------------------------------------------

    /**
     * 判斷是否是一般型(華創 General)或泛用型(車王電 Generic)
     *
     * @param type VIG Type.
     */
    public static boolean isGeneralVIG(int type) {

        if (type == CarVigType.HAITEC) {
            return true;
        } else {
            return false;
        }
    }


    // ----------------------------------------------------

    /**
     * 取得是一般型(華創 General)或泛用型(車王電 Generic)
     *
     * @param type VIG Type.
     * @return General or Generic
     */
    @CarVigType.CarVig
    public static int getVIGType(int type) {

        if (isGeneralVIG(type)) {
            return CarVigType.HAITEC;
        } else {
            return CarVigType.MOBILETRON;
        }
    }


    // ----------------------------------------------------

}
