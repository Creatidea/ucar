package tw.com.car_plus.carshare.rent.success.charge;

import android.view.View;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

/**
 * Created by winni on 2017/7/26.
 * 充電站
 */

public class ChargingStationActivity extends CustomBaseActivity {

    @Override
    public void init() {
        setContentView(R.layout.activity_charging_station);
        ButterKnife.bind(this);

        setToolbar(R.drawable.ic_back, R.drawable.img_toolbar_car_info, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        goToFragment(R.id.container_frame_layout, new ChargingStationFragment());

    }

}
