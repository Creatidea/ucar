package tw.com.car_plus.carshare.rent.success.charge;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.Marker;
import com.neilchen.complextoolkit.util.map.MapSetting;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.rent.RentConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.rent.success.charge.adapter.ChargingStationAdapter;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStation;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStationDetail;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.DialogUtil;

/**
 * Created by winni on 2017/7/26.
 * 充電站
 */

public class ChargingStationFragment extends CustomBaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleMap.OnInfoWindowClickListener,
        LocationListener {

    private MapSetting mapSetting;
    private RentConnect rentConnect;
    private List<ChargingStation> chargingStations;
    private ChargingStationAdapter chargingStationAdapter;
    private Location userLocation;
    private HashMap<String, Integer> chargingStationData;
    private DialogUtil dialogUtil;
    private Marker marker;
    private ImageView toolbarImage;
    private boolean isFirst = true;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChargingStationActivity) {
            this.toolbarImage = ((ChargingStationActivity) context).toolbarImage;
        }
    }

    // ----------------------------------------------------
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MapsInitializer.initialize(getActivity());

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_charging_station, container, false);
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        ButterKnife.bind(this, view);
        init();

        return view;
    }

    // ----------------------------------------------------
    protected void init() {

        mapSetting = new MapSetting(this, R.id.map);
        rentConnect = new RentConnect(activity);
        chargingStationAdapter = new ChargingStationAdapter();
        chargingStationData = new HashMap<>();
        dialogUtil = new DialogUtil(activity);

        toolbarImage.setImageResource(R.drawable.img_toolbar_charge);
        rentConnect.getChargingStation();

        mapSetting.initApiClient(this);
        mapSetting.apiClient.connect();

    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.CHARGING_STATION) {
            chargingStations = (List<ChargingStation>) data.get("data");
            addMarkerOnMap();
            mapSetting.moveCamera(userLocation, 16, false);
            chargingStationAdapter.setData(chargingStations);
        }

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(ChargingStationDetail chargingStationDetail) {

        final ChargingStation chargingStation = chargingStations.get(chargingStationData.get(marker.getTitle()));

        dialogUtil.setMessage(String.format(getString(R.string.charging_stations_info), chargingStation.getTitle(), chargingStationDetail
                .getAvailableCount()))
                .setConfirmButton(R.string.navigation, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        startGoogleMapNavigation(chargingStation.getLat(), chargingStation.getLng());
                    }
                }).setCancelButton(R.string.cancel, null).showDialog(false);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mapSetting.mMap.setOnInfoWindowClickListener(this);
        mapSetting.startLocationUpdates(this, 1, 1);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mapSetting.mMap.setMyLocationEnabled(true);
            UiSettings uiSettings = mapSetting.mMap.getUiSettings();
            uiSettings.setMyLocationButtonEnabled(false);
        }
    }

    // ----------------------------------------------------
    @Override
    public void onConnectionSuspended(int i) {

    }

    // ----------------------------------------------------
    @Override
    public void onLocationChanged(Location location) {
        userLocation = location;
        if (isFirst) {
            mapSetting.moveCamera(location, 16);
            isFirst = false;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onInfoWindowClick(Marker marker) {
        this.marker = marker;
        rentConnect.getChargingStationDetail(chargingStations.get(chargingStationData.get(marker.getTitle())).getStationID());
    }

    // ----------------------------------------------------
    @OnClick(R.id.my_location)
    public void onViewClicked() {

        if (checkPermission()) {
            mapSetting.moveCamera(userLocation, false);
        }
    }

    // ----------------------------------------------------

    /**
     * 添加大頭釘
     */
    private void addMarkerOnMap() {

        mapSetting.mMap.clear();
        chargingStationData.clear();

        for (int count = 0; count < chargingStations.size(); count++) {
            ChargingStation chargingStation = chargingStations.get(count);
            mapSetting.addMarker(chargingStation);
            chargingStationData.put(chargingStation.getStationName(), count);
        }

    }

    // ----------------------------------------------------

    /**
     * 檢查是否同意權限以及是否有取得到使用者目前位置
     *
     * @return
     */
    private boolean checkPermission() {

        if (mapSetting != null) {
            if (userLocation != null) {
                return true;
            } else {
                Toast.makeText(activity, getString(R.string.need_location_info), Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            return false;
        }
    }

    // ----------------------------------------------------
}
