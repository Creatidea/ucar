package tw.com.car_plus.carshare.rent.success.charge.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.rent.success.charge.model.ChargingStation;
import tw.com.car_plus.carshare.util.BaseRecyclerViewAdapter;

/**
 * Created by winni on 2017/7/26.
 */

public class ChargingStationAdapter extends BaseRecyclerViewAdapter {

    private List<ChargingStation> chargingStations;

    public ChargingStationAdapter() {
        chargingStations = new ArrayList<>();
    }

    // ----------------------------------------------------
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_charging_station, parent, false));
    }

    // ----------------------------------------------------
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).title.setText(chargingStations.get(position).getStationName());
        }
    }

    // ----------------------------------------------------
    @Override
    public int getItemCount() {
        return chargingStations.size();
    }

    // ----------------------------------------------------
    public void setData(List<ChargingStation> chargingStations) {
        this.chargingStations = chargingStations;
        notifyDataSetChanged();
    }

    // ----------------------------------------------------
    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    // ----------------------------------------------------
}
