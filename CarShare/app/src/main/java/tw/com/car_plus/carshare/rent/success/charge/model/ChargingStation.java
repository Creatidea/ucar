package tw.com.car_plus.carshare.rent.success.charge.model;

import com.google.gson.annotations.SerializedName;
import com.neilchen.complextoolkit.util.map.PointInfo;

/**
 * Created by winni on 2017/7/26.
 */

public class ChargingStation extends PointInfo {


    /**
     * StationID : 0006
     * StationName : 裕隆大樓示範站
     * Longitude : 121.545245
     * Latitude : 24.977766
     */

    @SerializedName("StationID")
    private String StationID;
    @SerializedName("StationName")
    private String StationName;
    @SerializedName("Longitude")
    private String Lng;
    @SerializedName("Latitude")
    private String Lat;

    public String getStationID() {
        return StationID;
    }

    public void setStationID(String StationID) {
        this.StationID = StationID;
    }

    public String getStationName() {
        return StationName;
    }

    public void setStationName(String StationName) {
        this.StationName = StationName;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }
}
