package tw.com.car_plus.carshare.rent.success.charge.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by winni on 2017/7/26.
 */

public class ChargingStationDetail {


    /**
     * StationID : 0006
     * AvailableCount : 0
     * SocketStatusList : [{"SpotAbrv":"YECPW1-CM1303005","SpotStatus":"07"}]
     */

    @SerializedName("StationID")
    private String StationID;
    @SerializedName("AvailableCount")
    private String AvailableCount;
    @SerializedName("SocketStatusList")
    private List<SocketStatusListBean> SocketStatusList;

    public String getStationID() {
        return StationID;
    }

    public void setStationID(String StationID) {
        this.StationID = StationID;
    }

    public String getAvailableCount() {
        return AvailableCount;
    }

    public void setAvailableCount(String AvailableCount) {
        this.AvailableCount = AvailableCount;
    }

    public List<SocketStatusListBean> getSocketStatusList() {
        return SocketStatusList;
    }

    public void setSocketStatusList(List<SocketStatusListBean> SocketStatusList) {
        this.SocketStatusList = SocketStatusList;
    }

    public static class SocketStatusListBean {
        /**
         * SpotAbrv : YECPW1-CM1303005
         * SpotStatus : 07
         */

        @SerializedName("SpotAbrv")
        private String SpotAbrv;
        @SerializedName("SpotStatus")
        private String SpotStatus;

        public String getSpotAbrv() {
            return SpotAbrv;
        }

        public void setSpotAbrv(String SpotAbrv) {
            this.SpotAbrv = SpotAbrv;
        }

        public String getSpotStatus() {
            return SpotStatus;
        }

        public void setSpotStatus(String SpotStatus) {
            this.SpotStatus = SpotStatus;
        }
    }
}
