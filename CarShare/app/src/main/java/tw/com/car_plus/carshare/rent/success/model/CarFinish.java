package tw.com.car_plus.carshare.rent.success.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neil on 2017/3/20.
 */

public class CarFinish implements Parcelable {

    /**
     * CarId : 0
     * CarSeries :
     * CarNo :
     * CarPic :
     * OrderId : 0
     * RentStartOnUtc : 0001-01-01T00:00:00
     * CarEnergyType : 2
     * Status : false
     */

    @SerializedName("CarId")
    private int CarId;
    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("CarPic")
    private String CarPic;
    @SerializedName("CarEnergyType")
    private int CarEnergyType;
    @SerializedName("CarVigType")
    private int CarVigType;
    @SerializedName("OrderId")
    private int OrderId;
    @SerializedName("RentStartOnUtc")
    private String RentStartOnUtc;
    @SerializedName("VigId")
    private String VigId;
    @SerializedName("MacAddr")
    private String MacAddr;
    @SerializedName("Status")
    private boolean Status;

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int CarId) {
        this.CarId = CarId;
    }

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getCarPic() {
        return CarPic;
    }

    public void setCarPic(String CarPic) {
        this.CarPic = CarPic;
    }

    public int getCarEnergyType() {
        return CarEnergyType;
    }

    public void setCarEnergyType(int carEnergyType) {
        CarEnergyType = carEnergyType;
    }

    public int getCarVigType() {
        return CarVigType;
    }

    public void setCarVigType(int carVigType) {
        CarVigType = carVigType;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public String getRentStartOnUtc() {
        return RentStartOnUtc;
    }

    public void setRentStartOnUtc(String RentStartOnUtc) {
        this.RentStartOnUtc = RentStartOnUtc;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public String getVigId() {
        return VigId;
    }

    public void setVigId(String vigId) {
        VigId = vigId;
    }

    public String getMacAddr() {
        return MacAddr;
    }

    public void setMacAddr(String macAddr) {
        MacAddr = macAddr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CarId);
        dest.writeString(this.CarSeries);
        dest.writeString(this.CarNo);
        dest.writeString(this.CarPic);
        dest.writeInt(this.CarEnergyType);
        dest.writeInt(this.CarVigType);
        dest.writeInt(this.OrderId);
        dest.writeString(this.RentStartOnUtc);
        dest.writeString(this.VigId);
        dest.writeString(this.MacAddr);
        dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
    }

    public CarFinish() {
    }

    protected CarFinish(Parcel in) {
        this.CarId = in.readInt();
        this.CarSeries = in.readString();
        this.CarNo = in.readString();
        this.CarPic = in.readString();
        this.CarEnergyType = in.readInt();
        this.CarVigType = in.readInt();
        this.OrderId = in.readInt();
        this.RentStartOnUtc = in.readString();
        this.VigId = in.readString();
        this.MacAddr = in.readString();
        this.Status = in.readByte() != 0;
    }

    public static final Parcelable.Creator<CarFinish> CREATOR = new Parcelable.Creator<CarFinish>() {
        @Override
        public CarFinish createFromParcel(Parcel source) {
            return new CarFinish(source);
        }

        @Override
        public CarFinish[] newArray(int size) {
            return new CarFinish[size];
        }
    };
}
