package tw.com.car_plus.carshare.rent.success.model;

/**
 * Created by neil on 2017/12/21.
 * <p>
 * 檢查VIG是否有成功取得Key
 */

public class CheckVig {

    /**
     * KeyInVig : true
     * Status : true
     * RentStartOnUtc :
     */

    private boolean KeyInVig;
    private boolean Status;
    private String RentStartOnUtc;

    public CheckVig() {
        RentStartOnUtc = "0001-01-01T00:00:00";
        KeyInVig = false;
    }

    public boolean isKeyInVig() {
        return KeyInVig;
    }

    public void setKeyInVig(boolean KeyInVig) {
        this.KeyInVig = KeyInVig;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public String getRentStartOnUtc() {
        return RentStartOnUtc;
    }

    public void setRentStartOnUtc(String RentStartOnUtc) {
        this.RentStartOnUtc = RentStartOnUtc;
    }
}
