package tw.com.car_plus.carshare.rent.success.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by winni on 2017/3/20.
 */

public class EstimateAmount {


    /**
     * CarSeries : 堅達
     * CarNo : 3083-KK
     * RentStartOnUtc : 2017-05-19T03:09:39.43Z
     * RentEndOnUtc : 2017-05-19T03:28:47.2120749Z
     * CreditCards : [{"SerialNo":"e6ar+L/h160bFGxWG99ubw==","CardNo":"3560-****-****-2402","IsDefault":true}]
     * TotalAmount : 100
     * OdoMeter : 0
     * OdoMeterAmount : 0
     * TimeForHour : 0
     * TimeForMinute : 19
     * TimeAmount : 100
     */

    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("RentStartOnUtc")
    private String RentStartOnUtc;
    @SerializedName("RentEndOnUtc")
    private String RentEndOnUtc;
    @SerializedName("TotalAmount")
    private int TotalAmount;
    @SerializedName("OdoMeter")
    private int OdoMeter;
    @SerializedName("OdoMeterAmount")
    private int OdoMeterAmount;
    @SerializedName("ETagAmount")
    private int ETagAmount;
    @SerializedName("TimeForHour")
    private int TimeForHour;
    @SerializedName("TimeForMinute")
    private int TimeForMinute;
    @SerializedName("TimeAmount")
    private int TimeAmount;
    @SerializedName("CreditCards")
    private List<CreditCardsBean> CreditCards;
    @SerializedName("IsPromotion")
    private boolean IsPromotion;
    @SerializedName("PromotionTimeAmount")
    private int PromotionTimeAmount;
    @SerializedName("PromotionTotalAmount")
    private int PromotionTotalAmount;
    @SerializedName("PromotionInfoDetail")
    private PromotionInfoDetailBean PromotionInfoDetail;

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getRentStartOnUtc() {
        return RentStartOnUtc;
    }

    public void setRentStartOnUtc(String RentStartOnUtc) {
        this.RentStartOnUtc = RentStartOnUtc;
    }

    public String getRentEndOnUtc() {
        return RentEndOnUtc;
    }

    public void setRentEndOnUtc(String RentEndOnUtc) {
        this.RentEndOnUtc = RentEndOnUtc;
    }

    public int getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(int TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    public int getOdoMeter() {
        return OdoMeter;
    }

    public void setOdoMeter(int OdoMeter) {
        this.OdoMeter = OdoMeter;
    }

    public int getOdoMeterAmount() {
        return OdoMeterAmount;
    }

    public void setOdoMeterAmount(int OdoMeterAmount) {
        this.OdoMeterAmount = OdoMeterAmount;
    }

    public int getETagAmount() {
        return ETagAmount;
    }

    public void setETagAmount(int ETagAmount) {
        this.ETagAmount = ETagAmount;
    }

    public int getTimeForHour() {
        return TimeForHour;
    }

    public void setTimeForHour(int TimeForHour) {
        this.TimeForHour = TimeForHour;
    }

    public int getTimeForMinute() {
        return TimeForMinute;
    }

    public void setTimeForMinute(int TimeForMinute) {
        this.TimeForMinute = TimeForMinute;
    }

    public int getTimeAmount() {
        return TimeAmount;
    }

    public void setTimeAmount(int TimeAmount) {
        this.TimeAmount = TimeAmount;
    }

    public boolean isPromotion() {
        return IsPromotion;
    }

    public void setPromotion(boolean promotion) {
        IsPromotion = promotion;
    }

    public int getPromotionTimeAmount() {
        return PromotionTimeAmount;
    }

    public void setPromotionTimeAmount(int promotionTimeAmount) {
        PromotionTimeAmount = promotionTimeAmount;
    }

    public int getPromotionTotalAmount() {
        return PromotionTotalAmount;
    }

    public void setPromotionTotalAmount(int promotionTotalAmount) {
        PromotionTotalAmount = promotionTotalAmount;
    }

    public PromotionInfoDetailBean getPromotionInfoDetail() {
        return PromotionInfoDetail;
    }

    public void setPromotionInfoDetail(PromotionInfoDetailBean PromotionInfoDetail) {
        this.PromotionInfoDetail = PromotionInfoDetail;
    }

    public List<CreditCardsBean> getCreditCards() {
        return CreditCards;
    }

    public void setCreditCards(List<CreditCardsBean> CreditCards) {
        this.CreditCards = CreditCards;
    }

    public static class CreditCardsBean {
        /**
         * SerialNo : e6ar+L/h160bFGxWG99ubw==
         * CardNo : 3560-****-****-2402
         * IsDefault : true
         */
        @SerializedName("SerialNo")
        private String SerialNo;
        @SerializedName("CardNo")
        private String CardNo;
        @SerializedName("IsDefault")
        private boolean IsDefault;

        public String getSerialNo() {
            return SerialNo;
        }

        public void setSerialNo(String SerialNo) {
            this.SerialNo = SerialNo;
        }

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String CardNo) {
            this.CardNo = CardNo;
        }

        public boolean isIsDefault() {
            return IsDefault;
        }

        public void setIsDefault(boolean IsDefault) {
            this.IsDefault = IsDefault;
        }
    }

    public static class PromotionInfoDetailBean {
        /**
         * Title : 測試1
         * Info : testetstastewsgf
         * StartTime :
         * EndTime :
         */

        @SerializedName("Title")
        private String Title;
        @SerializedName("Info")
        private String Info;
        @SerializedName("StartTime")
        private String StartTime;
        @SerializedName("EndTime")
        private String EndTime;

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getInfo() {
            return Info;
        }

        public void setInfo(String Info) {
            this.Info = Info;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }
    }
}
