package tw.com.car_plus.carshare.rent.success.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by winni on 2017/6/5.
 */

public class Mileage {


    /**
     * OdoMeter : 0
     * RentStartOnUtc : 2017-06-05T10:28:08.02+08:00
     * Status : true
     */

    @SerializedName("OdoMeter")
    private int OdoMeter;
    @SerializedName("RentStartOnUtc")
    private String RentStartOnUtc;
    @SerializedName("Status")
    private boolean Status;

    public int getOdoMeter() {
        return OdoMeter;
    }

    public void setOdoMeter(int OdoMeter) {
        this.OdoMeter = OdoMeter;
    }

    public String getRentStartOnUtc() {
        return RentStartOnUtc;
    }

    public void setRentStartOnUtc(String RentStartOnUtc) {
        this.RentStartOnUtc = RentStartOnUtc;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
