package tw.com.car_plus.carshare.rent.success.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType.CarVig;

/**
 * Created by neil on 2017/3/17.
 */

public class Order implements Parcelable {

    /**
     * OrderId : 4860
     * GetCarOnUtc : 2017-03-17T08:02:25.6351723Z
     * GetCarExpireOnUtc : 2017-03-17T08:12:25.6351723Z
     * IsExtenPreorder : false
     * CarBrand : 納智捷(Luxgen)(國產)
     * CarSeries : S3
     * CarNo : RBL-1291
     * CarPic : /FileDownload/GetPic?fileName=2d3853773ffa4defa17200e07afcfadf.png&uploadType=3
     * CarId : 29
     * Status : true
     * Errors : []
     */

    @SerializedName("OrderId")
    private int OrderId;
    @SerializedName("GetCarOnUtc")
    private String GetCarOnUtc;
    @SerializedName("GetCarExpireOnUtc")
    private String GetCarExpireOnUtc;
    @SerializedName("IsExtenPreorder")
    private boolean IsExtenPreorder;
    @SerializedName("CarBrand")
    private String CarBrand;
    @SerializedName("CarSeries")
    private String CarSeries;
    @SerializedName("CarNo")
    private String CarNo;
    @SerializedName("CarPic")
    private String CarPic;
    @SerializedName("CarId")
    private int CarId;
    @SerializedName("CarEnergyType")
    private int CarEnergyType;
    @SerializedName("Status")
    private boolean Status;
    @SerializedName("CarVigType")
    private int CarVigType;

    public int getCarVigType() {
        return CarVigType;
    }

    public void setCarVigType(int carVigType) {
        CarVigType = carVigType;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public String getGetCarOnUtc() {
        return GetCarOnUtc;
    }

    public void setGetCarOnUtc(String GetCarOnUtc) {
        this.GetCarOnUtc = GetCarOnUtc;
    }

    public String getGetCarExpireOnUtc() {
        return GetCarExpireOnUtc;
    }

    public void setGetCarExpireOnUtc(String GetCarExpireOnUtc) {
        this.GetCarExpireOnUtc = GetCarExpireOnUtc;
    }

    public boolean isIsExtenPreorder() {
        return IsExtenPreorder;
    }

    public void setIsExtenPreorder(boolean IsExtenPreorder) {
        this.IsExtenPreorder = IsExtenPreorder;
    }

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String CarBrand) {
        this.CarBrand = CarBrand;
    }

    public String getCarSeries() {
        return CarSeries;
    }

    public void setCarSeries(String CarSeries) {
        this.CarSeries = CarSeries;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String CarNo) {
        this.CarNo = CarNo;
    }

    public String getCarPic() {
        return CarPic;
    }

    public void setCarPic(String CarPic) {
        this.CarPic = CarPic;
    }

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int CarId) {
        this.CarId = CarId;
    }

    public int getCarEnergyType() {
        return CarEnergyType;
    }

    public void setCarEnergyType(int carEnergyType) {
        CarEnergyType = carEnergyType;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public Order() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.OrderId);
        dest.writeString(this.GetCarOnUtc);
        dest.writeString(this.GetCarExpireOnUtc);
        dest.writeByte(this.IsExtenPreorder ? (byte) 1 : (byte) 0);
        dest.writeString(this.CarBrand);
        dest.writeString(this.CarSeries);
        dest.writeString(this.CarNo);
        dest.writeString(this.CarPic);
        dest.writeInt(this.CarId);
        dest.writeInt(this.CarEnergyType);
        dest.writeByte(this.Status ? (byte) 1 : (byte) 0);
        dest.writeInt(this.CarVigType);
    }

    protected Order(Parcel in) {
        this.OrderId = in.readInt();
        this.GetCarOnUtc = in.readString();
        this.GetCarExpireOnUtc = in.readString();
        this.IsExtenPreorder = in.readByte() != 0;
        this.CarBrand = in.readString();
        this.CarSeries = in.readString();
        this.CarNo = in.readString();
        this.CarPic = in.readString();
        this.CarId = in.readInt();
        this.CarEnergyType = in.readInt();
        this.Status = in.readByte() != 0;
        this.CarVigType = in.readInt();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
