package tw.com.car_plus.carshare.rent.success.model;


/**
 * Created by winni on 2017/3/17.
 */

public class Postpone {

    /**
     * ServerUtcNow : 0001-01-01T00:00:00
     * ExpireOnUtc : 0001-01-01T00:00:00
     * Status : false
     * Errors : [{"ErrorCode":-99,"ErrorMsg":"可延長時限已超過，無法延長。"}]
     */

    private String ServerUtcNow;
    private String ExpireOnUtc;
    private boolean Status;

    public String getServerUtcNow() {
        return ServerUtcNow;
    }

    public void setServerUtcNow(String ServerUtcNow) {
        this.ServerUtcNow = ServerUtcNow;
    }

    public String getExpireOnUtc() {
        return ExpireOnUtc;
    }

    public void setExpireOnUtc(String ExpireOnUtc) {
        this.ExpireOnUtc = ExpireOnUtc;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

}
