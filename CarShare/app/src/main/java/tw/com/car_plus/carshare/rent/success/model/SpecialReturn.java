package tw.com.car_plus.carshare.rent.success.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by winni on 2017/11/14.
 */

public class SpecialReturn {


    /**
     * IsSpecialReturn : true
     * ParkinglotId : 7
     * Status : true
     */

    @SerializedName("IsSpecialReturn")
    private boolean IsSpecialReturn;
    @SerializedName("ParkinglotId")
    private int ParkinglotId;
    @SerializedName("Status")
    private boolean Status;

    public boolean isIsSpecialReturn() {
        return IsSpecialReturn;
    }

    public void setIsSpecialReturn(boolean IsSpecialReturn) {
        this.IsSpecialReturn = IsSpecialReturn;
    }

    public int getParkinglotId() {
        return ParkinglotId;
    }

    public void setParkinglotId(int ParkinglotId) {
        this.ParkinglotId = ParkinglotId;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
