package tw.com.car_plus.carshare.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.UUID;

import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.util.command.CarControllerGatt;

import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;

/**
 * Created by winni on 2017/7/10.
 * VIG Service
 */

public class BluetoothLeService extends Service {

    private static final String TAG = "BluetoothLeService";

    public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID HEART_RATE_MEASUREMENT = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_HEART_RATE_MEASUREMENT = HEART_RATE_MEASUREMENT;

    public static final String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public static final String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
    public static final String DEVICE_DOES_NOT_SUPPORT_UART = "com.example.bluetooth.le.DEVICE_DOES_NOT_SUPPORT_UART";

    public static final UUID SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID WRITE_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID READ_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private final IBinder mBinder = new LocalBinder();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    @CarVigType.CarVig
    private int vigType;

    // ----------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    // ----------------------------------------------------
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // ----------------------------------------------------
    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    // ----------------------------------------------------

    /**
     * 初始化local Bluetooth adapter
     *
     * @return 回傳是否初始化成功
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.VIG_READ) {
            readCustomCharacteristic();
        } else if ((int) data.get("type") == EventCenter.VIG_WRITE) {
            byte[] value = (byte[]) data.get("data");
            if (writeCustomCharacteristic(value) != CarControllerGatt.WRITE_STATUS_SUCCESS) {
                writeCustomCharacteristic(value);
            }
        }
    }

    // ----------------------------------------------------

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address, @CarVigType.CarVig int vigType) {

        this.vigType = vigType;

        if (mBluetoothAdapter == null || address == null) {
            Log.e(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        // TODO: 2018/7/3 當曾經連線過的BLE直接再次連線(mBluetoothGatt.connect()連線會等待過久，因此將此段判斷隱藏)
        /*
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {

            Log.e(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }*/

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

        if (device == null) {
            Log.e(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Little hack with reflect to use the connect gatt with defined transport in Lollipop
            Method connectGattMethod = null;

            try {
                connectGattMethod = device.getClass().getMethod("connectGatt", Context.class, boolean.class, BluetoothGattCallback.class, int.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

            try {
                mBluetoothGatt = (BluetoothGatt) connectGattMethod.invoke(device, BluetoothLeService.this, false, mGattCallback, TRANSPORT_LE);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } else {
            mBluetoothGatt = device.connectGatt(BluetoothLeService.this, false, mGattCallback);
        }

        Log.e(TAG, "Trying to create a new connection.");

        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;

        return true;
    }

    // ----------------------------------------------------

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.e(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    // ----------------------------------------------------

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {

        if (mBluetoothGatt == null) {
            return;
        }

        mBluetoothDeviceAddress = null;
        disconnect();
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    // -------------------------------------------------------------------

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth
     * .BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    // -------------------------------------------------------------------
    public void readCustomCharacteristic() {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "read > BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "read > Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(READ_UUID);
        if (!mBluetoothGatt.readCharacteristic(mReadCharacteristic)) {
            Log.e(TAG, "read > Failed to read characteristic");
        }

        mBluetoothGatt.setCharacteristicNotification(mReadCharacteristic, true);
        BluetoothGattDescriptor descriptor = mReadCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);

    }

    // -------------------------------------------------------------------
    public int writeCustomCharacteristic(byte[] value) {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.e(TAG, "write > BluetoothAdapter not initialized");
            EventCenter.getInstance().sendWriteVIGStatus(CarControllerGatt.WRITE_STATUS_ERROR);
            return CarControllerGatt.WRITE_STATUS_ERROR;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(SERVICE_UUID);
        if (mCustomService == null) {
            Log.e(TAG, "write > Custom BLE Service not found");
            EventCenter.getInstance().sendWriteVIGStatus(CarControllerGatt.WRITE_STATUS_SERVER_ERROR);
            return CarControllerGatt.WRITE_STATUS_SERVER_ERROR;
        }

        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(WRITE_UUID);
        mWriteCharacteristic.setValue(value);

        if (!mBluetoothGatt.writeCharacteristic(mWriteCharacteristic)) {
            Log.e(TAG, "write > Failed to write characteristic");
            EventCenter.getInstance().sendWriteVIGStatus(CarControllerGatt.WRITE_STATUS_ERROR);
            return CarControllerGatt.WRITE_STATUS_ERROR;
        } else {
            Log.e(TAG, "write > send data is success.");
            EventCenter.getInstance().sendWriteVIGStatus(CarControllerGatt.WRITE_STATUS_SUCCESS);
            return CarControllerGatt.WRITE_STATUS_SUCCESS;
        }
    }

    // ----------------------------------------------------
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    // -------------------------------------------------------------------
    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                // TODO: 2017/7/19 這裡會有因為os系統升級讓原本沒支援BLE改為支援因此需打開相關資料
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    // -------------------------------------------------------------------
    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {

        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic
        // .heart_rate_measurement.xml
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {

            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d(TAG, "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d(TAG, "Heart rate format UINT8.");
            }

            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));

        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();

            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));

                Log.e(TAG, stringBuilder.toString() + "<<<<<<");

                // TODO: 2017/7/11 這裡為回傳Read的資料
                if (vigType == CarVigType.HAITEC) {
                    intent.putExtra(EXTRA_DATA, new String(data) + "\n" + getStringData(data));
                } else {
                    intent.putExtra(EXTRA_DATA, new String(data));
                }
            }
        }

        sendBroadcast(intent);
    }

    // -------------------------------------------------------------------
    private String getStringData(byte[] txValue) {

        String text = "";

        try {

            boolean canTranslateToText = true;

            if (txValue.length != 16) {

                for (int i = 0; i < txValue.length; i++) {
                    int txValueInt = (txValue[i]);
                    txValueInt = txValueInt & 255;
                    if (txValueInt < 32 || txValueInt > 126) {
                        canTranslateToText = false;
                        break;
                    }
                }
            } else {
                canTranslateToText = false;
            }

            byte[] receivedData = new byte[txValue.length];
            if (!canTranslateToText) {
                text = "(binary2):";
                for (int i = 0; i < txValue.length; i++) {
                    receivedData[i] = txValue[i];
                }
                for (int i = 0; i < receivedData.length; i++) {
                    int txValueToInteger = (int) receivedData[i];
                    txValueToInteger = txValueToInteger & 255;
                    text = text + Integer.toHexString(txValueToInteger);
                    if (i < receivedData.length - 1) {
                        text = text + ",";
                    }
                }
            } else {
                text = new String(txValue, "8859_1");
            }

            Log.e(TAG, "READ DATA:" + text);

            if (!canTranslateToText) {
                EventCenter.getInstance().sendBLESecurityWrite(receivedData);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return text;
    }

    // -------------------------------------------------------------------

    /**
     * Enables or disables notification on a give characteristic.
     */
    public void setCharacteristicNotification() {

        BluetoothGattService RxService = mBluetoothGatt.getService(SERVICE_UUID);

        if (RxService == null) {
            Log.e(TAG, "Rx service not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        BluetoothGattCharacteristic TxChar = RxService.getCharacteristic(READ_UUID);
        if (TxChar == null) {
            Log.e(TAG, "Tx charateristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(TxChar, true);

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

}
