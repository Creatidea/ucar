package tw.com.car_plus.carshare.service;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.service.model.Time;

/**
 * Created by neil on 2017/3/18.
 */

public class CountDownService extends Service {

    // ----------------------------------------------------
    private final int MAX_TIME = 60 * 10 * 1000;

    private CountDownTimer timer;
    private StringBuffer minuteBuffer;
    private StringBuffer secBuffer;
    private Time time;

    // ----------------------------------------------------
    public CountDownService() {

        time = new Time();
        minuteBuffer = new StringBuffer();
        secBuffer = new StringBuffer();
    }

    // ----------------------------------------------------
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            getCountDownTime(intent.getLongExtra("time", 1000));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    // ----------------------------------------------------
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // ----------------------------------------------------
    private void getCountDownTime(final long maxTime) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

        long currentTime = formatter.parse(formatter.format(new Date(System.currentTimeMillis()))).getTime();
        long totalTime = maxTime - currentTime;

        time.setLongTime(totalTime);

        if (totalTime < 0) {
            time.setFinish(true);
            time.setMessage(getString(R.string.over_time));
            EventCenter.getInstance().sendCountDownTime(time);
            return;
        }

        if (totalTime > MAX_TIME) {
            startCountDown(MAX_TIME);
        } else {
            startCountDown(totalTime);
        }

    }

    // ----------------------------------------------------
    private void startCountDown(long countDownTime) {

        timer = new CountDownTimer(countDownTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                EventCenter.getInstance().sendCountDownTime(getTime(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                time.setFinish(true);
                time.setMessage(getString(R.string.over_time));
                EventCenter.getInstance().sendCountDownTime(time);
            }
        }.start();
    }

    // ----------------------------------------------------
    private Time getTime(long millisUntilFinished) {

        long minute = (millisUntilFinished / 1000) / 60;
        long sec = (millisUntilFinished / 1000) % 60;

        //minute
        if (minute < 10) {
            minuteBuffer.append(0).append(minute);
        } else {
            minuteBuffer.append(minute);
        }

        //sec
        if (sec < 10) {
            secBuffer.append(0).append(sec);
        } else {
            secBuffer.append(sec);
        }

        time.setMinute(minuteBuffer.toString());
        time.setSec(secBuffer.toString());

        minuteBuffer.setLength(0);
        secBuffer.setLength(0);

        return time;
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (timer != null) {
            timer.cancel();
        }
    }

    // ----------------------------------------------------

}
