package tw.com.car_plus.carshare.service;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.List;

import tw.com.car_plus.carshare.FirstOperatingActivity;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.notification.NotificationActivity;
import tw.com.car_plus.carshare.notification.model.PushData;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;


/**
 * Created by jhen on 2016/11/7.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
        sendNotification(intent.getExtras());
    }

    //--------------------------------------------------

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param bundle FCM Message data.
     */
    private void sendNotification(Bundle bundle) {

        PushData pushData = new PushData();
        pushData.setTitle(bundle.getString("title"));
        pushData.setBody(bundle.getString("body"));

        String latitude = bundle.getString("Latitude");
        String longitude = bundle.getString("Longitude");

        if (latitude != null && latitude.length() > 0 && longitude != null && longitude.length() > 0) {
            pushData.setLatitude(Double.parseDouble(latitude));
            pushData.setLongitude(Double.parseDouble(longitude));
        }

        pushData.setPushType(bundle.getInt("PushType"));

        if (isAppOnForeground()) {

            SharedPreferenceUtil sp = new SharedPreferenceUtil(getApplicationContext());

            User user = sp.getUserData();

            if (user != null) {

                if (user.getOperationStatus() == UserStatus.RENT_NONE) {
                    goToNotificationActivity(pushData);
                }

            } else {
                goToNotificationActivity(pushData);
            }

        } else {

            Intent intent = new Intent();
            intent.setClass(this, FirstOperatingActivity.class);
            Bundle bundle1 = new Bundle();
            bundle1.putParcelable("pushData", pushData);
            intent.putExtras(bundle1);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(bundle.getString("title"))
                    .setContentText(bundle.getString("body"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }

    }

    //--------------------------------------------------

    /**
     * 前往顯示通知Dialog的頁面
     *
     * @param pushData
     */
    private void goToNotificationActivity(PushData pushData) {
        Intent intent = new Intent();
        intent.setClass(this, NotificationActivity.class);
        Bundle bundle1 = new Bundle();
        bundle1.putParcelable("pushData", pushData);
        intent.putExtras(bundle1);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    //--------------------------------------------------

    /**
     * 判斷是否開啟APP
     *
     * @return
     */
    private boolean isAppOnForeground() {

        List<ActivityManager.RunningTaskInfo> tasksInfo = ((ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1);
        if (tasksInfo.size() > 0) {

            if (getPackageName().equals(tasksInfo.get(0).topActivity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

}
