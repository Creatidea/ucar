package tw.com.car_plus.carshare.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import tw.com.car_plus.carshare.connect.ble.BleConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.command.CarControllerGatt;
import tw.com.haitec.ae.project.carsharing.ChannelsStatusListener;
import tw.com.haitec.ae.project.carsharing.CommandCallback;
import tw.com.haitec.ae.project.carsharing.CommunicationAgent;
import tw.com.haitec.ae.project.carsharing.DataChannel;

import static java.lang.Thread.sleep;

/**
 * Created by jhen on 2017/7/12.
 * 資安的Service
 */

public class SecurityService extends Service {

    private final int BLE_PACKAGE_LENGTH = 16;
    //寫入Security為註冊
    private final int WRITE_SECURITY_REGISTER = 0;
    //寫入Security為取車
    private final int WRITE_SECURITY_GET_CAR = 1;
    //寫入Security為還車
    private final int WRITE_SECURITY_RETURN_CAR = 2;
    //寫Security為取消租單
    private final int WRITE_SECURITY_CANCEL_CAR = 3;

    private CommunicationAgent ca;
    private DataChannel mainDataChannel = null;
    private DataChannel bleDataChannel = null;
    private DataChannel securityServerDataChannel = null;
    private RegisterCallback securityServerRc = new RegisterCallback();
    private ConnectCallback securityServerCc = new ConnectCallback();
    private DisconnectCallback disconnectCallback = new DisconnectCallback();
    private OnDataListener onDataListener = new OnDataListener();
    private SendToBleQueue sendToBleQueue = null;
    private SharedPreferenceUtil sp;
    private BleConnect bleConnect;
    private ArrayBlockingQueue<byte[]> bleDataArray = new ArrayBlockingQueue<>(100);
    private int securityType = -1;
    private String acctId;
    private String vigId = "";
    private boolean isInitializeCA = false;
    private byte[] writeByte;


    // ----------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        bleConnect = new BleConnect(getApplicationContext());
        sp = new SharedPreferenceUtil(getApplicationContext());
    }

    // ----------------------------------------------------
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendToBleQueue = new SendToBleQueue();
        sendToBleQueue.start();

        EventCenter.getInstance().sendServiceCreated();
        return super.onStartCommand(intent, flags, startId);
    }

    // ----------------------------------------------------

    /**
     * 初始化CA
     *
     * @param acctId
     */
    private void initializeCA(String acctId) {

        this.acctId = acctId;

        if (!isInitializeCA) {
            ca = new CommunicationAgent(getApplicationContext(), BLE_PACKAGE_LENGTH, acctId, onDataListener);
            init();
        } else {
            EventCenter.getInstance().sendInitializeCAFinish();
        }
    }

    // ----------------------------------------------------
    private void init() {

        if (ca.getCommunicationAgentState() != CommunicationAgent.CA_STATUS_READY_FOR_CONNECT) {

            if (ca.getInitStatus() == CommunicationAgent.INIT_STATUS_SQLITE_ERROR) {
                //please check if license of sqlite is ready
                //  TODO: 2017/7/12 有機會資料庫無法使用所以需`要做相關的Toast之類的提示給使用者

            } else if (ca.getInitStatus() == CommunicationAgent.INIT_STATUS_FAILED) {
                //unknown error!!!
            }

        } else {
            mainDataChannel = ca.getMainDataChannel();
            bleDataChannel = ca.getBLEDataChannel();
            securityServerDataChannel = ca.getSecurityServerDataChannel();
            isInitializeCA = true;
            EventCenter.getInstance().sendInitializeCAFinish();
        }
    }

    // ----------------------------------------------------
    public CommunicationAgent getCA() {
        return ca;
    }

    // ----------------------------------------------------
    public DataChannel getMainDataChannel() {
        return mainDataChannel;
    }

    // ----------------------------------------------------
    public DataChannel getSecurityServerDataChannel() {
        return securityServerDataChannel;
    }

    // ----------------------------------------------------
    public DataChannel getBleDataChannel() {
        return bleDataChannel;
    }

    // ----------------------------------------------------

    /**
     * 重新設定CA
     */
    public void reSetCommunicationAgent() {
        ca.reset();
    }

    // ----------------------------------------------------

    /**
     * 將資安連線中斷
     */
    public void disconnectFromVehicle() {
        ca.disconnectFromVehicle(vigId, disconnectCallback);
    }

    // ----------------------------------------------------

    /**
     * 註冊SecurityServer
     */
    public void registerSecurityServer() {
        securityType = WRITE_SECURITY_REGISTER;
        ca.registerToSecurityServer(securityServerRc);
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.APP_DATA) {

            String appData[] = ((String) data.get("data")).split(",");

            if (appData[0].equals("SUCCESS") || appData[0].equals("ERROR")) {

                switch (securityType) {
                    case WRITE_SECURITY_REGISTER:
                        sp.setRegisterSecurityServer(true);
                        EventCenter.getInstance().sendRegisterSecurityFinish(appData[0].equals("SUCCESS"));
                        break;
                    case WRITE_SECURITY_GET_CAR:
                        EventCenter.getInstance().sendGetCar(appData[0].equals("SUCCESS"));
                        break;
                    case WRITE_SECURITY_RETURN_CAR:
                        EventCenter.getInstance().sendReturnCarFinish(appData[0].equals("SUCCESS"));
                        break;
                    case WRITE_SECURITY_CANCEL_CAR:
                        EventCenter.getInstance().sendCancelCarFinish(appData[0].equals("SUCCESS"));
                        break;

                }

            } else {

                if (securityServerDataChannel != null) {
                    securityServerDataChannel.write(appData[0].getBytes(StandardCharsets.UTF_8));
                    if (appData.length > 1) {
                        switch (securityType) {
                            case WRITE_SECURITY_RETURN_CAR:
                                EventCenter.getInstance().sendReturnCarFinish(appData[1].equals("SUCCESS"));
                                break;
                            case WRITE_SECURITY_CANCEL_CAR:
                                EventCenter.getInstance().sendCancelCarFinish(appData[1].equals("SUCCESS"));
                                break;
                        }

                    }
                }
            }
        } else if ((int) data.get("type") == EventCenter.APP_DATA_ERROR) {
            String strData = (String) data.get("data");
            bleConnect.sendAppData(strData, acctId);
        }
        //初始化CA
        else if ((int) data.get("type") == EventCenter.INIT_CA) {
            String acctId = (String) data.get("data");
            initializeCA(acctId);
        }
        //通知RentServer幫我通知ikey去租車以及還車動作
        else if ((int) data.get("type") == EventCenter.COMMAND_TO_SERVER_HAITEC) {
            securityType = (int) data.get("data");
        }
        //重新設定CA
        else if ((int) data.get("type") == EventCenter.SECURITY_RESET) {
            reSetCommunicationAgent();
        }
        //建立資安的連線
        else if ((int) data.get("type") == EventCenter.SECURITY_CONNECT) {
            vigId = (String) data.get("data");
            ca.connectToVehicle(vigId, new ConnectCallback());
        }
        //中斷資安的連線
        else if ((int) data.get("type") == EventCenter.SECURITY_DISCONNECT) {
            disconnectFromVehicle();
        }
        //註冊SecurityServer
        else if ((int) data.get("type") == EventCenter.REGISTER_SECURITY) {
            registerSecurityServer();
        }
        //將指令寫入CA_Main
        else if ((int) data.get("type") == EventCenter.MAIN_SECURITY_WRITE) {
            byte[] value = (byte[]) data.get("data");
            ca.getMainDataChannel().write(value);
        }
        //將指令寫入CA_BLE
        else if ((int) data.get("type") == EventCenter.BLE_SECURITY_WRITE) {
            byte[] value = (byte[]) data.get("data");
            ca.getBLEDataChannel().write(value);
        }
        //根據寫入狀態正確才將資料移除
        else if ((int) data.get("type") == EventCenter.VIG_WRITE_STATUS) {
            int status = (int) data.get("data");
            if (status == 1) {
                try {
                    bleDataArray.remove(writeByte);
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } else if (status == CarControllerGatt.WRITE_STATUS_SERVER_ERROR) {
                EventCenter.getInstance().sendBluetoothError(status);
                bleDataArray.clear();
            }
        }
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    // ----------------------------------------------------
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // ----------------------------------------------------
    public class OnDataListener implements ChannelsStatusListener {

        @Override
        public void securityServerDataChannelHasData() {

            try {

                while (securityServerDataChannel.hasData()) {
                    byte[] data = securityServerDataChannel.take();
                    final String str = new String(data, StandardCharsets.UTF_8);
                    Log.e("SecurityService", str);
                    bleConnect.sendAppData(str, acctId);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void bleDataChannelHasData() {

            try {

                while (bleDataChannel.hasData()) {

                    byte[] data = bleDataChannel.take();
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data)
                        stringBuilder.append(String.format("%02X ", byteChar));
                    Log.e("SecurityService", "bleDataChannel:" + stringBuilder.toString() + ">>>>>");
                    bleDataArray.offer(data);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void mainDataChannelHasData() {

            try {

                while (mainDataChannel.hasData()) {
                    byte[] data = mainDataChannel.take();
                    final String str = new String(data, StandardCharsets.UTF_8);
                    Log.e("SecurityService", "mainDataChannel:" + str);
                    EventCenter.getInstance().sendMainSecurityRead(str);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    // ----------------------------------------------------

    /**
     * 註冊SecurityServer的狀態
     */
    public class RegisterCallback implements CommandCallback {

        public RegisterCallback() {
        }

        @Override
        public void commandComplete(int commandResultCode, HashMap options) {

            boolean isSuccess;

            switch (commandResultCode) {
                case CommandCallback.REGISTER_STATUS_SUCCESS:
                    //do something
                    isSuccess = true;
                    break;
                case CommandCallback.REGISTER_STATUS_TIMEOUT:
                    //do something
                    isSuccess = false;
                    break;
                case CommandCallback.REGISTER_STATUS_ALREADY_REGISTERED:
                    isSuccess = true;
                    //do something
                    break;
                case CommandCallback.REGISTER_STATUS_FAILED:
                    //do something
                    isSuccess = false;
                    break;
                default:
                    //do something
                    isSuccess = false;
                    break;
            }

            sp.setRegisterSecurityServer(isSuccess);
            EventCenter.getInstance().sendRegisterSecurityFinish(isSuccess);
        }
    }

    // ----------------------------------------------------

    /**
     * 建立資安連線的狀態
     */
    public class ConnectCallback implements CommandCallback {

        public ConnectCallback() {
        }

        @Override
        public void commandComplete(int commandResultCode, HashMap options) {
            Log.e("SecurityService", "ConnectCallback:" + commandResultCode);
            EventCenter.getInstance().sendSecurityConnectStatus(commandResultCode);
            if (commandResultCode != CommandCallback.CONNECTING_STATUS_SUCCESS) {
                bleDataArray.clear();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 建立中段資安連線的狀態
     */
    public class DisconnectCallback implements CommandCallback {

        public DisconnectCallback() {
        }

        @Override
        public void commandComplete(int commandResultCode, HashMap options) {

            Log.e("SecurityService", "DisconnectCallback:" + commandResultCode);

            switch (commandResultCode) {
                case CommandCallback.DISCONNECTING_STATUS_TIMEOUT:
                    EventCenter.getInstance().sendSecurityDisconnectState(false);
                    break;
                case CommandCallback.DISCONNECTING_STATUS_SUCCESS:
                    EventCenter.getInstance().sendSecurityDisconnectState(true);
                    break;
                case CommandCallback.DISCONNECTING_STATUS_FALSE:
                    EventCenter.getInstance().sendSecurityDisconnectState(false);
                    break;
                default:
                    break;
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 持續傳送BleData的資料
     */
    private class SendToBleQueue extends Thread {

        public SendToBleQueue() {

        }

        @Override
        public void run() {
            boolean isLoop = true;

            while (isLoop) {

                if (bleDataArray.size() > 0) {

                    Log.e("SecurityService", "DataSize:" + bleDataArray.size() + ">>");

                    try {

                        byte[] data = bleDataArray.peek();
                        if (data == null) {
                            sleep(5);
                            continue;
                        }

                        writeByte = data;
                        EventCenter.getInstance().sendWriteVIG(data);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
