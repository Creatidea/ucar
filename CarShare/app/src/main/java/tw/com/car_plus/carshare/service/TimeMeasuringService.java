package tw.com.car_plus.carshare.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.service.model.Time;


/**
 * Created by winni on 2017/4/13.
 */

public class TimeMeasuringService extends Service {

    //開始時間
    private Long startTime;
    private Long newTime;
    private StringBuffer dayBuffer, hourBuffer, minuteBuffer, secBuffer;
    private Time time;
    private Handler handler = new Handler();

    // ----------------------------------------------------
    public TimeMeasuringService() {

        time = new Time();
        dayBuffer = new StringBuffer();
        hourBuffer = new StringBuffer();
        minuteBuffer = new StringBuffer();
        secBuffer = new StringBuffer();
    }

    // ----------------------------------------------------
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startTime = intent.getLongExtra("time", 1000);
        handler.postDelayed(updateTimer, 1000);

        return super.onStartCommand(intent, flags, startId);
    }

    // ----------------------------------------------------
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // ----------------------------------------------------
    //固定要執行的方法
    private Runnable updateTimer = new Runnable() {

        public void run() {
            EventCenter.getInstance().sendTimeMeasuring(getTime());
            handler.postDelayed(this, 1000);
        }
    };

    // ----------------------------------------------------
    private Time getTime() {

        newTime = System.currentTimeMillis();
        Long spentTime = newTime - startTime;

        long day = spentTime / (24 * 60 * 60 * 1000);//天
        long hour = (spentTime / (60 * 60 * 1000) - day * 24);//時
        long minute = ((spentTime / (60 * 1000)) - day * 24 * 60 - hour * 60);//分
        long sec = (spentTime / 1000) % 60;//秒

        time.setDay(getStringTime(day, dayBuffer));
        time.setHour(getStringTime(hour, hourBuffer));
        time.setMinute(getStringTime(minute, minuteBuffer));
        time.setSec(getStringTime(sec, secBuffer));
        time.setLongTime(spentTime);

        dayBuffer.setLength(0);
        hourBuffer.setLength(0);
        minuteBuffer.setLength(0);
        secBuffer.setLength(0);

        return time;
    }

    // ----------------------------------------------------

    /**
     * 取得字串時間並補上0
     *
     * @param time
     * @param secBuffer
     * @return
     */
    private String getStringTime(Long time, StringBuffer secBuffer) {

        if (time < 10) {
            secBuffer.append(0).append(time);
        } else {
            secBuffer.append(time);
        }

        return secBuffer.toString();
    }

    // ----------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (handler != null && updateTimer != null) {
            handler.removeCallbacks(updateTimer);
            handler = null;
        }
    }
}
