package tw.com.car_plus.carshare.service.model;

/**
 * Created by jhen on 2017/7/12.
 */

public class CommandData {

    private int type;
    private int carId;
    private int vigType;
    private int orderId;
    private String startTime;
    private String endTime;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getVigType() {
        return vigType;
    }

    public void setVigType(int vigType) {
        this.vigType = vigType;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
