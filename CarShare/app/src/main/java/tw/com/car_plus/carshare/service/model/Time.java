package tw.com.car_plus.carshare.service.model;

/**
 * Created by neil on 2017/3/18.
 */

public class Time {

    private String day;
    private String hour;
    private String minute;
    private String sec;
    private Long longTime;
    private boolean finish;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public Long getLongTime() {
        return longTime;
    }

    public void setLongTime(Long longTime) {
        this.longTime = longTime;
    }

    public String getCountDownTime() {

        StringBuffer buffer = new StringBuffer();
        buffer.append(minute).append(":").append(sec);
        return buffer.toString();
    }
}
