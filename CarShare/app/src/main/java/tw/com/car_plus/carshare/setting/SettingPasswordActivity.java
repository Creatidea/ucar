package tw.com.car_plus.carshare.setting;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.setting.SettingConnect;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

public class SettingPasswordActivity extends CustomBaseActivity {

    @BindView(R.id.old_password)
    EditText oldPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.check_password)
    EditText checkPassword;

    // ----------------------------------------------------
    private SettingConnect connect;


    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_setting_password);
        ButterKnife.bind(this);

        setBackToolBar(R.drawable.img_change_password);

        IndexActivity.activities.add(this);
        connect = new SettingConnect(this);
    }

    // ----------------------------------------------------
    @OnClick({R.id.clean, R.id.send})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.clean:
                checkPassword.setText("");
                newPassword.setText("");
                oldPassword.setText("");
                break;
            case R.id.send:
                connect.changePassword(getPassword(oldPassword), getPassword(newPassword), getPassword(checkPassword));
                break;
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Boolean isSuccess) {

        if (isSuccess) {

            if (sp.getPassword().length() > 0) {
                sp.setPassword(getPassword(newPassword));
            }

            sp.getUserData().setPassword(getPassword(newPassword));

            Toast.makeText(this, getString(R.string.setting_change_password_success), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    // ----------------------------------------------------
    private String getPassword(EditText edit) {
        return edit.getText().toString().trim();
    }

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
}
