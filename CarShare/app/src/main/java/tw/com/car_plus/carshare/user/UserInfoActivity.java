package tw.com.car_plus.carshare.user;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.user.credit_card.CreditCardInfoFragment;
import tw.com.car_plus.carshare.user.info.UserInfoFragment;
import tw.com.car_plus.carshare.util.CustomBaseActivity;

public class UserInfoActivity extends CustomBaseActivity {

    @BindView(R.id.account_btn)
    Button accountBtn;
    @BindView(R.id.credit_card_btn)
    Button creditCardBtn;
    @BindView(R.id.layout_info)
    LinearLayout layoutInfo;

    // ----------------------------------------------------
    private Button chooseBtn;
    private boolean isOpenCreditCard;

    // ----------------------------------------------------
    @Override
    public void init() {
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);

        setToolbar(R.drawable.ic_back, R.drawable.image_user_info, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        IndexActivity.activities.add(this);

        if (isCompanyAccount()) {
            layoutInfo.setVisibility(View.GONE);
            goToFragment(R.id.content_layout, new UserInfoFragment());

        } else {
            isOpenCreditCard = getIntent().getBooleanExtra("isOpenCreditCard", false);

            if (!isOpenCreditCard) {
                accountBtn.setSelected(true);
                chooseBtn = accountBtn;
                goToFragment(R.id.content_layout, new UserInfoFragment());

            } else {
                creditCardBtn.setSelected(true);
                chooseBtn = creditCardBtn;
                goToFragment(R.id.content_layout, new CreditCardInfoFragment());
            }
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.account_btn, R.id.credit_card_btn})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.account_btn:
                if (!isSelectSameBtn(accountBtn)) {
                    chooseBtn = accountBtn;
                    cleanStack();
                    goToFragment(R.id.content_layout, new UserInfoFragment());
                }
                break;
            case R.id.credit_card_btn:
                if (!isSelectSameBtn(creditCardBtn)) {
                    chooseBtn = creditCardBtn;
                    cleanStack();
                    goToFragment(R.id.content_layout, new CreditCardInfoFragment());
                }
                break;
        }

        cleanBtnSelect();
        chooseBtn.setSelected(true);
    }

    // ----------------------------------------------------

    /**
     * 清除所有存入的FragmentBackStack
     */
    private void cleanStack() {

        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            // Get the back stack fragment id.
            int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();
            getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    // ----------------------------------------------------
    private void cleanBtnSelect() {
        accountBtn.setSelected(false);
        creditCardBtn.setSelected(false);
    }

    // ----------------------------------------------------

    /**
     * 判斷是否選擇一樣的按鈕
     */
    private boolean isSelectSameBtn(Button button) {

        boolean isSame = false;
        if (chooseBtn == button) {
            isSame = true;
        }
        return isSame;
    }

    // ----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IndexActivity.activities.remove(this);
    }

    // ----------------------------------------------------
}
