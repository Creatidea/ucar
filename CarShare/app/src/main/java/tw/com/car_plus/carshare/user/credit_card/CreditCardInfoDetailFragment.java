package tw.com.car_plus.carshare.user.credit_card;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.CreditCardConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.CreditCardsData;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/5/5.
 */

public class CreditCardInfoDetailFragment extends CustomBaseFragment {

    @BindView(R.id.card_no)
    TextView cardNo;
    @BindView(R.id.valid_period)
    TextView validPeriod;
    @BindView(R.id.security_code)
    TextView securityCode;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.send)
    Button send;

    private CreditCardConnect creditCardConnect;
    private CreditCardsData.CreditCardsBean creditCardsBean;

    @Override
    protected void init() {
        setView(R.layout.fragment_credit_card_info_detail);

        creditCardsBean = getArguments().getParcelable("data");
        creditCardConnect = new CreditCardConnect(activity);

        setData();
    }

    //--------------------------------------------------
    private void setData() {

        cardNo.setText(creditCardsBean.getCardNo());
        securityCode.setText(creditCardsBean.getSecurityCode());
        validPeriod.setText(String.format(getString(R.string.validity_period_format), Integer.valueOf(creditCardsBean.getGtYear()), Integer.valueOf
                (creditCardsBean.getGtMonth())));

        cancel.setText(getString(R.string.user_back));
        send.setText(getString(R.string.credit_card_delete));

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.USER_DELETE_CREDIT_CARD) {
            Boolean isSuccess = (Boolean) data.get("data");

            if (isSuccess) {
                creditCardConnect.getUserCreditCard();
                activity.onBackPressed();
            }
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    //--------------------------------------------------
    @OnClick({R.id.cancel, R.id.send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.send:
                creditCardConnect.sendDeleteCreditCard(creditCardsBean.getSerialNo());
                break;
        }
    }
}
