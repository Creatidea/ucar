package tw.com.car_plus.carshare.user.credit_card;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;

import butterknife.OnClick;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.CreditCardConnect;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.register.RegisterCreditCardFragment;
import tw.com.car_plus.carshare.register.model.CreditCardsData;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.user.UserInfoActivity;
import tw.com.car_plus.carshare.user.credit_card.adapter.CreditCardAdapter;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.UserUtil;

/**
 * Created by winni on 2017/5/5.
 */

public class CreditCardInfoFragment extends CustomBaseFragment implements AdapterView.OnItemClickListener {


    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.add_credit_card)
    Button addCreditCard;

    private CreditCardConnect creditCardConnect;
    private LoginConnect loginConnect;
    private CreditCardAdapter creditCardAdapter;
    private SharedPreferenceUtil sp;
    private UserAccountData userAccountData;
    private User userData;
    private UserUtil userUtil;
    private List<CreditCardsData.CreditCardsBean> creditCardsBeanList;
    private boolean isLogin = false;

    // ----------------------------------------------------
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof UserInfoActivity) {
            this.sp = ((UserInfoActivity) context).sp;
        }
    }

    //--------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_credit_card_info);

        creditCardConnect = new CreditCardConnect(activity);
        loginConnect = new LoginConnect(activity);
        creditCardAdapter = new CreditCardAdapter(activity, creditCardConnect);
        userAccountData = new UserAccountData();
        userUtil = new UserUtil(activity, sp);

        userData = sp.getUserData();
        userAccountData.setAcctId(userData.getAcctId());
        userAccountData.setLoginId(userData.getLoginId());

        setData();
    }

    //--------------------------------------------------
    private void setData() {
        icon.setImageResource(R.drawable.icon_credit_card);
        title.setText(getString(R.string.user_choose_credit_card));
        list.setAdapter(creditCardAdapter);
        list.setOnItemClickListener(this);

        creditCardConnect.getUserCreditCard();
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(CreditCardsData creditCardsData) {

        creditCardAdapter.setData(creditCardsData.getCreditCards());
        creditCardsBeanList = creditCardsData.getCreditCards();

        if (creditCardsBeanList.size() == 3) {
            addCreditCard.setVisibility(View.GONE);
        } else {
            addCreditCard.setVisibility(View.VISIBLE);
        }

        if (creditCardsBeanList.size() > 0 && userData.isNoCreditCard() && !isLogin) {
            loginConnect.connectLogin(EventCenter.USER_PAY_CREDIT_CARD_LOGIN, userData.getLoginId(), userData.getPassword());
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.USER_PAY_CREDIT_CARD) {

            Boolean isSuccess = (Boolean) data.get("data");
            if (isSuccess) {
                creditCardConnect.getUserCreditCard();
            }

        } else if ((int) data.get("type") == EventCenter.USER_PAY_CREDIT_CARD_LOGIN) {

            isLogin = true;
            User user = (User) data.get("data");
            userUtil.setUser(user);
            userUtil.setAccountInfo();
            sp.setUserData(userUtil.getUser());

        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onErrorEvent(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    //--------------------------------------------------
    @OnClick(R.id.add_credit_card)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putInt("from", EventCenter.USER);
        bundle.putParcelable("UserAccountData", userAccountData);
        replaceFragment(R.id.content_layout, new RegisterCreditCardFragment(), true, bundle);
    }

    //--------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", creditCardsBeanList.get(position));
        replaceFragment(R.id.content_layout, new CreditCardInfoDetailFragment(), true, bundle);
    }
}
