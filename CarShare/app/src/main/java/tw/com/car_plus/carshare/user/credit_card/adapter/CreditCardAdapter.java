package tw.com.car_plus.carshare.user.credit_card.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.CreditCardConnect;
import tw.com.car_plus.carshare.register.model.CreditCardsData;

/**
 * Created by winni on 2017/5/5.
 */

public class CreditCardAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private CreditCardConnect creditCardConnect;
    private List<CreditCardsData.CreditCardsBean> creditCardsBeenList;

    public CreditCardAdapter(Context context, CreditCardConnect creditCardConnect) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.creditCardConnect = creditCardConnect;
        creditCardsBeenList = new ArrayList<>();

    }

    //--------------------------------------------------
    @Override
    public int getCount() {
        return creditCardsBeenList.size();
    }

    //--------------------------------------------------
    @Override
    public Object getItem(int position) {
        return null;
    }

    //--------------------------------------------------
    @Override
    public long getItemId(int position) {
        return 0;
    }

    //--------------------------------------------------
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_credit_card, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.sort.setText(String.format(context.getString(R.string.user_credit_card_sort), position + 1));
        viewHolder.cardNumber.setText(creditCardsBeenList.get(position).getCardNo());
        viewHolder.check.setSelected(creditCardsBeenList.get(position).isDefault());

        viewHolder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creditCardConnect.setPayCreditCard(creditCardsBeenList.get(position).getSerialNo());
            }
        });

        return convertView;
    }

    //--------------------------------------------------

    /**
     * 設定資料
     *
     * @param creditCardsBeanList
     */
    public void setData(List<CreditCardsData.CreditCardsBean> creditCardsBeanList) {
        this.creditCardsBeenList = creditCardsBeanList;
        notifyDataSetChanged();
    }

    //--------------------------------------------------
    class ViewHolder {

        @BindView(R.id.sort)
        TextView sort;
        @BindView(R.id.card_number)
        TextView cardNumber;
        @BindView(R.id.check)
        ImageView check;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
