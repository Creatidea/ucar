package tw.com.car_plus.carshare.user.info;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.connect.register.RegisterConnect;
import tw.com.car_plus.carshare.connect.user.UserConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.UserStatus;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.page.PageCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.register.model.RegisterInfo;

import tw.com.car_plus.carshare.register.model.UploadFile;
import tw.com.car_plus.carshare.register.model.UserAccountData;
import tw.com.car_plus.carshare.user.info.modify.ModifyAddressFragment;
import tw.com.car_plus.carshare.user.info.modify.ModifyBirthdayFragment;
import tw.com.car_plus.carshare.user.info.modify.ModifyMailFragment;
import tw.com.car_plus.carshare.user.info.modify.ModifyNameFragment;
import tw.com.car_plus.carshare.user.info.modify.ModifyPhoneFragment;
import tw.com.car_plus.carshare.user.info.modify.model.Address;
import tw.com.car_plus.carshare.user.model.UserInfo;
import tw.com.car_plus.carshare.user.model.VerifyResult;
import tw.com.car_plus.carshare.util.DialogUtil;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;
import tw.com.car_plus.carshare.util.address.AddressUtil;
import tw.com.car_plus.carshare.util.camera.BaseCameraFragment;


/**
 * Created by neil on 2017/4/13.
 */
@RuntimePermissions
public class UserInfoFragment extends BaseCameraFragment implements View.OnClickListener {

    @BindView(R.id.account)
    TextView account;
    @BindView(R.id.company_account)
    TextView companyAccount;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.birth)
    TextView birth;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.mail)
    TextView mail;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.contact_address)
    TextView contactAddress;
    @BindView(R.id.current_bonus)
    TextView currentBonus;

    @BindView(R.id.new_name)
    TextView newName;
    @BindView(R.id.new_birth)
    TextView newBirth;
    @BindView(R.id.new_phone)
    TextView newPhone;
    @BindView(R.id.new_mail)
    TextView newMail;
    @BindView(R.id.new_address)
    TextView newAddress;
    @BindView(R.id.new_contact_address)
    TextView newContactAddress;

    @BindView(R.id.now_name_layout)
    LinearLayout nowNameLayout;
    @BindView(R.id.now_birth_layout)
    LinearLayout nowBirthLayout;
    @BindView(R.id.now_phone_layout)
    LinearLayout nowPhoneLayout;
    @BindView(R.id.now_mail_layout)
    LinearLayout nowMailLayout;
    @BindView(R.id.now_address_layout)
    LinearLayout nowAddressLayout;
    @BindView(R.id.now_contact_address_layout)
    LinearLayout nowContactAddressLayout;

    @BindView(R.id.new_name_layout)
    LinearLayout newNameLayout;
    @BindView(R.id.new_birth_layout)
    LinearLayout newBirthLayout;
    @BindView(R.id.new_phone_layout)
    LinearLayout newPhoneLayout;
    @BindView(R.id.new_mail_layout)
    LinearLayout newMailLayout;
    @BindView(R.id.new_address_layout)
    LinearLayout newAddressLayout;
    @BindView(R.id.new_contact_address_layout)
    LinearLayout newContactAddressLayout;

    @BindView(R.id.account_layout)
    View viewAccount;
    @BindView(R.id.layout_driver_license_positive)
    View viewLicensePositive;
    @BindView(R.id.layout_driver_license_negative)
    View viewLicenseNegative;
    @BindView(R.id.layout_id_card_positive)
    View viewCardPositive;
    @BindView(R.id.layout_id_card_negative)
    View viewCardNegative;
    @BindView(R.id.send)
    Button btnSend;
    @BindString(R.string.user_apistatus_pass)
    String pass;

    //駕照正面
    private final int DRIVER_LICENSE_POSITIVE = 22;
    //駕照反面
    private final int DRIVER_LICENSE_NEGATIVE = 33;
    //身份證正面
    private final int ID_CARD_POSITIVE = 44;
    //身分證反面
    private final int ID_CARD_NEGATIVE = 55;
    //註冊證件照
    private final int REGISTER_CREDENTIALS = 77;
    //送出審核
    private final int REGISTER_APPLY_VERIFY = 88;

    // ----------------------------------------------------
    private ImageView icon;
    private TextView title;
    private TextView info;

    //    View licensePositive;
    private ImageView icLicensePositive, ivLicensePositive, icErrorLicensePositive;
    private TextView titleLicensePositive, infoLicensePositive;
    //    View licenseNegative;
    private ImageView icLicenseNegative, ivLicenseNegative, icErrorLicenseNegative;
    private TextView titleLicenseNegative, infoLicenseNegative;
    //    View cardPositive;
    private ImageView icCardPositive, ivCardPositive, icErrorCardPositive;
    private TextView titleCardPositive, infoCardPositive;
    //    View cardNegative;
    private ImageView icCardNegative, ivCardNegative, icErrorCardNegative;
    private TextView titleCardNegative, infoCardNegative;

    // ----------------------------------------------------
    private String driverLicensePositivePath = "", driverLicenseNegativePath = "", idCardPositivePath = "", idCardNegativePath = "";
    //      Image Name
    private String driverLicensePositiveName = "", driverLicenseNegativeName = "", idCardPositiveName = "", idCardNegativeName = "";
    private int imageType;
    //用來判斷 是否都已經補拍缺漏的照片
    private boolean isDriverLicenceHeadPass = true, isDriverLicenceTailPass = true, isIdCardHeadPass = true, isIdCardTailPass = true;
    // ----------------------------------------------------
    private UserConnect connect;
    private AddressUtil addressUtil;
    private UserInfo userInfo;
    private User userData;
    private Address addressBean, contactAddressBean, newAddressBean, newContactAddressBean;
    private RegisterConnect registerConnect;
    private LoginConnect loginConnect;
    private UserAccountData userAccountData;
    private SharedPreferenceUtil sp;
    private DialogUtil dialogUtil;
    private ArrayList<City> cities;
    private ArrayList<Area> addressArea, contactAddressArea;
    private List<String> pathList = new ArrayList<>();
    private int register = REGISTER_CREDENTIALS;
    private boolean isLoadContactArea = false;
    private String uploadPath;//用來放上傳的檔案路徑

    // ----------------------------------------------------

    @Override
    protected void init() {
        setView(R.layout.fragment_user_account);

        registerConnect = new RegisterConnect(activity);
        connect = new UserConnect(activity);
        loginConnect = new LoginConnect(activity);
        addressUtil = new AddressUtil(activity);
        dialogUtil = new DialogUtil(activity);
        cities = new ArrayList<>();
        addressArea = new ArrayList<>();
        contactAddressArea = new ArrayList<>();
        addressBean = new Address();
        contactAddressBean = new Address();
        newAddressBean = new Address();
        newContactAddressBean = new Address();
        sp = new SharedPreferenceUtil(activity);

        userData = sp.getUserData();
        setAccountLayout();
        connect.loadUserInfo();
    }

    // ----------------------------------------------------

    /**
     * 設定帳號資訊的Title
     */
    private void setAccountLayout() {

        icon = (ImageView) viewAccount.findViewById(R.id.icon);
        title = (TextView) viewAccount.findViewById(R.id.title);
        info = (TextView) viewAccount.findViewById(R.id.info);

        icon.setImageResource(R.drawable.ic_account);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.user_info_account));
    }

    // ----------------------------------------------------
    private void initCardStatusView() {
//        viewLicensePositive
        icErrorLicensePositive = (ImageView) viewLicensePositive.findViewById(R.id.error);
        icLicensePositive = (ImageView) viewLicensePositive.findViewById(R.id.icon);
        ivLicensePositive = (ImageView) viewLicensePositive.findViewById(R.id.image);
        titleLicensePositive = (TextView) viewLicensePositive.findViewById(R.id.title);
        infoLicensePositive = (TextView) viewLicensePositive.findViewById(R.id.info);
//        viewLicenseNegative
        icErrorLicenseNegative = (ImageView) viewLicenseNegative.findViewById(R.id.error);
        icLicenseNegative = (ImageView) viewLicenseNegative.findViewById(R.id.icon);
        ivLicenseNegative = (ImageView) viewLicenseNegative.findViewById(R.id.image);
        titleLicenseNegative = (TextView) viewLicenseNegative.findViewById(R.id.title);
        infoLicenseNegative = (TextView) viewLicenseNegative.findViewById(R.id.info);
//        viewCardPositive;
        icErrorCardPositive = (ImageView) viewCardPositive.findViewById(R.id.error);
        icCardPositive = (ImageView) viewCardPositive.findViewById(R.id.icon);
        ivCardPositive = (ImageView) viewCardPositive.findViewById(R.id.image);
        titleCardPositive = (TextView) viewCardPositive.findViewById(R.id.title);
        infoCardPositive = (TextView) viewCardPositive.findViewById(R.id.info);
//        viewCardNegative;
        icErrorCardNegative = (ImageView) viewCardNegative.findViewById(R.id.error);
        icCardNegative = (ImageView) viewCardNegative.findViewById(R.id.icon);
        ivCardNegative = (ImageView) viewCardNegative.findViewById(R.id.image);
        titleCardNegative = (TextView) viewCardNegative.findViewById(R.id.title);
        infoCardNegative = (TextView) viewCardNegative.findViewById(R.id.info);
    }

    //region EventBus
    // ----------------------------------------------------
    @Subscribe
    public void onConnectSuccess(UserInfo userInfo) {

        this.userInfo = userInfo;
        addressUtil.loadCity();
        setUserData();

        if (UserStatus.verifyStatus == UserStatus.FAIL_REVIEW) {

            userAccountData = new UserAccountData();
            userAccountData.setLoginId(userInfo.getLoginID());
            userAccountData.setAcctId(String.valueOf(userInfo.getAcctId()));
            info.setVisibility(View.VISIBLE);
            info.setText(getString(R.string.fail_review));
            initCardStatusView();
            connect.getVerifyResult();

        } else if (UserStatus.verifyStatus == UserStatus.UNDER_REVIEW) {
            info.setText(getString(R.string.under_review));
        }
    }

    // ----------------------------------------------------
    private void setUserData() {

        currentBonus.setText(String.format(getString(R.string.user_bonus), userInfo.getBonus()));
        account.setText(userInfo.getLoginID().replace(userInfo.getLoginID().substring(1, 7), "......"));
        companyAccount.setText(userInfo.getCompanyAccount() == null ? "" : userInfo.getCompanyAccount().getCompanyName());
        name.setText(userInfo.getAcctName());
        birth.setText(userInfo.getBirth());
        phone.setText(userInfo.getMaincell());
        mail.setText(userInfo.getEmail());
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.CITY) {
            //城市
            addressUtil.setCities((List<City>) data.get("data"));
            cities.addAll(addressUtil.getCities());
            addressUtil.loadArea(userInfo.getHHRCityID());
        } else if ((int) data.get("type") == EventCenter.AREA) {
            //區域
            addressUtil.setAreas((List<Area>) data.get("data"));

            if (isLoadContactArea) {
                contactAddress.setText(addressUtil.findCityAndArea(userInfo.getCityID(), userInfo.getAreaID()) + userInfo.getAddr());
                contactAddressArea.addAll(addressUtil.getAreas());
                setAddressBean(contactAddressBean, userInfo.getCityID(), userInfo.getAreaID(), addressUtil.findCity(userInfo.getCityID()),
                        addressUtil.findArea(userInfo.getAreaID()), userInfo.getAddr(), contactAddress.getText().toString());
                isLoadContactArea = false;

            } else {
                address.setText(addressUtil.findCityAndArea(userInfo.getHHRCityID(), userInfo.getHHRAreaID()) + userInfo.getHHRAddr());
                setAddressBean(addressBean, userInfo.getHHRCityID(), userInfo.getHHRAreaID(), addressUtil.findCity(userInfo.getHHRCityID()),
                        addressUtil.findArea(userInfo.getHHRAreaID()), userInfo.getHHRAddr(), address.getText().toString());
                addressArea.addAll(addressUtil.getAreas());

                if (userInfo.getHHRCityID() == userInfo.getCityID()) {
                    contactAddress.setText(addressUtil.findCityAndArea(userInfo.getCityID(), userInfo.getAreaID()) + userInfo.getAddr());
                    setAddressBean(contactAddressBean, userInfo.getCityID(), userInfo.getAreaID(), addressUtil.findCity(userInfo.getCityID()),
                            addressUtil.findArea(userInfo.getAreaID()), userInfo.getAddr(), contactAddress.getText().toString());
                    contactAddressArea.addAll(addressUtil.getAreas());
                } else {
                    isLoadContactArea = true;
                    addressUtil.loadArea(userInfo.getCityID());
                }
            }
        } else if ((int) data.get("type") >= EventCenter.USER_NAME && (int) data.get("type") <= EventCenter.USER_EMAIL) {
            setData((int) data.get("type"), (String) data.get("data"));

        } else if ((int) data.get("type") == EventCenter.USER_ADDRESS) {
            //戶籍地址
            newAddressBean = (Address) data.get("data");
            setData((int) data.get("type"), newAddressBean.getStrAddress());
        } else if ((int) data.get("type") == EventCenter.USER_CONTACT_ADDRESS) {
            //通訊地址
            newContactAddressBean = (Address) data.get("data");
            setData((int) data.get("type"), newContactAddressBean.getStrAddress());
        } else if ((int) data.get("type") == EventCenter.USER_UPDATE) {
            //更新會員資料
            if ((Boolean) data.get("data")) {
                connect.loadUserInfo();
                cleanAllData();

                btnSend.setBackgroundResource(R.drawable.btn_bg);
                btnSend.setText(R.string.check_send);
            }
        } else if ((int) data.get("type") == EventCenter.USER) {
            //Login
            User user = (User) data.get("data");
            user.setLoginId(userData.getLoginId());
            user.setPassword(userData.getPassword());
            new PageCenter(activity).setUser(user);
            sp.setUserData(user);

            connect.sendUpdateUserData(getRegisterInfo());//更新資料
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(final VerifyResult verifyResult) {//若照片沒有通過審核則顯示並可以補拍

        btnSend.setBackgroundResource(R.drawable.btn_bg_review);
        btnSend.setText(R.string.user_send_review);

        if (!verifyResult.getDriverLicenceHeadStatus().equals(pass)) {
            setViewVisible(viewLicensePositive, icErrorLicensePositive, infoLicensePositive);
            setPictureBackgroundOnView(ivLicensePositive, titleLicensePositive, DRIVER_LICENSE_POSITIVE, R.drawable
                    .img_driverslicense_positive_register, getString(R.string.user_driverlicencehead));
            icLicensePositive.setImageResource(R.drawable.icon_credit_card);
            icErrorLicensePositive.setImageResource(R.drawable.ic_error);
            infoLicensePositive.setText(String.format(getString(R.string.user_review_result), verifyResult.getDriverLicenceHeadStatus()));
            isDriverLicenceHeadPass = false;//作為後面判斷 是否有補拍照片的依據
        }
        if (!verifyResult.getDriverLicenceTailStatus().equals(pass)) {
            setViewVisible(viewLicenseNegative, icErrorLicenseNegative, infoLicenseNegative);
            setPictureBackgroundOnView(ivLicenseNegative, titleLicenseNegative, DRIVER_LICENSE_NEGATIVE, R.drawable
                    .img_driverslicense_negative_register, getString(R.string.user_driverlicencetail));
            icLicenseNegative.setImageResource(R.drawable.icon_credit_card);
            icErrorLicenseNegative.setImageResource(R.drawable.ic_error);
            infoLicenseNegative.setText(String.format(getString(R.string.user_review_result), verifyResult.getDriverLicenceTailStatus()));
            isDriverLicenceTailPass = false;//作為後面判斷 是否有補拍照片的依據

        }
        if (!verifyResult.getIdCardHeadStatus().equals(pass)) {
            setViewVisible(viewCardPositive, icErrorCardPositive, infoCardPositive);
            setPictureBackgroundOnView(ivCardPositive, titleCardPositive, ID_CARD_POSITIVE, R.drawable.img_id_card_positive_register, getString(R
                    .string.user_cardhead));
            icCardPositive.setImageResource(R.drawable.icon_credit_card);
            icErrorCardPositive.setImageResource(R.drawable.ic_error);
            infoCardPositive.setText(String.format(getString(R.string.user_review_result), verifyResult.getIdCardHeadStatus()));
            isIdCardHeadPass = false;//作為後面判斷 是否有補拍照片的依據
        }
        if (!verifyResult.getIdCardTailStatus().equals(pass)) {
            setViewVisible(viewCardNegative, icErrorCardNegative, infoCardNegative);
            setPictureBackgroundOnView(ivCardNegative, titleCardNegative, ID_CARD_NEGATIVE, R.drawable.img_id_card_negative_register, getString(R
                    .string.user_cardtail));
            icCardNegative.setImageResource(R.drawable.icon_credit_card);
            icErrorCardNegative.setImageResource(R.drawable.ic_error);
            infoCardNegative.setText(String.format(getString(R.string.user_review_result), verifyResult.getIdCardTailStatus()));
            isIdCardTailPass = false;//作為後面判斷 是否有補拍照片的依據
        }
    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(UserAccountData userAccountData) {

        switch (register) {
            case REGISTER_CREDENTIALS:
                register = REGISTER_APPLY_VERIFY;
                registerConnect.connectApplyVerify(userAccountData);
                break;
            case REGISTER_APPLY_VERIFY:
                viewLicensePositive.setVisibility(View.GONE);
                viewLicenseNegative.setVisibility(View.GONE);
                viewCardPositive.setVisibility(View.GONE);
                viewCardNegative.setVisibility(View.GONE);

                if (userData.getCompanyAccount() != null) {
                    loginConnect.connectLoginCompany(EventCenter.USER, userData.getLoginId(), userData.getPassword(), sp.getCompanyTaxID());
                } else {
                    loginConnect.connectLogin(EventCenter.USER, userData.getLoginId(), userData.getPassword());
                }
                break;
        }
    }

    // ----------------------------------------------------

    @Subscribe
    public void onSuccessEvent(UploadFile uploadFile) {
        switch (imageType) {
            case DRIVER_LICENSE_POSITIVE://上傳駕照正面
                isShowDialog();
                driverLicensePositiveName = uploadFile.getFileName();

                if (!driverLicenseNegativePath.equals("")) {
                    imageType = DRIVER_LICENSE_NEGATIVE;
                    uploadPath = driverLicenseNegativePath;
                } else if (!idCardPositivePath.equals("")) {
                    imageType = ID_CARD_POSITIVE;
                    uploadPath = idCardPositivePath;
                } else if (!idCardNegativePath.equals("")) {
                    imageType = ID_CARD_NEGATIVE;
                    uploadPath = idCardNegativePath;
                } else {
                    uploadPath = "";
                }
                registerData();//確認是否完成上傳所有檔案註冊資料
                break;
            case DRIVER_LICENSE_NEGATIVE://上傳駕照反面
                isShowDialog();
                driverLicenseNegativeName = uploadFile.getFileName();
                if (!idCardPositivePath.equals("")) {
                    imageType = ID_CARD_POSITIVE;
                    uploadPath = idCardPositivePath;
                } else if (!idCardNegativePath.equals("")) {
                    imageType = ID_CARD_NEGATIVE;
                    uploadPath = idCardNegativePath;
                } else {//都沒有要上傳的檔案了
                    uploadPath = "";
                }
                registerData();//確認是否完成上傳所有檔案註冊資料
                break;
            case ID_CARD_POSITIVE://上傳身分證正面
                isShowDialog();
                idCardPositiveName = uploadFile.getFileName();
                if (!idCardNegativePath.equals("")) {
                    imageType = ID_CARD_NEGATIVE;
                    uploadPath = idCardNegativePath;
                } else {
                    uploadPath = "";
                }
                registerData();//確認是否完成上傳所有檔案註冊資料
                break;
            case ID_CARD_NEGATIVE://上傳身分證反面
                isShowDialog();
                idCardNegativeName = uploadFile.getFileName();
                uploadPath = "";
                registerData();//確認是否完成上傳所有檔案註冊資料
                break;
        }
    }

    //endregion

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessEvent(String errorMessage) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
    }

    // ----------------------------------------------------
    //擺入拍照區背景圖及對應名稱
    private void setPictureBackgroundOnView(ImageView imageView, TextView textView, int id, int background, String title) {
        textView.setText(title);
        imageView.setId(id);
        imageView.setOnClickListener(this);
        Glide.with(getActivity()).load(background).into(imageView);
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // ----------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();

        User user = sp.getUserData();
        user.setName(name.getText().toString());
        EventCenter.getInstance().sendUserEvent(EventCenter.USER, user);
    }

    //region OnClickListener

    // ----------------------------------------------------
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            //駕照正面
            case DRIVER_LICENSE_POSITIVE:
                cleanPicture(driverLicensePositivePath);
                UserInfoFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, DRIVER_LICENSE_POSITIVE);
                break;
            //駕照反面
            case DRIVER_LICENSE_NEGATIVE:
                cleanPicture(driverLicenseNegativePath);
                UserInfoFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, DRIVER_LICENSE_NEGATIVE);
                break;
            //身份證正面
            case ID_CARD_POSITIVE:
                cleanPicture(idCardPositivePath);
                UserInfoFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, ID_CARD_POSITIVE);
                break;
            //身分證反面
            case ID_CARD_NEGATIVE:
                cleanPicture(idCardNegativePath);
                UserInfoFragmentPermissionsDispatcher.goToTakePictureWithCheck(this, ID_CARD_NEGATIVE);
                break;
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.send, R.id.clean_name, R.id.clean_birth, R.id.clean_phone, R.id.clean_mail, R.id.clean_address, R.id
            .clean_contact_address})
    public void onClickClicked2(View view) {
        switch (view.getId()) {
            //取消清除
            case R.id.cancel:
                cleanAllData();
                break;
            //確認送出
            case R.id.send:
                if (UserStatus.verifyStatus == UserStatus.FAIL_REVIEW) {
                    if (checkNeedPictures()) {
                        //上傳
                        if (networkChecker.isNetworkAvailable()) {
                            if (!driverLicensePositivePath.equals("")) {
                                uploadPath = driverLicensePositivePath;
                                imageType = DRIVER_LICENSE_POSITIVE;
                            } else if (!driverLicenseNegativePath.equals("")) {
                                imageType = DRIVER_LICENSE_NEGATIVE;
                                uploadPath = driverLicenseNegativePath;
                            } else if (!idCardPositivePath.equals("")) {
                                imageType = ID_CARD_POSITIVE;
                                uploadPath = idCardPositivePath;
                            } else if (!idCardNegativePath.equals("")) {
                                imageType = ID_CARD_NEGATIVE;
                                uploadPath = idCardNegativePath;
                            }
                            fileUploadConnect.ConnectUploadFile(uploadPath);
                        }
                    } else {

                        StringBuilder sb = new StringBuilder();
                        if (idCardPositivePath.equals("") && !isIdCardHeadPass) {
                            sb.append(getString(R.string.user_cardhead) + "\n");
                        }
                        if (idCardNegativePath.equals("") && !isIdCardTailPass) {
                            sb.append(getString(R.string.user_cardtail) + "\n");
                        }
                        if (driverLicensePositivePath.equals("") && !isDriverLicenceHeadPass) {
                            sb.append(getString(R.string.user_driverlicencehead) + "\n");
                        }
                        if (driverLicenseNegativePath.equals("") && !isDriverLicenceTailPass) {
                            sb.append(getString(R.string.user_driverlicencetail) + "\n");
                        }

                        showDialog(String.format(getString(R.string.user_not_update), sb));
                    }

                } else if (UserStatus.verifyStatus == UserStatus.UNDER_REVIEW) {
                    showDialog(getString(R.string.user_under_review));
                } else {
                    connect.sendUpdateUserData(getRegisterInfo());//更新資料
                }
                break;
            //清除姓名
            case R.id.clean_name:
                cleanData(EventCenter.USER_NAME);
                break;
            //清除生日
            case R.id.clean_birth:
                cleanData(EventCenter.USER_BIRTHDAY);
                break;
            //清除電話
            case R.id.clean_phone:
                cleanData(EventCenter.USER_PHONE);
                break;
            //清除信箱
            case R.id.clean_mail:
                cleanData(EventCenter.USER_EMAIL);
                break;
            //清除戶籍地址
            case R.id.clean_address:
                cleanData(EventCenter.USER_ADDRESS);
                break;
            //清除通訊地址
            case R.id.clean_contact_address:
                cleanData(EventCenter.USER_CONTACT_ADDRESS);
                break;
        }

    }

    // ----------------------------------------------------
    @OnClick({R.id.name_layout, R.id.birth_layout, R.id.phone_layout, R.id.mail_layout, R.id.address_layout, R.id.contact_address_layout})
    public void onViewClicked(View view) {

        if (UserStatus.verifyStatus == UserStatus.UNDER_REVIEW) {
            showDialog(getString(R.string.user_under_review));

        } else {

            Bundle bundle = new Bundle();
            Fragment fragment = null;

            switch (view.getId()) {
                //姓名
                case R.id.name_layout:
                    bundle.putString("name", userInfo.getAcctName());
                    fragment = new ModifyNameFragment();
                    break;
                //生日
                case R.id.birth_layout:
                    bundle.putString("birthday", userInfo.getBirth());
                    fragment = new ModifyBirthdayFragment();
                    break;
                //電話
                case R.id.phone_layout:
                    bundle.putString("phone", userInfo.getMaincell());
                    fragment = new ModifyPhoneFragment();
                    break;
                //信箱
                case R.id.mail_layout:
                    bundle.putString("mail", userInfo.getEmail());
                    fragment = new ModifyMailFragment();
                    break;
                //戶籍地址
                case R.id.address_layout:
                    bundle.putParcelableArrayList("cityList", cities);
                    bundle.putParcelableArrayList("areaList", addressArea);
                    bundle.putParcelable("address", addressBean);
                    bundle.putInt("type", EventCenter.USER_ADDRESS);
                    fragment = new ModifyAddressFragment();
                    break;
                //通訊地址
                case R.id.contact_address_layout:
                    bundle.putParcelableArrayList("cityList", cities);
                    bundle.putParcelableArrayList("areaList", contactAddressArea);
                    bundle.putParcelable("address", contactAddressBean);
                    bundle.putInt("type", EventCenter.USER_CONTACT_ADDRESS);
                    fragment = new ModifyAddressFragment();
                    break;
            }

            replaceFragment(R.id.content_layout, fragment, true, bundle);
        }

    }

    //endregion

    // ----------------------------------------------------
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            compressionPicture(new CompressionPictureListener() {
                @Override
                public void compressionListener() {
                    switch (requestCode) {
                        case DRIVER_LICENSE_POSITIVE:
                            driverLicensePositivePath = setImageView(ivLicensePositive);
                            pathList.add(driverLicensePositivePath);//為後面清除路徑用
                            setReadyReview(icErrorLicensePositive, infoLicensePositive);
                            break;
                        case DRIVER_LICENSE_NEGATIVE:
                            driverLicenseNegativePath = setImageView(ivLicenseNegative);
                            pathList.add(driverLicenseNegativePath);//為後面清除路徑用
                            setReadyReview(icErrorLicenseNegative, infoLicenseNegative);
                            break;
                        case ID_CARD_POSITIVE:
                            idCardPositivePath = setImageView(ivCardPositive);
                            pathList.add(idCardPositivePath);//為後面清除路徑用
                            setReadyReview(icErrorCardPositive, infoCardPositive);
                            break;
                        case ID_CARD_NEGATIVE:
                            idCardNegativePath = setImageView(ivCardNegative);
                            pathList.add(idCardNegativePath);//為後面清除路徑用
                            setReadyReview(icErrorCardNegative, infoCardNegative);
                            break;
                    }
                }
            });
        }
    }

    // ----------------------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        UserInfoFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // -------------------------------------------------------

    /**
     * 同意開啟相機與寫入資料的權限
     */
    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void goToTakePicture(int type) {
        takePictures(type);
    }

    // ----------------------------------------------------

    /**
     * 拒絕權限讀取
     */
    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showDeniedForTakePicture() {
        Toast.makeText(activity, getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------
    private boolean checkNeedPictures() {//確認使用者是否補拍所缺證件

        if (driverLicensePositivePath.length() != 0) {
            isDriverLicenceHeadPass = true;
        }

        if (driverLicenseNegativePath.length() != 0) {
            isDriverLicenceTailPass = true;
        }

        if (idCardPositivePath.length() != 0) {
            isIdCardHeadPass = true;
        }

        if (idCardNegativePath.length() != 0) {
            isIdCardTailPass = true;
        }

        return isDriverLicenceHeadPass && isDriverLicenceTailPass && isIdCardHeadPass && isIdCardTailPass;
    }

    // ----------------------------------------------------

    /**
     * 依照uploadPath為空字串則註冊新資料並關閉Dialog非空字串則上傳檔案
     */
    private void registerData() {

        if (!uploadPath.equals("")) {
            fileUploadConnect.ConnectUploadFile(uploadPath);
        } else {
            register = REGISTER_CREDENTIALS;
            cleanPicture(pathList);
            registerConnect.connectCredentials(userAccountData, driverLicensePositiveName, driverLicenseNegativeName, idCardPositiveName,
                    idCardNegativeName);
            loadingDialogManager.dismiss();
        }
    }

    // ----------------------------------------------------

    /**
     * 清除資料
     *
     * @param type
     */
    private void cleanData(int type) {
        switch (type) {
            case EventCenter.USER_NAME:
                cleanNewDataView(nowNameLayout, newNameLayout, newName);
                break;
            case EventCenter.USER_BIRTHDAY:
                cleanNewDataView(nowBirthLayout, newBirthLayout, newBirth);
                break;
            case EventCenter.USER_PHONE:
                cleanNewDataView(nowPhoneLayout, newPhoneLayout, newPhone);
                break;
            case EventCenter.USER_EMAIL:
                cleanNewDataView(nowMailLayout, newMailLayout, newMail);
                break;
            case EventCenter.USER_ADDRESS:
                newAddressBean = null;
                newAddressBean = new Address();
                cleanNewDataView(nowAddressLayout, newAddressLayout, newAddress);
                break;
            case EventCenter.USER_CONTACT_ADDRESS:
                newContactAddressBean = null;
                newContactAddressBean = new Address();
                cleanNewDataView(nowContactAddressLayout, newContactAddressLayout, newContactAddress);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 清除所有資料
     */
    private void cleanAllData() {
        for (int i = EventCenter.USER_NAME; i <= EventCenter.USER_CONTACT_ADDRESS; i++) {
            cleanData(i);
        }
    }

    // ----------------------------------------------------

    /**
     * 設定資料
     *
     * @param type
     * @param text
     */
    private void setData(int type, String text) {
        switch (type) {
            case EventCenter.USER_NAME:
                setNewDataView(nowNameLayout, newNameLayout, newName, text);
                break;
            case EventCenter.USER_BIRTHDAY:
                setNewDataView(nowBirthLayout, newBirthLayout, newBirth, text);
                break;
            case EventCenter.USER_PHONE:
                setNewDataView(nowPhoneLayout, newPhoneLayout, newPhone, text);
                break;
            case EventCenter.USER_EMAIL:
                setNewDataView(nowMailLayout, newMailLayout, newMail, text);
                break;
            case EventCenter.USER_ADDRESS:
                setNewDataView(nowAddressLayout, newAddressLayout, newAddress, text);
                break;
            case EventCenter.USER_CONTACT_ADDRESS:
                setNewDataView(nowContactAddressLayout, newContactAddressLayout, newContactAddress, text);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 設定顯示新資料的view
     *
     * @param nowLayout
     * @param newLayout
     * @param textView
     * @param text
     */
    private void setNewDataView(LinearLayout nowLayout, LinearLayout newLayout, TextView textView, String text) {
        nowLayout.setVisibility(View.GONE);
        newLayout.setVisibility(View.VISIBLE);
        textView.setText(text);
    }

    // ----------------------------------------------------

    /**
     * 將新資料清除並顯示原有的資料
     *
     * @param nowLayout
     * @param newLayout
     * @param textView
     */
    private void cleanNewDataView(LinearLayout nowLayout, LinearLayout newLayout, TextView textView) {
        nowLayout.setVisibility(View.VISIBLE);
        newLayout.setVisibility(View.GONE);
        textView.setText("");
    }

    // ----------------------------------------------------

    /**
     * 設定address的資料
     *
     * @param addressBean
     * @param cityId
     * @param areaId
     * @param cityName
     * @param areaName
     * @param streets
     */
    private void setAddressBean(Address addressBean, int cityId, int areaId, String cityName, String areaName, String streets, String strAddress) {
        addressBean.setCityId(cityId);
        addressBean.setAreaId(areaId);
        addressBean.setCityName(cityName);
        addressBean.setAreaName(areaName);
        addressBean.setStreets(streets);
        addressBean.setStrAddress(strAddress);
    }

    // ----------------------------------------------------

    /**
     * 取得使用者更新的資料
     *
     * @return
     */
    private RegisterInfo getRegisterInfo() {

        RegisterInfo registerInfo = new RegisterInfo();

        registerInfo.setAcctId(userInfo.getAcctId());
        registerInfo.setLoginId(userInfo.getLoginID());

        registerInfo.setChtName(newNameLayout.isShown() ? newName.getText().toString() : userInfo.getAcctName());
        registerInfo.setBirthday(newBirthLayout.isShown() ? newBirth.getText().toString() : userInfo.getBirth());
        registerInfo.setCellPhone(newPhoneLayout.isShown() ? newPhone.getText().toString() : userInfo.getMaincell());
        registerInfo.setEmail(newMailLayout.isShown() ? newMail.getText().toString() : userInfo.getEmail());
        registerInfo.setJurisdiction(userInfo.getJurisdiction());

        registerInfo.setHhrCityId(newAddressLayout.isShown() ? newAddressBean.getCityId() : userInfo.getHHRCityID());
        registerInfo.setHhrAreaId(newAddressLayout.isShown() ? newAddressBean.getAreaId() : userInfo.getHHRAreaID());
        registerInfo.setHhrAddr(newAddressLayout.isShown() ? newAddressBean.getStreets() : userInfo.getHHRAddr());

        registerInfo.setMailingCityId(newContactAddressLayout.isShown() ? newContactAddressBean.getCityId() : userInfo.getCityID());
        registerInfo.setMailingAreaId(newContactAddressLayout.isShown() ? newContactAddressBean.getAreaId() : userInfo.getAreaID());
        registerInfo.setMailingAddr(newContactAddressLayout.isShown() ? newContactAddressBean.getStreets() : userInfo.getAddr());

        registerInfo.setSubscriptionEDM(userInfo.getEpaperSts());
        registerInfo.setReceiveSMS(userInfo.getSmsSts());

        return registerInfo;
    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog
     *
     * @param message
     */
    private void showDialog(String message) {
        dialogUtil.setMessage(message).setConfirmButton(R.string.confirm, null).showDialog(true);
    }

    // ----------------------------------------------------
    private void setReadyReview(ImageView icon, TextView result) {
        result.setText(String.format(getString(R.string.user_review_result), getString(R.string.user_wait_review)));
        icon.setImageResource(R.drawable.ic_done);
    }

    // ----------------------------------------------------
    private void setViewVisible(View... views) {

        for (View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    // = = = == = = = = = = = ==  = == = = = = = = = = = = = = = = =  = = == = = = = = = = = = = = =
    private void isShowDialog() {//show dialog
        if (!loadingDialogManager.dialog.isShowing()) {
            loadingDialogManager.show();
        }
    }
}
