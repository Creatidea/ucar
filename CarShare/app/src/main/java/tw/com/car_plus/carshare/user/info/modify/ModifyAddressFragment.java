package tw.com.car_plus.carshare.user.info.modify;


import android.view.View;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;
import tw.com.car_plus.carshare.rent.adapter.AreaAdapter;
import tw.com.car_plus.carshare.rent.adapter.CityAdapter;
import tw.com.car_plus.carshare.user.info.modify.model.Address;
import tw.com.car_plus.carshare.util.CustomBaseFragment;
import tw.com.car_plus.carshare.util.address.AddressUtil;

/**
 * Created by winni on 2017/4/26.
 */

public class ModifyAddressFragment extends CustomBaseFragment implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.address_city_title)
    TextView addressCityTitle;
    @BindView(R.id.address_city)
    Spinner addressCity;
    @BindView(R.id.address_area_title)
    TextView addressAreaTitle;
    @BindView(R.id.address_area)
    Spinner addressArea;
    @BindView(R.id.address_streets_title)
    TextView addressStreetsTitle;
    @BindView(R.id.address_streets)
    EditText addressStreets;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.send)
    Button send;

    private AddressUtil addressUtil;
    private Address address, newAddress;
    private List<City> cities;
    private List<Area> areas;
    private int type;
    private String errorMessage = "";


    // ----------------------------------------------------
    @Override
    protected void init() {
        setView(R.layout.fragment_modify_user_address);

        type = getArguments().getInt("type");
        address = getArguments().getParcelable("address");
        cities = getArguments().getParcelableArrayList("cityList");
        areas = getArguments().getParcelableArrayList("areaList");

        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        addressUtil = new AddressUtil(activity);
        newAddress = new Address();

        addressUtil.setCities(cities);
        addressUtil.setAreas(areas);

        if (type == EventCenter.USER_CONTACT_ADDRESS) {
            addressCityTitle.setText(getString(R.string.address_city));
            addressAreaTitle.setText(getString(R.string.address_area));
            addressStreetsTitle.setText(getString(R.string.address_streets));
        }
        addressStreets.setText(address.getStreets());

        setCitySpinner();
        addressCity.setSelection(addressUtil.getCityPosition(address.getCityId()), false);
        addressCity.setOnItemSelectedListener(this);

        setAreaSpinner(areas);
        addressArea.setSelection(addressUtil.getAreaPosition(areas, address.getAreaId()), false);
        addressArea.setOnItemSelectedListener(this);

        newAddress.setCityName(address.getCityName());
        newAddress.setCityId(address.getCityId());
        newAddress.setAreaName(address.getAreaName());
        newAddress.setAreaId(address.getAreaId());
        newAddress.setStreets(address.getStreets());
        newAddress.setStrAddress(address.getStrAddress());

    }

    // ----------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<String, Object> data) {

        if ((int) data.get("type") == EventCenter.AREA) {
            addressUtil.setAreas((List<Area>) data.get("data"));
            setAreaSpinner(addressUtil.getAreas());
        }
    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.send:
                newAddress.setStreets(addressStreets.getText().toString());
                newAddress.setStrAddress(newAddress.getCityName() + newAddress.getAreaName() + newAddress.getStreets());

                if (checkAddressStreets()) {
                    if (checkChangeAddress()) {
                        activity.onBackPressed();
                        if (type == EventCenter.USER_ADDRESS) {
                            EventCenter.getInstance().sendUserAddress(newAddress);
                        } else {
                            EventCenter.getInstance().sendUserContactAddress(newAddress);
                        }
                    }
                }

                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // ----------------------------------------------------
    private void setAreaSpinner(List<Area> areas) {
        AreaAdapter adapter = new AreaAdapter(areas);
        addressArea.setAdapter(adapter);
    }

    // ----------------------------------------------------
    private void setCitySpinner() {
        CityAdapter adapter = new CityAdapter(cities);
        addressCity.setAdapter(adapter);
    }

    // ----------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        int viewId = parent.getId();

        switch (viewId) {
            case R.id.address_city:
                addressUtil.loadArea(cities.get(position).getCityId());
                newAddress.setCityId(cities.get(position).getCityId());
                newAddress.setCityName(cities.get(position).getCityName());
                break;
            case R.id.address_area:
                newAddress.setAreaId(addressUtil.getAreas().get(position).getAreaId());
                newAddress.setAreaName(addressUtil.getAreas().get(position).getAreaName());
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    // ----------------------------------------------------

    /**
     * 檢查地址的街道是否有填寫
     *
     * @return
     */
    private boolean checkAddressStreets() {

        boolean isStreets = true;

        if (newAddress.getStreets().isEmpty()) {
            addressStreets.setError(activity.getString(R.string.error_format));
            isStreets = false;
        }

        return isStreets;
    }

    // ----------------------------------------------------

    /**
     * 判斷是否有改變姓名，有輸入但一樣也不算改變
     *
     * @return
     */
    private boolean checkChangeAddress() {

        if (address.getStrAddress().equals(newAddress.getStrAddress())) {
            return false;
        }
        return true;
    }

    // ----------------------------------------------------

}
