package tw.com.car_plus.carshare.user.info.modify;


import android.app.DatePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/4/24.
 */

public class ModifyBirthdayFragment extends CustomBaseFragment {

    @BindView(R.id.birthday)
    EditText birthday;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.send)
    Button send;

    private String strBirthday;
    private String birthdayArray[];
    private int year, month, day;

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_modify_user_birthday);
        strBirthday = getArguments().getString("birthday");

        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        birthday.setHint(strBirthday);
        cancel.setText(getString(R.string.cancel));
        send.setText(getString(R.string.confirm));

        birthdayArray = strBirthday.split("/");

        year = Integer.valueOf(birthdayArray[0]);
        month = Integer.valueOf(birthdayArray[1]);
        day = Integer.valueOf(birthdayArray[2]);

    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.send, R.id.calendar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.send:
                activity.onBackPressed();
                if (birthday.getText().toString().length() > 0) {
                    EventCenter.getInstance().sendUserBirthday(birthday.getText().toString());
                }
                break;
            case R.id.calendar:
                showDateDialog(year, month, day);
                break;
        }
    }

    //--------------------------------------------------

    /**
     * 顯示日期的Dialog
     */
    private void showDateDialog(int year, int month, int day) {

        final Calendar c = Calendar.getInstance();
        int maxYear = c.get(Calendar.YEAR) - 20;
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        c.set(maxYear, mMonth, mDay);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                String format = String.format(getString(R.string.date_format), year, month + 1, day);
                birthday.setText(format);
            }

        }, year, month - 1, day);

        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

}
