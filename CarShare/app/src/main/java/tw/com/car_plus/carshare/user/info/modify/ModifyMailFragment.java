package tw.com.car_plus.carshare.user.info.modify;


import android.view.View;

import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.util.CheckDataUtil;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/4/24.
 */

public class ModifyMailFragment extends CustomBaseFragment {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.send)
    Button send;

    private CheckDataUtil checkDataUtil;
    private String strMail;

    @Override
    protected void init() {
        setView(R.layout.fragment_modify_user_mail);
        strMail = getArguments().getString("mail");
        checkDataUtil = new CheckDataUtil();
        setData();
    }

    // ----------------------------------------------------
    private void setData() {
        cancel.setText(getString(R.string.cancel));
        send.setText(getString(R.string.confirm));
        email.setHint(strMail);
    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.send:

                if (checkChangeEmail()) {
                    if (checkDataUtil.checkEmail(email.getText().toString())) {
                        activity.onBackPressed();
                        EventCenter.getInstance().sendUserMail(email.getText().toString());
                    } else {
                        email.setError(getString(R.string.format_error));
                    }
                } else {
                    activity.onBackPressed();
                }
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 判斷是否有改變姓名，有輸入但一樣也不算改變
     *
     * @return
     */
    private boolean checkChangeEmail() {

        if (email.getText().toString().length() == 0) {
            return false;
        } else if (email.getText().toString().equals(strMail)) {
            return false;
        }

        return true;
    }
}
