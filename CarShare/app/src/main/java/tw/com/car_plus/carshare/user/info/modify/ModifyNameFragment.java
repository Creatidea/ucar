package tw.com.car_plus.carshare.user.info.modify;


import android.view.View;

import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/4/20.
 */

public class ModifyNameFragment extends CustomBaseFragment {

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.send)
    Button send;

    private String strName;

    // ----------------------------------------------------
    @Override
    protected void init() {

        setView(R.layout.fragment_modify_user_name);
        strName = getArguments().getString("name");
        setData();
    }

    // ----------------------------------------------------
    private void setData() {

        cancel.setText(getString(R.string.cancel));
        send.setText(getString(R.string.confirm));
        name.setHint(strName);
    }

    // ----------------------------------------------------
    @OnClick({R.id.cancel, R.id.send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                activity.onBackPressed();
                break;
            case R.id.send:
                activity.onBackPressed();
                if (checkChangeName()) {
                    EventCenter.getInstance().sendUserName(name.getText().toString());
                }
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 判斷是否有改變姓名，有輸入但一樣也不算改變
     *
     * @return
     */
    private boolean checkChangeName() {

        if (name.getText().toString().length() == 0) {
            return false;
        } else if (name.getText().toString().equals(strName)) {
            return false;
        }

        return true;
    }
}
