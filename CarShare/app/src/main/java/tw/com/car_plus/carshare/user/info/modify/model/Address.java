package tw.com.car_plus.carshare.user.info.modify.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winni on 2017/4/26.
 */

public class Address implements Parcelable {

    private int cityId;
    private String cityName;
    private int areaId;
    private String areaName;
    private String streets;
    private String strAddress;


    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStreets() {
        return streets;
    }

    public void setStreets(String streets) {
        this.streets = streets;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.cityId);
        dest.writeString(this.cityName);
        dest.writeInt(this.areaId);
        dest.writeString(this.areaName);
        dest.writeString(this.streets);
        dest.writeString(this.strAddress);
    }

    public Address() {
    }

    protected Address(Parcel in) {
        this.cityId = in.readInt();
        this.cityName = in.readString();
        this.areaId = in.readInt();
        this.areaName = in.readString();
        this.streets = in.readString();
        this.strAddress = in.readString();
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
