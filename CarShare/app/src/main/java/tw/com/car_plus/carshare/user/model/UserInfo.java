package tw.com.car_plus.carshare.user.model;

/**
 * Created by neil on 2017/4/13.
 */

public class UserInfo {


    /**
     * AcctId : 194455
     * LoginID : F254863846
     * AcctName : 陳煒真
     * Birth : 1992/07/25
     * MainTelAreaCode :
     * MainTel :
     * MainTelExt :
     * Maincell : 0988725047
     * HHRCityID : 3
     * HHRAreaID : 43
     * HHRAddr : 永康街
     * CityID : 3
     * AreaID : 45
     * Addr : 長安街
     * Jurisdiction : 1234567890123
     * Email : s22838524@gmail.com
     * EpaperSts : N
     * SmsSts : N
     * Bonus : 53
     * CompanyAccount : {"CompanyId":"CarPlus","CompanyName":"格上租車"}
     * Status : true
     */

    private int AcctId;
    private String LoginID;
    private String AcctName;
    private String Birth;
    private String MainTelAreaCode;
    private String MainTel;
    private String MainTelExt;
    private String Maincell;
    private int HHRCityID;
    private int HHRAreaID;
    private String HHRAddr;
    private int CityID;
    private int AreaID;
    private String Addr;
    private String Jurisdiction;
    private String Email;
    private String EpaperSts;
    private String SmsSts;
    private int Bonus;
    private CompanyAccountBean CompanyAccount;
    private boolean Status;

    public int getAcctId() {
        return AcctId;
    }

    public void setAcctId(int AcctId) {
        this.AcctId = AcctId;
    }

    public String getLoginID() {
        return LoginID;
    }

    public void setLoginID(String LoginID) {
        this.LoginID = LoginID;
    }

    public String getAcctName() {
        return AcctName;
    }

    public void setAcctName(String AcctName) {
        this.AcctName = AcctName;
    }

    public String getBirth() {
        return Birth;
    }

    public void setBirth(String Birth) {
        this.Birth = Birth;
    }

    public String getMainTelAreaCode() {
        return MainTelAreaCode;
    }

    public void setMainTelAreaCode(String MainTelAreaCode) {
        this.MainTelAreaCode = MainTelAreaCode;
    }

    public String getMainTel() {
        return MainTel;
    }

    public void setMainTel(String MainTel) {
        this.MainTel = MainTel;
    }

    public String getMainTelExt() {
        return MainTelExt;
    }

    public void setMainTelExt(String MainTelExt) {
        this.MainTelExt = MainTelExt;
    }

    public String getMaincell() {
        return Maincell;
    }

    public void setMaincell(String Maincell) {
        this.Maincell = Maincell;
    }

    public int getHHRCityID() {
        return HHRCityID;
    }

    public void setHHRCityID(int HHRCityID) {
        this.HHRCityID = HHRCityID;
    }

    public int getHHRAreaID() {
        return HHRAreaID;
    }

    public void setHHRAreaID(int HHRAreaID) {
        this.HHRAreaID = HHRAreaID;
    }

    public String getHHRAddr() {
        return HHRAddr;
    }

    public void setHHRAddr(String HHRAddr) {
        this.HHRAddr = HHRAddr;
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int CityID) {
        this.CityID = CityID;
    }

    public int getAreaID() {
        return AreaID;
    }

    public void setAreaID(int AreaID) {
        this.AreaID = AreaID;
    }

    public String getAddr() {
        return Addr;
    }

    public void setAddr(String Addr) {
        this.Addr = Addr;
    }

    public String getJurisdiction() {
        return Jurisdiction;
    }

    public void setJurisdiction(String Jurisdiction) {
        this.Jurisdiction = Jurisdiction;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getEpaperSts() {
        return EpaperSts;
    }

    public void setEpaperSts(String EpaperSts) {
        this.EpaperSts = EpaperSts;
    }

    public String getSmsSts() {
        return SmsSts;
    }

    public void setSmsSts(String SmsSts) {
        this.SmsSts = SmsSts;
    }

    public int getBonus() {
        return Bonus;
    }

    public void setBonus(int Bonus) {
        this.Bonus = Bonus;
    }

    public CompanyAccountBean getCompanyAccount() {
        return CompanyAccount;
    }

    public void setCompanyAccount(CompanyAccountBean CompanyAccount) {
        this.CompanyAccount = CompanyAccount;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class CompanyAccountBean {
        /**
         * CompanyId : CarPlus
         * CompanyName : 格上租車
         */

        private String CompanyId;
        private String CompanyName;

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String CompanyId) {
            this.CompanyId = CompanyId;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }
    }
}
