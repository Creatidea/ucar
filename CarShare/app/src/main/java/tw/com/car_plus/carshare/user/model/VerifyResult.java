package tw.com.car_plus.carshare.user.model;

import java.util.List;

/**
 * Created by winni on 2017/5/10.
 */

public class VerifyResult {


    /**
     * IdCardHeadStatus : 資訊錯誤
     * IdCardHeadReason :
     * IdCardTailStatus : 模糊
     * IdCardTailReason :
     * DriverLicenceHeadStatus : 模糊
     * DriverLicenceHeadReason :
     * DriverLicenceTailStatus : 通過
     * DriverLicenceTailReason :
     * IsVerifyPass : false
     * Status : true
     * Errors : []
     */

    private String IdCardHeadStatus;
    private String IdCardHeadReason;
    private String IdCardTailStatus;
    private String IdCardTailReason;
    private String DriverLicenceHeadStatus;
    private String DriverLicenceHeadReason;
    private String DriverLicenceTailStatus;
    private String DriverLicenceTailReason;
    private boolean IsVerifyPass;
    private boolean Status;
    private List<?> Errors;

    public String getIdCardHeadStatus() {
        return IdCardHeadStatus;
    }

    public void setIdCardHeadStatus(String IdCardHeadStatus) {
        this.IdCardHeadStatus = IdCardHeadStatus;
    }

    public String getIdCardHeadReason() {
        return IdCardHeadReason;
    }

    public void setIdCardHeadReason(String IdCardHeadReason) {
        this.IdCardHeadReason = IdCardHeadReason;
    }

    public String getIdCardTailStatus() {
        return IdCardTailStatus;
    }

    public void setIdCardTailStatus(String IdCardTailStatus) {
        this.IdCardTailStatus = IdCardTailStatus;
    }

    public String getIdCardTailReason() {
        return IdCardTailReason;
    }

    public void setIdCardTailReason(String IdCardTailReason) {
        this.IdCardTailReason = IdCardTailReason;
    }

    public String getDriverLicenceHeadStatus() {
        return DriverLicenceHeadStatus;
    }

    public void setDriverLicenceHeadStatus(String DriverLicenceHeadStatus) {
        this.DriverLicenceHeadStatus = DriverLicenceHeadStatus;
    }

    public String getDriverLicenceHeadReason() {
        return DriverLicenceHeadReason;
    }

    public void setDriverLicenceHeadReason(String DriverLicenceHeadReason) {
        this.DriverLicenceHeadReason = DriverLicenceHeadReason;
    }

    public String getDriverLicenceTailStatus() {
        return DriverLicenceTailStatus;
    }

    public void setDriverLicenceTailStatus(String DriverLicenceTailStatus) {
        this.DriverLicenceTailStatus = DriverLicenceTailStatus;
    }

    public String getDriverLicenceTailReason() {
        return DriverLicenceTailReason;
    }

    public void setDriverLicenceTailReason(String DriverLicenceTailReason) {
        this.DriverLicenceTailReason = DriverLicenceTailReason;
    }

    public boolean isIsVerifyPass() {
        return IsVerifyPass;
    }

    public void setIsVerifyPass(boolean IsVerifyPass) {
        this.IsVerifyPass = IsVerifyPass;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public List<?> getErrors() {
        return Errors;
    }

    public void setErrors(List<?> Errors) {
        this.Errors = Errors;
    }
}
