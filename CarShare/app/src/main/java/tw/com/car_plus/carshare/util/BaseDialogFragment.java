package tw.com.car_plus.carshare.util;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;

/**
 * Created by neil on 2016/12/1.
 */

public abstract class BaseDialogFragment extends DialogFragment {

    // ----------------------------------------------------
    protected View view;

    // ----------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            // initial.
            init();
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        return view;
    }

    // ----------------------------------------------------

    /**
     * initial
     */
    protected abstract void init();

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }

    // ----------------------------------------------------
    protected void clearAllFragment() {

        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
    // ----------------------------------------------------

    /**
     * base fragment replace.
     */
    private void baseReplace(Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction;
        transaction = getFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.container_framelayout, fragment);
        transaction.commit();

        if (getFragmentManager() != null) {
            getFragmentManager().executePendingTransactions();
        }
    }
    // ----------------------------------------------------

    /**
     * 顯示Dialog Fragment page.
     */
    public void showDialogFragment(DialogFragment fragment, Bundle bundle) {

        //跳Dialog頁
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        // Create and show the dialog.
        fragment.setArguments(bundle);

        fragment.show(ft, "dialog");
    }

    // ----------------------------------------------------
    public void setView(int resource) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.view = LayoutInflater.from(getActivity()).inflate(resource, null);
        ButterKnife.bind(this, view);
    }

    // ----------------------------------------------------
    public void replaceFragment(Fragment fragment, boolean addToBackStack, Bundle bundle) {

        fragment.setArguments(bundle);
        baseReplace(fragment, addToBackStack);
    }

    // ----------------------------------------------------
    public void replaceFragment(Fragment fragment, boolean addToBackStack) {

        baseReplace(fragment, addToBackStack);
    }

    // ----------------------------------------------------
}
