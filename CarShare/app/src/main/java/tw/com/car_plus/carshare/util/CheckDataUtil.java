package tw.com.car_plus.carshare.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by winni on 2017/3/3.
 */

public class CheckDataUtil {


    // ----------------------------------------------------

    /**
     * 檢查電話號碼
     *
     * @param phone
     * @return
     */
    public boolean checkPhone(String phone) {

        if (phone.length() > 0) {
            String expression = "^[0-9]{10}$";
            CharSequence inputStr = phone;

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);

            return matcher.matches();
        } else
            return false;

    }

    // -------------------------------------------------------

    /**
     * 檢查信箱是否正確
     */
    public boolean checkEmail(String mail) {

        if (mail.length() > 0) {

            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = mail;

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);

            return matcher.matches();

        } else {
            return false;
        }
    }

    //--------------------------------------------------

    /**
     * 檢查身分證字號
     *
     * @param strAccount
     * @return
     */
    public boolean checkAccount(String strAccount) {

        String expression = "[a-zA-Z][0-9]{9}";

        CharSequence inputStr = strAccount;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();

    }

    // ----------------------------------------------------

    /**
     * 檢查密碼
     *
     * @param min      最小值
     * @param max      最大值
     * @param password 輸入的密碼
     * @return
     */
    public boolean checkPassword(int min, int max, String password) {
        return password.length() >= min && password.length() <= max;
    }

    // ----------------------------------------------------

    /**
     * 檢查縣市
     *
     * @param cityId
     * @return
     */
    public boolean checkCity(int cityId) {

        if (cityId != -1) {
            return true;
        } else {
            return false;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查區域
     *
     * @param areaId
     * @return
     */
    public boolean checkArea(int areaId) {

        if (areaId != -1) {
            return true;
        } else {
            return false;
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查地址詳細資料
     *
     * @param streets
     * @return
     */
    public boolean checkStreets(String streets) {
        if (streets.length() != 0) {
            return true;
        } else {
            return false;
        }
    }

}
