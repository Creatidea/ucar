package tw.com.car_plus.carshare.util;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.metrics.MetricsManager;

import java.util.HashMap;

import butterknife.BindView;
import tw.com.car_plus.carshare.R;

/**
 * Created by winni on 2017/3/18.
 */

public abstract class CustomBaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_image)
    public ImageView toolbarImage;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.toolbar_change)
    public LinearLayout toolbarChange;
    @BindView(R.id.toolbar_icon)
    public ImageView toolbarIcon;
    @BindView(R.id.toolbar_account)
    public TextView toolbarAccount;

    public SharedPreferenceUtil sp;

    // ----------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MetricsManager.register(getApplication());
        sp = new SharedPreferenceUtil(this);
        init();
        setToolbarChange();
    }

    // ----------------------------------------------------
    abstract public void init();

    // ----------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    // ----------------------------------------------------

    /**
     * 設定toolbar的資料
     *
     * @param resourceIcon  NavigationIcon
     * @param resourceTitle toolbarTitle
     * @param listener      按下的監聽事件
     */
    protected void setToolbar(int resourceIcon, int resourceTitle, View.OnClickListener listener) {

        toolbarImage.setImageResource(resourceTitle);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(resourceIcon);
        toolbar.setNavigationOnClickListener(listener);
    }

    // ----------------------------------------------------

    /**
     * 設定toolbar的資料
     *
     * @param resourceIcon NavigationIcon
     * @param listener     按下的監聽事件
     */
    protected void setToolbar(int resourceIcon, View.OnClickListener listener) {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(resourceIcon);
        toolbar.setNavigationOnClickListener(listener);
    }

    // ----------------------------------------------------

    /**
     * 直接是返回型態
     */
    public void setBackToolBar() {

        setToolbar(R.drawable.ic_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 設定目前登入的帳號(個人/企業/未登入)
     */
    public void setToolbarChange() {

        toolbarChange.setVisibility(View.VISIBLE);

        if (isCompanyAccount()) {
            toolbarChange.setBackgroundResource(R.drawable.img_toolbar_change_bg_blue);
            toolbarIcon.setImageResource(R.drawable.img_toolbar_company);
            toolbarAccount.setText(getString(R.string.company));
        } else if (sp.getAcctId().length() > 0) {
            toolbarChange.setBackgroundResource(R.drawable.img_toolbar_change_bg_orange);
            toolbarIcon.setImageResource(R.drawable.img_toolbar_personal);
            toolbarAccount.setText(getString(R.string.personal));
        } else {
            toolbarChange.setVisibility(View.INVISIBLE);
        }
    }

    // ----------------------------------------------------

    /**
     * 是否為企業帳號
     *
     * @return
     */
    public boolean isCompanyAccount() {
        return sp.getUserData().getCompanyAccount() != null;
    }

    // ----------------------------------------------------

    /**
     * 直接是返回型態
     *
     * @param resourceTitle toolbar 的圖片
     */
    public void setBackToolBar(int resourceTitle) {

        setToolbar(R.drawable.ic_back, resourceTitle, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // ----------------------------------------------------

    /**
     * 前往Fragment
     *
     * @param containerViewId replace的layoutId
     * @param fragment        fragment
     */
    public void goToFragment(int containerViewId, Fragment fragment) {
        goFragment(containerViewId, fragment, null);
    }

    // ----------------------------------------------------

    /**
     * 前往Fragment
     *
     * @param containerViewId replace的layoutId
     * @param fragment        fragment
     * @param bundle          所需要傳入的參數
     */
    public void goToFragment(int containerViewId, Fragment fragment, Bundle bundle) {
        goFragment(containerViewId, fragment, bundle);
    }

    // ----------------------------------------------------

    /**
     * 開啟google導航
     */
    public void startGoogleMapNavigation(String lat, String lng) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    // ----------------------------------------------------
    private void goFragment(int containerViewId, Fragment fragment, Bundle bundle) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        transaction.replace(containerViewId, fragment);
        transaction.commit();
    }

    // ----------------------------------------------------

    /**
     * 註冊hockeyapp Crashes事件
     */
    private void checkForCrashes() {
        CrashManager.register(this);
    }


    // ----------------------------------------------------

    /**
     * 傳送HockeyAppEvent事件
     *
     * @param event
     * @param count
     */
    public void sendHockeyAppEvent(String event, int count) {

        HashMap<String, String> properties = new HashMap<>();
        properties.put("VersionName", getVersionName());
        properties.put("UUID", sp.getUUID());
        properties.put("AcctId", sp.getAcctId());
        properties.put("Count", String.valueOf(count));

        MetricsManager.trackEvent(event, properties);
    }

    // ----------------------------------------------------

    /**
     * 傳送HockeyAppEvent事件
     *
     * @param event
     */
    public void sendHockeyAppEvent(String event) {

        HashMap<String, String> properties = new HashMap<>();
        properties.put("VersionName", getVersionName());
        properties.put("UUID", sp.getUUID());
        properties.put("AcctId", sp.getAcctId());

        MetricsManager.trackEvent(event, properties);
    }

    // ----------------------------------------------------

    /**
     * 取的APP的VersionName
     *
     * @return
     */
    private String getVersionName() {

        try {

            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            return info.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return "";
    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog Fragment page.
     */
    public DialogFragment showDialogFragment(DialogFragment fragment, Bundle bundle) {

        //跳Dialog頁
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        fragment.setArguments(bundle);
        fragment.show(ft, "dialog");

        return fragment;
    }


    // ----------------------------------------------------

}
