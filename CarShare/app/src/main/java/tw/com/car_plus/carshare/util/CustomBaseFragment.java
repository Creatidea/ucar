package tw.com.car_plus.carshare.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.hockeyapp.android.metrics.MetricsManager;

import java.util.HashMap;

import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.SignInActivity;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.index.IndexActivity;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.record.RentRecordActivity;
import tw.com.car_plus.carshare.rent.reservation_car.ReservationCarActivity;
import tw.com.car_plus.carshare.rent.success.charge.ChargingStationActivity;
import tw.com.car_plus.carshare.user.UserInfoActivity;

/**
 * Created by winni on 2017/3/2.
 */

public abstract class CustomBaseFragment extends Fragment {

    // ----------------------------------------------------
    protected View view;
    protected AppCompatActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IndexActivity) {
            activity = (IndexActivity) context;
            EventCenter.getInstance().sendSetToolbarChangeNull();
        } else if (context instanceof ReservationCarActivity) {
            activity = (ReservationCarActivity) context;
        } else if (context instanceof UserInfoActivity) {
            activity = (UserInfoActivity) context;
        } else if (context instanceof RentRecordActivity) {
            activity = (RentRecordActivity) context;
        } else if (context instanceof ChargingStationActivity) {
            activity = (ChargingStationActivity) context;
        } else if (context instanceof SignInActivity) {
            activity = (SignInActivity) context;
        }
    }

    // ----------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            // initial.
            init();
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        return view;
    }

    // ----------------------------------------------------

    /**
     * initial
     */
    protected abstract void init();


    // ----------------------------------------------------

    /**
     * 是否為企業帳戶
     *
     * @return
     */
    public boolean isCompanyAccount(User user) {
        return user.getCompanyAccount() != null;
    }

    // ----------------------------------------------------

    /**
     * 前往Fragment
     *
     * @param fragment       要進入的Fragment
     * @param addToBackStack 是否需要返回
     * @param bundle         傳送的資料
     */
    public void replaceFragment(Fragment fragment, boolean addToBackStack, Bundle bundle) {

        fragment.setArguments(bundle);
        baseReplace(fragment, addToBackStack);
    }

    // ----------------------------------------------------

    /**
     * 是Fragment裡面又有包含Fragment的狀況時，使用此method
     *
     * @param layoutId       子Fragment的id
     * @param fragment
     * @param addToBackStack
     * @param bundle
     */
    public void replaceFragment(int layoutId, Fragment fragment, boolean addToBackStack, Bundle bundle) {

        fragment.setArguments(bundle);
        baseReplace(layoutId, fragment, addToBackStack);
    }

    // ----------------------------------------------------

    /**
     * 前往Fragment
     *
     * @param fragment       要進入的Fragment
     * @param addToBackStack 是否需要返回
     */
    public void replaceFragment(Fragment fragment, boolean addToBackStack) {

        baseReplace(fragment, addToBackStack);
    }

    // ----------------------------------------------------

    /**
     * 是Fragment裡面又有包含Fragment的狀況時，使用此method
     *
     * @param layoutId       子Fragment的id
     * @param fragment
     * @param addToBackStack
     */
    public void replaceFragment(int layoutId, Fragment fragment, boolean addToBackStack) {

        baseReplace(layoutId, fragment, addToBackStack);
    }

    // ----------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
        System.gc();
    }

    // ----------------------------------------------------

    /**
     * base fragment replace.
     */
    private void baseReplace(Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction;
        transaction = getFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.container_framelayout, fragment);
        transaction.commit();

        if (getFragmentManager() != null) {
            getFragmentManager().executePendingTransactions();
        }
    }

    // ----------------------------------------------------

    /**
     * base fragment replace.
     */
    private void baseReplace(int layoutId, Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction;
        transaction = getFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(layoutId, fragment);
        transaction.commit();

        if (getChildFragmentManager() != null) {
            getChildFragmentManager().executePendingTransactions();
        }
    }

    // ----------------------------------------------------
    public void setView(int resource) {
        this.view = LayoutInflater.from(getActivity()).inflate(resource, null);
        ButterKnife.bind(this, view);
    }

    // ----------------------------------------------------

    /**
     * 前往Activity
     *
     * @param cls
     */
    public void goToActivity(int type, Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(activity, cls);
        startActivityForResult(intent, type);
    }

    // ----------------------------------------------------

    /**
     * 前往Activity
     *
     * @param intent
     * @param type
     * @param cls
     */
    public void goToActivity(Intent intent, int type, Class<?> cls) {
        intent.setClass(activity, cls);
        startActivityForResult(intent, type);
    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog Fragment page.
     */
    public DialogFragment showDialogFragment(DialogFragment fragment, Bundle bundle) {

        //跳Dialog頁
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        // Create and show the dialog.
        fragment.setArguments(bundle);
        fragment.show(ft, "dialog");

        return fragment;
    }

    // ----------------------------------------------------

    /**
     * 開啟google導航
     */
    public void startGoogleMapNavigation(String lat, String lng) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    // ----------------------------------------------------

    /**
     * 傳送HockeyAppEvent事件
     *
     * @param event
     * @param count
     */
    public void sendHockeyAppEvent(String event, int count, String uuid, String acctId) {

        HashMap<String, String> properties = new HashMap<>();
        properties.put("VersionName", getVersionName());
        properties.put("UUID", uuid);
        properties.put("AcctId", acctId);
        properties.put("Count", String.valueOf(count));

        MetricsManager.trackEvent(event, properties);
    }

    // ----------------------------------------------------

    /**
     * 傳送HockeyAppEvent事件
     *
     * @param event
     */
    public void sendHockeyAppEvent(String event, String uuid, String acctId) {

        HashMap<String, String> properties = new HashMap<>();
        properties.put("VersionName", getVersionName());
        properties.put("UUID", uuid);
        properties.put("AcctId", acctId);

        MetricsManager.trackEvent(event, properties);
    }

    // ----------------------------------------------------

    /**
     * 取的APP的VersionName
     *
     * @return
     */
    private String getVersionName() {

        try {

            PackageManager manager = activity.getPackageManager();
            PackageInfo info = manager.getPackageInfo(activity.getPackageName(), 0);
            return info.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return "";
    }

    // ----------------------------------------------------
}
