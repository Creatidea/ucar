package tw.com.car_plus.carshare.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by winni on 2017/3/17.
 */

public class DateParser {

    /**
     * 將utc格式的日期時間轉換成當地時間的格式
     *
     * @param timeFormatUtc
     * @return
     */
    public String getDateTime(String timeFormatUtc) {

        SimpleDateFormat existingUTCFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.TAIWAN);
        SimpleDateFormat requiredFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

        try {

            existingUTCFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = existingUTCFormat.parse(timeFormatUtc);

            String strDate = requiredFormat.format(date);

            return strDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";

    }


    // ----------------------------------------------------

    /**
     * 將utc格式的日期時間轉換成當地時間的格式，指取得日期
     *
     * @param timeFormatUtc
     * @return
     */
    public String getDate(String timeFormatUtc) {

        SimpleDateFormat existingUTCFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.TAIWAN);
        SimpleDateFormat requiredFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN);

        try {

            existingUTCFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = existingUTCFormat.parse(timeFormatUtc);

            String strDate = requiredFormat.format(date);

            return strDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";

    }

    // ----------------------------------------------------

    /**
     * 取得秒
     */
    public long getMillise(String timeFormatUtc) {

        SimpleDateFormat existingUTCFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.TAIWAN);
        SimpleDateFormat requiredFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

        try {

            existingUTCFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = existingUTCFormat.parse(timeFormatUtc);

            return requiredFormat.parse(requiredFormat.format(date)).getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // ----------------------------------------------------
}
