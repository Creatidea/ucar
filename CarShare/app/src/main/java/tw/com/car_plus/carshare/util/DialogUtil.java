package tw.com.car_plus.carshare.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import tw.com.car_plus.carshare.R;

/**
 * Created by winni on 2017/6/5.
 */

public class DialogUtil {

    private Context context;
    private Dialog mDialog;
    private TextView tvMessage;
    private Button btnCancel, btnConfirm;
    private CharSequence message = "", cancelText = "", confirmText = "";
    private View.OnClickListener cancelListener, confirmListener;

    public DialogUtil(Context context) {
        this.context = context;
    }

    // ----------------------------------------------------

    /**
     * 設定Dialog的訊息
     *
     * @param message
     * @return
     */
    public DialogUtil setMessage(CharSequence message) {
        this.message = message;
        return this;
    }

    // ----------------------------------------------------

    /**
     * 設定Dialog的訊息
     *
     * @param textId
     * @return
     */
    public DialogUtil setMessage(@StringRes int textId) {
        message = context.getText(textId);
        return this;
    }

    // ----------------------------------------------------

    /**
     * 設定取消按鈕的文字及按下事件
     *
     * @param text
     * @param listener
     * @return
     */
    public DialogUtil setCancelButton(CharSequence text, View.OnClickListener listener) {
        cancelText = text;
        cancelListener = listener;

        return this;
    }

    // ----------------------------------------------------

    /**
     * 設定確認按鈕的文字級按下事件
     *
     * @param text
     * @param listener
     * @return
     */
    public DialogUtil setConfirmButton(CharSequence text, View.OnClickListener listener) {
        confirmText = text;
        confirmListener = listener;
        return this;

    }

    // ----------------------------------------------------

    /**
     * 設定取消按鈕的文字及按下事件
     *
     * @param textId
     * @param listener
     * @return
     */
    public DialogUtil setCancelButton(@StringRes int textId, View.OnClickListener listener) {
        cancelText = context.getText(textId);
        cancelListener = listener;

        return this;
    }

    // ----------------------------------------------------

    /**
     * 設定確認按鈕的文字級按下事件
     *
     * @param textId
     * @param listener
     * @return
     */
    public DialogUtil setConfirmButton(@StringRes int textId, View.OnClickListener listener) {
        confirmText = context.getText(textId);
        confirmListener = listener;
        return this;

    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog
     *
     * @param isCancelable 是否可以按下螢幕後Dialog即可消失
     */
    public void showDialog(boolean isCancelable) {

        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        mDialog.setCancelable(isCancelable);
        mDialog.setCanceledOnTouchOutside(isCancelable);
        mDialog.setContentView(R.layout.module_dialog);

        tvMessage = (TextView) mDialog.findViewById(R.id.message);
        btnCancel = (Button) mDialog.findViewById(R.id.cancel);
        btnConfirm = (Button) mDialog.findViewById(R.id.confirm);

        tvMessage.setText(message);
        setButtonShow(btnCancel, cancelText, cancelListener);
        setButtonShow(btnConfirm, confirmText, confirmListener);

        mDialog.show();

    }

    // ----------------------------------------------------

    /**
     * 顯示Dialog
     *
     * @param isCancelable 是否可以按下螢幕後Dialog即可消失
     */
    public void showTitleDialog(String title, boolean isCancelable) {

        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        mDialog.setCancelable(isCancelable);
        mDialog.setCanceledOnTouchOutside(isCancelable);
        mDialog.setContentView(R.layout.module_dialog_title);

        TextView tvTitle = (TextView)mDialog.findViewById(R.id.title);
        tvMessage = (TextView) mDialog.findViewById(R.id.message);
        btnCancel = (Button) mDialog.findViewById(R.id.cancel);
        btnConfirm = (Button) mDialog.findViewById(R.id.confirm);

        tvTitle.setText(title);
        tvMessage.setText(message);
        setButtonShow(btnCancel, cancelText, cancelListener);
        setButtonShow(btnConfirm, confirmText, confirmListener);

        mDialog.show();

    }

    // ----------------------------------------------------

    /**
     * 設定按鈕是否需要顯示，以及按鈕文字按下事件的設定
     *
     * @param btn
     * @param text
     * @param listener
     */
    private void setButtonShow(Button btn, CharSequence text, final View.OnClickListener listener) {

        if (text.length() > 0) {
            btn.setVisibility(View.VISIBLE);
            btn.setText(text);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onClick(v);
                    }

                    cleanData();
                    mDialog.dismiss();
                }
            });

        } else {
            btn.setVisibility(View.GONE);
        }

    }

    // ----------------------------------------------------

    /**
     * 清除資料
     */
    private void cleanData() {
        message = "";
        cancelText = "";
        confirmText = "";
        cancelListener = null;
        confirmListener = null;
    }

}
