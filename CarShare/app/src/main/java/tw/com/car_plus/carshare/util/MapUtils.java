package tw.com.car_plus.carshare.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by neil on 2017/3/15.
 */

public class MapUtils {

    // ----------------------------------------------------
    private final int TEXT_SIZE = 20;

    private Context mContext;

    // ----------------------------------------------------
    public MapUtils(Context mContext) {
        this.mContext = mContext;
    }

    // ----------------------------------------------------
    public Bitmap getBitmapMarker(int resourceId, String mText) {

        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);

            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();

            // set default bitmap config if none.
            if (bitmapConfig == null)
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;

            bitmap = bitmap.copy(bitmapConfig, true);

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.WHITE);
            paint.setTextSize((int) (TEXT_SIZE * scale));
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);

            // draw text to the Canvas center.
            drawCenter(canvas, paint, mText, bitmap);

            return bitmap;

        } catch (Exception e) {
            return null;
        }
    }

    // ----------------------------------------------------

    /**
     * draw text to the Canvas center.
     * <p>
     * reference link : http://stackoverflow.com/questions/11120392/android-center-text-on-canvas
     */
    private void drawCenter(Canvas canvas, Paint paint, String mText, Bitmap bitmap) {

        Rect bounds = new Rect();
        canvas.getClipBounds(bounds);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(mText, 0, mText.length(), bounds);

        float x = (bitmap.getWidth() - bounds.width()) / 2f - bounds.left;
        float y = (bitmap.getHeight() + bounds.height()) / 2f;

        //因為是長方形，所以Y軸需調整中心點
        canvas.drawText(mText, x, y - 9f, paint);
    }
    // ----------------------------------------------------
}
