package tw.com.car_plus.carshare.util;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.com.car_plus.carshare.R;

/**
 * Created by winni on 2018/3/29.
 */

public class PhotoViewActivity extends AppCompatActivity {

    @BindView(R.id.photo_view)
    PhotoView photoView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photoview);
        ButterKnife.bind(this);
        setImage();
    }

    // ----------------------------------------------------
    private void setImage() {
        Glide.with(this).load(getIntent().getStringExtra("imagePath")).into(photoView);
    }
}
