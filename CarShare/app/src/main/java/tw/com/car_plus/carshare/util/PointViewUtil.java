package tw.com.car_plus.carshare.util;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by winni on 2017/10/17.
 */

public class PointViewUtil {

    private Context context;
    private LinearLayout pointLayout;
    private ArrayList<ImageView> imageViews;
    private int selectImage = -1;
    private int size;

    public PointViewUtil(Context context, LinearLayout pointLayout) {
        imageViews = new ArrayList<>();
        this.pointLayout = pointLayout;
        this.context = context;
    }

    // ----------------------------------------------------

    /**
     * 設定指示器的點的數量
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    // ----------------------------------------------------

    /**
     * 取得Imageview的array
     *
     * @return
     */
    public ArrayList<ImageView> getImageViews() {
        return imageViews;
    }

    // ----------------------------------------------------

    /**
     * 設定指示器點的選取圖片
     *
     * @param resource select的Image
     */
    public void setSelectPointImage(int resource) {
        selectImage = resource;
    }

    // ----------------------------------------------------

    /**
     * 設定要選取的位置
     *
     * @param position
     */
    public void setSelectPoint(int position) {

        if (imageViews.size() > 0) {
            for (ImageView image : imageViews) {
                image.setSelected(false);
            }

            imageViews.get(position).setSelected(true);
        }

    }

    // ----------------------------------------------------

    /**
     * 建置指示器的畫面
     */
    public void setPointView() {

        if (selectImage != -1) {

            for (int i = 0; i < size; i++) {
                ImageView image = new ImageView(context);
                image.setImageResource(selectImage);
                image.setPadding(5, 0, 5, 0);
                imageViews.add(image);
                pointLayout.addView(image);
            }
        }
    }

}
