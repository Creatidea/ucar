package tw.com.car_plus.carshare.util;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import tw.com.car_plus.carshare.R;

/**
 * 優惠時間
 * Created by winni on 2018/1/15.
 */

public class PromotionDateUtil {

    private Context context;

    public PromotionDateUtil(Context context) {
        this.context = context;
    }

    // ----------------------------------------------------

    /**
     * 取得該顯示的起訖
     *
     * @return
     */
    public String getDate(String startDate, String endDate) {

        //起訖都沒有(當年的第一天到當年的最後一天)
        if (startDate.isEmpty() && endDate.isEmpty()) {
            return String.format(context.getString(R.string.preferential_date), getFirstDate(), getLastDate());
        }
        //開始日沒有但結束日有(當天日到結束日)
        else if (startDate.isEmpty() && !endDate.isEmpty()) {
            return String.format(context.getString(R.string.preferential_date), getNowDate(), endDate);
        }
        //開始日有但結束日沒有(開始日到當年的最後一天)
        else if (!startDate.isEmpty() && endDate.isEmpty()) {
            return String.format(context.getString(R.string.preferential_date), startDate, getLastDate());
        } else {
            return String.format(context.getString(R.string.preferential_date), startDate, endDate);
        }
    }


    // ----------------------------------------------------

    /**
     * 取得今日的日期
     *
     * @return
     */
    private String getNowDate() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN);
        return dateFormat.format(now);
    }

    // ----------------------------------------------------

    /**
     * 取得今年的第一天
     *
     * @return
     */
    private String getFirstDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN);
        return dateFormat.format(calendar.getTime());
    }


    // ----------------------------------------------------

    /**
     * 取得今年最後一天
     *
     * @return
     */
    private String getLastDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN);
        return dateFormat.format(calendar.getTime());
    }

}
