package tw.com.car_plus.carshare.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by winni on 2017/3/7.
 */

public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static MessageListener mMessageListener;

    public SMSBroadcastReceiver() {
        super();
    }

    // ----------------------------------------------------
    @Override
    public void onReceive(Context context, Intent intent) {

        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        for (Object pdu : pdus) {

            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
            String content = smsMessage.getMessageBody();

            if (mMessageListener != null) {
                mMessageListener.OnReceived(getIdentifyingCode(content));
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 取得驗證碼
     *
     * @param str
     * @return
     */
    public String getIdentifyingCode(String str) {

        Pattern continuousNumberPattern = Pattern.compile("[A-Z0-9]{10}");
        Matcher m = continuousNumberPattern.matcher(str);
        String dynamicPassword = "";
        while (m.find()) {
            dynamicPassword = m.group();
        }
        return dynamicPassword;
    }

    // ----------------------------------------------------
    public interface MessageListener {
        void OnReceived(String code);
    }

    // ----------------------------------------------------

    /**
     * 設定監聽事件
     *
     * @param messageListener
     */
    public void setOnReceivedMessageListener(MessageListener messageListener) {
        this.mMessageListener = messageListener;
    }

}
