package tw.com.car_plus.carshare.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.neilchen.complextoolkit.util.json.JSONParser;
import com.neilchen.complextoolkit.uuid.DeviceUuidFactory;

import java.util.UUID;

import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.rent.return_car.PayActivity;

import static tw.com.car_plus.carshare.rent.return_car.PayActivity.INVOICE_TYPE_1;
import static tw.com.car_plus.carshare.rent.return_car.PayActivity.INVOICE_TYPE_2;
import static tw.com.car_plus.carshare.rent.return_car.PayActivity.INVOICE_TYPE_3;

/**
 * Created by winni on 2017/3/6.
 */

public class SharedPreferenceUtil {

    //--------------------------------------------------
    private final String _NAME = "_CarShare";
    private final String _User = "User";
    private final String _AcctId = "AcctId";
    private final String _Account = "Account";
    private final String _IsSaveAccount = "IsSaveAccount";
    private final String _Password = "Password";
    private final String _CompanyTaxID = "CompanyTaxID";
    private final String _UUID = "UUID";
    private final String _REGISTER = "Register";
    private final String _Token = "Token";
    private final String _FIRST = "First";
    private final String _SECURITY = "Security";
    private final String _InvoiceType = "InvoiceType";

    //--------------------------------------------------
    private JSONParser parser;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    //--------------------------------------------------
    public SharedPreferenceUtil(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        parser = new JSONParser();
    }
    //--------------------------------------------------

    /***
     * 紀錄所選發票類型
     * @param invoiceType
     */
    public void setInvoiceType(@PayActivity.InvoiceType int invoiceType) {
        editor.putInt(_InvoiceType, invoiceType).commit();
    }
    //--------------------------------------------------

    /**
     * 取得前一次所選的發票類型
     *
     * @return
     */
    @PayActivity.InvoiceType
    public int getInvoiceType() {
        switch (sharedPreferences.getInt(_InvoiceType, INVOICE_TYPE_1)) {
            default:
            case INVOICE_TYPE_1:
                return INVOICE_TYPE_1;
            case INVOICE_TYPE_2:
                return INVOICE_TYPE_2;
            case INVOICE_TYPE_3:
                return INVOICE_TYPE_3;
        }
    }
    //--------------------------------------------------

    /**
     * 設定使用者的acctId
     *
     * @param acctId
     */
    public void setAcctId(String acctId) {
        editor.putString(_AcctId, acctId).commit();
    }

    //--------------------------------------------------

    /**
     * 取的使用者的acctId
     *
     * @return
     */
    public String getAcctId() {
        return sharedPreferences.getString(_AcctId, "");
    }

    //--------------------------------------------------

    /**
     * 設定帳號
     *
     * @param account
     */
    public void setAccount(String account) {
        editor.putString(_Account, account).commit();
    }

    //--------------------------------------------------

    /**
     * 取得存入帳號
     *
     * @return
     */
    public String getAccount() {
        return sharedPreferences.getString(_Account, "");
    }

    //--------------------------------------------------

    /**
     * 設定密碼
     *
     * @param password
     */
    public void setPassword(String password) {
        editor.putString(_Password, password).commit();
    }

    //--------------------------------------------------

    /**
     * 取得存入的密碼
     *
     * @return
     */
    public String getPassword() {
        return sharedPreferences.getString(_Password, "");
    }


    //--------------------------------------------------

    /**
     * 設定公司統一編號
     *
     * @param id
     */
    public void setCompanyTaxID(String id) {
        editor.putString(_CompanyTaxID, id).commit();
    }

    //--------------------------------------------------

    /**
     * 取得公司統一編號
     *
     * @return
     */
    public String getCompanyTaxID() {
        return sharedPreferences.getString(_CompanyTaxID, "");
    }


    //--------------------------------------------------

    /**
     * 設定是否要存入帳號
     *
     * @param isSave
     */
    public void setSaveAccount(boolean isSave) {
        editor.putBoolean(_IsSaveAccount, isSave).commit();
    }

    //--------------------------------------------------

    /**
     * 取得是否有設定存入帳號
     *
     * @return
     */
    public boolean getSaveAccount() {
        return sharedPreferences.getBoolean(_IsSaveAccount, false);
    }

    //--------------------------------------------------

    /**
     * 設定UserData
     *
     * @param userData user
     * @see User
     */
    public void setUserData(User userData) {

        if (userData == null) {
            editor.remove(_User).commit();
        } else {
            if (editor.putString(_User, new Gson().toJson(userData)).commit()) {
                setAcctId(userData.getAcctId());
            }
        }
    }

    //--------------------------------------------------

    /**
     * 取得User資料
     *
     * @return
     */
    public User getUserData() {
        return parser.getJSONData(sharedPreferences.getString(_User, "{}"), User.class);
    }

    //--------------------------------------------------

    /**
     * 設定資安是否成功狀態
     *
     * @param isSecurity
     */
    public void setSecurityStatus(boolean isSecurity) {
        editor.putBoolean(_SECURITY, isSecurity).commit();
    }

    //--------------------------------------------------

    /**
     * 取得資安是否成功的狀態
     *
     * @return
     */
    public boolean getSecurityStatus() {
        return sharedPreferences.getBoolean(_SECURITY, false);
    }

    //--------------------------------------------------

    /**
     * 清除所有資料
     */
    public void cleanUserData() {
        editor.remove(_AcctId).commit();

        if (!getSaveAccount()) {
            editor.remove(_Account).commit();
        }

        editor.remove(_Password).commit();
        editor.remove(_User).commit();
        editor.remove(_REGISTER).commit();
    }

    //--------------------------------------------------

    /**
     * 設定UUID
     *
     * @param uuid
     */
    public void setUUID(String uuid) {
        editor.putString(_UUID, uuid).commit();
    }

    //--------------------------------------------------

    /**
     * 取得UUID
     *
     * @return
     */
    public String getUUID() {

        String uuid;

        if (sharedPreferences.getString(_UUID, "").length() > 0) {
            uuid = sharedPreferences.getString(_UUID, "");
        } else {
            uuid = new DeviceUuidFactory(context).getUuid().toString();
            setUUID(uuid);
        }

        return uuid;
    }

    //--------------------------------------------------

    /**
     * 設定是否有註冊SecurityServer
     *
     * @param isRegister
     */
    public void setRegisterSecurityServer(boolean isRegister) {
        editor.putBoolean(_REGISTER, isRegister).commit();
    }

    //--------------------------------------------------

    /**
     * 取得是否有註冊過SecurityServer
     */
    public boolean getRegisterSecurityServer() {
        return sharedPreferences.getBoolean(_REGISTER, false);
    }

    //--------------------------------------------------

    /**
     * 設定裝置Token
     *
     * @param token
     */
    public void setToken(String token) {
        editor.putString(_Token, token).commit();
    }

    //--------------------------------------------------

    /**
     * 取得裝置Token
     *
     * @return
     */
    public String getToken() {
        return sharedPreferences.getString(_Token, "");
    }

    //--------------------------------------------------

    /**
     * 設定是否為第一次進入App
     *
     * @param isFirst
     */
    public void setFirst(boolean isFirst) {
        editor.putBoolean(_FIRST, isFirst).commit();
    }

    //--------------------------------------------------

    /**
     * 取得是否為第一次進入App
     *
     * @return
     */
    public boolean getFirst() {
        return sharedPreferences.getBoolean(_FIRST, true);
    }

    //--------------------------------------------------

}
