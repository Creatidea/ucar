package tw.com.car_plus.carshare.util;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.login.LoginFragment;
import tw.com.car_plus.carshare.login.SignInCheckDialog;

/**
 * Created by neil on 2017/11/28.
 * <p>
 * 使用者登入檢查
 */

public class UserSignInChecker {

    // ----------------------------------------------------
    private AppCompatActivity activity;
    private SharedPreferenceUtil sp;
    private SignInCheckDialog dialog;

    // ----------------------------------------------------
    public UserSignInChecker(AppCompatActivity activity) {
        this.activity = activity;
        sp = new SharedPreferenceUtil(activity);
    }

    // ----------------------------------------------------

    /**
     * 判斷使用者是否已經登入過
     */
    public boolean isSignIn() {

        if (sp.getAcctId().length() == 0) {
            showDialog();
            return false;
        } else {
            return true;
        }
    }

    // ----------------------------------------------------

    /**
     * 顯示提示視窗
     */
    private void showDialog() {

        dialog = new SignInCheckDialog(activity);
        dialog.show();
        dialog.setOnSignInClickListener(new SignInCheckDialog.OnSignInClickListener() {
            @Override
            public void onSignIn() {

                // 跳至登入頁面
                FragmentTransaction tran = activity.getSupportFragmentManager().beginTransaction();
                tran.replace(R.id.container_framelayout, new LoginFragment());
                tran.commit();
            }
        });
    }

    // ----------------------------------------------------
}
