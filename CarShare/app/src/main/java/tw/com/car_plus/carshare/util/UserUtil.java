package tw.com.car_plus.carshare.util;

import android.content.Context;

import tw.com.car_plus.carshare.connect.login.LoginConnect;
import tw.com.car_plus.carshare.login.model.User;
import tw.com.car_plus.carshare.page.PageCenter;

/**
 * Created by winni on 2017/5/26.
 */

public class UserUtil {

    private Context context;
    private SharedPreferenceUtil sp;
    private User newUser, user;
    private PageCenter pageCenter;

    // ----------------------------------------------------
    public UserUtil(Context context, SharedPreferenceUtil sp) {
        this.context = context;
        this.sp = sp;
        pageCenter = new PageCenter(context);
        user = sp.getUserData();
    }

    // ----------------------------------------------------

    /**
     * 設定新的User
     *
     * @param user
     */
    public void setUser(User user) {
        this.newUser = user;
        pageCenter.setUser(user);
    }

    // ----------------------------------------------------

    /**
     * 取得新的User
     *
     * @return
     */
    public User getUser() {

        if (newUser != null) {
            return newUser;
        }

        return sp.getUserData();
    }

    // ----------------------------------------------------

    /**
     * 設定User的帳密資料
     */
    public void setAccountInfo() {
        newUser.setLoginId(user.getLoginId());
        newUser.setPassword(user.getPassword());
    }

    // ----------------------------------------------------

    /**
     * 取得頁面中心
     *
     * @return
     */
    public PageCenter getPageCenter() {
        return pageCenter;
    }


    // ----------------------------------------------------

    /**
     * 切換目前帳號(個人/企業)
     */
    public void changeAccount(int type) {

        LoginConnect loginConnect = new LoginConnect(context);
        User user = sp.getUserData();

        if (sp.getCompanyTaxID().length() > 0) {

            if (user.getCompanyAccount() != null) {
                loginConnect.connectLogin(type, user.getLoginId(), user.getPassword());
            } else {
                loginConnect.connectLoginCompany(type, user.getLoginId(), user.getPassword(), sp.getCompanyTaxID());
            }
        }
    }

    // ----------------------------------------------------

}
