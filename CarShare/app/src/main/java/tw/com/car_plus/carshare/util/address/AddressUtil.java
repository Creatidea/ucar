package tw.com.car_plus.carshare.util.address;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tw.com.car_plus.carshare.connect.AddressConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.register.model.Area;
import tw.com.car_plus.carshare.register.model.City;

/**
 * Created by neil on 2017/4/13.
 */

public class AddressUtil {

    // ----------------------------------------------------
    private AddressConnect connect;
    private Context context;
    private List<City> cities;
    private List<Area> areas;

    // ----------------------------------------------------
    public AddressUtil(Context context) {
        this.context = context;
        connect = new AddressConnect(context);
    }

    // ----------------------------------------------------
    public void loadCity() {
        connect.connectGetCity();
    }

    // ----------------------------------------------------
    public void loadArea(int cityType) {
        connect.connectGetArea(cityType);
    }

    // ----------------------------------------------------
    public String findCity(int cityId) {

        for (City city : cities) {
            if (city.getCityId() == cityId) {
                return city.getCityName();
            }
        }
        return "";
    }

    // ----------------------------------------------------
    public String findArea(int areaId) {

        if (areas == null) {
            return "";
        }
        for (Area area : areas) {
            if (area.getAreaId() == areaId) {
                return area.getAreaName();
            }
        }
        return "";
    }

    // ----------------------------------------------------
    public int findCity(String city) {

        for (int count = 0; count < cities.size(); count++) {
            if (cities.get(count).getCityName().indexOf(city) != -1) {
                return cities.get(count).getCityId();
            }
        }
        return -1;
    }

    // ----------------------------------------------------
    public String findCityAndArea(int cityId, int areaId) {
        return findCity(cityId) + findArea(areaId);
    }

    // ----------------------------------------------------
    public int getCityId(Location userLocation) {

        Geocoder geo = new Geocoder(context, Locale.getDefault());
        int TAIPEI = 1;
        int cityCode = TAIPEI;

        try {
            List<Address> addresses = geo.getFromLocation(userLocation.getLatitude(), userLocation.getLongitude(), 1);

            if (addresses.size() > 0) {

                String strCity = addresses.get(0).getAdminArea();

                if (!strCity.equals("null")) {

                    if (strCity.contains("台")) {
                        strCity = strCity.replace("台", "臺");
                    }
                }

                //城市
                if ((cityCode = findCity(strCity)) != -1) {
                    return cityCode;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cityCode;
    }


    // ----------------------------------------------------

    /**
     * 取得使用者地址縣市列表的位置
     *
     * @param cityId
     * @return
     */
    public int getCityPosition(int cityId) {

        for (int i = 0; i < cities.size(); i++) {
            if (cities.get(i).getCityId() == cityId) {
                return i;
            }
        }
        return 0;
    }

    // ----------------------------------------------------

    /**
     * 取得使用者地址區域列表的位置
     *
     * @param areaId
     * @return
     */
    public int getAreaPosition(List<Area> areas, int areaId) {
        for (int i = 0; i < areas.size(); i++) {
            if (areas.get(i).getAreaId() == areaId) {
                return i;
            }
        }
        return 0;
    }

    // ----------------------------------------------------
    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    // ----------------------------------------------------
    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    // ----------------------------------------------------
    public List<City> getCities() {
        return cities;
    }

    // ----------------------------------------------------
    public List<Area> getAreas() {
        return areas;
    }
    // ----------------------------------------------------
}
