package tw.com.car_plus.carshare.util.ble;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;


import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.service.BluetoothLeService;
import tw.com.car_plus.carshare.util.ble.model.BleDevice;

/**
 * Created by winni on 2017/7/27.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class BLEScanUtil {

    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 5000;

    private Activity activity;
    private BluetoothAdapter mBluetoothAdapter;
    private LoadingDialogManager loadingDialogManager;
    private OnScanBleListener onScanBleListener;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;
    private ScanCallback leScanCallback;
    private Handler mHandler;
    private ArrayList<BleDevice> bluetoothDevices;
    private String macAddress = "";
    private boolean isSearch = false;
    private int carVigType;

    public BLEScanUtil(Activity activity, int carVigType) {
        this.activity = activity;
        this.carVigType = carVigType;
        mHandler = new Handler();
        loadingDialogManager = new LoadingDialogManager(activity);
        bluetoothDevices = new ArrayList<>();

        initBluetoothAdapter();
        initScanData();
    }

    // ----------------------------------------------------
    private void initBluetoothAdapter() {
        final BluetoothManager bluetoothManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    // ----------------------------------------------------
    private void initScanData() {

        if (CarVigType.isGeneralVIG(carVigType)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                leScanCallback = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        super.onScanResult(callbackType, result);
                        boolean duplicate = false;
                        for (BleDevice d : bluetoothDevices) {
                            if (d.getAddress().equals(result.getDevice().getAddress()))
                                duplicate = true;
                        }
                        if (!duplicate) {
                            bluetoothDevices.add(new BleDevice(result.getDevice().getName(), result.getDevice().getAddress(), result.getRssi()));
                        }
                    }

                    @Override
                    public void onBatchScanResults(List<ScanResult> results) {
                        super.onBatchScanResults(results);
                    }

                    @Override
                    public void onScanFailed(int errorCode) {
                        super.onScanFailed(errorCode);
                    }
                };

            } else {
                setOldScan();
            }

        } else {
            setOldScan();
        }
    }


    // ----------------------------------------------------

    /**
     * 是否支援Ble
     *
     * @return
     */
    public boolean isSupportedBle() {

        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            return false;
        } else if (mBluetoothAdapter == null) {
            return false;
        }

        return true;
    }

    // ----------------------------------------------------

    /**
     * 是否開啟Ble
     *
     * @return
     */
    public boolean isOpenBle() {

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                return false;
            }

            return false;
        }

        return true;
    }

    // ----------------------------------------------------

    /**
     * 開始搜尋
     *
     * @param macAddress
     * @param onScanBleListener
     */
    public void scanBle(String macAddress, @NonNull OnScanBleListener onScanBleListener) {

        this.macAddress = macAddress;
        this.onScanBleListener = onScanBleListener;

        if (isSupportedBle()) {

            if (isOpenBle()) {

                if (isLocationEnable()) {

                    bluetoothDevices.clear();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scanLeDevice(true);
                        }
                    }, 500);

                } else {
                    onScanBleListener.OnNotOpenLocation();
                }

            } else {
                onScanBleListener.OnNotOpenBle();
            }

        } else {
            onScanBleListener.OnNotSupportedBle();
        }

    }

    // ----------------------------------------------------

    /**
     * 停止搜尋
     */
    public void stopScanBle() {
        scanLeDevice(false);
    }

    // ----------------------------------------------------

    /**
     * 檢查搜尋到的ble是否有要連線的vig
     */
    private void checkSearch() {

        for (int i = 0; i < bluetoothDevices.size(); i++) {

            BleDevice device = bluetoothDevices.get(i);

            if (device.getAddress().equals(macAddress)) {
                isSearch = true;
                Log.e("BLEScanUtil", device.getRssi() + ">>");
                if (device.getRssi() <= -85) {
                    onScanBleListener.OnBLESignalLow();
                } else {
                    onScanBleListener.OnStopScanBle(true);
                }
                break;
            }
        }

        if (!isSearch) {
            onScanBleListener.OnStopScanBle(false);
        }
    }

    // ----------------------------------------------------
    private void scanLeDevice(final boolean enable) {

        loadingDialogManager.show(activity.getString(R.string.ble_scan));

        if (enable) {

            isSearch = false;
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadingDialogManager.dismiss();
                    stopScan();
                    checkSearch();
                }
            }, SCAN_PERIOD);
            startScan();
        } else {
            loadingDialogManager.dismiss();
            stopScan();
        }
    }

    // ----------------------------------------------------

    /**
     * 設定舊版本的掃描
     */
    private void setOldScan() {

        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

            @Override
            public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bluetoothDevices.add(new BleDevice(device.getName(), device.getAddress(), rssi));
                    }
                });
            }
        };
    }

    // ----------------------------------------------------

    /**
     * 開始搜尋
     */
    private void startScan() {

        if (mBluetoothAdapter != null) {

            if (CarVigType.isGeneralVIG(carVigType)) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBluetoothAdapter.getBluetoothLeScanner().startScan(leScanCallback);
                } else {
                    mBluetoothAdapter.startLeScan(new UUID[]{BluetoothLeService.SERVICE_UUID}, mLeScanCallback);
                }

            } else {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }

        } else {
            initBluetoothAdapter();
            startScan();
        }
    }

    // ----------------------------------------------------

    /**
     * 停止搜尋
     */
    private void stopScan() {

        if (mBluetoothAdapter != null) {

            if (CarVigType.isGeneralVIG(carVigType)) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBluetoothAdapter.getBluetoothLeScanner().stopScan(leScanCallback);
                } else {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }

            } else {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }

        } else {
            initBluetoothAdapter();
            stopScanBle();
        }
    }

    // ----------------------------------------------------

    /**
     * 檢查是否有開啟定位
     *
     * @return
     */
    public boolean isLocationEnable() {
        LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // ----------------------------------------------------
    public interface OnScanBleListener {

        void OnNotSupportedBle();

        void OnNotOpenBle();

        void OnStopScanBle(boolean isSearch);

        void OnBLESignalLow();

        void OnNotOpenLocation();

    }

}
