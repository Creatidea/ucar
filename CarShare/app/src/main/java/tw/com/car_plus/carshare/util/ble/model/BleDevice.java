package tw.com.car_plus.carshare.util.ble.model;

/**
 * Created by winni on 2017/10/16.
 */

public class BleDevice {

    private String name;
    private String address;
    private int rssi;

    public BleDevice(String name, String address, int rssi) {
        this.name = name;
        this.address = address;
        this.rssi = rssi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }
}
