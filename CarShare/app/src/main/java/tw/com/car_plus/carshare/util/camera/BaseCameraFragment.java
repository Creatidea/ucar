package tw.com.car_plus.carshare.util.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.neilchen.complextoolkit.util.loadingdialog.LoadingDialogManager;
import com.neilchen.complextoolkit.util.net.NetworkChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import tw.com.car_plus.carshare.BuildConfig;
import tw.com.car_plus.carshare.connect.FileUploadConnect;
import tw.com.car_plus.carshare.util.CustomBaseFragment;

/**
 * Created by winni on 2017/3/18.
 * 有開啟相機的CameraFragment可以繼承
 */
public abstract class BaseCameraFragment extends CustomBaseFragment {

    protected NetworkChecker networkChecker;
    protected FileUploadConnect fileUploadConnect;
    protected LoadingDialogManager loadingDialogManager;
    protected File currentImageFile;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        networkChecker = NetworkChecker.getInstance(activity);
        fileUploadConnect = new FileUploadConnect(activity);
        loadingDialogManager = new LoadingDialogManager(activity);
    }

    // ------------------------------------------------

    /**
     * 拍照
     *
     * @param type
     */
    protected void takePictures(int type) {

        File dir = new File(Environment.getExternalStorageDirectory(), "pictures");
        if (dir.exists()) {
            dir.mkdirs();
        }

        currentImageFile = new File(dir, System.currentTimeMillis() + ".jpg");
        if (!currentImageFile.exists()) {
            try {
                currentImageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, getUri());
        this.startActivityForResult(intent, type);
    }

    // ----------------------------------------------------

    /**
     * 壓縮照片(長寬為640*480)
     *
     * @param compressionPictureListener 完成壓縮照片的監聽
     */
    protected void compressionPicture(final CompressionPictureListener compressionPictureListener) {

        Glide.with(this)
                .load(getUri())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(640, 480) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {

                        try {
                            FileOutputStream fOut = new FileOutputStream(currentImageFile);
                            resource.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (compressionPictureListener != null) {
                            compressionPictureListener.compressionListener();
                        }
                    }
                });

    }


    // ------------------------------------------------

    /**
     * 取得file的uri
     *
     * @return
     */
    protected Uri getUri() {
        Uri uri;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", currentImageFile);
        } else {
            uri = Uri.fromFile(currentImageFile);
        }
        return uri;
    }

    // ----------------------------------------------------

    /**
     * 清除照片
     *
     * @param paths
     */
    protected void cleanPicture(List<String> paths) {

        for (String path : paths) {
            if (path.length() != 0) {
                new File(path).delete();
            }
        }
    }

    // ----------------------------------------------------

    /**
     * 清除照片
     *
     * @param path
     */
    protected void cleanPicture(String path) {

        if (path.length() != 0) {
            new File(path).delete();
        }
    }

    // ----------------------------------------------------

    /**
     * 將圖片設定到指定的imageview,並回傳路徑
     *
     * @param imageView
     * @return
     */
    protected String setImageView(ImageView imageView) {
        Glide.with(this).load(getUri()).into(imageView);
        return currentImageFile.getPath();
    }

    // ----------------------------------------------------
    public interface CompressionPictureListener {
        void compressionListener();
    }

}
