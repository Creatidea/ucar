package tw.com.car_plus.carshare.util.command;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.Toast;


import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;

/**
 * Created by neil on 2017/6/27.
 */

public class CarController implements BluetoothSPP.BluetoothConnectionListener {

    // ----------------------------------------------------
    private Fragment fragment;
    public BluetoothSPP bt;
    private String message;

    // ----------------------------------------------------
    public CarController(Fragment fragment) {
        this.fragment = fragment;
        init();
    }

    // ----------------------------------------------------
    private void init() {

        bt = new BluetoothSPP(fragment.getActivity());
        bt.setBluetoothConnectionListener(this);
    }

    // ----------------------------------------------------
    private void showToast(String message) {
        Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    // ----------------------------------------------------

    /**
     * door unlock.
     */
    public byte[] openCommand() {

        byte[] data;

        byte start = (byte) 0xA5;
        byte length = 0x0C;
        byte commandId = 0x02;
        byte[] openCommand = new byte[]{
                0x64, 0x6F, 0x6F
                , 0x72, 0x75, 0x6E
                , 0x6C, 0x6F, 0x63
                , 0x6B};


        data = new byte[12];
        data[0] = start;
        data[1] = length;
        data[2] = commandId;

        for (int i = 0; i < openCommand.length; i++) {
            data[i + 2] = openCommand[i];
        }

        return data;
    }

    // ------------------------------------------------

    /**
     * door lock.
     */
    public byte[] closeCommand() {

        byte[] data;

        byte start = (byte) 0xA5;
        byte length = 0x0A;
        byte commandId = 0x02;
        byte[] closeCommand = new byte[]{
                0x64, 0x6F, 0x6F
                , 0x72, 0x6C, 0x6F
                , 0x63, 0x6B};

        data = new byte[12];
        data[0] = start;
        data[1] = length;
        data[2] = commandId;

        for (int i = 0; i < closeCommand.length; i++) {
            data[i + 2] = closeCommand[i];
        }

        return data;
    }
    // ----------------------------------------------------

    /**
     * start service.
     */
    public void onStart() {

        if (!bt.isBluetoothEnabled()) {
            bt.enable();
        } else {
            if (!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER);
            }
        }
    }
    // ----------------------------------------------------

    /**
     * scan device then show list.
     */
    public void scanDevice() {

        Intent intent = new Intent(fragment.getActivity(), DeviceList.class);
        fragment.startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
    }

    // ----------------------------------------------------

    /**
     * check bluetooth is enable.
     */
    public boolean isBluetoothAvailable() {
        return bt.isBluetoothAvailable();
    }

    // ----------------------------------------------------

    /**
     * connect to device.
     *
     * @param data device data.
     */
    public void connect(Intent data) {
        bt.connect(data);
    }

    // ----------------------------------------------------

    /**
     * connect success of start service.
     */
    public void startService(int resultCode) {

        if (resultCode == Activity.RESULT_OK) {
            bt.setupService();
            bt.startService(BluetoothState.DEVICE_OTHER);
        } else {
            showToast("Bluetooth was not enabled.");
        }
    }

    // ----------------------------------------------------

    /**
     * send open/close command to device.
     */
    public void sendCommandToDevice(byte[] data) {
        bt.send(data, false);
    }

    // ----------------------------------------------------

    /**
     * get connect status message.
     */
    public String getMessage() {
        return message;
    }

    // ----------------------------------------------------

    /**
     * stop any thread.
     */
    public void stopService() {
        bt.stopService();
    }

    // ----------------------------------------------------
    @Override
    public void onDeviceConnected(String name, String address) {
        message = "Connected to :" + name;
        showToast(message);
    }

    @Override
    public void onDeviceDisconnected() {
        message = "Connection failed: no serial service.";
        showToast(message);
    }

    @Override
    public void onDeviceConnectionFailed() {
        message = "Not connect";
        showToast(message);
    }

    // ----------------------------------------------------

}
