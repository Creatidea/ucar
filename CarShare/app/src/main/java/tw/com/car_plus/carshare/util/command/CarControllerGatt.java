package tw.com.car_plus.carshare.util.command;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import tw.com.car_plus.carshare.R;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.service.BluetoothLeService;
import tw.com.car_plus.carshare.util.command.model.MobiletronSecurityData;
import tw.com.car_plus.carshare.util.db.DBUtil;
import tw.com.car_plus.carshare.util.db.SecretKeyData;
import tw.com.car_plus.carshare.util.security.AESUtil;
import tw.com.car_plus.carshare.util.security.SecurityUtil;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by winni on 2017/7/10.
 */

public class CarControllerGatt {

    public static final int NONE = -1;
    public static final int BY_PASS = 0;
    public static final int DOOR_UNLOCK = 1;
    public static final int DOOR_LOCK = 2;
    public static final int WRITE_STATUS_SERVER_ERROR = 0;
    public static final int WRITE_STATUS_SUCCESS = 1;
    public static final int WRITE_STATUS_ERROR = 2;

    private final static String TAG = "CarControllerGatt";

    private Activity activity;
    private BluetoothLeService mBluetoothLeService;
    private SecurityUtil securityUtil;
    private AESUtil aesUtil;
    private OnConnectListener onConnectListener;
    private boolean isFirst = true;
    private String mDeviceAddress, vigId;
    private boolean mConnected = false;
    @CarVigType.CarVig
    private int carVigType;

    public CarControllerGatt(Activity activity, @CarVigType.CarVig int type) {
        this.activity = activity;
        this.carVigType = type;
        securityUtil = new SecurityUtil(activity);
        aesUtil = new AESUtil();
    }

    // ----------------------------------------------------

    /**
     * 設定藍牙的MacAddress
     *
     * @param mDeviceAddress
     */
    public void setDeviceAddress(String mDeviceAddress) {
        this.mDeviceAddress = mDeviceAddress;
    }

    // ----------------------------------------------------

    /**
     * 設定VigId
     *
     * @param vigId
     */
    public void setVigId(String vigId) {
        this.vigId = vigId;
    }

    // ----------------------------------------------------

    /**
     * 啟動onStartBluetoothLeService
     */
    public void onStartBluetoothLeService() {
        Intent gattServiceIntent = new Intent(activity, BluetoothLeService.class);
        activity.bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    // ----------------------------------------------------

    /**
     * 註冊Receiver
     */
    public void registerGATT() {
        activity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    // ----------------------------------------------------

    /**
     * 將BLEService停止
     */
    public void onDestroy() {
        activity.unregisterReceiver(mGattUpdateReceiver);
        if (mBluetoothLeService != null) {
            activity.unbindService(mServiceConnection);
            mBluetoothLeService = null;
        }
    }

    // ----------------------------------------------------

    /**
     * BLEService建立連線
     */
    public void connect() {

        if (mBluetoothLeService != null) {
            boolean result = mBluetoothLeService.connect(mDeviceAddress, carVigType);
            if (!result) {
                onConnectListener.disconnected();
            }
            Log.e(TAG, "Connect request result=" + result);
        }
    }

    // ----------------------------------------------------

    /**
     * BLEService取消連線
     */
    public void disconnect() {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnect();
        }
    }

    // ----------------------------------------------------

    /**
     * 取得SecurityUtil
     *
     * @return
     */
    public SecurityUtil getSecurityUtil() {
        return securityUtil;
    }

    // ----------------------------------------------------

    /**
     * 對VIG寫入指令(寫入Security)
     *
     * @param type
     */
    public void writeCommandToSecurity(int type) {
        if (mBluetoothLeService != null) {
            securityUtil.writeMainData(getValue(type));
        }
    }

    // ----------------------------------------------------

    /**
     * 對VIG寫入資料
     *
     * @param type
     */
    public int writeCommand(int type) {

        if (mBluetoothLeService != null) {
            return mBluetoothLeService.writeCustomCharacteristic(getValue(type));
        }

        return -1;
    }

    // ----------------------------------------------------

    /**
     * 取得車王加密指的分段資料
     *
     * @param type
     * @param acctId
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidAlgorithmParameterException
     */
    public ArrayBlockingQueue<byte[]> getMobiletronCommandData(int type, String acctId) throws NoSuchPaddingException, UnsupportedEncodingException,
            InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

        DBUtil.getInstance().openDb(activity);
        SecretKeyData secretKeyData = DBUtil.getInstance().getSecurityKey(acctId);

        if (secretKeyData != null) {

            String key = new String(Base64.decode(secretKeyData.getPrivateKey(), Base64.DEFAULT));
//            String key = "WP9ns2xlU3A/1TqZoRoj8+CDpQGs7+vWs7umpAa82ad4WkiwNNUbGxYnunx1jCru7NRa/PqNSb92AiESmXs2/zeVdbX
// /rYpZB09rKm3UaRy6jnow4uLRDPRp95os/1K4bdl13uTmPuP8g99UtVzjbDl//fvu4OcG3XUzvp3Gx2PT0DDw8ijcQUj0OY+TWpe9nnVOBJtRARU/dImZeN5EDg==";
            String iv = key.substring(8, 24);

            MobiletronSecurityData mobiletronSecurityData = new MobiletronSecurityData(key, new String(getValue(type)));
            String json = new GsonBuilder().disableHtmlEscaping().create().toJson(mobiletronSecurityData);
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(json);
            json = m.replaceAll("");

            String data = aesUtil.encrypt(json + "\n", aesUtil.stringToSecretKey("L5FXRRP5TP2H6ZY4"), aesUtil.stringToIv(iv));

            return aesUtil.getSendData(data);

        }

        return new ArrayBlockingQueue<byte[]>(100);

    }

    // ----------------------------------------------------

    /**
     * 對VIG寫入資料
     *
     * @param value
     */
    public int writeCommand(byte[] value) {

        if (mBluetoothLeService != null) {
            return mBluetoothLeService.writeCustomCharacteristic(value);
        }
        return -1;
    }

    // ----------------------------------------------------

    /**
     * 讀取寫入後回傳的相關資料
     */
    public void readCommand() {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.readCustomCharacteristic();
        }
    }

    // ----------------------------------------------------

    /**
     * 建立資安連線
     */
    public void connectSecurity() {
        securityUtil.sendConnectSecurity(vigId);
    }

    // ----------------------------------------------------

    /**
     * 傳送中斷資安連線
     */
    public void disconnectSecurity() {
        securityUtil.sendDisconnectSecurity();
    }

    // ----------------------------------------------------

    /**
     * 重新設定資安
     */
    public void reSetCommunicationAgent() {
        securityUtil.sendReSetCA();
    }

    // ----------------------------------------------------

    /**
     * 是否已經連線
     *
     * @return
     */
    public boolean isConnect() {
        return mConnected;
    }

    // ----------------------------------------------------

    /**
     * 取得指令的byte
     *
     * @param type
     * @return
     */
    private byte[] getValue(int type) {

        byte[] value = new byte[]{};

        switch (type) {
            case BY_PASS:
                //connect command.
                value = new byte[]{0x56, 0x49, 0x47, 0x5F, 0x42, 0x59, 0x50, 0x41, 0x53, 0x53};
                break;
            case DOOR_LOCK:
                value = new byte[]{0x64, 0x6F, 0x6F, 0x72, 0x6C, 0x6F, 0x63, 0x6B};
                break;
            case DOOR_UNLOCK:
                value = new byte[]{0x64, 0x6F, 0x6F, 0x72, 0x75, 0x6E, 0x6C, 0x6F, 0x63, 0x6B};
                //lock/unlock command.
                break;
        }

        return value;
    }

    // ----------------------------------------------------
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
            }
            // Automatically connects to the device upon successful start-up initialization.
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // ----------------------------------------------------
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();

            if (action.equals(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mBluetoothLeService.setCharacteristicNotification();
            }

            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                Log.e(TAG, "Connected");

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {

                mConnected = false;
                isFirst = true;
                Log.e(TAG, "Disconnected");

                mBluetoothLeService.disconnect();
                securityUtil.sendReSetCA();

                if (onConnectListener != null) {
                    onConnectListener.disconnected();
                }

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                if (carVigType == CarVigType.HAITEC) {
                    if (isFirst && mConnected) {
                        writeCommandPass();
                    }
                } else {
                    if (onConnectListener != null) {
                        onConnectListener.connected();
                    }
                }

                // Show all the supported services and characteristics on the user interface.
//                displayGattServices(mBluetoothLeService.getSupportedGattServices());

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

                if (CarVigType.isGeneralVIG(carVigType)) {
                    displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

                } else {

                    String data = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);

                    Log.e("CarControllerGatt", "Read" + data);

                    if (data.contains(AESUtil.HEADER)) {
                        EventCenter.getInstance().sendReadVigData(data);
                    } else {
                        EventCenter.getInstance().sendReadVigDataCommand(data);
                    }
                }

            }

            //*********************//
            if (action.equals(BluetoothLeService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                Toast.makeText(activity, "Device doesn't support UART. Disconnecting", Toast.LENGTH_LONG).show();
                mBluetoothLeService.disconnect();
            }

        }
    };

    // ----------------------------------------------------

    /**
     * 寫入ByPass
     */
    private void writeCommandPass() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mConnected) {
                    int status = writeCommand(CarControllerGatt.BY_PASS);

                    if (status == WRITE_STATUS_ERROR) {
                        writeCommandPass();
                    } else if (status == WRITE_STATUS_SERVER_ERROR) {
                        Toast.makeText(activity, activity.getString(R.string.ble_restart), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, 300);
    }

    // ----------------------------------------------------

    /**
     * 得到read的資料
     *
     * @param data
     */
    private void displayData(String data) {

        if (data != null) {
            Log.e(TAG, "read data:" + data);
            if (data.contains("MAC CHK OK")) {
                isFirst = false;
                if (onConnectListener != null) {
                    reSetCommunicationAgent();
                    onConnectListener.connected();
                }
            }
        }
    }

    // ----------------------------------------------------
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    // ----------------------------------------------------

    /**
     * 設定連線監聽
     *
     * @param onConnectListener
     */
    public void setOnConnectListener(OnConnectListener onConnectListener) {
        this.onConnectListener = onConnectListener;
    }

    // ----------------------------------------------------
    public interface OnConnectListener {

        void connected();

        void disconnected();
    }

}
