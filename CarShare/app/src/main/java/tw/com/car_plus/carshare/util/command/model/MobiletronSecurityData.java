package tw.com.car_plus.carshare.util.command.model;

/**
 * Created by winni on 2017/12/12.
 */

public class MobiletronSecurityData {

    String key;
    String command;

    public MobiletronSecurityData(String key, String command) {
        this.key = key;
        this.command = command;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

}
