package tw.com.car_plus.carshare.util.db;

import android.content.Context;

import com.esotericsoftware.kryo.Kryo;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by neil on 2017/12/6.
 * <p>
 * Database Utility
 */

public class DBUtil {

    // ----------------------------------------------------
    private static String DATABASE_NAME = "koala";
    private final String SERVER_KEY = "ServerKey";

    private static final DBUtil ourInstance = new DBUtil();
    private static Context context;

    private DB db;

    // ----------------------------------------------------
    public static DBUtil getInstance() {
        return ourInstance;
    }

    // ----------------------------------------------------
    private DBUtil() {

    }

    // ----------------------------------------------------

    /**
     * 開啟DB
     *
     * @param cx
     */
    public void openDb(Context cx) {
        context = cx;

        try {
            db = DBFactory.open(context, DATABASE_NAME, new Kryo[0]);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------

    /**
     * 儲存密鑰
     */
    public void saveSecurityKey(SecretKeyData data) {

        deleteSecurityKey(data.getAccountId());

        try {
            Map<String, SecretKeyData> dbData;
            if (!db.exists(SERVER_KEY)) {
                dbData = new HashMap<>();
            } else {
                dbData = db.getObject(SERVER_KEY, HashMap.class);
            }

            dbData.put(data.getAccountId(), data);
            db.put(SERVER_KEY, dbData);

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------

    /**
     * 取得密鑰
     */
    public SecretKeyData getSecurityKey(String acctId) throws NullPointerException {

        Map<String, SecretKeyData> dbData = null;
        try {
            if (db.exists(SERVER_KEY)) {
                dbData = db.getObject(SERVER_KEY, HashMap.class);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        return dbData.get(acctId);
    }

    // ----------------------------------------------------

    /**
     * 刪除密鑰
     *
     * @return isDelete
     */
    public boolean deleteSecurityKey(String acctId) {

        try {
            if (db.exists(SERVER_KEY)) {
                Map<String, SecretKeyData> dbData = db.getObject(SERVER_KEY, HashMap.class);
                dbData.remove(acctId);
                return true;
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    // ----------------------------------------------------
}
