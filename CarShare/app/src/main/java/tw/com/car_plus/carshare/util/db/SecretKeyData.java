package tw.com.car_plus.carshare.util.db;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

/**
 * Created by jhen on 2017/12/5.
 */

public class SecretKeyData implements Parcelable {

    private String accountId;
    private String privateKey = null;
    private int orderId;
    private int carId;
    private Timestamp keyCreateTime = null;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public Timestamp getKeyCreateTime() {
        return keyCreateTime;
    }

    public void setKeyCreateTime(Timestamp keyCreateTime) {
        this.keyCreateTime = keyCreateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accountId);
        dest.writeString(this.privateKey);
        dest.writeInt(this.orderId);
        dest.writeInt(this.carId);
        dest.writeSerializable(this.keyCreateTime);
    }

    public SecretKeyData() {
    }

    protected SecretKeyData(Parcel in) {
        this.accountId = in.readString();
        this.privateKey = in.readString();
        this.orderId = in.readInt();
        this.carId = in.readInt();
        this.keyCreateTime = (Timestamp) in.readSerializable();
    }

    public static final Parcelable.Creator<SecretKeyData> CREATOR = new Parcelable.Creator<SecretKeyData>() {
        @Override
        public SecretKeyData createFromParcel(Parcel source) {
            return new SecretKeyData(source);
        }

        @Override
        public SecretKeyData[] newArray(int size) {
            return new SecretKeyData[size];
        }
    };
}
