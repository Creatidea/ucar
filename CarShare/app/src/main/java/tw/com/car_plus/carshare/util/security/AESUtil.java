package tw.com.car_plus.carshare.util.security;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by winni on 2017/11/30.
 */

public class AESUtil {

    public static final String HEADER = "0800222568";


    // ----------------------------------------------------

    /**
     * 加密
     *
     * @param content   未反編譯Base64的字串
     * @param secretKey
     * @param iv
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String encrypt(String content, SecretKey secretKey, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {

        Log.e("content", content);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
        byte[] temp = cipher.doFinal(content.getBytes("UTF-8"));
        return Base64.encodeToString(temp, Base64.NO_WRAP);
    }


    // ----------------------------------------------------

    /**
     * 解密
     *
     * @param content
     * @param secretKey
     * @param iv
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String decrypt(String content, SecretKey secretKey, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] temp = Base64.decode(content, Base64.NO_WRAP);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
        byte[] decodeBytes = cipher.doFinal(temp);
        return new String(decodeBytes);
    }

    // ----------------------------------------------------

    /**
     * 將字串轉成SecretKeySpec的Key
     *
     * @param strKey
     * @return
     */
    public SecretKey stringToSecretKey(String strKey) {
        byte[] key = strKey.getBytes();
        Log.e("key", strKey);
        return new SecretKeySpec(key, "AES");
    }

    // ----------------------------------------------------

    /**
     * 將字串轉換成IvParameterSpec
     *
     * @param strIv
     * @return
     */
    public IvParameterSpec stringToIv(String strIv) {
        Log.e("IV", strIv);
        byte[] iv = strIv.getBytes();
        return new IvParameterSpec(iv);
    }

    // ----------------------------------------------------

    /**
     * 取的分解好的byte array
     *
     * @param data 加密的資料
     * @return
     */
    public ArrayBlockingQueue<byte[]> getSendData(String data) {

        ArrayBlockingQueue<byte[]> bleDataArray = new ArrayBlockingQueue<>(100);

        int start = 0;
        int packetsToSend = data.length() % 14 == 0 ? (data.length() / 14) : (data.length() / 14) + 1;

        for (int i = 0; i < packetsToSend; i++) {

            int newEnd = (start + 14) > data.length() ? data.length() : start + 14;
//            String strData = String.format("%-16s", (String.format("%02d", i + 1) + data.substring(start, newEnd)));
                String strData = String.format("%02d", i + 1) + data.substring(start, newEnd);
            byte[] newByte = strData.getBytes();
            Log.e("AesUtil", new String(newByte) + newByte.length);
            bleDataArray.offer(newByte);
            start += 14;

        }

        String lastData = HEADER + packetsToSend;
        bleDataArray.offer(lastData.getBytes());

        return bleDataArray;
    }
}
