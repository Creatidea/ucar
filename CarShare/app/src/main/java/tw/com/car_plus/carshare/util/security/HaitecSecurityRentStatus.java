package tw.com.car_plus.carshare.util.security;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import tw.com.car_plus.carshare.R;

/**
 * Created by winni on 2017/8/4.
 * 華創資安相關類型
 */

public class HaitecSecurityRentStatus {


    /**
     * SecurityRentStatus-找不到accountId
     */
    public static final int ACCOUNT_NOT_FIND = 987;
    /**
     * SecurityRentStatus-AccountId停用
     */
    public static final int ACCOUNT_DISABLED = 986;
    /**
     * SecurityRentStatus-找不到此車輛
     */
    public static final int VIG_ERROR = 887;
    /**
     * SecurityRentStatus-此車輛已是出租的狀態
     */
    public static final int CAR_TO_RENT = 886;
    /**
     * SecurityRentStatus-此車輛已被其他使用者租借
     */
    public static final int CAR_RENT_OTHER_USER = 787;
    /**
     * SecurityRentStatus-此AccountId與vig尚未有租用關係
     */
    public static final int NOT_ESTABLISH = 786;
    /**
     * SecurityRentStatus-欄位解析錯誤
     */
    public static final int ANALYZE_ERROR = 687;
    /**
     * SecurityRentStatus-指令重複
     */
    public static final int COMMAND_DUPLICATE = 686;


    public HaitecSecurityRentStatus() {

    }

    // ----------------------------------------------------

    /**
     * 資安租車狀態
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ACCOUNT_NOT_FIND, ACCOUNT_DISABLED, VIG_ERROR, CAR_TO_RENT, CAR_RENT_OTHER_USER, NOT_ESTABLISH, ANALYZE_ERROR, COMMAND_DUPLICATE})
    public @interface SecurityStatus {
    }


    // ----------------------------------------------------

    /**
     * 根據狀態碼回傳相關的解釋
     *
     * @param type
     * @return
     */
    public int getStatusMessage(int type) {

        int message = 0;

        switch (type) {
            case ACCOUNT_NOT_FIND:
                message = R.string.account_not_find;
                break;
            case ACCOUNT_DISABLED:
                message = R.string.account_disabled;
                break;
            case VIG_ERROR:
                message = R.string.vig_error;
                break;
            case CAR_TO_RENT:
                message = R.string.car_to_rent;
                break;
            case CAR_RENT_OTHER_USER:
                message = R.string.car_rent_other_user;
                break;
            case NOT_ESTABLISH:
                message = R.string.not_establish;
                break;
            case ANALYZE_ERROR:
                message = R.string.analyze_error;
                break;
            case COMMAND_DUPLICATE:
                message = R.string.command_duplicate;
                break;

        }

        return message;

    }

}
