package tw.com.car_plus.carshare.util.security;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import tw.com.car_plus.carshare.connect.ble.BleConnect;
import tw.com.car_plus.carshare.event.EventCenter;
import tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType;
import tw.com.car_plus.carshare.service.SecurityService;
import tw.com.car_plus.carshare.service.model.CommandData;
import tw.com.car_plus.carshare.util.SharedPreferenceUtil;

import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType.HAITEC;
import static tw.com.car_plus.carshare.rent.success.car_structure_type.CarVigType.MOBILETRON;


/**
 * Created by jhen on 2017/7/12.
 */

public class SecurityUtil {

    //類型-1=租車、2=還車、3=註銷、4=暫停用車、5=恢復用車
    public static final int RENT_CAR = 1;
    public static final int RETURN_CAR = 2;
    public static final int CANCEL = 3;
    public static final int PAUSE_CAR = 4;
    public static final int RESTORE_CAR = 5;

    private Activity activity;

    // ----------------------------------------------------

    /**
     * 傳送給service目前使用者對於車子動作的狀態
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({RENT_CAR, RETURN_CAR, CANCEL, PAUSE_CAR, RESTORE_CAR})
    public @interface CarStatus {
    }


    // ----------------------------------------------------
    public SecurityUtil(Activity activity) {
        this.activity = activity;
    }

    // ----------------------------------------------------

    /**
     * 啟動Service
     */
    public void starService() {
        Intent intent = new Intent(activity, SecurityService.class);
        activity.startService(intent);
    }

    // ----------------------------------------------------

    /**
     * 初始化CA
     *
     * @param acctId
     */
    public void initializeCA(String acctId) {
        EventCenter.getInstance().sendInitializeCA(acctId);
    }

    // ----------------------------------------------------

    /**
     * 傳送SecurityService註冊Security
     */
    public void sendRegisterSecurity() {
        EventCenter.getInstance().sendRegisterSecurity();
    }

    // ----------------------------------------------------

    /**
     * 傳送連線SecurityConnect
     *
     * @param vigId
     */
    public void sendConnectSecurity(String vigId) {
        EventCenter.getInstance().sendSecurityConnect(vigId);
    }

    // ----------------------------------------------------

    /**
     * 傳送中斷SecurityConnect
     */
    public void sendDisconnectSecurity() {
        EventCenter.getInstance().sendSecurityDisConnect();
    }

    // ----------------------------------------------------

    /**
     * 傳送重新設定CA
     */
    public void sendReSetCA() {
        EventCenter.getInstance().sendReSetCA();
    }

    // ----------------------------------------------------

    /**
     * 連線傳送指令到RentServer
     *
     * @param type
     * @param carId
     * @param startTime
     * @param endTime
     */
    public void sendCommandToRentServer(@CarStatus int type, @CarVigType.CarVig int vigType, int carId, int orderId, String startTime, String
            endTime) {

        CommandData commandData = new CommandData();
        commandData.setType(type);
        commandData.setCarId(carId);
        commandData.setStartTime(startTime);
        commandData.setEndTime(endTime);
        commandData.setVigType(vigType);
        commandData.setOrderId(orderId);

        BleConnect bleConnect = new BleConnect(activity);
        SharedPreferenceUtil sp = new SharedPreferenceUtil(activity);

        switch (vigType) {
            case HAITEC:
                bleConnect.getBleData(sp.getAcctId(), commandData);
                EventCenter.getInstance().sendToSecurityServer(commandData.getType());
                break;
            case MOBILETRON:
                bleConnect.sendCommand(sp.getAcctId(), commandData);
                break;
        }
    }

    // ----------------------------------------------------

    /**
     * 將Command寫入MainData
     *
     * @param value
     */
    public void writeMainData(byte[] value) {
        EventCenter.getInstance().sendMainSecurityWrite(value);
    }

    // ----------------------------------------------------

    /**
     * 取得將MacAddress轉換成可連接藍芽的規則
     *
     * @param address
     * @return
     */
    public String getMacAddress(String address) {

        StringBuffer value = new StringBuffer();

        for (int i = 0; i < address.length(); i += 2) {

            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(address.charAt(i));
            stringBuffer.append(address.charAt(i + 1));

            if (i != 10) {
                stringBuffer.append(":");
            }

            value.append(stringBuffer);
        }

        return value.toString().toUpperCase();
    }

    // ----------------------------------------------------

    /**
     * get security server key.
     *
     * @param orderId rent car order id.
     */
    public void loadServerKey(int orderId) {
        BleConnect bleConnect = new BleConnect(activity);
        bleConnect.loadServerKey(orderId);
    }

    // ----------------------------------------------------

}
