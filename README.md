# 格上租車 #

### 介面 ###
![Screenshot_2017-05-31-12-02-38.png](https://bitbucket.org/repo/RXpMyR/images/2797764720-Screenshot_2017-05-31-12-02-38.png)
![Screenshot_2017-05-31-12-02-48.png](https://bitbucket.org/repo/RXpMyR/images/3837876152-Screenshot_2017-05-31-12-02-48.png)
![Screenshot_2017-05-31-12-03-04.png](https://bitbucket.org/repo/RXpMyR/images/3143676402-Screenshot_2017-05-31-12-03-04.png)
![Screenshot_2017-05-31-14-15-58.png](https://bitbucket.org/repo/RXpMyR/images/4179239366-Screenshot_2017-05-31-14-15-58.png)
![Screenshot_2017-05-31-14-16-04.png](https://bitbucket.org/repo/RXpMyR/images/3370282892-Screenshot_2017-05-31-14-16-04.png)

### 使用的jar ###

* android-async-http-1.4.6.jar
* httpclient-4.3.6.jar
* custom_tool2.2.0.jar

### 使用的library ###

* google play services
* butter knife [https://github.com/avast/android-butterknife-zelezny](https://github.com/avast/android-butterknife-zelezny)
* glide [https://github.com/bumptech/glide](https://github.com/bumptech/glide)
* PermissionsDispatcher [https://github.com/hotchemi/PermissionsDispatcher](https://github.com/hotchemi/PermissionsDispatcher)
* EventBus [https://github.com/greenrobot/EventBus]
* SublimePicker [https://github.com/vikramkakkar/SublimePicker]
* Zxing [https://github.com/journeyapps/zxing-android-embedded]
* ConvenientBanner [https://github.com/saiwu-bigkoo/Android-ConvenientBanner]
* SignaturePad [https://github.com/gcacace/android-signaturepad]
* Gson [https://github.com/google/gson]
* Jsoup [https://github.com/jhy/jsoup]